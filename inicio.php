<?php
include('model/config/ses.php');
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
        <title>Nutrición</title>
       

        <!--<link href="assets/js/lib/jquery-ui-1.12.1/jquery-ui.css" rel="stylesheet">    -->
  
        <link href="assets/js/lib/select2/select2.min.css" rel="stylesheet">
        <link href="assets/css/lib/toastr/toastr.min.css" rel="stylesheet">
      
        <!-- Bootstrap Core CSS -->
        <!--<link href="assets/css/lib/sweetalert/sweetalert.css" rel="stylesheet">-->
        <link href="assets/css/lib/sweetalert2/sweetalert2.min.css" rel="stylesheet">
        <link href="assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/lib/datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">

        
        <!-- Custom CSS -->
        <link href="assets/css/helper.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">
        
        <!--FORMATO TABLA ESTLIO REPORTES -->
        <link href="assets/css/tabla.css" rel="stylesheet">
     
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
        <!--[if lt IE 9]>
        <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                   <a href="inicio.php" title="HEG">Nutrición</a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>                        
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li style="padding-top:5px;"><a><i><span class="label label-inverse" id="lbl_usuario"></span></i></a><li>
                        
                    </ul>
                    <!-- User profile and search -->
                    
                    <ul class="navbar-nav my-lg-0">                      
                        <!-- Comment -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted link_menu" href="view/proc_entrega.php" title="Receta">
                             <i class="fa fa-font" style="color:#FFF "></i>								
							</a>                            
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted" href="dark.php" title="Cambiar Color">
                             <i class="fa fa-adjust" style="color:#FFF "></i>								
							</a>                            
                        </li>
                        <li class="nav-item dropdown">                        
                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="fa fa-cart-arrow-down " style="color:#FFF "></i>
								<div class="notify" id="notificacion_pedidos"> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notificaciones</div>
                                    </li>
                                    <li>
                                        <div class="message-center" id="detalle_notificacion_pedidos">
                                            <!-- Message -->
                                           
                                            <!-- Message -->                                            
                                        </div>
                                    </li>
                                  
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">                        
                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="fa fa-bell" style="color:#FFF "></i>
								<div class="notify" id="notificacion"> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notificaciones</div>
                                    </li>
                                    <li>
                                        <div class="message-center" id="detalle_notificacion">
                                            <!-- Message -->
                                           
                                            <!-- Message -->                                            
                                        </div>
                                    </li>
                                  
                                </ul>
                            </div>
                        </li>
                       
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user" style="color:#FFF "></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user"> 
                                <li><a href="recuperar.php" class="a_salir"><i class="fa fa-pencil-square-o"></i> Cambiar Contraseña</a></li>                                
                                    <li><a href="#" class="a_salir"><i class="fa fa-power-off" onClick="ac()"></i> Salir del Sistema</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->  
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="menu_lateral">
                        <?php include('model/config/menu.php');
                        ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
               
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid center-block "  id="principal_content">
                <!-- Start Page Content -->            			
            <?php include ('view/dashboard.html') ?>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
           
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
   
    <script src="assets/js/lib/jquery/jquery.min.js"></script>
    <script src="assets/js/lib/typeahead/bootstrap3-typeahead.min.js"></script>
    <script src="assets/js/lib/select2/select2.min.js"></script>
   <!-- <script src="assets/js/lib/jquery-ui-1.12.1/jquery-ui.js"></script>-->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/lib/datepicker/bootstrap-datepicker.min.js"></script>
    
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--<script src="assets/js/lib/sweetalert/sweetalert.min.js"></script>-->

    <script src="assets/js/lib/sweetalert2/sweetalert2.all.min.js"></script>
    <script src="assets/js/lib/sweetalert2/promise-polyfill"></script>

    <!--<script src="assets/js/lib/weather/jquery.simpleWeather.min.js"></script>
    <script src="assets/js/lib/weather/weather-init.js"></script>
-->
    <script src="assets/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="assets/js/lib/owl-carousel/owl.carousel-init.js"></script>

    <script src="assets/js/lib/toastr/toastr.min.js"></script>

  
   
    <!--Custom JavaScript -->
    <script src="assets/js/custom.min.js"></script>
    <script src="assets/js/menu.js"></script>

 <script src="controller/dashboard.js"></script>


    <script src="assets/js/lib/morris-chart/raphael-min.js"></script>
    <script src="assets/js/lib/morris-chart/morris.js"></script>
    <script src="assets/js/moment.js"></script>
    <script src="assets/js/toasts.js"></script>
    
           

        <script type="text/javascript" src="assets/js/lib/table2excel/jquery.table2excel.min.js"></script>
<!-- DATATABLES -->
<script src="assets/js/lib/datatables/datatables.min.js"></script>
<script src="assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

   

</body>

</html>