<?php
	require 'model/funcs/conexion.php';
	include 'model/funcs/funcs.php';

	$cnfin=array ();
	$errorF=array ();
	
	
	$user_id = $mysqli->real_escape_string($_POST['user_id']);
	$token = $mysqli->real_escape_string($_POST['token']);
	$password = $mysqli->real_escape_string($_POST['password']);
	$con_password = $mysqli->real_escape_string($_POST['con_password']);
	
	if(validaPassword($password, $con_password))
	{
		
		$pass_hash = MD5($password);
		
		if(cambiaPassword($pass_hash, $user_id, $token))
		{
			$cnfin[]="La contraseña se ha actualizado correctamente.<br />
				<a href='index.html' >Iniciar Sesión</a>";

			//resultBlockCnfFinal($cnfin);


	//		echo "Contrase&ntilde;a Modificada <br> <a href='index.html' >Iniciar Sesion</a>";
		}
		else 
		{
			$errorF[]="Error al modificar la contraseña.<br />
				<a href='javascript:history.back(1)'>Regresar</a>";
				
			//echo "Error al modificar contrase&ntilde;a";
		}
	}
	else
	{

		$errorF[]="Las contraseñas no coinciden.<br />
				<a href='javascript:history.back(1)'>Regresar</a>";
				
		//echo 'Las contraseñas no coinciden';
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cambiar Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link href="assets/css/lib/toastr/toastr.min.css" rel="stylesheet">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/css/lib/bootstrap/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/icons/font-awesome/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/icons/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="assets/css/login/util.css">
<link rel="stylesheet" type="text/css" href="assets/css/login/main.css">
<!--===============================================================================================-->
</head>
<body style="background:#8ba987 url('assets/images/fondo_sistema_nutricion.jpg') no-repeat center center fixed;
        background-size:cover;">
	
	<div class="container" style="opacity:0.8;">
			
		<div class="row">
		  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card card-signin my-5">
			  <div class="card-body" >
						<!--<img src="assets/images/g.png" name="luna" style="width: 90px; position: absolute; bottom: 35px; right: 0; background-repeat: no-repeat; z-index: 10;">-->
						<div class="form-label-group" style="text-align: center">
							<h3><FONT FACE="impact" SIZE=6 COLOR="BLACK">RECUPERAR PASSWORD</FONT></h3>
<hr>
					
					
				  </div>
					<div class="text-center">
						<img src="assets/images/logo1.jpg">
					</div> 
					 <div class="form-label-group">
						<a href="index.html">Iniciar Sesión</a>
					
				  </div>



			<?php echo resultBlockCnfFinal($cnfin); ?>
			<?php echo resultErrorF($errorF); ?>
				

			  </div>
			</div>
		  </div>
		</div>
	  </div>
	
<!--===============================================================================================-->
<script src="assets/js/lib/jquery/jquery.min.js"></script>


<!--===============================================================================================-->
	<script src="assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/js/lib/bootstrap/js/popper.min.js"></script>
	<script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="assets/js/lib/toastr/toastr.min.js"></script>
<!-- scripit init-->
<script src="assets/js/lib/toastr/toastr.init.js"></script>
<!--Custom JavaScript -->
<script src="assets/js/custom.min.js"></script>
<script src="assets/js/login/main.js"></script>
<script src="assets/js/toasts.js"></script>


    <script src="controller/lg.js"></script>
</body>
</html>