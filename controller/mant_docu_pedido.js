$(document).ready(function(){

    var model='model/kardex_pro/funciones.php';
    var idx;
    var accion;
    var table3;
    var table4;
    var unid_id;
    var unid_id_destino;
    var kard_id;
    var cantidad_pedido;
    var tipo_docu_id=19; //SOLO PEDIDO
    var fecha = $('#docu_fecha');  

    
     //DOCUMENTO PEDIDOS
     var tipo='P';

    fecha.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
   
    $('#tb_4_ped').hide();    
  
    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

    function docu_numero_p(){
        $.get(model,{funcion:'docu_numero',unid_id:unid_id,unid_id_destino:unid_id_destino,tipo:tipo,tipo_docu_id:tipo_docu_id},function(data){$('#docu_numero').val(data);});
    }

    $("#unid_id").change(function() {
        unid_id=$("#unid_id").val();
       docu_numero_p();
    });

    $("#unid_id_destino").change(function() {
        unid_id_destino=$("#unid_id_destino").val();
       docu_numero_p();
    });

    $("#tipo_docu_id").change(function() {          
        docu_numero_p();
     });

   //TABLA DATOS
   function transfer(){ 
    unid_id=$('#unid_id_s').val();
    unid_id_destino=$('#unid_id_destino_s').val();
    table3=$('#tb_3_ped').DataTable( {        
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo,unid_id_destino:unid_id_destino,recibe:0},
            dataSrc: ''
        },             
        columns: [                                 
            { data: 'docu_numero' },
            { data: 'unid_nombre' ,
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }  },
            { data: 'unid_destino',
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }   },
            { data: 'docu_fecha' },   
            { data: 'docu_autoriza',
            "render":function(data){
                return ucwords(data.toLowerCase());
            }  },   
            { data: 'docu_entrega' ,
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },                        
            { data: 'stat_docu_nombre',
                "render":function(data){
                    var r;
                    switch(data){
                        case 'No Procesado':
                            r='<span class="badge badge-danger">No Enviado</span>';
                        break;
                        case 'Procesado':
                            r='<span class="badge badge-success">Enviado</span>';
                        break;
                        case 'Anulado':
                            r='<span class="badge badge-warning">'+data+'</span>';
                        break;
                        case 'Borrado':
                            r='<span class="badge badge-dark">'+data+'</span>';
                        break;
                        default:
                            r='<span class="badge badge-warning">'+data+'</span>';                        
                    }
                    return r;
                }
            },
            { data: 'stat_docu_id_destino',
            "render":function(data){
                var r;
                switch(data){
                    case '0':
                        r='<span class="badge badge-danger">No Revisado</span>';
                    break;
                    case '1':
                        r='<span class="badge badge-success">Revisado</span>';
                    break;
                    case '2':
                        r='<span class="badge badge-warning">Anulado</span>';
                    break;
                    case '3':
                        r='<span class="badge badge-dark">Borrado</span>';
                    break;
                    default:
                        r='<span class="badge badge-warning">'+data+'</span>';                        
                }
                return r;
            }
        },
            { data: 'docu_id' ,
                "render": function ( data ) {                                              
                return '<button type="button" title="Detalles" class="btn btn-secondary btn-xs m-l-5 btn_detalle" id="'+data+'"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" title="Editar" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" title="Observaciones" class="btn btn-secondary btn-xs m-l-5 btn_obs" id="'+data+'"><i class="fa fa-bullhorn"></i></button><button type="button" title="Eliminar/Anular" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_ped" id="'+data+'"><i class="fa fa-print"></i></button><button type="button" title="Procesar" class="btn btn-secondary btn-xs m-l-5 btn_procesar_docu" id="'+data+'"><i class="fa fa-check"></i></button>';
                }
            } 
            
            
        ],
        "order": [[ 8, "desc" ]],
        responsive: true 
             
    });    
    }

     //IMPRIMIR PDF
     $(document).on('click', '.btn_print_ped', function(){
        idx=$(this).attr("id");  
        window.open('model/reportes/pdf.php?f=repor_ingresos&id='+idx, '_blank');
    });

    $(document).on('click', '.btn_edit', function(){          
        accion='editar';
        idx=$(this).attr("id");  
        $('#ModalNewPedido').modal('show'); 
        $('#ModalNewTitlePedido').text("Editar"); 
        editar(idx);
    });

    $(document).on('click', '.btn_del', function(){  
        accion='eliminar';
        idx=$(this).attr("id");  
        swal({
            title: "Anular/Eliminar ?",
            text: "Desea Eliminar/Anular éste Pedido?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx}
                        }).done(function(data) {       
                            table3.ajax.reload();            
                            swal("Notificación:", data, "info");
                         }); 
                })
              }
            });
    });

    $(document).on('click', '.btn_obs', function(){  
        idx=$(this).attr("id");  
        $.getJSON(model,{id:idx,funcion:'get_obs_doc'})
        .done(function(json) {        
         swal(json[0].obs)
        });   
    });

    $(document).on('click', '.btn_new', function(){        
        

        $("#unid_id").prop('disabled', false);
            $("#unid_id_destino").prop('disabled', false); 
            $("#docu_fecha").prop('disabled', false); 
            $("#tipo_docu_id").prop('disabled', false); 
            $("#docu_autoriza").prop('readOnly', false);
            $("#docu_entrega").prop('readOnly', false);
            $("#docu_observacion").prop('readOnly', false);    
            $('#btn_save_pedido').prop('disabled', false);

        $('#form22p').trigger("reset");        
        fecha.datepicker('setDate', new Date());       
        accion='nuevo';
        idx='';        
        $('#ModalNewPedido').modal('show'); 
        $('#ModalNewTitlePedido').text("Nuevo"); 
    });

    function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#docu_fecha').val(json[0].docu_fecha);
        $('#tipo_docu_id').val(json[0].tipo_docu_id);        
        $('#docu_numero').val(json[0].docu_numero);
        $('#unid_id').val(json[0].unid_id);
        $('#unid_id_destino').val(json[0].unid_id_destino);        
        $('#docu_autoriza').val(json[0].docu_autoriza);
        $('#docu_entrega').val(json[0].docu_entrega);      
        $('#docu_observacion').val(json[0].docu_observacion);    
        
        if(json[0].stat_docu_id==0){
            $("#unid_id").prop('disabled', false);
            $("#unid_id_destino").prop('disabled', false); 
            $("#docu_fecha").prop('disabled', false); 
            $("#tipo_docu_id").prop('disabled', false); 
            $("#docu_autoriza").prop('readOnly', false);
            $("#docu_entrega").prop('readOnly', false);
            $("#docu_observacion").prop('readOnly', false);    
            $('#btn_save_pedido').prop('disabled', false);
        }
        else{
            $("#unid_id").prop('disabled', true);
            $("#unid_id_destino").prop('disabled', true); 
            $("#docu_fecha").prop('disabled', true); 
            $("#tipo_docu_id").prop('disabled', true); 
            $("#docu_autoriza").prop('readOnly', true);
            $("#docu_entrega").prop('readOnly', true);
            $("#docu_observacion").prop('readOnly', true);    
            $('#btn_save_pedido').prop('disabled', true);
            }


        });       
    }

    $('#form22p').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO

        if($('#docu_numero').val().length<1){
            danger_toast('Verifique el Numero de Documento...');
            return false;
        }
        if($('#tipo_docu_id').val()==0){
            danger_toast('Verifique Tipo Documento...');
            return false;
        }
        if($('#unid_id').val()==0){
            danger_toast('Verifique la Unidad Origen...');
            return false;
        }
        if($('#unid_id_destino').val()==0){
            danger_toast('Verifique el Unidad Destino...');
            return false;
        }
      
        if($('#docu_observacion').val().length<1){
            danger_toast('Verifique la Observacion...');
            return false;
        }

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data,tipo:tipo},
            beforeSend: function () {
                    $("#btn_save_pedido").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table3.ajax.reload();	
                $("#btn_save_pedido").attr("disabled", false);	
                $('#ModalNewPedido').modal('hide'); 	                
                success_toast(data);
               
             });
    });

    $(document).on('click', '.btn_detalle', function(){   
        idx=$(this).attr("id");         

        $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {
            $('#mtitulo1p').html(' *# '+json[0].docu_numero);      
            $('#mtitulo2p').html(' *PEDIDO A: '+json[0].unid_nombre);
            $('#mtitulo3p').html(' *'+json[0].tipo_docu_nombre);

            //OBTIENE ID UNIDAD DONDE PEDIR
            unid_id=json[0].unid_id;

            //OBTIENE UNIDAD DESTINO PEDIDO
            unid_id_destino=json[0].unid_id_destino;
            
            //SOLO SI ESTADO ES 0 NO PROCESADO HABILITA BORON DE AGREGAR PRODUCTO
          //  if(json[0].stat_docu_id == 0)
               // $('#btn_agregar').prop('disabled', false);
        //    else
                //$('#btn_agregar').prop('disabled', true);
            
        }); 
        
        $('#ModalDetallePedido').modal('show'); 
        table4 = $('#tb_4_ped').DataTable();
        table4.destroy();
        $('#tb_4_ped').show();              
        accion='detalle_ingreso';        
        detalle_ingreso_e_transf(idx);
        
    });

    function detalle_ingreso_e_transf(idx){
         table4=$('#tb_4_ped').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,id:idx},
                dataSrc: ''
            },     
            columns: [                          
                { data: 'prod_nombre',
                    "render":function(data){
                        return '<h4><span style="white-space: nowrap;font-size:13px"><strong>'+ucwords(data.toLowerCase())+'<strong></span></h4>';
                    }
                },              
                { data: 'deta_docu_cantidad',
                "render":function(data){
                    return '<h4><span class="badge badge-success"><strong>'+data+'<strong></span></h4>';
                }},               
                { data: 'deta_docu_id',
                "render":function(data){
                    return '<button type="button" title="Eliminar" class="btn btn-secondary btn-xs m-l-5 btn_del_producto_detalle" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }}                         
            ],
             "order": [[ 2, "desc" ]],           
            responsive: true
           
            
        });    
    }
    $('#ModalDetallePedido').on('shown.bs.modal',function(event){
        $(document).off('focusin.modal');//PARA QUE AL MOMENTO DE ABRIR SWAL IMPUT LE DE FOCO A SWAL
        kardex();
    });
    $(document).on('click', '.btn_set_cantidad', function(){ 
        kard_id=$(this).attr("id");   
        pedir_cantidad();
    });

    function pedir_cantidad(){
        swal({
            title: "Cantidad a Pedir:",
            text: "",
            input: 'text',
            showCancelButton: true,
            closeOnConfirm: true,
            animation: "slide-from-top",
            inputPlaceholder: ""
        }).then((result)=>{
            if (result.value === false) return false;
            if (result.value === "" || parseInt(result.value)<1) {
                swal.showInputError("Por favor especifique una cantidad correcta !!!");
                cantidad_pedido=0;
                return false;       
            }else{        
               cantidad_pedido=result.value;
            }
            agregar_producto();
        });
       }

    function agregar_producto(){
       
        $.get(model,{funcion:'agregar_detalle_pedido',id:idx,kard_id:kard_id,cantidad_pedido:cantidad_pedido},function(data){ 
            table4.ajax.reload();
            // aqui cambiar el toast dependiendo la respuesta que da el modelo "class" del kardex_pro
           // warning_toast(data);
            if(data==1){
                            swal("Estado:", "Cantidad a Pedir Excede a lo Disponible en Stock !!!", "error");
                        }else{
                            if(data==2){
                                success_toast("Producto Agregado a Pedido");
                                //swal("Estado:", "Producto Agregado a Pedido", "success");
                            }else{
                                if(data==3){
                                    swal("Estado","NO SE PUEDE AGREGAR...","error");

                                }else{
                                    if(data==4){
                                        swal("Estado","No se puede Agregar Documento ya está procesado !!!","error");
                                    }else{
                                        if(data==5){
                                            swal("Estado","Éste Producto ya está agregado en esta lista...","error");
                                        }
                                    }
                                }
                            }
                        //  swal("Estado:", data, "success");  
                        }

        });
    }

    function kardex(){ 
        table=$('#tb_5_stock').DataTable();            
        table.destroy();        
        table.clear();
        table=$('#tb_5_stock').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:'stock_bodega_pedido',unid_id:unid_id,unid_id_destino:unid_id_destino},
                dataSrc: ''
            },     
            columns: [                                     
                { data: 'prod_nombre',
                    "render":function(data){
                            return ucwords(data.toLowerCase());
                    }
                },                                    
                { data: 'stock_real',
                "render":function(data){
                    var r='';
                    if(parseInt(data)<1)
                        r='<h4><span class="badge badge-danger"><strong>'+parseFloat(data).toFixed(4)+'</strong></span><h4>';
                    else    
                        r='<h4><span class="badge badge-info"><strong>'+parseFloat(data).toFixed(4)+'</strong></span><h4>';
                    return r;
                }},
                
                /*{ data: 'mi_stock',
                "render":function(data){
                    var r='';
                    if(parseInt(data)>0)
                        r='<h4><span class="badge badge-success"><strong>'+parseInt(data)+'</strong></span><h4>';
                    else    
                        r='<h4><span class="badge badge-danger"><strong>'+parseInt(data)+'</strong></span><h4>';
                    return r;
                }},*/

                { data: 'kard_id' ,
                    "render": function ( data ) {                                
                    return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_set_cantidad" id="'+data+'" ><i class="fa fa-plus"></i></button>';
                    }
                }         
            ],
            responsive: true,            
        });    
        }

    function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#unid_id_s';
        $.getJSON(model,{funcion:'get_unidad_pedido'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Todas--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_destino(){
        //LLENO COMBOBOX POR DATOS JSON
        combo33='#unid_id_destino_s';
        $.getJSON(model,{funcion:'get_unidad',tipo:tipo})
        .done(function(json) {   
            $(combo33).empty();   
            $(combo33).append('<option value="0" selected="selected">--Todos--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo33).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_destino_2(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#unid_id_destino';
        $.getJSON(model,{funcion:'get_unidad',tipo:tipo})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_2(){
        //LLENO COMBOBOX POR DATOS JSON
        combo5='#unid_id';
        $.getJSON(model,{funcion:'get_unidad_pedido'})
        .done(function(json) {   
            $(combo5).empty();   
            $(combo5).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo5).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_tipo_docu(){
        //LLENO COMBOBOX POR DATOS JSON
        combo10='#tipo_docu_id';
        $.getJSON(model,{funcion:'get_tipo_doc',tipo:tipo})
        .done(function(json) {   
            $(combo10).empty();   
            //$(combo8).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo10).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
			});
        });  
    }

    combo_unidad();
    combo_unidad_destino();
    combo_unidad_2();
    combo_unidad_destino_2();
    combo_tipo_docu();

    

    $("#unid_id_s").change(function() {
        table3.destroy();      
        transfer();
      });

      $("#unid_id_destino_s").change(function() {
        table3.destroy();      
        transfer();
      });

      transfer();//CARGA INICIAL

      //ELIMINA PRODUCTO DEL DETALLE DOCUMENTO
      $(document).on('click', '.btn_del_producto_detalle', function(){  
        accion='eliminar_producto_detalle';
        idx_d=$(this).attr("id");  
        swal({
            title: "Eliminar Producto ?",
            text: "Desea Elimina éste Producto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id_d:idx_d}
                        }).done(function(data) {       
                            table4.ajax.reload();            
                            swal("Notificación:", data, "info");
                         }); 
                })
              }
            });
    });


     //PROCESAR
     $(document).on('click', '.btn_procesar_docu', function(){  
        accion='procesar_documento_pedido';
        //SI RECIBE ES 0 PROCESO NORAML
        //SI RECIBE ES 1 PROCESO REVERSAR
        recibe=0;
        idx=$(this).attr("id");  
        swal({
            title: "Pedido ?",
            text: "Procesar Pedido?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Procesar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx,recibe:recibe}
                        }).done(function(data) {       
                            table3.ajax.reload();            
                            swal("Notificación:", data, "info");
                         });
                })
              }
            });
    });

        

     

});