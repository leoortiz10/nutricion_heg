


$(document).ready(function(){
    
    var model='model/dashboard/funciones.php';
    var model_2='model/inicio/funciones.php';
    var noti=0;     
    var noti_p=0;     
    var nom_usuario;

    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
      function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

    function saludo(){
        fecha = new Date(); 
        hora = fecha.getHours(); 
        texto='';
        if(hora >= 0 && hora < 12)
            texto = "Un Buen Día...";        
  
       if(hora >= 12 && hora < 18)
            texto = "Excelente Tarde...";      
 
        if(hora >= 18 && hora < 24)
            texto = "Gran Noche...";          
          
          return texto;
    }
    $('.a_salir').click(function(event){
        
        $.get(model,{funcion:'mi_turno'},function(data){            
            if(parseInt(data)>0){
                swal({
                    title: nom_usuario+', tienes turnos sin Cerrar...',
                    text: "Por favor Verifícalos !!!",
                    imageUrl: "assets/images/emoji2.png",
                    imageWidth: 200,
                    imageHeight: 200,
                    showCancelButton: true,
                    cancelButtonText: 'Sí, Verificar',
                    confirmButtonText: "Salir de todos modos",                    
                }).then((result)=>{
                    if (result.value) {
                        window.location.href = 'model/config/lgout.php';
                    }                    
                });
            }
            else
                window.location.href = 'model/config/lgout.php';
        });   
        
    });

    $.get(model_2,{funcion:'get_nombre_usuario'}, function(data){    
        nom_usuario=data;   
        $("#lbl_usuario").html(data);        
        //warning_toast_center(data,saludo());
        $("#info_unidad_user").html('De: '+data);

    });

    $.getJSON(model,{funcion:'get_nombre_unidad'},function(json){        
        $( ".info_unidad" ).append(" en "+json[0].unid_nombre);
    });

    //CARD NOTIFICAIONES CARD
    $.getJSON(model,{funcion:'transfer_pendientes'},function(json){$('#dash_inicio1').html(json[0].total);});
    $.get(model,{funcion:'total_turnos'},function(data){$('#dash_inicio2').html(data);});
    $.get(model,{funcion:'mi_turno'},function(data){$('#dash_inicio3').html(data);});
    $.getJSON(model,{funcion:'recetas_hoy'},function(json){$('#dash_inicio4').html(json[0].total);});

    /*table1_inicio=$('#tb_lote_caduca').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_lotes_caduca'},
            dataSrc: ''
        },     
        columns: [                      
            { data: 'producto',
            "render": function (data){
                return ucwords(data.toLowerCase());
            }
            },  
            { data: 'lote_codigo',
            "render":function(data){
                return '<span class="badge badge-info">'+data+'</span>';
            }
            },                    
            { data: 'lote_fech_venc'},  
            { data: 'lote_fech_venc',
            "render":function(data){
                var fecha1 =moment(data);
                var fecha2 = moment().format('YYYY-MM-DD');
                var dias= fecha1.diff(fecha2, 'days');
                var r;
                if(dias<0)
                    r='<span class="badge badge-dark">Caducado</span>';
                if(dias>=0 && dias<=60)               
                    r='<span class="badge badge-danger">Caduca en:<strong> '+dias+' </strong>dias</span>';
                if(dias>60)
                    r='<span class="badge badge-success">Estable</span>';
                return r;                       
                }
            },    
        ],
        "lengthChange": false,
        responsive: true ,
        "order": [[ 2, "asc" ]],        
    });   */

    table2_inicio=$('#tb_turnos_abiertos').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_turnos_abiertos'},
            dataSrc: ''
        },     
        columns: [                      
            { data: 'user_nick',
            "render": function (data){
                return ucwords(data.toLowerCase());
            }
            },  
            { data: 'desde'},                    
            { data: 'hasta'},              
            { data: 'turn_nombre'},
            { data: 'nume_recetas',
            "render":function (data){
                return  r='<span class="badge badge-success"><strong>'+data+'</strong></span>';
            }
            }, 
             
        ],
        "lengthChange": false,
        responsive: true ,
        "order": [[ 2, "asc" ]],        
    }); 
  
    $.getJSON(model,{funcion:'get_consumo_medicamentos'},function(data){ 
        Morris.Line({
            data: data,
            element: "morris-line-chart",
            xkey: "fecha",
            ykeys: ["total"],
            labels: ["Total Movimientos Unidades"],
          });    
    
    });
    
    function alerta_campana(){
        if(parseInt(noti)>0){
            var out='<span class="heartbit" style="background-color:#ff0400"></span><span class="point"></span>';
            $('#notificacion').append(out);   
        }
    }

    function alerta_campana_pedidos(){
        if(parseInt(noti_p)>0){
            var out='<span class="heartbit" style="background-color:#ff0400"></span><span class="point"></span>';
            $('#notificacion_pedidos').append(out);   
        }
    }


    //DETALLE NOTIFICACIONES
    $.getJSON(model,{funcion:'detalle_notificacion'}, function( data ) {
        $.each(data, function (i, items) {                              
                   noti+=1;
                   var out='<a href="#">';
                       out+='<div class="btn btn-circle m-r-10"><i class="fa fa-shopping-cart"></i></div>';
                       out+='<div class="mail-contnet">';
                       out+='<h5>Transfer:</h5><span class="mail-desc">'+items["docu_fecha"]+'</span> <span class="mail-desc">De: '+items["origen"]+'</span> <span class="time">A: '+items["destino"]+'</span>';
                       out+='</div>';
                       out+='</a>';
               
                $('#detalle_notificacion').append(out);    
        });
        alerta_campana();
      
    });

    //DETALLE PEDIDODS
    $.getJSON(model,{funcion:'detalle_notificacion_pedidos'}, function( data ) {
        $.each(data, function (i, items) {                              
                   noti_p+=1;
                   var out='<a href="#">';
                       out+='<div class="btn btn-circle m-r-10"><i class="fa fa-shopping-cart"></i></div>';
                       out+='<div class="mail-contnet">';
                       out+='<h5>Pedido:</h5><span class="mail-desc">'+items["docu_fecha"]+'</span> <span class="mail-desc">A: '+items["origen"]+'</span> <span class="time">Desde: '+items["destino"]+'</span><span class="time">'+items["docu_procesado"].substr(0,19)+'</span>';
                       out+='</div>';
                       out+='</a>';
               
                $('#detalle_notificacion_pedidos').append(out);    
        });
        alerta_campana_pedidos();
      
    });



     //FUNCION INTERNA
     function saldo_kardex(){
        $.get(model,{funcion:'ajustar_saldo_kardex'}, function(data){
        });
    }
    saldo_kardex();

//********************** 1 */



});