$(document).ready(function(){

    var model='model/reportes/generar.php';   
    var accion='repor_egresos';
    var unid_id;   

    var fini= $('#finir2');
    var ffin= $('#ffinr2'); 
    fini.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
    ffin.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });       

    fini.datepicker('setDate', new Date());
    ffin.datepicker('setDate', new Date());


    function combo_unidad_r4(){        
        combo1='#unid_id_r4';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();                       
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function grupo(){        
        combo2='#grupo_prod_id';
        $.getJSON(model,{funcion:'get_grupo'})
        .done(function(json) {   
            $(combo2).empty();                                   
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["grupo_prod_id"]).text(items["grupo_prod_nombre"]);
			});
        });  
    }
   
    combo_unidad_r4();    
    grupo();  
    
    generar_html_r5();

  
    function generar_html_r5(){        
        unid_id=$("#unid_id_r4").val();    
        grupo_prod_id=$("#grupo_prod_id").val();
       
        if(unid_id=='0'){
            $('#rep_4').empty();
            return false;
        }

        f_ini= $('#finir2').val();
        f_fin= $('#ffinr2').val(); 
        
        $('#rep_4').empty();
        $('#rep_4').html('Generando Reporte...');
        $.get(model,{funcion:accion,unid_id:unid_id,grupo_prod_id:grupo_prod_id,f_ini:f_ini,f_fin:f_fin},function(data){$('#rep_4').html(data);});
    }

     //IMPRIMIR PDF
     $( "#btn_rp4_pdf" ).click(function() {    
        //unid_id=$("#unid_id_r1").val();      
        //window.open('model/reportes/pdf.php?f='+accion+'&unid_id='+unid_id, '_blank');
      });

      $("#btn_genreceta").click(function() {            
        generar_html_r5();        
      });

  
                
              
      $("#btnexportar").click(function() {            
        $("#rep_4").table2excel({           
            name: "Excel_Medix"
        }); 
      });

     
});