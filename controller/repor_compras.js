$(document).ready(function(){

    var model='model/reportes/generar.php';   
    var accion='repor_compras';
    var unid_id;   
    var tipo_docu_id;
    var prov_id;

    var fini= $('#finir2');
    var ffin= $('#ffinr2'); 
    fini.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
    ffin.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });       

    fini.datepicker('setDate', new Date());
    ffin.datepicker('setDate', new Date());

    function combo_unidad_r4(){        
        combo1='#unid_id_r4';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();                       
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    $("#unid_id_r4").change(function() {  
        unid_id=$("#unid_id_r4").val();
        tipo_docu_id=$("#tipo_docu_id").val();
        proveedor();
    });

    function proveedor(){        
        combo2='#prov_id';
        $.getJSON(model,{funcion:'get_proveedor',unid_id:unid_id,tipo_docu_id:tipo_docu_id})
        .done(function(json) {   
            $(combo2).empty();
            $(combo2).append('<option value="0" selected="selected">--TODOS--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["prov_id"]).text(items["prov_nombre"]);
			});
        });  
    }

    function tipo_documento(){        
        combo3='#tipo_docu_id';
        $.getJSON(model,{funcion:'get_tipo_documento_ingresos',unid_id:unid_id,tipo_docu_id:tipo_docu_id})
        .done(function(json) {   
            $(combo3).empty();            
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
			});
        });  
    }
   
    combo_unidad_r4();         
    tipo_documento();
  
    function generar_html_r5(){        
        unid_id=$("#unid_id_r4").val();    
        tipo_docu_id=$("#tipo_docu_id").val();    
        prov_id=$("#prov_id").val();

        if(unid_id=='0'){
            warning_toast("Seleccione una Unidad...");
            return false;
        }

       
        if(unid_id=='0'){
            $('#rep_4').empty();
            return false;
        }

        f_ini= $('#finir2').val();
        f_fin= $('#ffinr2').val(); 
        
        $('#rep_4').empty();
        $('#rep_4').html('Generando Reporte...');
        $.get(model,{funcion:accion,unid_id:unid_id,prov_id:prov_id,f_ini:f_ini,f_fin:f_fin,tipo_docu_id:tipo_docu_id},function(data){$('#rep_4').html(data);});
    }

     //IMPRIMIR PDF
     $( "#btn_rp4_pdf" ).click(function() {    
        //unid_id=$("#unid_id_r1").val();      
        //window.open('model/reportes/pdf.php?f='+accion+'&unid_id='+unid_id, '_blank');
      });

      $("#btn_genreceta").click(function() {   
          
        generar_html_r5();        
      }); 
              
      $("#btnexportar").click(function() {            
        $("#rep_4").table2excel({           
            name: "Excel_Medix"
        }); 
      });

     
});