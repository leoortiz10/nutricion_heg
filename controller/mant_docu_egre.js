$(document).ready(function(){

    var model='model/kardex_pro/funciones.php';
    var idx;
    var accion;
    var table3;
    var table4;
    var unid_id;
    var tipo_docu_id;
    var fecha = $('#docu_fecha');  

    var fecha3 = $('#deta_docu_fech_venc');  

    var lote_cant;

     //DOCUMENTO EGRESOS
     var tipo='E';

    fecha.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        startDate: '-2d',
        endDate: '+1d'
    });
  
  /* 
    fecha3.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });*/
        
    $('#tb_4').hide();    
  
    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

    function docu_numero_e(){        
            $.get(model,{funcion:'docu_numero',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo},function(data){$('#docu_numero').val(data);});        
    }

    $("#unid_id").change(function() {
        unid_id=$("#unid_id").val();
       docu_numero_e();
    });

    $("#tipo_docu_id").change(function() {   
        tipo_docu_id=$("#tipo_docu_id").val();
        docu_numero_e();
     });
 //Para permitir input solo de números


 /*   $("#deta_docu_cantidad").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            } 
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });*/
                $("#deta_docu_cantidad").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

   //TABLA DATOS
   function egreso(){ 
    unid_id=$('#unid_id_s').val();
    tipo_docu_id=$('#tipo_docu_id_s').val();
    table3=$('#tb_3').DataTable( {        
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo},
            dataSrc: ''
        },             
        columns: [                      
            { data: 'tipo_docu_nombre',
            "render":function(data){
                return '<strong>'+data+'</strong>';
            }    
        },
            { data: 'docu_numero' },
            { data: 'unid_nombre' ,
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }  },
            { data: 'docu_fecha' },            
            { data: 'docu_autoriza',
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },                            
            { data: 'docu_entrega',
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },            
            { data: 'docu_recibe',
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },
            { data: 'stat_docu_nombre',
                "render":function(data){
                    var r;
                    switch(data){
                        case 'No Procesado':
                            r='<span class="badge badge-danger">'+data+'</span>';
                        break;
                        case 'Procesado':
                            r='<span class="badge badge-success">'+data+'</span>';
                        break;
                        case 'Anulado':
                            r='<span class="badge badge-warning">'+data+'</span>';
                        break;
                        case 'Borrado':
                            r='<span class="badge badge-dark">'+data+'</span>';
                        break;
                        default:
                            r='<span class="badge badge-warning">'+data+'</span>';                        
                    }
                    return r;
                }
            },
            { data: 'docu_id' ,
                "render": function ( data ) {                                              
                return '<button type="button" title="Detalles" class="btn btn-secondary btn-xs m-l-5 btn_detalle" id="'+data+'"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" title="Editar" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" title="Observaciones" class="btn btn-secondary btn-xs m-l-5 btn_obs" id="'+data+'"><i class="fa fa-bullhorn"></i></button><button type="button" title="Eliminar/Anular" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_egreso" id="'+data+'"><i class="fa fa-print"></i></button><button type="button" title="Procesar" class="btn btn-secondary btn-xs m-l-5 btn_procesar_docu" id="'+data+'"><i class="fa fa-check"></i></button>';
                }
            } 
            
            
        ],
        "order": [[ 3, "desc" ]],
        responsive: true 
             
    });    
    }

     //IMPRIMIR PDF
     $(document).on('click', '.btn_print_egreso', function(){
        idx=$(this).attr("id");  
        window.open('model/reportes/pdf.php?f=repor_egresos_pdf&id='+idx, '_blank');
    });

    $(document).on('click', '.btn_edit', function(){ 
        accion='editar';
        idx=$(this).attr("id");  

       $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {          
            
            if(json[0].stat_docu_id == 0){
              // $("#docu_fecha").prop('disabled', false); 
               $("#unid_id").prop('disabled', false);
               $("#tipo_docu_id").prop('disabled', false);
               $("#area_id").prop('disabled', false);
               $("#prov_id").prop('disabled', false);
               $("#docu_autoriza").prop('readOnly', false);
               $("#docu_entrega").prop('readOnly', false);
               $("#docu_entrega").prop('readOnly', false);
               $("#docu_observacion").prop('readOnly', false); 
               $("#docu_recibe").prop('readOnly', false);   
               $('#btn_save_ingreso').prop('disabled', false);  

            }else{
                if(json[0].stat_docu_id == 2){
              // $("#docu_fecha").prop('disabled', true); 
               $("#unid_id").prop('disabled', true);
               $("#tipo_docu_id").prop('disabled', true);
               $("#area_id").prop('disabled', true);
               $("#prov_id").prop('disabled', true);
               $("#docu_autoriza").prop('readOnly', true);
               $("#docu_entrega").prop('readOnly', true);
               $("#docu_entrega").prop('readOnly', true);
               $("#docu_observacion").prop('readOnly', true);   
               $("#docu_recibe").prop('readOnly', true);                 
               $('#btn_save_ingreso').prop('disabled', true); 
                }else{
              //  $("#docu_fecha").prop('disabled', true); 
               $("#unid_id").prop('disabled', true);
               $("#tipo_docu_id").prop('disabled', true);
               $("#area_id").prop('disabled', true);
               $("#prov_id").prop('disabled', true);
               $("#docu_autoriza").prop('readOnly', true);
               $("#docu_entrega").prop('readOnly', true);
               $("#docu_entrega").prop('readOnly', true);
               $("#docu_observacion").prop('readOnly', false);   
               $("#docu_recibe").prop('readOnly', true);                 
               $('#btn_save_ingreso').prop('disabled', false);
                }

                    
                                               
            }
}); 


        $('#ModalNew').modal('show'); 
        $('#ModalNewTitle').text("Editar"); 
        editar(idx);
    });

    $(document).on('click', '.btn_agregar', function(){    
        $('#lotes_info').html('');
        lote_cant=0;        
        $('#form33').trigger("reset");
        $("#prod_nombre").prop('disabled', false);
        $('#lote_id').val('');
        $('#prod_id').val('');        
        combo_pres();       
        accion='agregar_detalle_producto';        
        $('#ModalNewAgregar').modal('show'); 
        $('#ModalNewTitleAgregar').text("Agregar");         
    });

    $('#ModalNewAgregar').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
      });

    $(document).on('click', '.btn_del', function(){  
        accion='eliminar';
        idx=$(this).attr("id");  
        swal({
            title: "Anular/Eliminar ?",
            text: "Desea Eliminar/Anular éste Documento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx}
                        }).done(function(data) {       
                            table3.ajax.reload();            
                            swal("Notificación:", data, "info");
                         }); 
                })
              }
            });    
    });

    $(document).on('click', '.btn_obs', function(){  
        idx=$(this).attr("id");  
        $.getJSON(model,{id:idx,funcion:'get_obs_doc'})
        .done(function(json) {        
         swal(json[0].obs)
        });   
    });

    //$(document).on('click', '.btn_new', function(){ 
      $("#btn_new_egresos_nutricion").click(function(){
        $.get(model,{funcion:'consultar_documentos_pendientes_egresos'},function(data){
      if(parseInt(data)>0) {           
         warning_toast('Tiene documentos pendientes de Procesar....');
       }else{
        $("#docu_fecha").prop('disabled', false); 
               $("#unid_id").prop('disabled', false);
               $("#tipo_docu_id").prop('disabled', false);

               //$("#tipo_docu_id").prop('required', true);


               $("#area_id").prop('disabled', false);
               $("#prov_id").prop('disabled', false);
               $("#docu_autoriza").prop('readOnly', false);
               $("#docu_entrega").prop('readOnly', false);
               $("#docu_entrega").prop('readOnly', false);
               $("#docu_observacion").prop('readOnly', false); 
               $("#docu_recibe").prop('readOnly', false);   
               $('#btn_save_ingreso').prop('disabled', false);  
        $('#form22').trigger("reset");        
        fecha.datepicker('setDate', new Date());       
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('#ModalNewTitle').text("Nuevo Egreso"); 
       }

       });        
    });

    function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#docu_fecha').val(json[0].docu_fecha);
        $('#tipo_docu_id').val(json[0].tipo_docu_id);        
        $('#docu_numero').val(json[0].docu_numero);
        $('#unid_id').val(json[0].unid_id);        
        $('#area_id').val(json[0].area_id);  
        $('#prov_id').val(json[0].prov_id);  
        $('#docu_autoriza').val(json[0].docu_autoriza);
        $('#docu_entrega').val(json[0].docu_entrega);  
        $('#docu_recibe').val(json[0].docu_recibe);
        $('#docu_observacion').val(json[0].docu_observacion);        
        });       
    }

    $('#form22').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
         if($('#tipo_docu_id').val()==0){
            danger_toast('Verifique Tipo Documento...');
            $('#tipo_docu_id').focus();
            return false;
        }

           if($('#unid_id').val()==0){
            danger_toast('Verifique la Unidad...');
            $('#unid_id').focus();
            return false;
        }

        if($('#docu_numero').val().length<1){
            danger_toast('Verifique el Numero de Documento...');
            $('#docu_numero').focus();
            return false;
        }

          if($('#area_id').val()==0){
            danger_toast('Verifique el área de egreso...');
            $('#area_id').focus();
            return false;
        }

         if($('#prov_id').val()==0){
            danger_toast('Verifique el provedor...');
            $('#prov_id').focus();
            return false;
        }
                  
        
        if($('#docu_autoriza').val().length<1){
            danger_toast('Verifique el Responsable...');
            $('#docu_autoriza').focus();
            return false;
        }


        if($('#docu_entrega').val().length<1){
            danger_toast('Verifique el Responsable de entrega...');
            $('#docu_entrega').focus();
            return false;
        }

         if($('#docu_recibe').val().length<1){
            danger_toast('Verifique el Responsable que Recibe...');
            $('#docu_recibe').focus();
            return false;
        }

        if($('#docu_observacion').val().length<1){
            danger_toast('Verifique la Observacion...');
            $('#docu_observacion').focus();
            return false;
        }

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data,tipo:tipo},
            beforeSend: function () {
                    $("#btn_save_ingreso").attr("disabled", true);                    
                }
            }).done(function(data) {				
               /* table3.ajax.reload();	
                $("#btn_save_ingreso").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);*/
                if(data=='0'){
                                 warning_toast("NO SE PUDO PROCESAR LA PETICIÓN");
                             }
                             //swal("Notificación:", " NO SE PUDO PROCESAR LA PETICIÓN", "error");                                                  
                             else{
                             //swal("Notificación:", "LA PETICIÓN SE PROCESO CORRECTAMENTE", "success");
                             table3.ajax.reload();  
                             $("#btn_save_ingreso").attr("disabled", false); 
                             $('#ModalNew').modal('hide');
                             success_toast("LA PETICIÓN SE PROCESÓ CORRECTAMENTE");
                             
                             } 
               
             });
    });

    $(document).on('click', '.btn_detalle', function(){   
        idx=$(this).attr("id");         

        $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {
            $('#mtitulo1').html(' *Documento: '+json[0].docu_numero);      
            $('#mtitulo2').html(' *'+json[0].unid_nombre);
            $('#mtitulo3').html(' *'+json[0].tipo_docu_nombre);

            //OBTIENE ID UNIDAD
            unid_id=json[0].unid_id;
            
            //SOLO SI ESTADO ES 0 NO PROCESADO HABILITA BORON DE AGREGAR PRODUCTO
            if(json[0].stat_docu_id == 0)
                $('#btn_agregar').prop('disabled', false);
            else
                $('#btn_agregar').prop('disabled', true);
            
        }); 

        $('#ModalDetalleIngreso').modal('show'); 
        table4 = $('#tb_4').DataTable();
        table4.destroy();
        $('#tb_4').show();              
        accion='detalle_ingreso';        
        detalle_ingreso_e(idx);
    });

    function detalle_ingreso_e(idx){
         table4=$('#tb_4').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,id:idx},
                dataSrc: ''
            },     
            columns: [                          
                { data: 'prod_nombre',
                    "render":function(data){
                        return '<h4><span style="white-space: nowrap;font-size:13px"><strong>'+ucwords(data.toLowerCase())+'<strong></span></h4>';
                    }
                },
                { data: 'presentacion'},
               /* { data: 'deta_docu_lote',
                "render":function(data){
                    return '<h4><span class="badge badge-warning"><strong>'+data+'<strong></span></h4>';
                }},
                { data: 'deta_docu_fech_venc'},
                { data: 'deta_docu_regi_sani'},*/
                { data: 'deta_docu_cantidad',
                "render":function(data){
                    return '<h4><span class="badge badge-primary"><strong>'+data+'<strong></span></h4>';
                }},
                { data: 'deta_docu_valo_unit'},
                { data: 'deta_docu_iva'},
                { data: 'deta_docu_valo_iva'},
                { data: 'deta_docu_subtotal'}  ,
                { data: 'deta_docu_id',
                "render":function(data){
                    //return '<button type="button" title="Eliminar" class="btn btn-secondary btn-xs m-l-5 btn_del_producto_detalle" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" title="Cambiar" class="btn btn-secondary btn-xs m-l-5 btn_detalle_doc_cambiar" id="'+data+'"><i class="fa fa-file"></i></button>';
                    return '<button type="button" title="Eliminar" class="btn btn-secondary btn-xs m-l-5 btn_del_producto_detalle" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }}                         
            ],
            responsive: true,
            dom: 'lBfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print',
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                }
            ]       
        });    
    }

       function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#unid_id_s';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Todas--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_tipo_doc(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#tipo_docu_id_s';
        $.getJSON(model,{funcion:'get_tipo_doc_admin',tipo:tipo})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Todos--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
			});
        });  
    }

    function combo_unidad_2(){
        //LLENO COMBOBOX POR DATOS JSON
        combo5='#unid_id';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo5).empty();   
            $(combo5).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo5).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_tipo_doc_2(){
        //LLENO COMBOBOX POR DATOS JSON
        combo6='#tipo_docu_id';
        $.getJSON(model,{funcion:'get_tipo_doc',tipo:tipo})
        .done(function(json) {   
            $(combo6).empty();   
            $(combo6).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo6).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
			});
        });  
    }    

    function combo_area(){
        //LLENO COMBOBOX POR DATOS JSON
        combo7='#area_id';
        $.getJSON(model,{funcion:'get_area'})
        .done(function(json) {   
            $(combo7).empty();   
            $(combo7).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo7).val(items["area_id"]).text(items["area_nombre"]);
			});
        });  
    } 
    function combo_proveedor(){
        //LLENO COMBOBOX POR DATOS JSON
        combo9='#prov_id';
        $.getJSON(model,{funcion:'get_prov'})
        .done(function(json) {   
            $(combo9).empty();   
            $(combo9).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo9).val(items["prov_id"]).text(items["prov_nombre"]);
			});
        });  
    }   
   
    function combo_pres(){
        //LLENO COMBOBOX POR DATOS JSON
        combo8='#pres_id';
        $.getJSON(model,{funcion:'get_pres'})
        .done(function(json) {   
            $(combo8).empty();   
            $(combo8).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo8).val(items["pres_id"]).text(items["pres_nombre"]);
            });
            $("#pres_id").val("119"); //UNIDAD
        });  
    }  

    combo_unidad();
    combo_area();
    combo_tipo_doc();
    combo_unidad_2();
    combo_tipo_doc_2();
    combo_proveedor();
    

    $("#unid_id_s").change(function() {
        table3.destroy();      
        egreso();
      });

      $("#tipo_docu_id_s").change(function() {
        table3.destroy();      
        egreso();
      });

      egreso();//CARGA INICIAL

      //ELIMINA PRODUCTO DEL DETALLE DOCUMENTO
      $(document).on('click', '.btn_del_producto_detalle', function(){  
        accion='eliminar_producto_detalle';
        idx_d=$(this).attr("id");  
        swal({
            title: "Eliminar Producto ?",
            text: "Desea Elimina éste Producto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id_d:idx_d}
                        }).done(function(data) {       
                            table4.ajax.reload();            
                            swal("Notificación:", data, "info");
                         });
                })
              }
            });       
    });


     //PROCESAR
     $(document).on('click', '.btn_procesar_docu', function(){  
        accion='procesar_documento_kardex';
        //SI RECIBE ES 0 PROCESO NORAML
        //SI RECIBE ES 1 PROCESO REVERSAR
        recibe=0;
        idx=$(this).attr("id");  
        swal({
            title: "Procesar ?",
            text: "Desea Procesar el Documento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Procesar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx,recibe:recibe}
                        }).done(function(data) {       
                            
                            swal("Notificación:", data, "info");
                            table3.ajax.reload();            
                         });   
                })
              }
            });
    });

      //************************MODAL INGRESO DETALLE MEDICAMENTO*********************
     /* $("#prod_nombre").autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: model,
            dataType: "json",
            data: {
              term: request.term,funcion:'buscar_producto',unid_id:unid_id
            },
            success: function( data ) {
              response(data);
            }
          } );
        },
        minLength: 2,
        select: function( event, ui ) {
          console.log( "Selected: " + ui.item.value + " id:  " + ui.item.prod_id );
          var prod_id=ui.item.prod_id;
          $("#prod_id").val(prod_id);            
          buscar_lotes(prod_id);
        }
      });*/

      $('#prod_nombre').typeahead({
        displayText: function(item) {
             return item.value
        },
        afterSelect: function(item) {
            this.$element[0].value= item.value; 
            var prod_id=  item.prod_id    
            var caducidad_registroSanitario=item.caducidad_registroSanitario;    
            $("#prod_id").val(prod_id);       
            $("#caducidad_registroSanitario").val(caducidad_registroSanitario); 
            buscar_lotes(prod_id);                               
        },
        source: function (query, process) {
          return $.getJSON(model, { term: query,funcion:'buscar_producto',unid_id:unid_id}, function(data) {
            if(data[0].prod_nombre==0)
                window.location.href = "model/config/lgout.php";
            else
                process(data);
          })
        }   
      });

      function buscar_lotes(prod_id){
        
        $.getJSON(model,{funcion:'lotes_productos',prod_id:prod_id,unid_id:unid_id},function(json){            
                // $('#lotes_info').html(json[0].lote_codigo);
                  var html='';
                  $.each(json, function(index, array) {
                     html+='<div class="form-check">';
                     html+='<input class="form-check-input radio_lote" type="radio"  name="deta_docu_lote" pre="'+array['sald_kard_precio']+'" can="'+array['cantidad']+'" reg="'+array['regi']+'" fec="'+array['fech']+'"    id="'+array['lote_id']+'" value="'+array['lote_codigo']+'"pr="'+array['pn']+'"/>';
                     html+='<label class="form-check-label"><strong>Código:</strong> '+array['lote_codigo']+' <strong>Cantidad:</strong> '+array['cantidad']+'</label> ';
                   
                     html+='</div>';
                 });
                $('#lotes_info').html(html);
        });
      }

        //OBTIEN ID LOTE AL DAR CLICK EN RADIO BUTON LOTES
      $(document).on('click', '.radio_lote', function(){   
            $('#lote_id').val($(this).attr("id"));
            lote_cant=$(this).attr("can");
            //$('#deta_docu_lote').val($(this).attr("value"));
            $('#deta_docu_regi_sani').val($(this).attr("reg"));
            $('#deta_docu_fech_venc').val($(this).attr("fec"));      
            $("#deta_docu_valo_unit").val($(this).attr("pre"));     
            //$("#pres_nombre").val($(this).attr("pr"));
      });

      

      //EVENTO PARA GUARDAR PRODUCTO EN EL DETALLE
      $('#form33').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO

        if($('#prod_nombre').val().length<1){
            danger_toast('Verifique el Producto...');
            return false;
        }
     
        $("#prod_nombre").prop('disabled', true);

        if($('#prod_id').val().length<1){
            danger_toast('Verifique el Producto...');
            return false;
        }
        
        if($('#pres_id').val()==0){
            danger_toast('Verifique la Presentacion...');
            return false;
        }

        if($('#lote_id').val().length<1){
            danger_toast('Verifique el Lote...');
            return false;
        }

        if($('#deta_docu_lote').val()==''){
            danger_toast('Verifique el Lote...');
            return false;
        }

        if(parseFloat(lote_cant)<=0){
            danger_toast('Cantidad Lote Insuficiente... Seleccione otro Lote');
            return false;
        }

        if(parseFloat($('#deta_docu_cantidad').val())>parseFloat(lote_cant)){
            danger_toast('Cantidad es Mayor al Disponible del Lote Seleccionado!!!');
            return false;
        }
        
        if(parseFloat($('#deta_docu_cantidad').val())<0){
            danger_toast('Verifique la Cantidad...');
            return false;
        }
        if($('#deta_docu_valo_unit').val().length<1){
            danger_toast('Verifique el Valor Unitario...');
            return false;
        }        

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_save_ingreso").attr("disabled", true);                    
                }
            }).done(function(data) {	
                table4.ajax.reload();	
                $("#prod_nombre").prop('disabled', false);
                $("#btn_save_ingreso").attr("disabled", false);	
                if(data>0)
                    $('#ModalNewAgregar').modal('hide');                
                success_toast(data+' Producto Agregado...');
               
             });
    });

/********************TEMPORAL CAMBIO DOCUMENTO IGUALAR */
$(document).on('click', '.btn_detalle_doc_cambiar', function(){ 
    idx=$(this).attr("id");  
    $('#ModalCamDoc').modal('show'); 
    combo_doc_cambio();
});

$('#ModalCamDoC').on('hidden.bs.modal', function () {    
    $('body').addClass('modal-open');
  });
  function combo_doc_cambio(){
    //LLENO COMBOBOX POR DATOS JSON
    combo9='#cmb_docu_cambio';
    $.getJSON(model,{funcion:'get_docu_cambio_receta',unid_id:unid_id})
    .done(function(json) {   
        $(combo9).empty();   
        $(combo9).append('<option value="0" selected="selected">--Seleccione--</option>');
        $.each(json, function (i, items) {
             if(items !== null) $("<option>").appendTo(combo9).val(items["docu_id"]).text(items["docu_numero"]);
        });
        
    });  
}  

$("#btn_cam_doc").click(function() {
    docu_id_destino =$("#cmb_docu_cambio").val();
    $.get(model,{funcion:'cambiar_docu',id:idx,id_d:docu_id_destino},function(data){
        swal("Notificación:", data, "info");
        table4.ajax.reload();	        
        $('#ModalCamDoc').modal('hide');
    });
  });

});