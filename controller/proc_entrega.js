$(document).ready(function(){
    $('html, body').animate({scrollTop: '0px'}, 500); 
    $("#noen_ci").focus();  
    $("#btn_cerrar_turno").prop('disabled', true);

    var model='model/proc_entrega/funciones.php';
    var prod='model/proc_entrega/producto.php';
    var paciente='model/proc_entrega/paciente.php';
    var idx;
    var accion;
    var unid_id=0;
    var turn_id=0;
    var noen_id=0;
    var recibi_conforme;

    var table1;
    var table2;
    var table3;
    var table4;

    var c1;
    var h1;
    var n1;
    var fec;
    var nom;

    var fecha = $('#noen_fecha');  
    fecha.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
    fecha.datepicker('setDate', new Date());

    function combo_unidad_rec(){
        //LLENO COMBOBOX POR DATOS JSON
        combo1='#unid_id';
        $.getJSON(model,{funcion:'get_unidad_receta'})
        .done(function(json) {   
            $(combo1).empty();   
            $(combo1).append('<option value="0" selected="selected">--Seleccione Unidad--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
    function combo_turno_rec(){
        //LLENO COMBOBOX POR DATOS JSON
        combo2='#turn_id';
        $.getJSON(model,{funcion:'get_turno_unidad',unid_id:unid_id})
        .done(function(json) {   
            $(combo2).empty();   
            $(combo2).append('<option value="0" selected="selected">--Seleccione Turno--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["turn_id"]).text(items["turn_nombre"]);
			});
        });  
    }
    function combo_area_rec(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#area_id';
        $.getJSON(model,{funcion:'get_area'})
        .done(function(json) {   
            $(combo3).empty();   
            //$(combo3).append('<option value="0" selected="selected">--Seleccione Turno--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["area_id"]).text(items["area_nombre"]);
            });
            
            $.get(model,{funcion:'get_area_receta'},function(data){
                //$('#dash_inicio3').html(data);    
                $('#area_id').val(data).trigger('change');
            });
        });  
    }
    function combo_medico_rec(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#medi_id';
        $.getJSON(model,{funcion:'get_medico'})
        .done(function(json) {   
            $(combo4).empty();   
            //$(combo4).append('<option value="0" selected="selected">--Seleccione Turno--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["medi_id"]).text(items["medi_nombre"]);
            });
            $('#medi_id').val(735).trigger('change');
        });  
    }

    combo_unidad_rec();
    combo_medico_rec();
    combo_area_rec();

  //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
  function ucwords(oracion){
    return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
       return $1.toUpperCase(); 
    });
}

    //AL SELECCIONAR UNIDAD - BUSCA SUS TURNOS
    $("#unid_id").change(function() {   
        //$('.color_bg_turno').css('background-color', '#FFFFFF'); 
        table1=$('#tbreceta_1').DataTable();   
        table1.clear(); 
        table1.destroy();

        table3=$('#tbentregados_1').DataTable();   
        table3.clear(); 
        table3.destroy();

        table4=$('#tbstocks_1').DataTable();   
        table4.clear(); 
        table4.destroy();

        unid_id=$("#unid_id").val();
        turn_id=0;
        $('#turn_id').empty();  
        
        combo_turno_rec();
        //file_json();
        
    });

    $("#turn_id").change(function() { 
        $('#noen_ci').focus();
        //$('.color_bg_turno').css('background-color', '#D1C4E9');
        table1=$('#tbreceta_1').DataTable();
        table1.clear(); 
        table1.destroy();      

        table3=$('#tbentregados_1').DataTable();
        table3.clear(); 
        table3.destroy(); 

        table4=$('#tbstocks_1').DataTable();
        table4.clear(); 
        table4.destroy();

        turn_id=$("#turn_id").val();          
        /*if(turn_id==0){    
            $('.color_bg_turno').css('background-color', '#FFFFFF');         
            $("#btn_cerrar_turno").prop('disabled', true);
        }
        else{
            color_fondo();
            $("#btn_cerrar_turno").prop('disabled', false);            
            listado_recetas_tabla();
        }*/
        $("#btn_cerrar_turno").prop('disabled', false);
        listado_recetas_tabla();
        listado_productos_entregado();
        listado_stocks();

        

    });

   /* function color_fondo(){
        $.get(model,{funcion:'color_fondo',turn_id:turn_id},function(data){ $('.color_bg_turno').css('background-color', "'"+data+"'");});
    }
    */

    $( "#btn_limpiar" ).click(function() {

        limpiar();
      });

      function limpiar(){
        recibi_conforme="";
        $("#noen_historia_clinica").val("");
        $("#noen_ci").val("");
        $("#noen_nombre").val("");
        $("#noen_historia_clinica").empty();
        $("#noen_ci").empty();
        $("#noen_nombre").empty();
        $("#noen_ci").focus();
      }
 
      $("#prod_nombre").keydown(function(event){
          //DETECTA TECLAS CTRL+ENTER CTRL + P
		if ((event.ctrlKey && event.keyCode == 13) || (event.ctrlKey && event.keyCode == 80))  {
            event.preventDefault();       
          
            receta_recibi_conforme();
          }		
     }); 
     
     $("#deno_cantidad").keydown(function(event){
        //DETECTA TECLAS CTRL+ENTER
      if ((event.ctrlKey && event.keyCode == 13) || (event.ctrlKey && event.keyCode == 80)) {
        event.preventDefault();       
        receta_recibi_conforme();
        }		
   }); 

   function receta_recibi_conforme(){
    swal({
        title: "Recibí Conforme !!",
        text: "Datos de quien recibe...!!",
        input: 'text',
        showCancelButton: true,
        closeOnConfirm: true,
        animation: "slide-from-top",
        inputPlaceholder: ""
    }).then((result)=>{
        if (result.value) 
            recibi_conforme=result.value;
        else
            recibi_conforme="";
        guardar_receta(); 
    });
   }
    
     /**CLIC BOTON AGRGEGAR RECETA MUESTRA MODEL PARA RECETAR  */
     $('#formu1').on("submit",function(event){  
        event.preventDefault();                         
        if(unid_id==0){
            danger_toast('Seleccione una Unidad..');
            $('html, body').animate({scrollTop: '0px'}, 500);            
            return false;
         }

         if(turn_id==0){
            danger_toast('Seleccione un Turno..');
            $('html, body').animate({scrollTop: '0px'}, 500);
            return false;
         }

         fec=$('#noen_fecha').val().trim();
         var ced=$('#noen_ci').val().trim();
          nom=$('#noen_nombre').val().trim();
         var his=$('#noen_historia_clinica').val().trim();

         if(fec.length<5){
            danger_toast('Verifique la Fecha...');
             return false;
         }
         if(ced.length<2){
            danger_toast('Verifique la Cedula...');
             return false;
         }
         if(nom.length<5){
            danger_toast('Verifique el Nombre...');
             return false;
         }            
     
        
        $('#titulo_receta').html($('#noen_nombre').val());
        form_data=$(this).serialize();

         $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:'crear_receta_paciente',data:form_data,unid_id:unid_id,turn_id:turn_id},
            beforeSend: function () {
                    $("#btn_agregar_orden").attr("disabled", true);                    
                }
            }).done(function(data) {
                if(data!='0'){
                        noen_id=data;
                        $("#num_receta").html(noen_id);		
                        listado_items_tabla(noen_id); 	
                        table1.ajax.reload();	
                        $("#btn_agregar_orden").attr("disabled", false);               	                                 
                        $("#prod_nombre").val('');
                        $("#prod_id").val('');
                        $("#stock").val('');
                        $("#deno_cantidad").val('');
                        limpiar();   
                } else{
                //warning_toast('No se puede crear la Receta...');
                window.location.href = "model/config/lgout.php";
                }
             }); 
       
        $('#ModalNewReceta').modal('show');                 
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO     
    });

    $('#ModalNewReceta').on('shown.bs.modal', function () { 
            //VERIFICA SI PACIENTE YA TIENE RECETA PREVIA EN ESTE TURNO Y UNIDAD
         $.get(model,{funcion:'verifica_paciente_receta',unid_id:unid_id,turn_id:turn_id,fecha:fec,paciente:nom},function(data){            
                if(data!='0'){                    
                    swal({
                        title: '',                        
                        html:data,                        
                        focusConfirm: false,
                        confirmButtonText:
                          '<i class="fa fa-thumbs-up"></i> Ok!'
                      })
                }
          });

        recibi_conforme="";  
        $("#prod_nombre").val('');                     
        $("#prod_nombre").focus();                  
    });

    $('#ModalNewReceta').on('hidden.bs.modal', function(e) {         
        $(this).removeData();
        $(this).data('bs.modal', null);
        limpiar();
        //table2=$('#tbrecetadetalle_2').DataTable();            
        //table2.destroy();        
        //table2.clear();      
        table3.ajax.reload();
        table4.ajax.reload();
    });

    /**MODAL 2 BOTON AGREGAR ITEM O PRODUCTO A RECETA DEL PACIENTE */
    $('#formu2').on("submit",function(event){
        event.preventDefault();
        var prod_id=$("#prod_id").val();
        var deno_cantidad=$("#deno_cantidad").val();
        var stock=$("#stock").val();
        var deno_precio=$("#deno_precio").val();
        if(prod_id.length<1){
            danger_toast('Seleccione un Item...');
             return false;
         }
         if(deno_cantidad.length<1){
             deno_cantidad=1;
           // danger_toast('Verifique la Cantidad...');
            // return false;
         }
        
         if(Number(deno_cantidad)<1){
            danger_toast('Verifique la Cantidad...');
             return false;
         }

         if(Number(deno_cantidad)>Number(stock)){
            warning_toast('Cantidad Supera al Stock Disponible, Verifique!!!');
             return false;
         }
         $("#prod_id").val('');
         $("#prod_nombre").val('');	
         $("#prod_nombre").focus();
         $("#stock").val('');
         $("#deno_cantidad").val('');
         $("#deno_precio").val('');
         $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:'agregar_item_receta',unid_id:unid_id,turn_id:turn_id,noen_id:noen_id,prod_id:prod_id,deno_cantidad,deno_cantidad,deno_precio:deno_precio},
            beforeSend: function () {
                //$("#prod_nombre").focus();
                $("#btn_agregar_item").attr("disabled", true);                    
                }
            }).done(function(data) {                
                table2.ajax.reload();
                
                $("#btn_agregar_item").attr("disabled", false); 
                if(data=='0')
                    danger_toast(data+' NO SE PUDO AGREGAR, VERIFIQUE EL STOCK DISPONIBLE...');
                else if(data=='5')
                    danger_toast('NO SE PUEDE AGREGAR EL PRODUCTO MAS DE UNA VEZ EN LA MISMA RECETA!!!!');
                else
                    success_toast(data+' Item Agregado OK');
                
             }); 
    });

     //************************MODAL INGRESO DETALLE MEDICAMENTO*********************
     //DESHABILITADO ANTES SE USABA AUTOCOMPLETE DE JQIRY LUEGO SE IMPLEMENTE EASYAUTOCOMPLETE
   /*  $("#prod_nombre000").autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: 'model/proc_entrega/angel.json',
            dataType: "json",
            data: {
              term: request.term,unid_id:unid_id
            },
            success: function( data ) {
              response(data);
            }
          } );
        },
        minLength: 2,
        delay: 0,
        select: function( event, ui ) {          
          var prod_id=ui.item.prod_id;
          var deno_precio=ui.item.kard_costo;
          $("#prod_id").val(prod_id);
          $("#deno_precio").val(deno_precio);
          consultar_stock(prod_id);
          $("#deno_cantidad").focus();
        }
      });
      */

      function consultar_stock(){
          var prod_id= $("#prod_id").val();
        $.getJSON(model,{funcion:'consultar_stock',unid_id:unid_id,prod_id:prod_id}, function(json){       
            $("#stock").val(parseInt(json[0].stock_disponibe)); 
            $("#deno_precio").val(json[0].kard_costo);  
            $("#deno_cantidad").focus();         
        });
      }

      
      function listado_productos_entregado(){ 
        table3=$('#tbentregados_1').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:'listado_entregados',turn_id:turn_id,unid_id:unid_id},
                dataSrc: ''
            },     
            columns: [                      
                { data: 'prod_nombre' },
                { data: 'entregado',
                "render":function(data){                   
                    return '<h4><span class="badge badge-warning"><strong>'+data+'</strong></span></h4>';                       
                } },
                { data: 'disponible',
                "render":function(data){                   
                    return '<h4><span class="badge badge-info"><strong>'+data+'</strong></span></h4>';                       
                } },                
            ],
            "iDisplayLength": 5,
            "lengthChange": true,
            responsive: true ,
            "order": [[ 0, "asc" ]],   
            dom: 'lBfrtip',
            buttons: [
            'copy', 'excel', 'pdf',
            {
                text: 'Imprimir',
                action: function ( e, dt, node, config ) {
                    window.open("model/proc_entrega/imp_entregados.php?unid="+unid_id+"&turn="+turn_id); 
                }
            }
            ]         
        });    

        }

        function listado_stocks(){ 
            table4=$('#tbstocks_1').DataTable( {
                ajax: {
                    url: model,
                    type: "GET",
                    data:{funcion:'listado_stocks',unid_id:unid_id},
                    dataSrc: ''
                },     
                columns: [                      
                    { data: 'prod_nombre' },
                    { data: 'stock',
                    "render":function(data){                   
                        return '<h4><span class="badge badge-success"><strong>'+data+'</strong></span></h4>';                       
                    } }              
                ],
                "iDisplayLength": 5,
                "lengthChange": false,                
                responsive: true ,
                "order": [[ 0, "asc" ]],   
                dom: 'lBfrtip',
                buttons: [
                'copy', 'excel', 'pdf'
                ]         
            });    
    
            }

        function listado_recetas_tabla(){ 
            table1=$('#tbreceta_1').DataTable( {
                ajax: {
                    url: model,
                    type: "GET",
                    data:{funcion:'listado_recetas',turn_id:turn_id,unid_id:unid_id},
                    dataSrc: ''
                },     
                columns: [                      
                    { data: 'noen_id' },
                    { data: 'noen_fecha'},                    
                    { data: 'noen_ci'},
                    { data: 'noen_nombre'},                             
                    { data: 'total',
                    "render":function(data){        
                            return '<span class="badge badge-info"><strong>'+data+'</strong></span>';                     
                    }},       
                    { data: 'noen_status',
                        "render":function(data){
                            var r='';
                            switch(data){
                                case '0':
                                    r='<span class="badge badge-danger">No Guardada</span>';
                                break;
                                case '1':
                                    r='<span class="badge badge-success">Guardada</span>';
                                break;                            
                                default:
                                    r='<span class="badge badge-info">OK</span>';
                            }
                            return r;
                        }
                    }, 
                    { data: 'noen_id',
                        "render": function ( data ) {                                
                        return '<button type="button" title="Eliminar" class="btn btn-secondary btn-xs m-l-5 btn_del_receta" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" title="Ver y Editar Receta" class="btn btn-secondary btn-xs m-l-5 btn_edit_receta" id="'+data+'"><i class="fa fa-pencil"></i></button>';
                        }
                    }         
                ],
                "iDisplayLength": 5,
                "lengthChange": false,
                responsive: true ,
                "order": [[ 0, "desc" ]],      
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'excel',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },
                    //{
                      //  text: 'Imprimir',
                        //action: function ( e, dt, node, config ) {
                          //  window.open("model/proc_entrega/imp_recetas.php?unid="+unid_id+"&turn="+turn_id); 
                        //}
                    //}
                ]      
            });    
    
            }
  
        //AL DAR CLIC EN RECETA EDIT ABRE MODAL Y MUESTRA LISTADO ITEMS DE RECETA SELECCIONADA
  $(document).on('click', '.btn_edit_receta', function(){        
    noen_id=$(this).attr("id"); 
    $("#stock").val('');
    $('#num_receta').html(noen_id);    
    //OBTIENE NOMBRE PACIENTE
    $.getJSON(model,{funcion:'get_paciente_receta',noen_id:noen_id}, function(json){   
        nom=json[0].noen_nombre;
        $("#titulo_receta").html(nom);
    });
    listado_items_tabla(noen_id);
    $('#ModalNewReceta').modal('show');     
    });


    //ELIMINA ITEM DE RECETA
    $(document).on('click', '.btn_item_del', function(){    
        deno_id=$(this).attr("id"); 
        swal({
            title: "Eliminar Producto ?",
            text: "Desea Elimina éste Producto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:'eliminar_item_receta',deno_id:deno_id,unid_id:unid_id,noen_id:noen_id}
                        }).done(function(data) {     
                            table1.ajax.reload();  
                            table2.ajax.reload();       
                            table3.ajax.reload();
                            table4.ajax.reload();
                            $("#stock").val('');
                            $("#prod_nombre").val('');
                            $("#prod_id").val('');     
                            swal("Notificación:", data, "info");
                            $("#prod_nombre").focus();
                         });     
                })
              }
            });    
    });

    //ELIMINA TODA LA RECETA RECETA E ITEMS
    $(document).on('click', '.btn_del_receta', function(){    
        noen_id_2=$(this).attr("id"); 
        swal({
            title: "Eliminar Receta ?",
            text: "Desea Eliminar la Receta Completa y todos sus Items?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar la toda la Receta!!",
            closeOnConfirm: false,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:'eliminar_receta',noen_id:noen_id_2,unid_id:unid_id}
                        }).done(function(data) {       
                            table1.ajax.reload();
                            table3.ajax.reload();
                            table4.ajax.reload();
                            swal("Notificación:", data, "info");
                         });    
                })
              }
            });  
    });

      /**DATATABLES LISTADO ITEMS EN MODAL */
      function listado_items_tabla(noen_id){ 
        table2=$('#tbrecetadetalle_2').DataTable();            
        table2.destroy();        
        table2.clear();
        table2=$('#tbrecetadetalle_2').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:'listado_items',turn_id:turn_id,unid_id:unid_id,noen_id:noen_id},
                dataSrc: ''
            },     
            columns: [                      
                { data: 'prod_nombre',
                "render":function(data){
                    return '<span style="white-space: nowrap;font-size:16px"><strong>'+ucwords(data.toLowerCase())+'<strong></span>';
                } },
                { data: 'deno_cantidad',
                "render":function(data){                   
                        return '<h4><span class="badge badge-primary">'+data+'</span></h4>';      
                }       },                    
                { data: 'deno_precio'},
                { data: 'deno_total'},                
                { data: 'deno_id',
                    "render": function ( data ) {                                
                    return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_item_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                    }
                }         
            ],
            "lengthChange": false,           
            "order": [[ 4, "desc" ]],        
        });            
        }

        /***GUARDAR RECETA */
        function guardar_receta(){       
            
            if(noen_id.length<1){
                danger_toast('Receta Incorrecta...!!!');
                 return false;
             }       
                        $.get(model,{funcion:'guardar_receta',noen_id:noen_id,recibi_conforme:recibi_conforme}, function(data){    
                            $('#ModalNewReceta').modal('hide'); 
                            table1.ajax.reload();
                            table3.ajax.reload();
                            table4.ajax.reload();
                            imprimir_receta();                   
                            info_toast(data);  
                            limpiar();                   

                        });
           }

        $("#btn_guardar_receta").click(function() {
            receta_recibi_conforme();
          });

          //MODELO RECETA
         function imprimir_receta(){         
            //var content = document.getElementById('printableArea').innerHTML;    
            $.getJSON(model,{funcion:'receta_cabecera',noen_id:noen_id}, function(json){       
                var mywindow = window.open('', 'Print', 'height=600,width=600');
                var content='';
                var nombre='';
                var cedula='';
                var u_nom=json[0].unid_nombre;
                if(u_nom=='Farmacia VIH HEG')
                    u_nom='COD 08';
                //CABECERA RECETA
                nombre=json[0].noen_nombre;
                cedula=json[0].noen_ci;
                rconforme=json[0].recibi_conforme;
                content+='<p>&nbsp</p>';
                content+='<p>'+json[0].hosp_nombre+'</p>';
                content+='<p>'+u_nom+'</p>';
                content+='<p>N.: '+json[0].noen_id+'</p>';
                content+='<p>Servicio: '+json[0].area_nombre+'</p>';
                content+='<p>Paciente: '+nombre+'</p>';
                content+='<p>Cedula: '+cedula+'</p>';
                content+='<p>H. Clinica: '+json[0].noen_historia_clinica+'</p>';
                content+='<p>Fecha: '+json[0].noen_fecha+'</p>';
                content+='<p>Medico: '+json[0].medi_nombre+'</p>';
                content+='<p>Responsable: '+json[0].user_nick+'</p>';
                content+='<p>Hora: '+moment().format('HH:mm:ss a')+'</p>';
                //content+='<p>Generado: '+json[0].fecha_graba+'</p>';
                content+='<p>-------------------------------------</p>';
                //DETALLE RECETA
                $.getJSON(model,{funcion:'receta_detalle',noen_id:noen_id}, function(json){  
                    var sum=0;
                    $.each(json, function (i, items) {
                        if(items !== null) {
                            sum+=Number(items["total"]);
                            content+='<p>*'+items["prod_nombre"]+'</p>';
                            content+='<p>( '+items["deno_cantidad"]+') ( '+items["deno_precio"]+' )( '+items["total"]+' )</p>';
                        }
                   });
                   content+='<p>-------------------------------------</p>';
                   content+='<p>SUBTOTAL: '+parseFloat(sum).toFixed(4)+'</p>';
                   content+='<p>IVA 12%: 0.00</p>';
                   content+='<p>SUBSIDIO GOBIERNO: -'+parseFloat(sum).toFixed(4)+'</p>';
                   content+='<p>TOTAL A PAGAR: 0.00</p><br>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';                   
                   content+='<p>&nbsp</p>';
                   content+='<p>_________________________</p>';
                   content+='<p>RECIBI CONFORME:</p>';
                   content+='<p>'+rconforme+'</p>'; 
                   content+='<p>*************************</p>'; 
                   content+='<p>PACIENTE:</p>'; 
                   content+='<p>'+nombre+'</p>';
                   content+='<p>'+cedula+'</p>';
                   content+='<p>***MEDICAMENTO GRATUITO***</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   content+='<p>&nbsp</p>';
                   mywindow.document.write(content);
                   //mywindow.document.close();
                   mywindow.focus()
                   mywindow.print();
                   mywindow.close();
                   return true;
                });
             
                    

            });
                
            
            
                  
        }

        //CERRAR TURNO
        $(" #btn_cerrar_turno").click(function() {
            swal({
                title: "Cerrar Turno ?",
                text: "Está Seguro de Cerrar éste TURNO?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Cerrar Turno !!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url:model,
                            type:"GET",            
                            data: {funcion:'cerrar_turno',turn_id:turn_id,unid_id:unid_id}
                            }).done(function(data) {       
                                table1.ajax.reload();            
                                table3.ajax.reload();     
                                table4.ajax.reload();               
                                swal("Notificación: ", data, "info");                        
                             });    
                    })
                  }
                });        
        });

        $('#area_id').select2();
        $('#medi_id').select2();     

        function file_json(){
        $.get(model,{funcion:'generar_json_file',unid_id:unid_id},function(data){
            //GENERA ARCHIVO JSON DE PRODUCTOS, PARA QUE NO HAGA PERSISTENCIA A LA BASE DE DATOS Y ASI OPTIMIZAR RECURSOS
        });
        }

/*
        var options = {
            url: "model/proc_entrega/angel.json",        
            getValue: "prod_nombre",        
            list: {
                maxNumberOfElements: 12,
                match: {
                    enabled: true
                },                
                onSelectItemEvent: function() {
                    var prod_id = $("#prod_nombre").getSelectedItemData().prod_id;                                      
                    $("#prod_id").val(prod_id);                                                          
                },
                onChooseEvent: function(){
                    consultar_stock();
                }
            }            
        };        
        $("#prod_nombre").easyAutocomplete(options);
        */

       //AUTOBUSCADOR LIVE SEARCH AJAX 

      function llenar(c1,n1,h1){
          $('#noen_ci').val(c1);
          $('#noen_nombre').val(n1);
          $('#noen_historia_clinica').val(h1);
      }     

      $('#prod_nombre').typeahead({
        displayText: function(item) {
             return item.prod_nombre
        },
        afterSelect: function(item) {
            this.$element[0].prod_nombre= item.prod_nombre;
            $("#prod_id").val(item.prod_id);
            consultar_stock();
        },
        source: function (query, process) {
          return $.getJSON('model/proc_entrega/producto.php', { query: query,unid_id:unid_id }, function(data) {
            $('#prod_id').val('');
            $('#stock').val('');
            if(data[0].prod_nombre==0)
                window.location.href = "model/config/lgout.php";
            else
                process(data);
          })
        }   
      });

     $('#noen_ci').typeahead({
                displayText: function(item) {
                     return item.nombre
                },
                afterSelect: function(item) {
                    this.$element[0].value = item.value;
                    llenar(item.value,item.pers_nombre,item.pers_historia_clinica);
                },
                source: function (query, process) {
                  return $.getJSON('model/proc_entrega/paciente.php', { query: query,opc:'C' }, function(data) {                      
                     if(data[0].pers_id==0)
                        window.location.href = "model/config/lgout.php";
                     else
                        process(data);
                  })
                }   
          });

     $('#noen_nombre').typeahead({
                displayText: function(item) {
                     return item.nombre
                },
                afterSelect: function(item) {
                    this.$element[0].value = item.pers_nombre;
                    llenar(item.value,item.pers_nombre,item.pers_historia_clinica);
                },
                source: function (query, process) {
                    return $.getJSON('model/proc_entrega/paciente.php', { query: query,opc:'P' }, function(data) {
                    if(data[0].pers_id==0)
                        window.location.href = "model/config/lgout.php";
                    else
                        process(data);
                  })
                }   
    });

      $('#noen_historia_clinica').typeahead({
                displayText: function(item) {
                     return item.pers_hc
                },
                afterSelect: function(item) {
                    this.$element[0].value = item.pers_historia_clinica;
                    llenar(item.value,item.pers_nombre,item.pers_historia_clinica);
                },
                source: function (query, process) {
                  return $.getJSON('model/proc_entrega/paciente.php', { query: query,opc:'H'}, function(data) {
                    if(data[0].pers_id==0)
                        window.location.href = "model/config/lgout.php";
                     else
                        process(data);
                  })
                }   
              });

              $(" #btn_limp_prod").click(function() {                
                  $('#prod_nombre').val('');
                  $('#stock').val('');
                  $('#prod_nombre').focus();
              });

           
        
});