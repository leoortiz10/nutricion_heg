$(document).ready(function(){

    var model='model/module_caducados/funciones.php';
    var idx;
    var accion;
    var table1;
    var unid_id=0;

    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

  if (unid_id==0) {
    unid_id=$("#unid_id").val();
    listado_caducados_tabla();
  }


    $("#unid_id").change(function() {   
        //$('.color_bg_turno').css('background-color', '#FFFFFF'); 
        table1=$('#tb_1').DataTable();   
        table1.clear(); 
        table1.destroy();         
        unid_id=$("#unid_id").val();
                
        
        listado_caducados_tabla();
        
    });

   //TABLA DATOS

   function listado_caducados_tabla(){ 

       table1=$('#tb_1').DataTable( {

        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_lotes_caduca',unid_id:unid_id},
            dataSrc: ''
        },     
        columns: [                      
            { data: 'producto',
            "render": function (data){
                return ucwords(data.toLowerCase());
            }
            },  
            { data: 'lt',
            "render":function(data){
                return '<span class="badge badge-info">'+data+'</span>';
            }
            },                    
            { data: 'lote_fech_venc'}, 
            { data: 'stock',
            "render":function(data){
                return  r='<h4><span class="badge badge-success">'+parseFloat(data).toFixed(4)+'</span><h4>';
            }
            },
            { data: 'lote_fech_venc',
            "render":function(data){
                var fecha1 =moment(data);
                var fecha2 = moment().format('YYYY-MM-DD');
                var dias= fecha1.diff(fecha2, 'days');
                var r;
                if(dias<0)
                    r='<span class="badge badge-dark">Caducado</span>';
                if(dias>=0 && dias<=60)               
                    r='<span class="badge badge-danger">Caduca en:<strong> '+dias+' </strong>dias</span>';
                if(dias>60)
                    r='<span class="badge badge-success">Estable</span>';
                return r;                       
                }
            },    
        ],
        "lengthChange": false,
        responsive: true ,
        dom: 'lBfrtip',
        "order": [[ 2, "asc" ]], 
        buttons: [
            'excel', 'pdf', 'print'
        ]       
    }); 

   }
   



   function combo_unidad(){        
        combo1='#unid_id';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();                       
            $(combo1).append('<option value="0" selected="selected">--TODAS LAS UNIDADES--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
            });
        });  
    }


    //en esta sección se declaran a las funciones

    combo_unidad();  

});