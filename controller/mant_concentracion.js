$(document).ready(function(){

    var model='model/mant_concentracion/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'conc_nombre' },                   
            { data: 'conc_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
             swal({
            title: "Eliminar ?",
            text: "¿Está seguro que desea eliminar el elemento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,            
            preConfirm: function() {
              return new Promise(function(resolve) {
                  $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {        
                        table.ajax.reload();                               
                        $(".btn_del").attr("disabled", false);   

                        if(data==0){
                            swal("Estado:", "No se pudo ELIMINAR el registro!!!", "error");
                        }else{
                          swal("Estado:", data, "success");  
                        }

                        
                     });   
              })
            }
          });
    });









        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#conc_nombre').val(json[0].conc_nombre);        
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        var nombreConcentracion = $('#conc_nombre').val();

        if(nombreConcentracion.trim() == ''){
            //swal("Debe ingresar el nombre de la concentración");
            swal("¡Debe ingresar el nombre de la concentración!","", "warning");
             $('#conc_nombre').focus();
             return false;
        }else
        {
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });

        }
    });


    

});