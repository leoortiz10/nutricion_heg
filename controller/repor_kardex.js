$(document).ready(function(){

    var model='model/reportes/generar.php';   
    var accion='repor_kardex';
    var unid_id;
    var tipo_repor;
    var ffin= $('#ffinr2'); 

    ffin.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });       
   
    ffin.datepicker('setDate', new Date());


    function combo_unidad_r1(){        
        combo1='#unid_id_r1';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();   
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function grupo(){        
        combo2='#grupo_prod_id';
        $.getJSON(model,{funcion:'get_grupo'})
        .done(function(json) {   
            $(combo2).empty();   
            $(combo2).append('<option value="0" selected="selected">--TODOS--</option>');                                
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["grupo_prod_id"]).text(items["grupo_prod_nombre"]);
			});
        });  
    }

    combo_unidad_r1();    
    grupo();
  
    function generar_html_r1(){     
        unid_id=$("#unid_id_r1").val();   
        grupo_prod_id=$("#grupo_prod_id").val();  
        f_fin= $('#ffinr2').val(); 
        opcion=$("#cmb_opcion").val(); 
         
        if(unid_id=='0'){
            danger_toast('SELECCIONE LA UNIDA!!!!');
            $('#rep_1').empty();
            return false;
        }
        
        $('#rep_1').empty();
        $('#rep_1').html('Generando Reporte...');
        $.get(model,{funcion:accion,unid_id:unid_id,opcion:opcion,f_fin:f_fin,grupo_prod_id:grupo_prod_id},function(data){$('#rep_1').html(data);});
    }

   //IMPRIMIR PDF
     $( "#btn_rp1_pdf" ).click(function() {    
        unid_id=$("#unid_id_r1").val();   
        grupo_prod_id=$("#grupo_prod_id").val();  
        f_fin= $('#ffinr2').val(); 
        opcion=$("#cmb_opcion").val(); 
         
        if(unid_id=='0'){
            danger_toast('SELECCIONE LA UNIDA!!!!');
            $('#rep_1').empty();
            return false;
        }
        window.open('model/reportes/pdf.php?f=detalle_inicial_nutricion&unid_id='+unid_id+'&grupo_prod_id='+grupo_prod_id+'&f_fin='+f_fin, '_blank');
      });

      $("#btn_genreceta").click(function() {            
        generar_html_r1();        
      });
      
      $("#btnexportar").click(function() {            
        $("#rep_1").table2excel({           
            name: "Excel_Medix"
        }); 
      });
});