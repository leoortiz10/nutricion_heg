$(document).ready(function(){
//VARIBLES***********************************************************
    var model='model/ajuste_lotes/funciones.php';   
    var accion;
    var unid_id;   
    var prod_id;
    var sum_egre=0;

    var lote_id_origen;
    var lote_codigo_origen;
    var lote_cant_origen=0;
    var lote_regi_origen;
    var lote_fech_origen;
    var kard_id_origen;

    var lote_id_destino;
    var lote_codigo_destino=0;
    //var lote_cant_destino;
    var lote_regi_destino;
    var lote_fech_destino;
    var kard_id_destino;
    
//FUNCIONES**************************************************************
    function limpiar(){
        kard_id_origen=0;
        lote_id_origen=0;
        lote_codigo_origen=0;
        lote_regi_origen=0;
        lote_fech_origen=0;
        kard_id_destino=0;
        lote_id_destino=0;

        $('#lote_movimientos').html('');
        $('#lotes_info_cruzar').empty();
        $('#lotes_info_cruzar').html('');
        $('#lotes_info').empty();
        $('#lotes_info').html('');
        $('#suma_cant').val('0');
        sum_egre=0;
    }
    function combo_unidad(){        
        combo1='#unid_id';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();                       
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
    function combo_producto(){        
        combo2='#prod_id';
        $.getJSON(model,{funcion:'get_producto',unid_id:unid_id})
        .done(function(json) {   
            $(combo2).empty();     
            $(combo2).append('<option value="0" selected="selected">--Seleccione--</option>');                              
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["prod_id"]).text(items["prod_nombre"]);
			});
        });  
    } 
    function lotes_productos(){
        $.getJSON(model,{funcion:'lotes_productos',prod_id:prod_id,unid_id:unid_id},function(json){                        
              var html='';
              html+='<form action="">';
              $.each(json, function(index, array) {                 
                 html+='<div class="form-check">';
                 html+='<input class="form-check-input radio_lote" type="radio"  name="deta_docu_lote"  can="'+array['cantidad']+'" reg="'+array['regi']+'" kid="'+array['kard_id']+'" fec="'+array['fech']+'"  id="'+array['lote_id']+'" value="'+array['lote_codigo']+'"/>';
                 html+='<label class="form-check-label"><strong>Lote:</strong> '+array['lote_codigo']+' <strong> Cantidad:</strong> '+array['cantidad']+'<strong> Fech-Cad: </strong> '+array['fech']+'<strong> RegiSani: </strong> '+array['regi']+'</label> ';
                 html+='</div>';                
             });
             html+='</form>';
            $('#lotes_info').html(html);
           
    });
    }
    function lotes_productos_cruzar(){
        $.getJSON(model,{funcion:'lotes_productos_cruzar',prod_id:prod_id,unid_id:unid_id,lote_id_origen:lote_id_origen},function(json){                        
              var html='';
              html+='<form action="">';
              $.each(json, function(index, array) {                 
                 html+='<div class="form-check-cruzar">';
                 html+='<input class="form-check-input radio_lote_cruzar" type="radio"  name="deta_docu_lote" can="'+array['can']+'"  reg="'+array['regi']+'" kid="'+array['kard_id']+'" fec="'+array['fech']+'"  id="'+array['lote_id']+'" value="'+array['lote_codigo']+'"/>';
                 html+='<label class="form-check-label"><strong>Lote:</strong> '+array['lote_codigo']+'<strong> Fech-Cad: </strong> '+array['fech']+'<strong> RegiSani: </strong> '+array['regi']+'</label> ';
                 html+='</div>';
                
             });
             html+='</form>';
            $('#lotes_info_cruzar').html(html);
    });
    }
    function procesar_cruze(){ 
        var  array_deta_kard_id=[];               
        
        //if(parseInt(sum_egre)>parseInt(lote_cant_destino)){
          //  alert('Excede la cantidad'+sum_egre+', No se Puede Continuar!!!'+lote_cant_destino);
            //return false;
        //}else{
            $("input:checkbox[name=chk_deta_kard_mov]:checked").each(function(){
                array_deta_kard_id.push($(this).val());
            });
            
            $.get(model,{funcion:'cruze_lote',array_deta_kard_id:array_deta_kard_id,lote_id_origen:lote_id_origen,lote_id_destino:lote_id_destino,lote_codigo_destino:lote_codigo_destino,prod_id:prod_id,unid_id:unid_id,lote_fech_destino:lote_fech_destino,lote_regi_destino:lote_regi_destino,lote_codigo_origen:lote_codigo_origen},function(data){
                $('#resultados').html(data);                
                limpiar();
            });
       // }
        
    }
    function lote_movimientos(){
        $.getJSON(model,{funcion:'lote_movimientos',kard_id_origen:kard_id_origen,lote_id_origen:lote_id_origen },function(json){                        
              var html='';
              html+='<form action="">';
              $.each(json, function(index, array) {                 
                 html+='<div class="form-check">';
                 html+='<label class="form-check-label">';
                 html+='<input class="form-check-input check_mov" type="checkbox" name="chk_deta_kard_mov" cane="'+array['cant_egre']+'" id="'+array['docu_id']+'" value="'+array['deta_kard_id']+'"/>';
                 html+='<strong>Fecha Mov:</strong> '+array['fecha_movi']+ '<strong>Tipo:</strong> '+array['tipo_docu_nombre']+' <strong> C/Ingreso:</strong>';
                html+='<span class="badge badge-success"> '+array['cant_ingr']+'</span>';
                 html+='<strong> C/Egreso:</strong>';
                 html+='<span class="badge badge-danger"> '+array['cant_egre']+'</span>';
                 html+='</label>';
                 html+='</div>';                
             });
             html+='</form>';
            $('#lote_movimientos').html(html);
    });
    }
   
//ACCIONES***************************************************************
      //OBTIEN ID LOTE AL DAR CLICK EN RADIO BUTON LOTES
      $(document).on('click', '.radio_lote', function(){   
        kard_id_origen=$(this).attr("kid");
        lote_id_origen=$(this).attr("id");
        //lote_cant_origen=$(this).attr("can");       
        //lote_cant_origen=lote_cant_origen.replace(',','');
        lote_codigo_origen=$(this).attr("value");
        lote_regi_origen=$(this).attr("reg");
        lote_fech_origen=$(this).attr("fec");        
        sum_egre=0;
        $('#suma_cant').val('0');
        lote_movimientos();
        lotes_productos_cruzar();
  });

   //OBTIEN ID LOTE AL DAR CLICK EN RADIO BUTON LOTES
   $(document).on('click', '.radio_lote_cruzar', function(){   
    kard_id_destino=$(this).attr("kid");
    lote_id_destino=$(this).attr("id");
    lote_cant_destino=$(this).attr("can");
    lote_cant_destino=lote_cant_destino.replace(',','');
    lote_codigo_destino=$(this).attr("value");
    lote_regi_destino=$(this).attr("reg");
    lote_fech_destino=$(this).attr("fec");            
   
});

$(document).on('click', '.check_mov', function(){       
   // cc=$(this).attr("cane");
   // cc=cc.replace(',','')
   // if ($(this).is(':checked')) 
  //      sum_egre+=parseInt(cc);
  //  else 
   //     sum_egre-=parseInt(cc);    
   // $("#suma_cant").val(sum_egre);
});
  
//EVENTOS BOTONES COMBOX*************************************************
$("#unid_id").change(function() {
    limpiar();
    unid_id=$(this).val();
    combo_producto()
});

$("#prod_id").change(function() {
    limpiar();
    prod_id=$(this).val();
    lotes_productos();    
});

 $("#btn_cruze").click(function() {            
       procesar_cruze();       
      });

//INICIALIZACION******************************************************************
      combo_unidad();  
      $('#prod_id').select2();
     
});