$(document).ready(function(){

    var model='model/mant_perfil_usuario/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'perf_nombre' },         
            { data: 'user_nombre' },           
            { data: 'perf_user_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


  
    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
             swal({
            title: "Eliminar ?",
            text: "¿Está seguro que desea eliminar el elemento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,            
            preConfirm: function() {
              return new Promise(function(resolve) {
                  $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {        
                        table.ajax.reload();                               
                        $(".btn_del").attr("disabled", false);   
                          swal("Estado:", data, "success");                  
                     });   
              })
            }
          });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#perf_id').val(json[0].perf_id);
        $('#user_id').val(json[0].user_id);              
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();

         var idPerfil = $('#perf_id').val();
         var idUser = $('#user_id').val();

if(idPerfil.trim() == 0){

    Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar el perfil!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#perf_id").focus(), 10);
            }
        });
        return false;

}else if(idUser.trim() == 0){

    Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar el usuario!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#user_id").focus(), 10);
            }
        });
        return false;

}

else
{
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
        }
    });

    
   


    
    function combo_perfil(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#perf_id';
        $.getJSON(model,{funcion:'get_perf'})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["perf_id"]).text(items["perf_nombre"]);
			});
        });  
    }

    
    function combo_usuario(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#user_id';
        $.getJSON(model,{funcion:'get_user'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["user_id"]).text(items["user_nombre"]);
			});
        });  
    }

    
    combo_perfil();
    combo_usuario();
    

});