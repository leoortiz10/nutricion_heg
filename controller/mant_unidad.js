$(document).ready(function(){

    var model='model/mant_unidad/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'unid_nombre' },         
            { data: 'hosp_nombre' },
            { data: 'receta' },
            { data: 'unid_pedido' },
            { data: 'unid_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
        swal({
                title: "Eliminar ?",
                text: "Desea Eliminar la Seleccion ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar !!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {		
                        table.ajax.reload();		                       
                        $(".btn_del").attr("disabled", false);	                        
                        swal("Estado:", data, "success");
                     });                
            });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#unid_nombre').val(json[0].unid_nombre);
        $('#nick').val(json[0].nick);
        $('#unid_siglas').val(json[0].unid_siglas);
        $('#hosp_id').val(json[0].hosp_id);
        $('#unid_agrega_receta').val(json[0].unid_agrega_receta);
        $('#unid_pedido').val(json[0].unid_pedido);        
        
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();

        var nombreUnidad = $('#unid_nombre').val();
        var nickUnidad = $('#nick').val();
        var siglasUnidad = $('#unid_siglas').val();
        var recetaUnidad = $('#unid_agrega_receta').val();
        var unidadPedido = $('#unid_pedido').val();
        var idHospital = $('#hosp_id').val();

    if(nombreUnidad.trim() == ''){
        //valida que ingrese campo en input del nombre de la unidad
     
     Swal({
            title: 'Alerta',
            text: '¡Debe ingresar el nombre de la unidad!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#unid_nombre").focus(), 10);
            }
        });
        return false;
    }else if (nickUnidad.trim() ==''){
      
           Swal({
            title: 'Alerta',
            text: '¡Debe ingresar el nick de la unidad!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#nick").focus(), 10);
            }
        });
        return false;
    } else if(siglasUnidad.trim() ==''){
     

          Swal({
            title: 'Alerta',
            text: '¡Debe ingresar las siglas de la unidad!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#unid_siglas").focus(), 10);
            }
        });
        return false;
    }else if(recetaUnidad.trim()== ''){
       
          Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar si la unidad receta o no!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#unid_agrega_receta").focus(), 10);
            }
        });

        return false;
    }else if(unidadPedido.trim()== ''){
        
           Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar si es una unidad de pedido!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#unid_pedido").focus(), 10);
            }
        });
        return false;
    }else if(idHospital.trim()==0){
      
         Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar el hospital!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#hosp_id").focus(), 10);
            }
        });
        return false;
    }
    else
    {
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
        }
    });

    
   

    function combo_hospital(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#hosp_id';
        $.getJSON(model,{funcion:'get_hosp'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["hosp_id"]).text(items["hosp_nombre"]);
			});
        });  
    }

    
    combo_hospital();
    

});