$(document).ready(function(){

    var model='model/mant_pagina/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'pagi_nombre' },         
            { data: 'pagi_url' },         
            { data: 'pagi_codigo' },         
            { data: 'pagi_raiz_nombre' },         
            { data: 'pagi_prioridad' },         
            { data: 'pagi_icono' },
            { data: 'pagi_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
        swal({
                title: "Eliminar ?",
                text: "Desea Eliminar la Seleccion ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar !!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {		
                        table.ajax.reload();		                       
                        $(".btn_del").attr("disabled", false);	                        
                        swal("Estado:", data, "success");
                     });                
            });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#pagi_nombre').val(json[0].pagi_nombre);
        $('#pagi_url').val(json[0].pagi_url);
        $('#pagi_codigo').val(json[0].pagi_codigo);
        $('#pagi_raiz').val(json[0].pagi_raiz);
        $('#pagi_prioridad').val(json[0].pagi_prioridad);
        $('#pagi_icono').val(json[0].pagi_icono);
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
    });

    
   

    function combo_pagina(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#pagi_raiz';
        $.getJSON(model,{funcion:'get_pagina_raiz'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["pagi_codigo"]).text(items["pagi_nombre"]);
			});
        });  
    }

    
    combo_pagina();
    

});