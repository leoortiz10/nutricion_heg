$(document).ready(function(){

    var model='model/reportes/generar.php';   
    var accion='repor_lote_detalle';
    var unid_id;   

    function combo_unidad_r4(){        
        combo1='#unid_id_r4';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();                       
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
    function grupo(){        
        combo2='#grupo_prod_id';
        $.getJSON(model,{funcion:'get_grupo'})
        .done(function(json) {   
            $(combo2).empty();                                   
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["grupo_prod_id"]).text(items["grupo_prod_nombre"]);
			});
        });  
    }    
   
    combo_unidad_r4();    
    grupo();
  
    $("#unid_id_r4").change(function() {             
        generar_html_r4();
    });
  
    function generar_html_r4(){        
        unid_id=$("#unid_id_r4").val();
        grupo_prod_id=$("#grupo_prod_id").val();
        if(unid_id=='0'){
            $('#rep_4').empty();
            return false;
        }
        
        $('#rep_4').empty();
        $('#rep_4').html('Generando Reporte...');
        $.get(model,{funcion:accion,unid_id:unid_id,grupo_prod_id:grupo_prod_id},function(data){$('#rep_4').html(data);});
    }

     //IMPRIMIR PDF
     $( "#btn_rp4_pdf" ).click(function() {    
        //unid_id=$("#unid_id_r1").val();      
        //window.open('model/reportes/pdf.php?f='+accion+'&unid_id='+unid_id, '_blank');
      });

      $("#btnexportar").click(function() {            
        $("#rep_4").table2excel({           
            name: "Excel_Medix"
        }); 
      });
     
});