$(document).ready(function(){

    var model='model/mant_usuario_unidad/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'user_nombre' },         
            { data: 'unid_nombre' },           
            { data: 'user_unid_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


        $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
             swal({
            title: "Eliminar ?",
            text: "¿Está seguro que desea eliminar el elemento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,            
            preConfirm: function() {
              return new Promise(function(resolve) {
                  $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {        
                        table.ajax.reload();                               
                        $(".btn_del").attr("disabled", false);   

                          swal("Estado:", data, "success");  
                                                
                     });   
              })
            }
          });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#user_id').val(json[0].user_id);
        $('#unid_id_un').val(json[0].unid_id);              
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();

        var idUsuario = $('#user_id').val();
        var idUnidad = $('#unid_id_un').val();

         if(idUsuario.trim() == 0){

            Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar el usuario!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#user_id").focus(), 10);
            }
        });
        return false;

         }else if(idUnidad.trim() == 0){

            Swal({
            title: 'Alerta',
            text: '¡Debe seleccionar la unidad!',
            type: 'warning',
            confirmButtonText: 'OK',
            onAfterClose: () => {
                setTimeout(() => $("#unid_id_un").focus(), 10);
            }
        });
        return false;

         }
         else{

        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
        }
    });
   
    function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#unid_id_un';
        $.getJSON(model,{funcion:'get_unid'})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["unid_id"]).text(items["unid_nombre"]);
			});
        });  
    }

    
    function combo_usuario(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#user_id';
        $.getJSON(model,{funcion:'get_user'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["user_id"]).text(items["user_nombre"]);
			});
        });  
    }

    
    combo_unidad();
    combo_usuario();
    

});