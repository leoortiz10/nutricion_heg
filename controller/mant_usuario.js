$(document).ready(function(){
   
    var model='model/mant_usuario/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'user_nombre' },
            { data: 'user_correo' },
            //{ data: 'user_clave' },            
            { data: 'stat_nombre' },
            { data: 'user_nick' },
            { data: 'unid_nombre' },            
            { data: 'area_nombre' },
            { data: 'user_id' ,
                "render": function ( data ) {                                
                //return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_update_pass" id="'+data+'"><i class="fa fa-key"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
				return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_update_pass" id="'+data+'"><i class="fa fa-key"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form3').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalUpdate').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });
	
	    $(document).on('click', '.btn_update_pass', function(){ 
        $('#form2').trigger("reset");
        accion='editarPassword';
        idx=$(this).attr("id");
        $('#ModalNewUpdate').modal('show'); 
        $('.modal-title').text("Password");
        editarPass(idx);
    });
	
	     function editarPass(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id_password'})
        .done(function(json) {
     
        $('#user_clave_2').val(json[0].user_clave);
    
        });       
    }


    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
        swal({
                title: "Eliminar ?",
                text: "Desea Eliminar la Seleccion ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar !!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {		
                        table.ajax.reload();		                       
                        $(".btn_del").attr("disabled", false);	                        
                        swal("Estado:", data, "success");
                     });                
            });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#user_nombre_up').val(json[0].user_nombre);
        $('#user_correo_up').val(json[0].user_correo);
        //$('#user_clave_up').val(json[0].user_clave);
        $('#stat_id_up').val(json[0].stat_id);        
        $('#user_nick_up').val(json[0].user_nick);
        $('#unid_id_up').val(json[0].unid_id);
        $('#hosp_id_up').val(json[0].hosp_id);
        $('#area_id_up').val(json[0].area_id);
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data_us=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data_us},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
    });
	
	    $('#form2').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data_us=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data_us},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNewUpdate').modal('hide'); 	                
                success_toast(data);
               
             });
    });
	
		    $('#form3').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data_us=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data_us},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalUpdate').modal('hide'); 	                
                success_toast(data);
               
             });
    });
	
	

    function combo_unidad_usuario(){
        //LLENO COMBOBOX POR DATOS JSON
        combo1='#unid_id_us';
        $.getJSON(model,{funcion:'get_unidad_usuario'})
        .done(function(json) {   
            $(combo1).empty();   
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["unid_nombre"]);
			});
        });  
    }
	
	    function combo_unidad_usuario_up(){
        //LLENO COMBOBOX POR DATOS JSON
        combo1Up='#unid_id_up';
        $.getJSON(model,{funcion:'get_unidad_usuario'})
        .done(function(json) {   
            $(combo1Up).empty();   
            $(combo1Up).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1Up).val(items["unid_id"]).text(items["unid_nombre"]);
			});
        });  
    }

    function combo_area_usuario(){
        //LLENO COMBOBOX POR DATOS JSON
        combo2='#area_id';
        $.getJSON(model,{funcion:'get_area_usuario'})
        .done(function(json) {   
            $(combo2).empty();   
            $(combo2).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["area_id"]).text(items["area_nombre"]);
			});
        });  
    }
	    function combo_area_usuario_up(){
        //LLENO COMBOBOX POR DATOS JSON
        combo2Up='#area_id_up';
        $.getJSON(model,{funcion:'get_area_usuario'})
        .done(function(json) {   
            $(combo2Up).empty();   
            $(combo2Up).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2Up).val(items["area_id"]).text(items["area_nombre"]);
			});
        });  
    }

    function combo_hosp(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#hosp_id';
        $.getJSON(model,{funcion:'get_hosp'})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["hosp_id"]).text(items["hosp_nombre"]);
			});
        });  
    }
	
	    function combo_hosp_up(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3Up='#hosp_id_up';
        $.getJSON(model,{funcion:'get_hosp'})
        .done(function(json) {   
            $(combo3Up).empty();   
            $(combo3Up).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3Up).val(items["hosp_id"]).text(items["hosp_nombre"]);
			});
        });  
    }

	
    function combo_status(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#stat_id';
        $.getJSON(model,{funcion:'get_stat'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["stat_id"]).text(items["stat_nombre"]);
			});
        });  
    }
	
	    function combo_status_up(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4Up='#stat_id_up';
        $.getJSON(model,{funcion:'get_stat'})
        .done(function(json) {   
            $(combo4Up).empty();   
            $(combo4Up).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4Up).val(items["stat_id"]).text(items["stat_nombre"]);
			});
        });  
    }


    combo_area_usuario();
	combo_area_usuario_up();
    combo_hosp();
	combo_hosp_up();
    combo_status();
	combo_status_up();
    combo_unidad_usuario();
	combo_unidad_usuario_up();

});