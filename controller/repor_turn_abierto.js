$(document).ready(function(){

    var model='model/reportes/generar.php';   
    var accion='repor_turn_abierto';
    var unid_id;
  
    function combo_unidad_r3(){        
        combo1='#unid_id_r3';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();           
            $(combo1).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
   
    combo_unidad_r3();    
  
    $("#unid_id_r3").change(function() {             
        generar_html_r3();
    });
  
    function generar_html_r3(){        
        unid_id=$("#unid_id_r3").val();    
       
        if(unid_id=='0'){
            $('#rep_3').empty();
            return false;
        }
        
        $('#rep_3').empty();
        $('#rep_3').html('Generando Reporte...');
        $.get(model,{funcion:accion,unid_id:unid_id},function(data){$('#rep_3').html(data);});
    }

     //IMPRIMIR PDF
     $( "#btn_rp3_pdf" ).click(function() {    
        //unid_id=$("#unid_id_r1").val();      
        //window.open('model/reportes/pdf.php?f='+accion+'&unid_id='+unid_id, '_blank');
      });
     
});