$(document).ready(function(){

    var model='model/kardex_pro/funciones.php';
   
    var idx;
    var accion;
    var table3;
    var unid_id;
    var unid_id_destino;   
    var tipo_docu_id=19; //SOLO PEDIDO
  
     //DOCUMENTO PEDIDOS
     var tipo='P';  
   
    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }
   
   //TABLA DATOS
   function transfer(){ 
    unid_id=$('#unid_id_s').val();
    unid_id_destino=$('#unid_id_destino_s').val();
    table3=$('#tb_3_ped').DataTable( {        
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo,unid_id_destino:unid_id_destino,recibe:1},
            dataSrc: ''
        },             
        columns: [                                 
            { data: 'docu_numero' },
            { data: 'unid_nombre' ,
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }  },
            { data: 'unid_destino',
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }   },
            { data: 'docu_fecha' },   
            { data: 'docu_autoriza',
            "render":function(data){
                return ucwords(data.toLowerCase());
            }  },   
            { data: 'docu_entrega' ,
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },                        
            { data: 'stat_docu_nombre',
                "render":function(data){
                    var r;
                    switch(data){
                        case 'No Procesado':
                            r='<span class="badge badge-danger">No Enviado</span>';
                        break;
                        case 'Procesado':
                            r='<span class="badge badge-success">Enviado</span>';
                        break;
                        case 'Anulado':
                            r='<span class="badge badge-warning">'+data+'</span>';
                        break;
                        case 'Borrado':
                            r='<span class="badge badge-dark">'+data+'</span>';
                        break;
                        default:
                            r='<span class="badge badge-warning">'+data+'</span>';                        
                    }
                    return r;
                }
            },
            { data: 'stat_docu_id_destino',
            "render":function(data){
                var r;
                switch(data){
                    case '0':
                        r='<span class="badge badge-danger">No Revisado</span>';
                    break;
                    case '1':
                        r='<span class="badge badge-success">Revisado</span>';
                    break;
                    case '2':
                        r='<span class="badge badge-warning">Anulado</span>';
                    break;
                    case '3':
                        r='<span class="badge badge-dark">Borrado</span>';
                    break;
                    default:
                        r='<span class="badge badge-warning">'+data+'</span>';                        
                }
                return r;
            }
        },
            { data: 'docu_id' ,
                "render": function ( data ) {                                              
                return '<button type="button" title="Detalles" class="btn btn-secondary btn-xs m-l-5 btn_detalle" id="'+data+'"><i class="fa fa-plus"></i></button><button type="button" title="Observaciones" class="btn btn-secondary btn-xs m-l-5 btn_obs" id="'+data+'"><i class="fa fa-bullhorn"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_ped" id="'+data+'"><i class="fa fa-print"></i></button><button type="button" title="Revisar" class="btn btn-secondary btn-xs m-l-5 btn_procesar_docu" id="'+data+'"><i class="fa fa-check"></i></button>';
                }
            } 
            
            
        ],
        "order": [[ 8, "desc" ]],
        responsive: true 
             
    });    
    }

     //IMPRIMIR PDF
     $(document).on('click', '.btn_print_ped', function(){
        idx=$(this).attr("id");  
        window.open('model/reportes/pdf.php?f=repor_ingresos&id='+idx, '_blank');
    });
  

    $(document).on('click', '.btn_obs', function(){  
        idx=$(this).attr("id");  
        $.getJSON(model,{id:idx,funcion:'get_obs_doc'})
        .done(function(json) {        
         swal(json[0].obs)
        });   
    });

    $(document).on('click', '.btn_detalle', function(){   
        idx=$(this).attr("id");         

        $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {
            $('#mtitulo1p').html(' *# '+json[0].docu_numero);      
            $('#mtitulo2p').html(' *PEDIDO A: '+json[0].unid_nombre);
            $('#mtitulo3p').html(' *'+json[0].tipo_docu_nombre);

            //OBTIENE ID UNIDAD
            unid_id=json[0].unid_id; 
        
        $('#ModalDetallePedido').modal('show'); 
        table4 = $('#tb_4_ped_ver').DataTable();
        table4.destroy();
        $('#tb_4_ped_ver').show();              
        accion='detalle_ingreso';        
        detalle_ingreso_e_transf(idx);    
        });
    });

    function detalle_ingreso_e_transf(idx){
         table4=$('#tb_4_ped_ver').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,id:idx},
                dataSrc: ''
            },     
            columns: [                          
                { data: 'prod_nombre',
                    "render":function(data){
                        return '<h4><span style="white-space: nowrap;font-size:13px"><strong>'+ucwords(data.toLowerCase())+'<strong></span></h4>';
                    }
                },              
                { data: 'deta_docu_cantidad',
                "render":function(data){
                    return '<h4><span class="badge badge-success"><strong>'+data+'<strong></span></h4>';
                }}                      
            ],
            responsive: true,
            dom: 'lBfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]        
        });    
    }

        $('#ModalDetallePedido').on('shown.bs.modal',function(event){
        $(document).off('focusin.modal');//PARA QUE AL MOMENTO DE ABRIR SWAL IMPUT LE DE FOCO A SWAL
       
    });

  function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#unid_id_s';
        $.getJSON(model,{funcion:'get_unidad_pedido'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Todas--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_destino(){
        //LLENO COMBOBOX POR DATOS JSON
        combo33='#unid_id_destino_s';
        $.getJSON(model,{funcion:'get_unidad_todos',tipo:tipo})
        .done(function(json) {   
            $(combo33).empty();   
            $(combo33).append('<option value="0" selected="selected">--Todos--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo33).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    combo_unidad();
    combo_unidad_destino();
   

    $("#unid_id_s").change(function() {
        table3.destroy();      
        transfer();
      });

      $("#unid_id_destino_s").change(function() {
        table3.destroy();      
        transfer();
      });

      transfer();//CARGA INICIAL

       //PROCESAR
     $(document).on('click', '.btn_procesar_docu', function(){  
        accion='revisado_documento_pedido';
        //SI RECIBE ES 0 PROCESO NORAML
        //SI RECIBE ES 1 PROCESO REVERSAR
        recibe=0;
        idx=$(this).attr("id");  
        swal({
            title: "Pedido",
            text: "Marcar como Revisado/Leido ?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Si",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx,recibe:recibe}
                        }).done(function(data) {       
                            table3.ajax.reload();            
                            swal("Notificación:", data, "info");
                         });       
                })
              }
            });
    });

        

     

});