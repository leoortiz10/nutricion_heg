$(document).ready(function(){

    var model='model/mant_despachos_usuario/funciones.php';   
    var model_print='model/proc_entrega/funciones.php';   
    var accion='reporte_despachos';
    var unid_id;
    var area_id;
    var turn_id;
    var user_id;
    var noen_id;
    var prod_id;
    var fini= $('#finir3');
    var ffin= $('#ffinr3'); 
    var tabturn=$('#tb_tt1').DataTable({});
    fini.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
    ffin.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });       

    fini.datepicker('setDate', new Date());
    ffin.datepicker('setDate', new Date());

    function combo_unidad_r3(){        
        combo1='#unid_id_r3';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();           
            $(combo1).append('<option value="0" selected="selected">---SELECCIONE---</option>');;
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
    function combo_turno_r3(){        
        combo2='#turn_id_r3';
        $.getJSON(model,{funcion:'get_turno',unid_id:unid_id})
        .done(function(json) {   
            $(combo2).empty();               
            $(combo2).append('<option value="0" selected="selected">---TODOS---</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["turn_id"]).text(items["turn_nombre"]);
			});
        });  
    }
    function combo_user_r3(){        
        combo3='#user_id_r3';
        $.getJSON(model,{funcion:'get_user'})
        .done(function(json) {   
            $(combo3).empty();               
            $(combo3).append('<option value="0" selected="selected">---TODOS---</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["user_id"]).text(items["user_nick"]);
			});
        });  
    }
    function combo_area_r3(){        
        combo4='#area_id_r3';
        $.getJSON(model,{funcion:'get_area_despacho',unid_id:unid_id})
        .done(function(json) {   
            $(combo4).empty();               
            $(combo4).append('<option value="0" selected="selected">---TODOS---</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["area_id"]).text(items["area_nombre"]);
			});
        });  
    }
    function combo_producto(){        
        combo5='#prod_id';
        $.getJSON(model,{funcion:'get_prod_despacho',unid_id:unid_id})
        .done(function(json) {   
            $(combo5).empty();               
            $(combo5).append('<option value="0" selected="selected">---TODOS---</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo5).val(items["prod_id"]).text(items["prod_nombre"]);
			});
        });  
    }
    combo_unidad_r3();     
    combo_user_r3();
    

    $("#unid_id_r3").change(function() {
        unid_id=$("#unid_id_r3").val();
        combo_turno_r3();
        combo_area_r3();
        combo_producto();
      });
   
    function generar_(){          
        tabturn.destroy();           
        unid_id=$("#unid_id_r3").val();    
        area_id=$("#area_id_r3").val();    
        turn_id=$("#turn_id_r3").val();    
        user_id=$("#user_id_r3").val();
        prod_id=$("#prod_id").val();
        f_ini= $('#finir3').val();
        f_fin= $('#ffinr3').val(); 
     
        tabturn=$('#tb_tt1').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,turn_id:turn_id,unid_id:unid_id,user_id:user_id,f_ini:f_ini,f_fin:f_fin,area_id:area_id,prod_id:prod_id},
                dataSrc: ''
            },     
            columns: [           
                { data: 'nick' ,
                "render":function(data){
                    return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
                } },         
                { data: 'user_nick' },
                { data: 'area_nombre' },
                { data: 'turn_nombre' },    
                { data: 'noen_id' },
                { data: 'noen_fecha' },     
                { data: 'noen_ci' },    
                { data: 'noen_historia_clinica' },
                { data: 'noen_nombre' },    
                { data: 'total',
                "render":function(data){                                          
                            return '<span class="badge badge-info"><strong>'+data+'</strong></span>';
                } },    
                { data: 'noen_status',
                "render":function(data){
                    var r;
                    switch(data){
                        case '0':
                            r='<span class="badge badge-danger">No Guardado</span>';
                        break;
                        case '1':
                            r='<span class="badge badge-primary">Guardado</span>';
                        break;
                        case '2':
                            r='<span class="badge badge-success">Cerrado Turno</span>';
                        break; 
                        case '3':
                          r='<span class="badge badge-warning">Anulado/Eliminado</span>';
                        break;                       
                        default:
                            r='<span class="badge badge-warning">'+data+'</span>';                        
                    }
                    return r;
                } },    
                { data: 'fecha_graba' },    
                { data: 'noen_id' ,
                    "render": function ( data ) {                                
                    return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_ver" id="'+data+'"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_print_rc" id="'+data+'"><i class="fa fa-print"></i></button>';
                    }
                }         
            ],
            responsive: true,
            dom: 'lBfrtip',
            buttons: [
                'copy', 'excel', 'print',
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    text: 'Imprimir',
                    action: function ( e, dt, node, config ) {
                        window.open("model/proc_entrega/imp_recetas.php?unid="+unid_id+"&turn="+turn_id+"&fini="+f_ini+"&ffin="+f_fin+"&area="+area_id+"&user="+user_id); 
                    }
                }
            ]      
        }); 

    }

     //IMPRIMIR PDF
     $( "#btn_rp2_pdf" ).click(function() {    
        //unid_id=$("#unid_id_r1").val();      
        //window.open('model/reportes/pdf.php?f='+accion+'&unid_id='+unid_id, '_blank');
      });
     
      $("#btn_gen3").click(function() { 
        
        if($("#unid_id_r3").val()=='0'){
            warning_toast('Seleccione una Unidad !!!');
            return false;
        }
        generar_();        
      });


      $(document).on('click', '.btn_ver', function(){ 
        noen_id=$(this).attr("id"); 
        $('#det_des_receta').empty();
        $('#det_des_receta').html('');
        $('#ModalNewDetDes').modal('show');          
      });

      
    $('#ModalNewDetDes').on('shown.bs.modal', function () { 
      
     $.get(model,{funcion:'detalle_despacho_receta',noen_id:noen_id},function(dat){        
        $('#det_des_receta').html(dat);
      });

    recibi_conforme="";  
    $("#prod_nombre").val('');                     
    $("#prod_nombre").focus();                  
});

      $(document).on('click', '.btn_print_rc', function(){
          
        noen_id=$(this).attr("id");  
        imprimir_receta_rc();
    });
    $('#area_id_r3').select2();
    $('#user_id_r3').select2();
    $('#prod_id').select2();

    function imprimir_receta_rc(){                 
        //var content = document.getElementById('printableArea').innerHTML;    
        $.getJSON(model_print,{funcion:'receta_cabecera',noen_id:noen_id}, function(json){       
            var mywindow = window.open('', 'Print', 'height=500,width=600');
            var content='';
            var nombre='';
            var cedula='';
            //CABECERA RECETA
            nombre=json[0].noen_nombre;
            cedula=json[0].noen_ci;
            rconforme=json[0].recibi_conforme;
            content+='<p>&nbsp</p>';
            content+='<p>'+json[0].hosp_nombre+'</p>';
            content+='<p>'+json[0].unid_nombre+'</p>';
            content+='<p>N.: '+json[0].noen_id+'</p>';
            content+='<p>Servicio: '+json[0].area_nombre+'</p>';
            content+='<p>Paciente: '+nombre+'</p>';
            content+='<p>Cedula: '+cedula+'</p>';
            content+='<p>H. Clinica: '+json[0].noen_historia_clinica+'</p>';
            content+='<p>Fecha: '+json[0].noen_fecha+'</p>';
            content+='<p>Medico: '+json[0].medi_nombre+'</p>';
            content+='<p>Responsable: '+json[0].user_nick+'</p>';
            content+='<p>-------------------------------------</p>';
            //DETALLE RECETA
            $.getJSON(model_print,{funcion:'receta_detalle',noen_id:noen_id}, function(json){  
                var sum=0;
                $.each(json, function (i, items) {
                    if(items !== null) {
                        sum+=Number(items["total"]);
                        content+='<p>*'+items["prod_nombre"]+'</p>';
                        content+='<p>( '+items["deno_cantidad"]+') ( '+items["deno_precio"]+' )( '+items["total"]+' )</p>';
                    }
               });
               content+='<p>-------------------------------------</p>';
               content+='<p>SUBTOTAL: '+parseFloat(sum).toFixed(4)+'</p>';
               content+='<p>IVA 12%: 0.00</p>';
               content+='<p>SUBSIDIO GOBIERNO: -'+parseFloat(sum).toFixed(4)+'</p>';
               content+='<p>TOTAL A PAGAR: 0.00</p><br>';
               content+='<p>&nbsp</p>';
               content+='<p>&nbsp</p>';                   
               content+='<p>&nbsp</p>';
               content+='<p>_________________________</p>';
               content+='<p>RECIBI CONFORME:</p>';
               content+='<p>'+rconforme+'</p>'; 
               content+='<p>*************************</p>'; 
               content+='<p>PACIENTE:</p>'; 
               content+='<p>'+nombre+'</p>';
               content+='<p>'+cedula+'</p>';
               content+='<p>***MEDICAMENTO GRATUITO***</p>';
               content+='<p>&nbsp</p>';
               mywindow.document.write(content);
               mywindow.document.close();
               mywindow.focus()
               mywindow.print();
               mywindow.close();
               return true;
            });
         
                

        });
            
        
        
              
    }

});