$(document).ready(function(){

    var model='model/mant_hospital/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'hosp_nombre' },
            { data: 'hosp_direccion' },         
            { data: 'hosp_telefono' },
            { data: 'hosp_ruc' },
            { data: 'hosp_tipo' },
            { data: 'hosp_canton' },
            { data: 'hosp_parroquia' },
            { data: 'hosp_zona' },
            { data: 'hosp_distrito' },
            { data: 'hosp_circuito' },
            { data: 'hosp_tipo_unidad' },
            { data: 'hosp_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
        swal({
                title: "Eliminar ?",
                text: "Desea Eliminar la Seleccion ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar !!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {		
                        table.ajax.reload();		                       
                        $(".btn_del").attr("disabled", false);	                        
                        swal("Estado:", data, "success");
                     });                
            });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#hosp_nombre').val(json[0].hosp_nombre);
        $('#hosp_direccion').val(json[0].hosp_direccion);
        $('#hosp_telefono').val(json[0].hosp_telefono);
        $('#hosp_ruc').val(json[0].hosp_ruc);        
        $('#hosp_tipo').val(json[0].hosp_tipo);
        $('#hosp_canton').val(json[0].hosp_canton);
        $('#hosp_parroquia').val(json[0].hosp_parroquia);
        $('#hosp_zona').val(json[0].hosp_zona);
        $('#hosp_distrito').val(json[0].hosp_distrito);
        $('#hosp_circuito').val(json[0].hosp_circuito);
        $('#hosp_tipo_unidad').val(json[0].hosp_tipo_unidad);
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
    });


    
});