$(document).ready(function(){

    var model='model/reportes/generar.php';   
    var accion='reporte_receta';
    var unid_id;
    var area_id;
    var user_id;
    var fini= $('#finir2');
    var ffin= $('#ffinr2'); 
    fini.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
    ffin.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });       

    fini.datepicker('setDate', new Date());
    ffin.datepicker('setDate', new Date());

    function combo_unidad_r2(){        
        combo1='#unid_id_r2';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo1).empty();           
            $(combo1).append('<option value="0" selected="selected">---TODAS---</option>');;
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo1).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
    function combo_area_r2(){        
        combo2='#area_id_r2';
        $.getJSON(model,{funcion:'get_area'})
        .done(function(json) {   
            $(combo2).empty();               
            $(combo2).append('<option value="0" selected="selected">---TODAS---</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo2).val(items["area_id"]).text(items["area_nombre"]);
			});
        });  
    }
    function combo_user_r2(){        
        combo3='#user_id_r2';
        $.getJSON(model,{funcion:'get_user'})
        .done(function(json) {   
            $(combo3).empty();               
            $(combo3).append('<option value="0" selected="selected">---TODOS---</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["user_id"]).text(items["user_nick"]);
			});
        });  
    }
    combo_unidad_r2();    
    combo_area_r2();
    combo_user_r2();
   
    function generar_html_r2(){        
        unid_id=$("#unid_id_r2").val();    
        area_id=$("#area_id_r2").val();    
        user_id=$("#user_id_r2").val();
        f_ini= $('#finir2').val();
        f_fin= $('#ffinr2').val(); 
        
        $('#rep_2').empty();
        $('#rep_2').html('Generando Reporte...');
        $.get(model,{funcion:accion,area_id:area_id,unid_id:unid_id,user_id:user_id,f_ini:f_ini,f_fin:f_fin},function(data){$('#rep_2').html(data);});
    }

     //IMPRIMIR PDF
     $( "#btn_rp2_pdf" ).click(function() {    
        //unid_id=$("#unid_id_r1").val();      
        //window.open('model/reportes/pdf.php?f='+accion+'&unid_id='+unid_id, '_blank');
      });
     
      $("#btn_genreceta").click(function() {            
        generar_html_r2();        
      });

      $("#btnexportar").click(function() {            
        $("#rep_2").table2excel({           
            name: "Excel_Medix"
        }); 
      });

});