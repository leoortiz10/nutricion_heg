$(document).ready(function(){

    var model='model/mant_lote/funciones.php';
    var idx;
    var accion;

    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [  
            { data: 'producto',
            "render": function (data){
                return ucwords(data.toLowerCase());
            }
            },            
            { data: 'lote_codigo' },
            { data: 'lote_regi_sani' },
            { data: 'lote_fech_venc'},
        
            
                { data: 'lote_fech_venc',
            "render":function(data){
                var fecha1 =moment(data);
                var fecha2 = moment().format('YYYY-MM-DD');
                var dias= fecha1.diff(fecha2, 'days');
                var r;
                if(dias<0)
                    r='<span class="badge badge-dark">Caducado</span>';
                if(dias>=0 && dias<=60)               
                    r='<span class="badge badge-danger">Caduca en:<strong> '+dias+' </strong>dias</span>';
                if(dias>60)
                    r='<span class="badge badge-success">Estable</span>';
                return r;   
                
            }
         }
          //  { data: 'lote_id' ,
            //    "render": function ( data ) {                                
              //  return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button>';
                //}
            //}     
        ],
        responsive: true          
    });    

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });

           function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#producto').html(ucwords(json[0].producto.toLowerCase()));
        $('#lote_codigo').val(json[0].lote_codigo);
        $('#lote_fech_venc').val(json[0].lote_fech_venc);
        $('#lote_regi_sani').val(json[0].lote_regi_sani);
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
    });

});