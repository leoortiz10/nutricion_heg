$(document).ready(function(){

    var model='model/kardex_pro/funciones.php';
    var model_imprimir='model/reportes/generar.php';
    var model_pdf='model/reportes/pdf.php';
    var idx;
    var accion;
    var table;
    var table2;
    var unid_id;
    var tipo_docu_id;
    var fecha = $('#docu_fecha');  
    var fecha2 = $('#docu_prov_fech');  
    var fecha3 = $('#deta_docu_fech_venc');  
    var propio=0;
    var id_det;

    //DOCUMENTO INGRESOS
    var tipo='I';

    fecha.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
    fecha2.datepicker({
        format: 'yyyy-mm-dd',
        startDate: "-5d",
        autoclose: true,
        todayHighlight: true
    });
   
    fecha3.datepicker({
        format: 'yyyy-mm-dd',        
        startDate: "+1d",
        todayHighlight: true,       
        autoclose: true


    });
        
    $('#tb_4').hide();    
  
    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

    function docu_numero_i(){
        $.get(model,{funcion:'docu_numero',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo},function(data){$('#docu_numero').val(data);});
    }

  function numero_lote(){
        $.get(model,{funcion:'count_lote'},function(data){$('#deta_docu_lote').val(data);});
    }
 function registro_sanitario(){
        $.get(model,{funcion:'registro_sanitario'},function(data){$('#deta_docu_regi_sani').val(data);});
    }

    $("#unid_id").change(function() {
        unid_id=$("#unid_id").val();
       docu_numero_i();

    });

    $("#tipo_docu_id").change(function() {   
        tipo_docu_id=$("#tipo_docu_id").val();
        docu_numero_i();
     });

    //Para permitir input solo de números


    /*$("#deta_docu_cantidad").keydown(function(event) {
       if(event.shiftKey)
       {
            event.preventDefault();
       }

       if (event.keyCode == 46 || event.keyCode == 8)    {
       }
       else {
            if (event.keyCode < 95) {
              if (event.keyCode < 48 || event.keyCode > 57) {
                    event.preventDefault();
              }
            } 
            else {
                  if (event.keyCode < 96 || event.keyCode > 105) {
                      event.preventDefault();
                  }
            }
          }
       });*/

         /*       $("#deta_docu_cantidad").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
       */

         /*$("#deta_docu_valo_unit").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });*/

        $("#deta_docu_cantidad").on("keypress keyup blur",function (event) {
        match = (/(\d{0,9})[^.]*((?:\.\d{0,6})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
       this.value = match[1] + match[2];
                 //alert(match);
        });



               $("#deta_docu_valo_unit").on("keypress keyup blur",function (event) {
                match = (/(\d{0,9})[^.]*((?:\.\d{0,6})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
                 this.value = match[1] + match[2];
                 //alert(match);
        });




   //TABLA DATOS
   function ingreso(){ 
    unid_id=$('#unid_id_s').val();
    tipo_docu_id=$('#tipo_docu_id_s').val();
    table=$('#tb_3').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo},
            dataSrc: ''
        },     
        columns: [                      
            { data: 'tipo_docu_nombre',
            "render":function(data){
                return '<strong>'+data+'</strong>';
            }    
        },
            { data: 'docu_numero' },
            { data: 'unid_nombre' ,
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }  },
            { data: 'docu_fecha' },
            { data: 'prov_nombre',
                "render":function(data){
                    return ucwords(data.toLowerCase());
                }
            },
            { data: 'docu_prov_fech' },
            { data: 'docu_factura' },            
            { data: 'docu_recibe',
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },            
            { data: 'stat_docu_nombre',
                "render":function(data){
                    var r;
                    switch(data){
                        case 'No Procesado':
                            r='<span class="badge badge-danger">'+data+'</span>';
                        break;
                        case 'Procesado':
                            r='<span class="badge badge-success">'+data+'</span>';
                        break;
                        case 'Anulado':
                            r='<span class="badge badge-warning">'+data+'</span>';
                        break;
                        case 'Borrado':
                            r='<span class="badge badge-dark">'+data+'</span>';
                        break;
                        default:
                            r='<span class="badge badge-warning">'+data+'</span>';                        
                    }
                    return r;
                }
            },
            { data: 'docu_id' ,
                "render": function ( data ) {                                              
                return '<button type="button" title="Detalles" class="btn btn-secondary btn-xs m-l-5 btn_detalle" id="'+data+'"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" title="Editar" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" title="Observaciones" class="btn btn-secondary btn-xs m-l-5 btn_obs" id="'+data+'"><i class="fa fa-bullhorn"></i></button><button type="button" title="Eliminar/Anular" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_ingreso" id="'+data+'"><i class="fa fa-print"></i></button><button type="button" title="Procesar" class="btn btn-secondary btn-xs m-l-5 btn_procesar_docu" id="'+data+'"><i class="fa fa-check"></i></button>';
                }
            } 
            
            
        ],
        "order": [[ 9, "desc" ]],
        responsive: true          
    });    
    }

    //IMPRIMIR PDF
    $(document).on('click', '.btn_print_ingreso', function(){
        idx=$(this).attr("id");  
        window.open('model/reportes/pdf.php?f=repor_ingresos&id='+idx, '_blank');
    });



    $(document).on('click', '.btn_edit', function(){  
        
        accion='editar';
        idx=$(this).attr("id");  


                   $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {
           
            if(json[0].stat_docu_id == 0){

               
                
        $("#unid_id").prop('disabled', false);
        $("#tipo_docu_id").prop('disabled', false);
        //$("#docu_fecha").prop('disabled', false);        
        $("#docu_numero").prop('readOnly', true);
        $("#docu_prov_fech").prop('disabled', false);
        $("#prov_id").prop('disabled', false);
        $("#area_id").prop('disabled', false);
        $("#docu_factura").prop('readOnly', false);
        $("#docu_recibe").prop('readOnly', false);
        $("#docu_observacion").prop('readOnly', false);
        $('#btn_save_ingreso').prop('disabled', false);
        
            }
            
            else{
                if(json[0].stat_docu_id == 2){
                    $("#unid_id").prop('disabled', true);
        $("#tipo_docu_id").prop('disabled', true);
       // $("#docu_fecha").prop('disabled', true);        
        $("#docu_numero").prop('readOnly', true);
        $("#docu_prov_fech").prop('disabled', true);
        $("#prov_id").prop('disabled', true);
        $("#area_id").prop('disabled', true);
        $("#docu_factura").prop('readOnly', true);
        $("#docu_recibe").prop('readOnly', true);
        $("#docu_observacion").prop('readOnly', true);      
        $('#btn_save_ingreso').prop('disabled', true);

                }else
                {
            
        $("#unid_id").prop('disabled', true);
        $("#tipo_docu_id").prop('disabled', true);
       // $("#docu_fecha").prop('disabled', true);        
        $("#docu_numero").prop('readOnly', true);
        $("#docu_prov_fech").prop('disabled', true);
        $("#prov_id").prop('disabled', true);
        $("#area_id").prop('disabled', true);
        $("#docu_factura").prop('readOnly', true);
        $("#docu_recibe").prop('readOnly', true);
        $("#docu_observacion").prop('readOnly', false);      
        $('#btn_save_ingreso').prop('disabled', false);
    }
        }              
        }); 




        $('#ModalNew').modal('show'); 
        $('#ModalNewTitle').text("Editar"); 
        editar(idx);
    });




    $(document).on('click', '.btn_agregar', function(){    
        $('#lotes_info').html('');
        $('#ModalNewTitleAgregar').text("Agregar");     
        $('#form3').trigger("reset");             
        accion='agregar_detalle_producto';  
        $("#prod_nombre").prop('disabled', false);
        $('#prod_id').val('');  

        $("#prod_nombre").focus();
             
        $('#ModalNewAgregarIng').modal('show'); 
            
    });  

    $('#ModalNewAgregarIng').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
      });

      $('#ModalNewAgregarIng').on('shown.bs.modal', function () { 
        $('#lotes_info').html('');
        $('#prod_nombre').focus();
      });


    $(document).on('click', '.btn_del', function(){  
        accion='eliminar';
        idx=$(this).attr("id");  
        swal({
            title: "Anular/Eliminar ?",
            text: "Desea Eliminar/Anular éste Documento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,            
            preConfirm: function() {
              return new Promise(function(resolve) {
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx}
                    }).done(function(data) {       
                        table.ajax.reload();            
                        swal("Notificación:", data, "info");
                     });  
              })
            }
          });    
     });


    $(document).on('click', '.btn_obs', function(){  
        idx=$(this).attr("id");  
        $.getJSON(model,{id:idx,funcion:'get_obs_doc'})
        .done(function(json) {        
         swal(json[0].obs)
        });   
    });

    //$(document).on('click', '.btn_new', function(){ 
        $( "#btn_new_ingresos_nutricion").click(function(){

             $.get(model,{funcion:'consultar_documentos_pendientes'},function(data){
      if(parseInt(data)>0) {           
         warning_toast('Tiene documentos pendientes de Procesar....');
       }
        else{
        $("#unid_id").prop('disabled', false);
        $("#tipo_docu_id").prop('disabled', false);
        //$("#docu_fecha").prop('disabled', true);        
        $("#docu_numero").prop('readOnly', true);
        $("#docu_prov_fech").prop('disabled', false);
        $("#prov_id").prop('disabled', false);
        $("#area_id").prop('disabled', false);
        $("#docu_factura").prop('readOnly', false);
        $("#docu_recibe").prop('readOnly', false);
        $("#docu_observacion").prop('readOnly', false); 
        $('#btn_save_ingreso').prop('disabled', false);                   
        $('#form2').trigger("reset");

        fecha.datepicker('setDate', new Date());
        fecha2.datepicker('setDate', new Date());
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('#ModalNewTitle').text("Nuevo"); 
        }

       }); 
    });

    function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {

        $('#docu_fecha').val(json[0].docu_fecha);
        $('#tipo_docu_id').val(json[0].tipo_docu_id);        
        $('#docu_numero').val(json[0].docu_numero);
        $('#unid_id').val(json[0].unid_id);
        $('#docu_prov_fech').val(json[0].docu_prov_fech);

        $('#prov_id').val(json[0].prov_id);
        $('#area_id').val(json[0].area_id);
        $('#docu_factura').val(json[0].docu_factura);
        $('#docu_recibe').val(json[0].docu_recibe);
        $('#docu_observacion').val(json[0].docu_observacion);        
        });       
    }

    $('#form2').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
         if($('#tipo_docu_id').val()==0){
            danger_toast('Verifique Tipo Documento...');
            $('#tipo_docu_id').focus();
            return false;
        }

        if($('#docu_numero').val().length<1){
            danger_toast('Verifique el Numero de Documento...');
            $('#docu_numero').focus();
            return false;
        }
       
        if($('#unid_id').val()==0){
            danger_toast('Verifique la Unidad...');
            $('#unid_id').focus();
            return false;
        }

        if($('#prov_id').val()==0){
            danger_toast('Verifique el Proveedor...');
            $('#prov_id').focus();
            return false;
        }
        if($('#area_id').val()==0){
            danger_toast('Verifique el servicio...');
            $('#area_id').focus();
            return false;
        }
        
        if($('#docu_factura').val().length<1){
            danger_toast('Verifique el Número de Factura...');
            $('#docu_factura').focus();
            return false;
        }
        if($('#docu_recibe').val().length<1){
            danger_toast('Verifique el Responsable...');
            $('#docu_recibe').focus();
            return false;
        }
        if($('#docu_observacion').val().length<1){
            danger_toast('Verifique la Observación...');
            $('#docu_observacion').focus();
            return false;
        }

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data,tipo:tipo},
            beforeSend: function () {
                    $("#btn_save_ingreso").attr("disabled", true);                    
                }
            }).done(function(data) {                
               /* table.ajax.reload();    
                $("#btn_save_ingreso").attr("disabled", false); 
                $('#ModalNew').modal('hide');                   
                success_toast(data);*/
                
                 if(data=='0'){
                                 warning_toast("NO SE PUDO PROCESAR LA PETICIÓN");
                             }
                             //swal("Notificación:", " NO SE PUDO PROCESAR LA PETICIÓN", "error");                                                  
                             else{
                             //swal("Notificación:", "LA PETICIÓN SE PROCESO CORRECTAMENTE", "success");
                             table.ajax.reload();   
                             $("#btn_save_ingreso").attr("disabled", false); 
                             $('#ModalNew').modal('hide'); 
                             success_toast("LA PETICIÓN SE PROCESÓ CORRECTAMENTE");
                             
                             } 
               
             });
    });

    $('#form4i').on("submit",function(event){
        event.preventDefault();
        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:'editar_deta_ingre',id_d:id_det,data:form_data,tipo:tipo},           
            }).done(function(data) {                
                table2.ajax.reload();                   
                $('#ModalEditIngreso').modal('hide');                   
                success_toast(data);
               
             });
    });

    $(document).on('click', '.btn_detalle', function(){   
        idx=$(this).attr("id");         

        $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {
            $('#mtitulo1').html(' *Documento: '+json[0].docu_numero);      
            $('#mtitulo2').html(' *'+json[0].unid_nombre);
            $('#mtitulo3').html(' *'+json[0].tipo_docu_nombre);
            tipo_docu_id=json[0].tipo_docu_id;
            unid_id=json[0].unid_id;
            propio=json[0].propio_unidad;//PARA QUE BUSQUE Y MUESTRE LOTES PRIPOS PORQ ES REINGRESO
            //SOLO SI ESTADO ES 0 NO PROCESADO HABILITA BORON DE AGREGAR PRODUCTO
            if(json[0].stat_docu_id == 0)
                $('#btn_agregar').prop('disabled', false);
            else
                $('#btn_agregar').prop('disabled', true);
            
        }); 

        $('#ModalDetalleIngreso').modal('show'); 
        table2 = $('#tb_4').DataTable();
        table2.destroy();
        $('#tb_4').show();              
        accion='detalle_ingreso';        
        detalle_ingreso_docu(idx);
    });

    function detalle_ingreso_docu(idx){
         table2=$('#tb_4').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,id:idx},
                dataSrc: ''
            },     
            columns: [                          
                { data: 'prod_nombre',
                    "render":function(data){
                        return '<h4><span style="white-space: nowrap;font-size:13px"><strong>'+ucwords(data.toLowerCase())+'<strong></span></h4>';
                    }
                },
               { data: 'presentacion',
                "render":function(data){
                    var r='Sin Presentación';
                    if(data===null)                       
                    return '<span class="badge badge-inverse"><strong>'+r+'</strong></span>';
                    else
                    return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
                }
               },
                /*{ data: 'deta_docu_lote',
                "render":function(data){
                    return '<h4><span class="badge badge-warning"><strong>'+data+'<strong></span></h4>';
                }},
                { data: 'deta_docu_fech_venc'},
                { data: 'deta_docu_regi_sani'},*/
                { data: 'deta_docu_cantidad',
                "render":function(data){
                    return '<h4><span class="badge badge-primary"><strong>'+data+'<strong></span></h4>';
                }},
                { data: 'deta_docu_valo_unit'},
                { data: 'deta_docu_iva'},
                { data: 'deta_docu_valo_iva'},
                { data: 'deta_docu_subtotal'}  ,
                { data: 'deta_docu_id',
                "render":function(data){
                    return '<button type="button" title="Eliminar" class="btn btn-secondary btn-xs m-l-5 btn_del_producto_detalle" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit_det_ingre" title="Editar Item" id="'+data+'"><i class="fa fa-pencil"></i></button>';
                }}                         
            ],
            responsive: true,
            "order": [[ 7, "desc" ]],
            dom: 'lBfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print',
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                }
            ]      
        });    
    }

       function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#unid_id_s';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Todas--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo4).val(items["unid_id"]).text(items["nick"]);
            });
        });          
        }

        function combo_area(){
            //LLENO COMBOBOX POR DATOS JSON
            combo10='#area_id';
            $.getJSON(model,{funcion:'get_area'})
            .done(function(json) {   
                $(combo10).empty();   
                $(combo10).append('<option value="0" selected="selected">--Seleccione--</option>');
                $.each(json, function (i, items) {
                     if(items !== null) $("<option>").appendTo(combo10).val(items["area_id"]).text(items["area_nombre"]);
                });
            });  
            
            }

    function combo_tipo_doc(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#tipo_docu_id_s';
        $.getJSON(model,{funcion:'get_tipo_doc',tipo:tipo})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Todos--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo3).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
            });
        });  
    }

    function combo_unidad_2(){
        //LLENO COMBOBOX POR DATOS JSON
        combo5='#unid_id';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo5).empty();   
            $(combo5).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo5).val(items["unid_id"]).text(items["nick"]);
            });
        });  
    }

    function combo_tipo_doc_2(){
        //LLENO COMBOBOX POR DATOS JSON
        combo6='#tipo_docu_id';
        $.getJSON(model,{funcion:'get_tipo_doc',tipo:tipo})
        .done(function(json) {   
            $(combo6).empty();   
            $(combo6).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo6).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
            });
        });  
    }    

    function combo_prov(){
        //LLENO COMBOBOX POR DATOS JSON
        combo7='#prov_id';
        $.getJSON(model,{funcion:'get_prov'})
        .done(function(json) {   
            $(combo7).empty();   
            $(combo7).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo7).val(items["prov_id"]).text(items["prov_nombre"]);
            });
        });  
    }   
   
    function combo_pres(){
        //LLENO COMBOBOX POR DATOS JSON
        combo8='#pres_id';
        $.getJSON(model,{funcion:'get_pres'})
        .done(function(json) {   
            $(combo8).empty();   
            $(combo8).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo8).val(items["pres_id"]).text(items["pres_nombre"]);
            });
            $("#pres_id").val("65"); //UNIDAD
        }); 
       
    }  

    combo_unidad();
    combo_area();
    combo_tipo_doc();
    combo_unidad_2();
    combo_tipo_doc_2();
    combo_prov();
    combo_pres();
    

    $("#unid_id_s").change(function() {
        table.destroy();      
        ingreso();
      });

      $("#tipo_docu_id_s").change(function() {
        table.destroy();      
        ingreso();
      });

      ingreso();//CARGA INICIAL

      $(document).on('click', '.btn_edit_det_ingre', function(){
            id_det=$(this).attr("id"); 
            limpiar_ing_det();
            $('#ModalEditIngreso').modal('show');

            $.getJSON(model,{id_d:id_det,funcion:'get_det_ingre_by_id'})
            .done(function(json) {
               
                 $('#lote_edit').val(json[0].deta_docu_lote);
                 $('#fecha_edit').val(json[0].deta_docu_fech_venc);        
                 $('#regsani_edit').val(json[0].deta_docu_regi_sani);
                 $('#cant_edit').val(json[0].deta_docu_cantidad);
                 $('#vunit_edit').val(json[0].deta_docu_valo_unit);                      
            });    
      });

      function limpiar_ing_det(){
        $('#lotes_info').html('');
        $('#lote_edit').val('');
        $('#fecha_edit').val('');        
        $('#regsani_edit').val('');
        $('#cant_edit').val('');
        $('#vunit_edit').val(''); 
      }

      //ELIMINA PRODUCTO DEL DETALLE DOCUMENTO
      $(document).on('click', '.btn_del_producto_detalle', function(){  
        accion='eliminar_producto_detalle';
        idx_d=$(this).attr("id");  
        swal({
            title: "Eliminar Producto ?",
            text: "Desea Elimina éste Producto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id_d:idx_d}
                        }).done(function(data) {       
                            table2.ajax.reload();            
                            swal("Notificación:", data, "info");
                         });
                })
              }
            });

    });


     //PROCESAR
     $(document).on('click', '.btn_procesar_docu', function(){  
        accion='procesar_documento_kardex';
        //SI RECIBE ES 0 PROCESO NORAML
        //SI RECIBE ES 1 PROCESO REVERSAR
        recibe=0;
        idx=$(this).attr("id");  

        $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {

             if(json[0].stat_docu_id == 0){
                     swal({
            title: "Procesar ?",
            text: "Desea Procesar el Documento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Procesar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,            
            preConfirm: function() {
              return new Promise(function(resolve) {
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx,recibe:recibe}
                    }).done(function(data) {       
                                    
                        swal("Notificación:", data, "info");
                        table.ajax.reload();
                     });  
              })
            }
        });

             }else{
                      if(json[0].stat_docu_id == 2){
                    swal("Alerta:", "El documento fue anulado anteriormente", "warning");

                }else{
                    swal("Alerta:", "El documento ya fue procesado anteriormente", "warning"); 
                }

             }



                 });


   
    });

    
      //************************MODAL INGRESO DETALLE MEDICAMENTO*********************
      /*
      $("#prod_nombre").autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: model,
            dataType: "json",
            data: {
              term: request.term,funcion:'buscar_producto',unid_id:unid_id
            },
            success: function( data ) {
              response(data);
            }
          } );
        },
        minLength: 2,
        select: function( event, ui ) {
          //console.log( "Selected: " + ui.item.value + " id:  " + ui.item.prod_id );
          $("#prod_id").val(ui.item.prod_id);          
          $("#deta_docu_valo_unit").val(ui.item.kard_costo);
          $("#deta_docu_lote").focus();
        }
      });
      */
     function buscar_lotes(prod_id){        
        $.getJSON(model,{funcion:'lotes_productos_reingreso',prod_id:prod_id,unid_id:unid_id},function(json){            
                // $('#lotes_info').html(json[0].lote_codigo);
                  var html='';
                  $.each(json, function(index, array) {
                     html+='<div class="form-check">';
                     html+='<input class="form-check-input radio_lote" type="radio"  name="deta_docu_lote" pre="'+array['sald_kard_precio']+'" reg="'+array['regi']+'" fec="'+array['fech']+'"    id="'+array['lote_id']+'" value="'+array['lote_codigo']+'"/>';
                     html+='<label class="form-check-label"><strong>Lote:</strong> '+array['lote_codigo']+' <strong> R/S:</strong> '+array['regi']+' <strong> Fech-Cad: </strong> '+array['fech']+'</label> ';
                     html+='</div>';
                 });
                 html+='<div class="form-check">';
                 html+='<input class="form-check-input radio_lote" type="radio"  name="deta_docu_lote" pre="" reg="" fec="" id="" value=""/>';
                 html+='<label class="form-check-label"><strong>Otro Lote</label>';
                 html+='</div>';
                $('#lotes_info').html(html);
                $('#deta_docu_cantidad').focus();
        });
      }

       //OBTIEN ID LOTE AL DAR CLICK EN RADIO BUTON LOTES
       $(document).on('click', '.radio_lote', function(){   
        prec1=$("#deta_docu_valo_unit").val();
        $('#deta_docu_lote').val($(this).attr("value"));
        $('#deta_docu_regi_sani').val($(this).attr("reg"));
        $('#deta_docu_fech_venc').val($(this).attr("fec"));      
        prec=$(this).attr("pre");        
        if(Number(prec)>0)
            $("#deta_docu_valo_unit").val(prec);     
        else
            $("#deta_docu_valo_unit").val(prec1);
       

        $('#deta_docu_cantidad').focus();
  });

      $('#prod_nombre').typeahead({
        displayText: function(item) {
             return item.value
        },
        afterSelect: function(item) {


//var temp =f.getFullYear() + "-" + (f.getMonth() + 2) + "-" +(f.setDate(f.getDate()+15));
f=new Date();
// Milisegundos de 3 días mas
//suma3dias= 60*24*60*60*1000; 
// Sumamos a la fecha de hoy en milisegundos, los 3 días más en milisegundos
// Tendremos una nueva variable en milisegundos de la fecha actual + 3 días
fecha_add_mes=f.getTime()+(60*24*60*60*1000);
//fecha_add_mes=f.setDate(f.getDate() + 60);

// Si la queremos en formato fecha
fecha_final_add_mes= new Date(fecha_add_mes);
if(fecha_final_add_mes.getMonth()==0){
$ano_final=f.getFullYear();
$dic=12;
}else{
$ano_final=fecha_final_add_mes.getFullYear();
$dic=fecha_final_add_mes.getMonth();
}

var temp = $ano_final + "-" + $dic + "-" +fecha_final_add_mes.getDate();



/*f2=new Date();
fecha_add_mes2=f2.getTime()+(60*50*60*60*1000);
fecha_final_add_mes2= new Date(fecha_add_mes2);*/
//var temp2 =fecha_final_add_mes2.getFullYear() + "-" + fecha_final_add_mes2.getMonth() + "-" +fecha_final_add_mes2.getDate();
//fecha_temp=new Date();


var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

/*var fecha = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;*/

//var fecha = "(day<10 ? '0' : '') + day + (month<10 ? '0' : '') + month + d.getFullYear()";
var fecha =((''+day).length<2 ? '0' : '') + day + '-' +
    ((''+month).length<2 ? '0' : '') + month + '-' +
   d.getFullYear();

 var separador = separador || "-";
  var arrayFecha = fecha.split(separador);
  var dia = arrayFecha[0];
  var mes = arrayFecha[1];
  var anio = arrayFecha[2];  
 var intervalo = '+3';
 var dma='d';
  var fechaInicial = new Date(anio, mes - 1, dia);
  var fechaFinal = fechaInicial;
  if(dma=="m" || dma=="M"){
    fechaFinal.setMonth(fechaInicial.getMonth()+parseInt(intervalo));
  }else if(dma=="y" || dma=="Y"){
    fechaFinal.setFullYear(fechaInicial.getFullYear()+parseInt(intervalo));
  }else if(dma=="d" || dma=="D"){
    fechaFinal.setDate(fechaInicial.getDate()+parseInt(intervalo));
  }else{
    return fecha;
  }
  dia = fechaFinal.getDate();
  mes = fechaFinal.getMonth() + 1;
  anio = fechaFinal.getFullYear();

  dia = (dia.toString().length == 1) ? "0" + dia.toString() : dia;
  mes = (mes.toString().length == 1) ? "0" + mes.toString() : mes;
var temp2 = anio + "-" + mes + "-" + dia;



            this.$element[0].value= item.value;           
            prod_id=item.prod_id;
            caducidad_registroSanitario=item.caducidad_registroSanitario;
            grupo_prod_id=item.grupo_prod_id;
            $("#prod_id").val(prod_id);  
            $("#caducidad_registroSanitario").val(caducidad_registroSanitario);  


          if(caducidad_registroSanitario==1){
                var html='';
                var html2='';
                var html3='';
                var html4='';

                if(grupo_prod_id==11 || grupo_prod_id==12 || grupo_prod_id==13){
                $("#deta_docu_lote").val('');
                html+='<label>Fecha Vencimiento:</label>';
                html+='<input type="date" id="deta_docu_fech_venc" name="deta_docu_fech_venc" class="form-control" required>';
                 
                $('#fecha_venc').html(html);



                html2+='<label>Registro Sanitario:</label>';
                html2+='<input type="text" id="deta_docu_regi_sani" name="deta_docu_regi_sani" class="form-control" required>';
                 
                $('#registro_sani').html(html2);

                html4+=' <label>Lote:</label>';
                html4+='<input type="text" id="deta_docu_lote" name="deta_docu_lote" class="form-control" required>';
                 
                $('#lotes_info_new').html(html4);

                }else{

                    

              //  html4+=' <label>Lote:</label>';
                html4+='<input type="hidden" id="deta_docu_lote" name="deta_docu_lote" class="form-control" readOnly>';
                 
                $('#lotes_info_new').html(html4);
                    var lote_nuevo= numero_lote();
                    $("#deta_docu_lote").val(lote_nuevo);    

                    html+='<label>Fecha Vencimiento:</label>';
                 html+='<input type="date" id="deta_docu_fech_venc" name="deta_docu_fech_venc" class="form-control" required>';
                 
                $('#fecha_venc').html(html);



                html2+='<label>Registro Sanitario:</label>';
                html2+='<input type="text" id="deta_docu_regi_sani" name="deta_docu_regi_sani" class="form-control" required>';
                 
                $('#registro_sani').html(html2);
                }

            }else{


               // numero_lote();
                

                var html='';
                var html2='';
                var html4='';

                if(grupo_prod_id==2){

                //html4+=' <label>Lote:</label>';
                html4+='<input type="hidden" id="deta_docu_lote" name="deta_docu_lote" class="form-control" >';
                 
                $('#lotes_info_new').html(html4);
                    var lote_nuevo= numero_lote();
                    $("#deta_docu_lote").val(lote_nuevo);    

                // html+='<label>Fecha Vencimiento:</label>';
                 html+='<input type="hidden" id="deta_docu_fech_venc" name="deta_docu_fech_venc" class="form-control" required>';
                 
                $('#fecha_venc').html(html);
                $("#deta_docu_fech_venc").val(temp2);         

       
                  

             //  html2+='<label>Registro Sanitario:</label>';
                html2+='<input type="hidden" id="deta_docu_regi_sani" name="deta_docu_regi_sani" class="form-control" required>';
                 var registro=registro_sanitario();
                $('#registro_sani').html(html2);
                $("#deta_docu_regi_sani").val(registro);   

                }else{
                       // html4+=' <label>Lote:</label>';
                html4+='<input type="hidden" id="deta_docu_lote" name="deta_docu_lote" class="form-control" >';
                 
                $('#lotes_info_new').html(html4);
                    var lote_nuevo= numero_lote();
                    $("#deta_docu_lote").val(lote_nuevo);    

                // html+='<label>Fecha Vencimiento:</label>';
                 html+='<input type="hidden" id="deta_docu_fech_venc" name="deta_docu_fech_venc" class="form-control" required>';
                 
                $('#fecha_venc').html(html);
                $("#deta_docu_fech_venc").val(temp);         

       
                  

             //  html2+='<label>Registro Sanitario:</label>';
                html2+='<input type="hidden" id="deta_docu_regi_sani" name="deta_docu_regi_sani" class="form-control" required>';
                 var registro=registro_sanitario();
                $('#registro_sani').html(html2);
                $("#deta_docu_regi_sani").val(registro);   
                }

                          
                 

            }

            if(Number(item.kard_costo)>0)
                $("#deta_docu_valo_unit").val(item.kard_costo);
            
            $("#deta_docu_lote").focus();    


            //SI ES PROPIO 1 SIGNIFICA QUE ES REINGRESO POR LO CUAL BUSCA LOTES YA EXISTENTES
            if(propio=='1')
             buscar_lotes(prod_id);

        },
        source: function (query, process) {
          return $.getJSON(model, { term: query,funcion:'buscar_producto',unid_id:unid_id,tipo_docu_id:tipo_docu_id}, function(data) {
            $('#prod_id').val('');
            $('#caducidad_registroSanitario').val('');
            $('#deta_docu_fech_venc').val('');
            $('#deta_docu_regi_sani').val('');
            $('#deta_docu_cantidad').val('');                
            $('#deta_docu_valo_unit').val('');
            $('#deta_docu_lote').val('');
            $('#deta_docu_fech_venc').val('');
            $('#lotes_info').html('');
            $('#registro_sani').html('');

            if(data[0].prod_nombre==0)
                window.location.href = "model/config/lgout.php";
            else
                process(data);  
          })
        }   
      });

 


      //EVENTO PARA GUARDAR PRODUCTO EN EL DETALLE
      $('#form3').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO

        if($('#prod_nombre').val().length<1){
            danger_toast('Verifique el Producto...');
            return false;
        }
     
        $("#prod_nombre").prop('disabled', true);

       /* if($('#prod_id').val().length<1){
            danger_toast('Verifique el Producto...');
            return false;
        }

        if($('#deta_docu_lote').val().length<1){
            danger_toast('Verifique el Lote...');
            return false;
        }
        if($('#pres_id').val()==0){
            danger_toast('Verifique la Presentacion...');
            return false;
        }
        if($('#deta_docu_fech_venc').val().length<1){
            danger_toast('Verifique la Fecha...');
            return false;
        }*/
        if($('#deta_docu_cantidad').val()<0){
            danger_toast('Verifique la Cantidad...');
            return false;
        }
        if($('#deta_docu_valo_unit').val().length<1){
            danger_toast('Verifique el Valor Unitario...');
            return false;
        }        

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_save_ingreso").attr("disabled", true);                    
                }
            }).done(function(data) {    
                table2.ajax.reload();   
                $("#prod_nombre").prop('disabled', false);
                $("#btn_save_ingreso").attr("disabled", false); 
                if(data>0)
                    $('#ModalNewAgregarIng').modal('hide');         
                success_toast(data+' Producto Agregado...');
                
                
             });
    });

  
});