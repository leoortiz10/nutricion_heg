$(document).ready(function(){

    var model='model/kardex_pro/funciones.php';
    var idx;
    var accion;
    var table3;
    var table4;
    var unid_id;
    var unid_id_destino;
    var tipo_docu_id=5; //SOLO TRANSFERENCIA
    var fecha = $('#docu_fecha');  

    var fecha3 = $('#deta_docu_fech_venc');  

    var lote_cant;

     //DOCUMENTO TRANSFERENCIAS
     var tipo='T';

    fecha.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });
  
   /*
    fecha3.datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });*/
        
    $('#tb_4_tra').hide();    
  
    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

    function docu_numero_t(){
        $.get(model,{funcion:'docu_numero',unid_id:unid_id,unid_id_destino:unid_id_destino,tipo:tipo,tipo_docu_id:tipo_docu_id},function(data){$('#docu_numero').val(data);});
    }

    $("#unid_id").change(function() {
        unid_id=$("#unid_id").val();
       docu_numero_t();
       combo_unidad_destino_2(unid_id)
    });

    $("#unid_id_destino").change(function() {
        unid_id_destino=$("#unid_id_destino").val();
       docu_numero_t();
    });

    $("#tipo_docu_id").change(function() {          
        docu_numero_t();
     });

   //TABLA DATOS
   function transfer(){ 
    unid_id=$('#unid_id_s').val();
    unid_id_destino=$('#unid_id_destino_s').val();
    table3=$('#tb_3_tra').DataTable( {        
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all',unid_id:unid_id,tipo_docu_id:tipo_docu_id,tipo:tipo,unid_id_destino:unid_id_destino},
            dataSrc: ''
        },             
        columns: [                                 
            { data: 'docu_numero' },
            { data: 'unid_nombre' ,
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }  },
            { data: 'unid_destino',
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            }   },
            { data: 'docu_fecha' },   
            { data: 'docu_autoriza',
            "render":function(data){
                return ucwords(data.toLowerCase());
            }  },   
            { data: 'docu_entrega' ,
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },                               
            { data: 'docu_recibe',
            "render":function(data){
                return ucwords(data.toLowerCase());
            } },            
            { data: 'stat_docu_nombre',
                "render":function(data){
                    var r;
                    switch(data){
                        case 'No Procesado':
                            r='<span class="badge badge-danger">'+data+'</span>';
                        break;
                        case 'Procesado':
                            r='<span class="badge badge-success">'+data+'</span>';
                        break;
                        case 'Anulado':
                            r='<span class="badge badge-warning">'+data+'</span>';
                        break;
                        case 'Borrado':
                            r='<span class="badge badge-dark">'+data+'</span>';
                        break;
                        default:
                            r='<span class="badge badge-warning">'+data+'</span>';                        
                    }
                    return r;
                }
            },
            { data: 'stat_docu_id_destino',
            "render":function(data){
                var r;
                switch(data){
                    case '0':
                        r='<span class="badge badge-danger">No Procesado</span>';
                    break;
                    case '1':
                        r='<span class="badge badge-success">Procesado</span>';
                    break;
                    case '2':
                        r='<span class="badge badge-warning">Anulado</span>';
                    break;
                    case '3':
                        r='<span class="badge badge-dark">Borrado</span>';
                    break;
                    default:
                        r='<span class="badge badge-warning">'+data+'</span>';                        
                }
                return r;
            }
        },
            { data: 'docu_id' ,
                "render": function ( data ) {                                              
                return '<button type="button" title="Detalles" class="btn btn-secondary btn-xs m-l-5 btn_detalle" id="'+data+'"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" title="Editar" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" title="Observaciones" class="btn btn-secondary btn-xs m-l-5 btn_obs" id="'+data+'"><i class="fa fa-bullhorn"></i></button><button type="button" title="Eliminar/Anular" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_envtra" id="'+data+'"><i class="fa fa-print"></i></button><button type="button" title="Procesar" class="btn btn-secondary btn-xs m-l-5 btn_procesar_docu" id="'+data+'"><i class="fa fa-check"></i></button>';
                }
            } 
            
            
        ],
        "order": [[ 9, "desc" ]],
        responsive: true 
             
    });    
    }

     //IMPRIMIR PDF
     $(document).on('click', '.btn_print_envtra', function(){
        idx=$(this).attr("id");  
        window.open('model/reportes/pdf.php?f=repor_ingresos&id='+idx, '_blank');
    });

    $(document).on('click', '.btn_edit', function(){          
        accion='editar';
        idx=$(this).attr("id");  
        $('#ModalNew').modal('show'); 
        $('#ModalNewTitle').text("Editar"); 
        editar(idx);
    });

    $(document).on('click', '.btn_agregar', function(){    
        $('#lotes_info').html('');
        lote_cant=0;        
        $('#form33').trigger("reset");
        $("#prod_nombre").prop('disabled', false);
        $('#lote_id').val('');
        $('#prod_id').val('');        
        combo_pres();       
        accion='agregar_detalle_producto';        
        $('#ModalNewAgregar').modal('show'); 
        $('#ModalNewTitleAgregar').text("Agregar");         
    });
    $('#ModalNewAgregar').on('hidden.bs.modal', function () {
        $('body').addClass('modal-open');
      });

    $(document).on('click', '.btn_del', function(){  
        accion='eliminar';
        idx=$(this).attr("id");  
        swal({
            title: "Anular/Eliminar ?",
            text: "Desea Eliminar/Anular éste Documento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx}
                        }).done(function(data) {       
                            table3.ajax.reload();            
                            swal("Notificación:", data, "info");
                         });
                })
              }
            });
    });

    $(document).on('click', '.btn_obs', function(){  
        idx=$(this).attr("id");  
        $.getJSON(model,{id:idx,funcion:'get_obs_doc'})
        .done(function(json) {        
         swal(json[0].obs)
        });   
    });

    $(document).on('click', '.btn_new', function(){        
        $("#unid_id").prop('disabled', false);
            $("#unid_id_destino").prop('disabled', false);
            $("#docu_fecha").prop('disabled', false);
            $("#tipo_docu_id").prop('disabled', false);
            $("#docu_autoriza").prop('readOnly', false);
            $("#docu_entrega").prop('readOnly', false);
            $("#docu_recibe").prop('readOnly', false);
            $("#docu_observacion").prop('readOnly', false);
            $("#btn_save_ingreso").prop('disabled', false);
                  
        $('#form22').trigger("reset");        
        fecha.datepicker('setDate', new Date());       
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('#ModalNewTitle').text("Nuevo"); 
    });

    function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#docu_fecha').val(json[0].docu_fecha);
        $('#tipo_docu_id').val(json[0].tipo_docu_id);        
        $('#docu_numero').val(json[0].docu_numero);
        $('#unid_id').val(json[0].unid_id);
        $('#unid_id_destino').val(json[0].unid_id_destino);        
        $('#docu_autoriza').val(json[0].docu_autoriza);
        $('#docu_entrega').val(json[0].docu_entrega);
        $('#docu_recibe').val(json[0].docu_recibe);
        $('#docu_observacion').val(json[0].docu_observacion);    
        
        if(json[0].stat_docu_id==0){
            $("#unid_id").prop('disabled', false);
            $("#unid_id_destino").prop('disabled', false);
            $("#docu_fecha").prop('disabled', false);
            $("#tipo_docu_id").prop('disabled', false);
            $("#docu_autoriza").prop('readOnly', false);
            $("#docu_entrega").prop('readOnly', false);
            $("#docu_recibe").prop('readOnly', false);
            $("#docu_observacion").prop('readOnly', false); 
            $("#btn_save_ingreso").prop('disabled', false);                
        }
        else{
            
            $("#unid_id").prop('disabled', true);
            $("#unid_id_destino").prop('disabled', true);
            $("#docu_fecha").prop('disabled', true);
            $("#tipo_docu_id").prop('disabled', true);
            $("#docu_autoriza").prop('readOnly', true);
            $("#docu_entrega").prop('readOnly', true);
            $("#docu_recibe").prop('readOnly', true);
            $("#docu_observacion").prop('readOnly', true);
            $("#btn_save_ingreso").prop('disabled', true);
        }


        });       
    }

    $('#form22').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO

        if($('#tipo_docu_id').val()==0){
            danger_toast('Verifique Tipo Documento...');
            $('#tipo_docu_id').focus();
            return false;
        }
        if($('#unid_id').val()==0){
            danger_toast('Verifique la Unidad Origen...');
            $('#unid_id').focus();
            return false;
        }
          if($('#unid_id_destino').val()==0){
            danger_toast('Verifique la Unidad Destino...');
            $('#unid_id_destino').focus();
            return false;
        }

        if($('#docu_numero').val().length<1){
            danger_toast('Verifique el Numero de Documento...');
            return false;
        }
        
      
      if($('#docu_autoriza').val().length<1){
            danger_toast('Verifique el Responsable que Autoriza...');
            $('#docu_autoriza').focus();
            return false;
        }


      if($('#docu_entrega').val().length<1){
            danger_toast('Verifique el Responsable que Entrega...');
            $('#docu_entrega').focus();
            return false;
        }        
        
        if($('#docu_recibe').val().length<1){
            danger_toast('Verifique el Responsable que Recibe...');
            $('#docu_recibe').focus();
            return false;
        }
        if($('#docu_observacion').val().length<1){
            danger_toast('Verifique la Observación...');
            $('#docu_observacion').focus();
            return false;
        }

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data,tipo:tipo},
            beforeSend: function () {
                    $("#btn_save_ingreso").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table3.ajax.reload();	
                $("#btn_save_ingreso").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
    });

    $(document).on('click', '.btn_detalle', function(){   
        idx=$(this).attr("id");         

        $.getJSON(model,{id:idx,funcion:'get_info_docu'})
        .done(function(json) {
            $('#mtitulo1').html(' *Documento: '+json[0].docu_numero);      
            $('#mtitulo2').html(' *'+json[0].unid_nombre);
            $('#mtitulo3').html(' *'+json[0].tipo_docu_nombre);

            //OBTIENE ID UNIDAD
            unid_id=json[0].unid_id;
            unid_id_destino=json[0].unid_id_destino;
            
            //SOLO SI ESTADO ES 0 NO PROCESADO HABILITA BORON DE AGREGAR PRODUCTO
            if(json[0].stat_docu_id == 0)
                $('#btn_agregar').prop('disabled', false);
            else
                $('#btn_agregar').prop('disabled', true);
            
        }); 

        $('#ModalDetalleIngreso').modal('show'); 
        table4 = $('#tb_4_tra').DataTable();
        table4.destroy();
        $('#tb_4_tra').show();              
        accion='detalle_ingreso';        
        detalle_ingreso_e_transf(idx);
    });

    function detalle_ingreso_e_transf(idx){
         table4=$('#tb_4_tra').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,id:idx},
                dataSrc: ''
            },     
            columns: [                          
                { data: 'prod_nombre',
                    "render":function(data){
                        return '<h4><span style="white-space: nowrap;font-size:13px"><strong>'+ucwords(data.toLowerCase())+'<strong></span></h4>';
                    }
                },
                { data: 'pres_nombre'},
                { data: 'deta_docu_lote',
                "render":function(data){
                    return '<h4><span class="badge badge-warning"><strong>'+data+'<strong></span></h4>';
                }},
                { data: 'deta_docu_fech_venc'},
                { data: 'deta_docu_regi_sani'},
                { data: 'deta_docu_cantidad',
                "render":function(data){
                    return '<h4><span class="badge badge-primary"><strong>'+data+'<strong></span></h4>';
                }},
                { data: 'deta_docu_valo_unit'},
                { data: 'deta_docu_iva'},
                { data: 'deta_docu_valo_iva'},
                { data: 'deta_docu_subtotal'}  ,
                { data: 'deta_docu_id',
                "render":function(data){
                    return '<button type="button" title="Eliminar" class="btn btn-secondary btn-xs m-l-5 btn_del_producto_detalle" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }}                         
            ],
            responsive: true,
            dom: 'lBfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'print',
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                }
            ]    
        });    
    }

       function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#unid_id_s';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Todas--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_destino(){
        //LLENO COMBOBOX POR DATOS JSON
        combo33='#unid_id_destino_s';
        $.getJSON(model,{funcion:'get_unidad_todos_admin',tipo:tipo})
        .done(function(json) {   
            $(combo33).empty();   
            $(combo33).append('<option value="0" selected="selected">--Todos--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo33).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_destino_2(unid_id){
         if(unid_id === undefined){
    var ej=0;
    }else{
        var ej=unid_id;
    }
        //LLENO COMBOBOX POR DATOS JSON
        var ej=unid_id;
        combo3='#unid_id_destino';
        $.getJSON(model,{funcion:'get_unidad_todos',tipo:tipo,'identificador':ej})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo3).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    function combo_unidad_2(){
        //LLENO COMBOBOX POR DATOS JSON

        combo5='#unid_id';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo5).empty();   
            $(combo5).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo5).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }
 
    function combo_pres(){
        //LLENO COMBOBOX POR DATOS JSON
        combo8='#pres_id';
        $.getJSON(model,{funcion:'get_pres'})
        .done(function(json) {   
            $(combo8).empty();   
            $(combo8).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo8).val(items["pres_id"]).text(items["pres_nombre"]);
            });
            //$("#pres_id").val("119"); //UNIDAD
        });  
    }  

    function combo_tipo_docu(){
        //LLENO COMBOBOX POR DATOS JSON
        combo10='#tipo_docu_id';
        $.getJSON(model,{funcion:'get_tipo_doc',tipo:tipo})
        .done(function(json) {   
            $(combo10).empty();   
            //$(combo8).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo10).val(items["tipo_docu_id"]).text(items["tipo_docu_nombre"]);
			});
        });  
    }

    combo_unidad();
    combo_unidad_destino();
    combo_unidad_2();
    combo_unidad_destino_2();
    combo_tipo_docu();

    

    $("#unid_id_s").change(function() {
        table3.destroy();      
        transfer();
      });

      $("#unid_id_destino_s").change(function() {
        table3.destroy();      
        transfer();
      });

      transfer();//CARGA INICIAL

      //ELIMINA PRODUCTO DEL DETALLE DOCUMENTO
      $(document).on('click', '.btn_del_producto_detalle', function(){  
        accion='eliminar_producto_detalle';
        idx_d=$(this).attr("id");  
        swal({
            title: "Eliminar Producto ?",
            text: "Desea Elimina éste Producto?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id_d:idx_d}
                        }).done(function(data) {       
                            table4.ajax.reload();            
                            swal("Notificación:", data, "info");
                         }); 
                })
              }
            });
    });


     //PROCESAR
     $(document).on('click', '.btn_procesar_docu', function(){  
        accion='procesar_documento_kardex';
        //SI RECIBE ES 0 PROCESO NORAML
        //SI RECIBE ES 1 PROCESO REVERSAR
        recibe=0;
        idx=$(this).attr("id");  
        swal({
            title: "Procesar ?",
            text: "Desea Procesar el Documento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Procesar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    $.ajax({
                        url:model,
                        type:"GET",            
                        data: {funcion:accion,id:idx,recibe:recibe}
                        }).done(function(data) {       
                            table3.ajax.reload();            
                            swal("Notificación:", data, "info");
                         });   
                })
              }
            });
    });

      //************************MODAL INGRESO DETALLE MEDICAMENTO*********************
     /* $("#prod_nombre").autocomplete({
        source: function( request, response ) {
          $.ajax( {
            url: model,
            dataType: "json",
            data: {
              term: request.term,funcion:'buscar_producto',unid_id:unid_id
            },
            success: function( data ) {
              response(data);
            }
          } );
        },
        minLength: 2,
        select: function( event, ui ) {
          //console.log( "Selected: " + ui.item.value + " id:  " + ui.item.prod_id );
          var prod_id=ui.item.prod_id;
          $("#prod_id").val(prod_id);         
          //buscar_lotes(prod_id);
          consultar_stock(prod_id);
        }
      });
      */

      $('#prod_nombre').typeahead({
        
        displayText: function(item) {
             return item.value
        },
        afterSelect: function(item) {
            this.$element[0].value= item.value;  
            var prod_id=item.prod_id;         
            $("#prod_id").val(prod_id);   
            consultar_stock(prod_id);    
            
        },
       items:10,
        source: function (query, process) {
            $('#lotes_info').html('');
            $("#prod_id").val('');
            $('#deta_docu_regi_sani').val('');
            $('#deta_docu_fech_venc').val('');      
            $("#deta_docu_valo_unit").val(''); 
          return $.getJSON(model, { term: query,funcion:'buscar_producto',unid_id:unid_id}, function(data) {
            if(data[0].prod_nombre==0)
                window.location.href = "model/config/lgout.php";
            else
                process(data);            
          })         
        }        
      });

      function consultar_stock(prod_id){
        $.get(model,{funcion:'consultar_stock',prod_id:prod_id,unid_id:unid_id},function(data){
        if(parseInt(data)>0)
            buscar_lotes(prod_id);
        else{
            warning_toast('Éste Producto No Tiene Stock Suficiente en Lotes para Continuar....');
        }
        });
      }

      function buscar_lotes(prod_id){        
        $.getJSON(model,{funcion:'lotes_productos',prod_id:prod_id,unid_id:unid_id,unid_id_destino:unid_id_destino},function(json){            
                // $('#lotes_info').html(json[0].lote_codigo);
                  var html='';
                  $.each(json, function(index, array) {
                     html+='<div class="form-check">';
                     html+='<input class="form-check-input radio_lote" type="radio"  name="deta_docu_lote" pre="'+array['sald_kard_precio']+'" can="'+array['cantidad']+'" reg="'+array['regi']+'" prese="'+array['pres']+'" fec="'+array['fech']+'"    id="'+array['lote_id']+'" value="'+array['lote_codigo']+'"/>';
                     html+='<label class="form-check-label"><strong>Lote:</strong> '+array['lote_codigo']+' <strong> Cantidad:</strong> '+array['cantidad']+' <strong> Fech-Cad: </strong> '+array['fech']+'</label> ';
                     html+='</div>';
                 });
                $('#lotes_info').html(html);
                $('#deta_docu_cantidad').focus();
        });
      }

        //OBTIEN ID LOTE AL DAR CLICK EN RADIO BUTON LOTES
      $(document).on('click', '.radio_lote', function(){   
            $('#lote_id').val($(this).attr("id"));
            lote_cant=$(this).attr("can");
            //$('#deta_docu_lote').val($(this).attr("value"));
            $('#deta_docu_regi_sani').val($(this).attr("reg"));
            $('#deta_docu_fech_venc').val($(this).attr("fec"));      
            $("#deta_docu_valo_unit").val($(this).attr("pre"));     
            $("#pres_id").val($(this).attr("prese"));   
            $('#deta_docu_cantidad').focus();
      });

      

      //EVENTO PARA GUARDAR PRODUCTO EN EL DETALLE
      $('#form33').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO

        if($('#prod_nombre').val().length<1){
            danger_toast('Verifique el Producto...');
            return false;
        }
     
        $("#prod_nombre").prop('disabled', true);

        if($('#prod_id').val().length<1){
            danger_toast('Verifique el Producto...');
            return false;
        }
        
        if($('#pres_id').val()=='null'){
            danger_toast('Verifique la Presentacion...');
         return false;
        }

        if($('#lote_id').val().length<1){
            danger_toast('Verifique el Lote...');
            return false;
        }

        if($('#deta_docu_lote').val()==''){
            danger_toast('Verifique el Lote...');
            return false;
        }

        if(parseInt(lote_cant)<=0){
            danger_toast('Cantidad Lote Insuficiente... Seleccione otro Lote');
            return false;
        }

        if(parseInt($('#deta_docu_cantidad').val())>parseInt(lote_cant)){
            danger_toast('Cantidad es Mayor al Disponible del Lote Seleccionado!!!');
            return false;
        }
        
        if(parseInt($('#deta_docu_cantidad').val())<1){
            danger_toast('Verifique la Cantidad...');
            return false;
        }
        if($('#deta_docu_valo_unit').val().length<1){
            danger_toast('Verifique el Valor Unitario...');
            return false;
        }        

        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_save_ingreso").attr("disabled", true);                    
                }
            }).done(function(data) {	
                table4.ajax.reload();	
                $("#prod_nombre").prop('disabled', false);
                $("#btn_save_ingreso").attr("disabled", false);	
                if(data>0)
                    $('#ModalNewAgregar').modal('hide');                
                success_toast(data+' Producto Agregado...');
               
             });
    });


});