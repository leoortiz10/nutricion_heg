$(document).ready(function(){

    var model='model/mant_kardex/funciones.php';
    var idx;
    var idk;
    var accion;
    var table;
    var table2;
    var unid_id;
    var psico;
    var psicos;
    var cambios;
    
    
    $('#tb_2').hide();
    
  
    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }

   //TABLA DATOS
   function kardex(unid_id,cambios){ 

    if(cambios === undefined){
    var ej=0;
    }else{
        var ej=cambios;
    }
    
   
 
    table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all',unid_id:unid_id,'identificador':ej},
            dataSrc: ''
        },     
        columns: [                      
            { data: 'unid_nombre',
            "render":function(data){
                return '<span class="badge badge-inverse"><strong>'+data+'</strong></span>';
            } },
            { data: 'prod_nombre',
                "render":function(data){
                        return ucwords(data.toLowerCase());
                }
            },                    
            { data: 'kard_minimo'},
            { data: 'kard_maximo'},
            { data: 'stock',
            "render":function(data){
                return parseFloat(data).toFixed(4);
            }},
            { data: 'kard_entregado',
            "render":function(data){
                return  r='<h4><span class="badge badge-primary">'+parseFloat(data).toFixed(4)+'</span><h4>';
            }},
            { data: 'stock_real',
            "render":function(data){
                return  r='<h4><span class="badge badge-success">'+parseFloat(data).toFixed(4)+'</span><h4>';
            }
            },
                       
            { data: 'kard_costo'},  
            { data: 'kard_total'},
            { data: 'kard_id' ,
                "render": function ( data ) {                                
                //return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_detalle_kardexp" id="'+data+'" data-toggle="modal" data-target="#ModalDetalle" ><i class="fa fa-plus"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_editk" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_det_kardex" id="'+data+'"><i class="fa fa-print"></i></button>';
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_detalle_kardexp" id="'+data+'" data-toggle="modal" data-target="#ModalDetalle" ><i class="fa fa-plus"></i></button><button type="button" title="Imprimir" class="btn btn-secondary btn-xs m-l-5 btn_print_det_kardex" id="'+data+'"><i class="fa fa-print"></i></button>';
                }
            }         
        ],
        responsive: true,
        dom: 'lBfrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ]    
    });    
    }

    //IMPRIMIR PDF
    $(document).on('click', '.btn_print_det_kardex', function(){
        idx=$(this).attr("id");  
        window.open('model/reportes/pdf.php?f=repor_deta_kardex&id='+idx, '_blank');
    });

    $(document).on('click', '.btn_editk', function(){  
        idx=$(this).attr("id");  
        $('#ModalKardex').modal('show'); 
        editark(idx);
    });

    function editark(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#kard_minimo').val(json[0].kard_minimo);
        $('#kard_maximo').val(json[0].kard_maximo);      
        $('#kard_costo').val(json[0].kard_costo);   
        });       
    }

    $('#form2').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:'editar',id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_save_kardex").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_save_kardex").attr("disabled", false);	
                $('#ModalKardex').modal('hide'); 	                
                success_toast(data);
               
             });
    });

    

    $(document).on('click', '.btn_detalle_kardexp', function(){   
      
        idx=$(this).attr("id");
        idk=$(this).attr("id");
       // $('#ModalDetalle').modal('show'); 
        //$('#morris-area-chart_kardex').empty();        
                            
        $.getJSON(model,{id:idx,funcion:'get_totales'})
        .done(function(json) {
            $('#lblingre').html(parseInt(json[0].ingreso));      
            $('#lblegre').html(parseInt(json[0].egreso));
            $('#lbltot').html(parseInt(json[0].total));
        });  

        $.getJSON(model,{id:idx,funcion:'get_producto_info'})
        .done(function(json) {
            $('#mtitulo').html(ucwords(json[0].prod_nombre.toLowerCase()));      
        });        
       
        table2 = $('#tb_2').DataTable();
        table2.destroy();
        $('#tb_2').show();              
        accion='detalle_kardex';
        
        detalle_kardex(idx);
       
       // dash_movimientos(idx);

    });
       
    $('#ModalDetalle').on('shown.bs.modal',function(event){
 
        $.getJSON(model,{funcion:'get_movimientos_kardex',id:idx},function(data){ 
            mychart= Morris.Area( {
                responsive : true,
                animation: true,
                showScale: true,
                   element: 'morris-area-chart_kardex',
                   data: data,
                   xkey: 'fecha',
                   ykeys: [ 'salida', 'ingreso' ],
                   labels: [ 'Salidas', 'Ingresos' ],
                   pointSize: 3,
                   fillOpacity: 0,
                   pointStrokeColors: [ '#FFC300','#1565C0'],
                   behaveLikeLine: true,
                   gridLineColor: '#e0e0e0',
                   lineWidth: 3,
                   hideHover: 'auto',
                   lineColors: [  '#FFC300','#1565C0' ],
                   resize: false
               });
       });
    }).on('hidden.bs.modal',function(event){        
        var modal = $(this);       
        $(this).data('bs.modal', null);
        $('#morris-area-chart_kardex').empty();
    });

    function detalle_kardex(idx){
         table2=$('#tb_2').DataTable( {
            ajax: {
                url: model,
                type: "GET",
                data:{funcion:accion,id:idx},
                dataSrc: ''
            },     
            columns: [                
                { data: 'deta_kard_id' },      
                { data: 'deta_kard_fech_movi' },                              
                { data: 'deta_kard_fecha'},
                { data: 'tipo_docu_nombre'},
                { data: 'docu_id',
                    "render":function(data){                       
                       return '<button type="button" title="Ver Más Detalles" class="btn btn-secondary btn-xs m-l-5 btn_info_kardex_docu" id="'+data+'"><i class="fa fa-plus"></i></button>';
                    }
                },
                { data: 'deta_kard_status',
                    "render":function(data){
                        var r;
                        if(data=='OK')
                            r='<span class="badge badge-success">'+data+'</span>';
                        else
                            r='<span class="badge badge-danger">'+data+'</span>';
                        return r;
                    }       

                },
                { data: 'docu_numero'},
                
               /* { data: 'lote_codigo',
                "render":function(data){                   
                        return '<span class="badge badge-warning">'+data+'</span>';                    
                } },*/
                { data: 'lote_fech_venc'},
                { data: 'deta_kard_cant_ingr',
                "render":function(data){
                    if(data !=null)
                        return '<span class="badge badge-primary">'+parseFloat(data).toFixed(2)+'</span>';
                    else 
                        return '';
                }},
                { data: 'deta_kard_cost_ingr',
                    "render":function(data){
                        if(data!=null)
                            return '$'+data;
                        else    
                            return '';
                    }
                },
                { data: 'deta_kard_cant_egre',
                "render":function(data){
                    if(data !=null)
                    return '<span class="badge badge-primary">'+parseFloat(data).toFixed(2)+'</span>';
                else 
                    return '';
                }},
                { data: 'deta_kard_cost_egre',
                "render":function(data){
                    if(data!=null)
                        return '$'+data;
                    else    
                        return '';
                }
                },
                { data: 'deta_kard_cant_sald',
                "render":function(data){
                    if(data !=null)
                    return '<span class="badge badge-success">'+parseFloat(data).toFixed(2)+'</span>';
                else 
                    return '';
                }},
                { data: 'deta_kard_cost_sald',
                "render":function(data){
                    if(data!=null)
                        return '$'+data;
                    else    
                        return '';
                }},
                { data: 'deta_kard_total',
                "render":function(data){
                    if(data!=null)
                        return '$'+data;
                    else    
                        return '';
                }},
            ],
            responsive: true,
            dom: 'lBfrtip',
            buttons: [
                'copy',  'excel',  'print',
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                }
            ]        
        });   
        
        $.get(model,{funcion:'ajuste_kardex',id:idx},function(data){
            //info_toast('Detalle Kardex');
        });
    }

    $(document).on('click', '.btn_info_kardex_docu', function(){          
        idx=$(this).attr("id"); //DOCU ID DE BOTON  
        //alert(idx+' '+idk);
        $.get(model,{funcion:'info_kardex_documento',id:idx,idk:idk},function(data){            
                swal({
                    title: '',                        
                    html:data,                        
                    focusConfirm: false,
                    confirmButtonText:
                      '<i class="fa fa-thumbs-up"></i> Cerrar'
                  });            
        });        
    });


    function combo_unidad(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#unid_id';
        $.getJSON(model,{funcion:'get_unidad'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Todas--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["unid_id"]).text(items["nick"]);
			});
        });  
    }

    combo_unidad();
    kardex(0);//ENVIO 0 AL LA CARGA INICIAL Y MUESTRA DE TODAS LA UNIDADES

    $("#unid_id").change(function() {
        table.destroy();
        unid_id=$('#unid_id').val();
            if($("#psico").is(':checked')) {  
           cambios=1;

        }else{
            cambios=0;            
        }    

        
   
        kardex(unid_id,cambios);
      });


          $("#psico").change(function() {
           table.destroy();
        if($("#psico").is(':checked')) {  
           cambios=1;

        }else{
            cambios=0;            
        }
        unid_id=$('#unid_id').val();
        kardex(unid_id,cambios);
        });

      
  
      $("#btn_imp_inv").click(function() {
        unid=$('#unid_id').val();
       // psico=$('#psico').val();  

if($("#psico").is(':checked')) {  
           psico=1;

        }else{
            psico=0;            
        }
 //alert('Seleccionado');


        if(unid=='0'){            
            warning_toast('Seleccione una Unidad..');
            return false;
          }
          else{
            window.open("model/mant_kardex/imp_inventario.php?unid="+unid+"&&psico="+psico); 
          }
    });


});