$(document).ready(function(){

    var model='model/mant_producto/funciones.php';
    var idx;
    var accion;

    //MAYUSCULA PRIMERA LETRA DE CADA PALABRA
    function ucwords(oracion){
        return oracion.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1){
           return $1.toUpperCase(); 
        });
    }
    
   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [   
            { data: 'prod_id' },          
            { data: 'prod_descripcion',
                "render": function (data){
                    return ucwords(data.toLowerCase());
                }
             },            
            { data: 'presen' },

            
            
                        { data: 'caducidad_registroSanitario',
            "render":function(data){
                var r;
                switch(data){
                    case '0':
                        r='<span class="badge badge-danger">No</span>';
                    break;
                    case '1':
                        r='<span class="badge badge-success">Si</span>';
                    break;
                   
                    default:
                        r='<span class="badge badge-warning">'+data+'</span>';                        
                }
                return r;
            }
        },
            
            
            { data: 'habilitado' },            
            { data: 'grupo_prod_nombre' }, 
            { data: 'prod_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true,
        dom: 'lBfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]    
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });





        $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
             swal({
            title: "Eliminar ?",
            text: "¿Está seguro que desea eliminar el elemento?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar !!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,            
            preConfirm: function() {
              return new Promise(function(resolve) {
                  $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {        
                        table.ajax.reload();                               
                        $(".btn_del").attr("disabled", false);   

                        if(data==0){
                            swal("Estado:", "No se pudo ELIMINAR el registro!!!", "error");
                        }else{
                          swal("Estado:", data, "success");  
                        }

                        
                     });   
              })
            }
          });
    });




        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
            $('#tipo_prod_id').val(json[0].tipo_prod_id);
            $('#prod_descripcion').val(json[0].prod_descripcion);            
            $('#prod_codigo').val(json[0].prod_codigo);
            $('#prod_status').val(json[0].prod_status);
            $('#grupo_prod_id').val(json[0].grupo_prod_id);
            $('#pres_id').val(json[0].pres_id);
            $('#caducidad_registroSanitario').val(json[0].caducidad_registroSanitario);

        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        
        
        
        var tipoProducto = $('#tipo_prod_id').val();
        var nombreProducto = $('#prod_descripcion').val();
        
        var presentacionProducto = $('#pres_id').val();
        var grupoProducto = $('#grupo_prod_id').val();
               

        if(tipoProducto.trim() == 0){
          //  alert(form_data);
         alert('Seleccione el tipo de producto.');
        $('#tipo_prod_id').focus();
        return false;
    }else if(nombreProducto.trim() == ''){
        alert('Debe ingresar el nombre del producto.');
        $('#prod_descripcion').focus();
        return false;
        
    }else if (grupoProducto.trim() == 0){
        alert('Debe ingresar el grupo del producto.');
        $('#grupo_prod_id').focus();
        return false;
    }else if(presentacionProducto.trim() == 0){
        alert('Debe ingresar la presentación del producto');
        $('#pres_id').focus();
        return false;
    }

    else
    {
$.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {                
                table.ajax.reload();    
                $("#btn_aceptar").attr("disabled", false);  
                $('#ModalNew').modal('hide');                   
                success_toast(data);
               
             });
    }
        
    });

    function combo_tipo(){
        //LLENO COMBOBOX POR DATOS JSON
        combo2='#tipo_prod_id';
        $.getJSON(model,{funcion:'get_tipo'})
        .done(function(json) {   
            $(combo2).empty();   

            $(combo2).append('<option value="" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo2).val(items["tipo_prod_id"]).text(items["tipo_prod_nombre"]);
            });
              
        });  
    }

    function combo_forma(){
        //LLENO COMBOBOX POR DATOS JSON
        combo3='#form_id';
        $.getJSON(model,{funcion:'get_forma'})
        .done(function(json) {   
            $(combo3).empty();   
            $(combo3).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo3).val(items["form_id"]).text(items["form_nombre"]);
            });
        });  
    }

    function combo_concentracion(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#conc_id';
        $.getJSON(model,{funcion:'get_concentracion'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo4).val(items["conc_id"]).text(items["conc_nombre"]);
            });
        });  
    }

  function combo_presentacion(){
        //LLENO COMBOBOX POR DATOS JSON
        combo6='#pres_id';
        $.getJSON(model,{funcion:'get_presentacion'})
        .done(function(json) {   
            $(combo6).empty();   
            $(combo6).append('<option value="0" selected="selected">--Seleccione Presentación--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo6).val(items["pres_id"]).text(items["pres_nombre"]);
            });
        });  
    }
    function combo_grupo(){
        //LLENO COMBOBOX POR DATOS JSON
        combo5='#grupo_prod_id';
        $.getJSON(model,{funcion:'get_grupo'})
        .done(function(json) {   
            $(combo5).empty();   
            $(combo5).append('<option value="0" selected="selected">--Seleccione Grupo--</option>');
            $.each(json, function (i, items) {
                 if(items !== null) $("<option>").appendTo(combo5).val(items["grupo_prod_id"]).text(items["grupo_prod_nombre"]);
            });
        });  
    }

    combo_tipo();
    combo_forma();
    combo_concentracion();
    combo_presentacion();
    combo_grupo();
   
    

});