$(document).ready(function(){

	function login(){
		var u = $("#u").val();
		var p = $("#p").val();		
		if(u=='' || p==''){
			danger_toast("Especifique Usuario y Password");
			return false;
		}
		
		$.ajax({
	            type: "POST",
	            url:"model/config/lg.php",
				data: {u:u,p:p},
				beforeSend: function () {
						$("#btnlg").attr("disabled", true);
						//("#mensaje").html("Verificando...");
					}
                }).done(function(a) {					
					$("#btnlg").attr("disabled", false);			
					if (a == 1)	
						window.location.href = 'inicio.php';
					else{
						//$("#mensaje").html("");
						danger_toast("Usuario o Password Incorrecto");
						$("#p").val('');
						$("#u").val('');
						$("#u").focus();
						}
				 });
	}
	$("#u").keypress(function(e) {        
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            $("#p").focus();
        }
	});
	
	$("#p").keypress(function(e) {        
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            login();
        }
    });

$('#btnlg').click(function(){
			login();  			
});
});