$(document).ready(function(){

    var model='model/mant_area/funciones.php';
    var idx;
    var accion;

   //TABLA DATOS
   var table=$('#tb_1').DataTable( {
        ajax: {
            url: model,
            type: "GET",
            data:{funcion:'get_all'},
            dataSrc: ''
        },     
        columns: [           
            { data: 'area_nombre' },         
            { data: 'aten_nombre' },
            { data: 'ah' },    
            { data: 'ad' },    
            { data: 'ai' },    
            { data: 'area_id' ,
                "render": function ( data ) {                                
                return '<button type="button" class="btn btn-secondary btn-xs m-l-5 btn_edit" id="'+data+'"><i class="fa fa-pencil"></i></button><button type="button" class="btn btn-secondary btn-xs m-l-5 btn_del" id="'+data+'"><i class="fa fa-trash"></i></button>';
                }
            }         
        ],
        responsive: true          
    }); 
  
    $(document).on('click', '.btn_new', function(){ 
        $('#form1').trigger("reset");
        accion='nuevo';
        idx='';        
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Nuevo"); 
    });

    $(document).on('click', '.btn_edit', function(){ 
        $('#form1').trigger("reset");
        accion='editar';
        idx=$(this).attr("id");
        $('#ModalNew').modal('show'); 
        $('.modal-title').text("Editar");
        editar(idx);
    });


    $(document).on('click','.btn_del', function(){
        accion='eliminar';
        idx=$(this).attr("id");        
        swal({
                title: "Eliminar ?",
                text: "Desea Eliminar la Seleccion ?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminar !!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:model,
                    type:"GET",            
                    data: {funcion:accion,id:idx},
                    beforeSend: function () {
                            $(".btn_del").attr("disabled", true);                    
                        }
                    }).done(function(data) {		
                        table.ajax.reload();		                       
                        $(".btn_del").attr("disabled", false);	                        
                        swal("Estado:", data, "success");
                     });                
            });
    });

        function editar(idx){
        $.getJSON(model,{id:idx,funcion:'get_by_id'})
        .done(function(json) {
        $('#area_nombre').val(json[0].area_nombre);
        $('#aten_id').val(json[0].aten_id);
        $('#area_hospitalizacion').val(json[0].area_hospitalizacion);
        $('#area_dosi_unit').val(json[0].area_dosi_unit);
        $('#area_ingreso').val(json[0].area_ingreso);
        });       
    }

    $('#form1').on("submit",function(event){
        event.preventDefault();
        //OBTIENE TODOS LOS DATOS DE LOS CAMPOS DEL FORMULARIO
        form_data=$(this).serialize();
        $.ajax({
            url:model,
            type:"GET",            
            data: {funcion:accion,id:idx,data:form_data},
            beforeSend: function () {
                    $("#btn_aceptar").attr("disabled", true);                    
                }
            }).done(function(data) {				
                table.ajax.reload();	
                $("#btn_aceptar").attr("disabled", false);	
                $('#ModalNew').modal('hide'); 	                
                success_toast(data);
               
             });
    });

    function combo_atencion(){
        //LLENO COMBOBOX POR DATOS JSON
        combo4='#aten_id';
        $.getJSON(model,{funcion:'get_aten'})
        .done(function(json) {   
            $(combo4).empty();   
            $(combo4).append('<option value="0" selected="selected">--Seleccione--</option>');
            $.each(json, function (i, items) {
				 if(items !== null) $("<option>").appendTo(combo4).val(items["aten_id"]).text(items["aten_nombre"]);
			});
        });  
    }
   
    combo_atencion();
    
    $("#btn_p1").click(function() {
        $.get(model,{funcion:'proceso1'},function(data){
            swal("", data, "success");
        });
      });
});