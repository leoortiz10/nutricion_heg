<?php
//include ('../config/db.php');
        //$this->con=$con; 
require 'model/funcs/conexion.php';
require 'model/funcs/funcs.php';

$errors = array ();
$band=0;
$cnf=array ();


	if(!empty($_POST))
	{
		$email = $mysqli->real_escape_string($_POST['email']);
		
		if(!isEmail($email))
		{
			$errors[] = "Debe ingresar un correo electrónico valido";
			$band=1;

		}
		
		if(emailExiste($email))
		{			

			$user_id = getValor('user_id', 'user_correo', $email);
			$nombre = getValor('user_nick', 'user_correo', $email);
			
			$token = generaTokenPass($user_id);
			
			//$url = 'http://138.10.0.75/medix/cambia_pass.php?user_id='.$user_id.'&token='.$token;
			$url = 'http://138.10.0.74/nutricion/cambia_pass.php?user_id='.$user_id.'&token='.$token;
			
			
			
			$asunto = 'Recuperar Password - Sistema de Usuarios';
			$cuerpo = "Hola $nombre: <br /><br />Se ha solicitado un reinicio de contrase&ntilde;a. <br/><br/>Para restaurar la contrase&ntilde;a, visita la siguiente direcci&oacute;n: <a href='$url'>$url</a>";
			
			if(enviarEmail($email, $nombre, $asunto, $cuerpo)){
				//echo "Hemos enviado un correo electronico a las direcion $email para restablecer tu password.<br />";
				//echo "<a href='index.html' >Iniciar Sesion</a>";

				$cnf[] = "Hemos enviado un correo electrónico a las direción $email para restablecer tu password.<br />
				<a href='index.html' >Iniciar Sesion</a>";

				//exit;
			}
			} else {
			$errors[] = "La dirección de correo electrónico no existe";
			$band=1;
		}
	}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Medix</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link href="assets/css/lib/toastr/toastr.min.css" rel="stylesheet">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/css/lib/bootstrap/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/icons/font-awesome/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/icons/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="assets/css/login/util.css">
<link rel="stylesheet" type="text/css" href="assets/css/login/main.css">
<!--===============================================================================================-->
</head>
<body style="background:#8ba987 url('assets/images/fondo_sistema_nutricion.jpg') no-repeat center center fixed;
        background-size:cover;">
	
	<div class="container" style="opacity:0.8;">
			
		<div class="row">
		  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card card-signin my-5">
			  <div class="card-body" >
						<!--<img src="assets/images/g.png" name="luna" style="width: 90px; position: absolute; bottom: 35px; right: 0; background-repeat: no-repeat; z-index: 10;">-->
						<div class="form-label-group" style="text-align: center">
							<h3><FONT FACE="impact" SIZE=6 COLOR="BLACK">RECUPERAR PASSWORD</FONT></h3>
<hr>
					
					
				  </div>
					<div class="text-center">
						<img src="assets/images/logo1.jpg">
					</div> 
					 <div class="form-label-group">
						<a href="index.html">Iniciar Sesión</a>
					
				  </div>
			<!--	<form class="form-signin">
				
	
				  <div class="form-label-group">
							<label for="inputPassword">Correo:</label>
					<input type="email" id="email" class="form-control" maxlength="20"  required>
					
				  </div>

				 
					
				  <button class="btn btn-lg btn-info btn-block text-uppercase" type="button" id="btnlgrecu">Enviar</button>-->
				 
				<!--	<hr class="my-4">
					<div style="text-align: center;color:aliceblue">
						<a href="PEDIDOS.pdf" target="_blank">	<img src="assets/images/pdf.png" width="50px"></a>
						<span class="badge bg-dark">PDF - TUTORIAL PEDIDOS</span>
						
					</div>			
				 
				</form>-->

				<form id="loginform" class="form-horizontal" role="form" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST" autocomplete="off">
							
							<div style="margin-bottom: 25px" class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
								<input id="email" type="email" class="form-control" name="email" placeholder="email" required>                                        
							</div>
							
							<div style="margin-top:10px" class="form-group">
								<div class="col-sm-12 controls">
									<button id="btn-login" type="submit" class="btn btn-success">Enviar</a>
								</div>
							</div>
							
						<!--	<div class="form-group">
								<div class="col-md-12 control">
									<div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
										No tiene una cuenta! <a href="registro.php">Registrate aquí</a>
									</div>
								</div>
							</div>  -->  
						</form>

			 <?php echo resultBlock($errors); ?>
			 <?php echo resultBlockCnf($cnf); ?>
				

			  </div>
			</div>
		  </div>
		</div>
	  </div>
	
<!--===============================================================================================-->
<script src="assets/js/lib/jquery/jquery.min.js"></script>


<!--===============================================================================================-->
	<script src="assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/js/lib/bootstrap/js/popper.min.js"></script>
	<script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="assets/js/lib/toastr/toastr.min.js"></script>
<!-- scripit init-->
<script src="assets/js/lib/toastr/toastr.init.js"></script>
<!--Custom JavaScript -->
<script src="assets/js/custom.min.js"></script>
<script src="assets/js/login/main.js"></script>
<script src="assets/js/toasts.js"></script>


    <script src="controller/lg.js"></script>
</body>
</html>