
$(document).ready(function(){
    //CARGA EL CONTENIDO DE LOS MENUS DINAMICANTE
    $('.link_menu').click(function(event){
        link=$(this).attr("href"); 
        $.ajax({
            type:'get',
            url:link,
            async: false,
        })
        .done(function(html){
            $("#principal_content").fadeOut( 100 , function() {
                $('#principal_content').empty().append(html);
            }).fadeIn( 500 );           
        })
        return false;
    });  
});