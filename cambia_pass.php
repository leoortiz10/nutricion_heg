<?php
	require 'model/funcs/conexion.php';
	include 'model/funcs/funcs.php';
	session_start();

	$cnfin=array ();
	
	if(empty($_GET['user_id'])){
		header('Location: index.html');
	}
	
	if(empty($_GET['token'])){
		header('Location: index.html');
	}
	
	$user_id = $mysqli->real_escape_string($_GET['user_id']);
	$token = $mysqli->real_escape_string($_GET['token']);
	
	if(!verificaTokenPass($user_id, $token))
	{
		echo 'No se pudo verificar los Datos';
		exit;
	} 

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cambiar Password</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link href="assets/css/lib/toastr/toastr.min.css" rel="stylesheet">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/css/lib/bootstrap/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/icons/font-awesome/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/icons/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="assets/css/login/util.css">
<link rel="stylesheet" type="text/css" href="assets/css/login/main.css">
<!--===============================================================================================-->
</head>
<body style="background:#8ba987 url('assets/images/fondo_sistema_nutricion.jpg') no-repeat center center fixed;
        background-size:cover;">
	
	<div class="container" style="opacity:0.8;">
			
		<div class="row">
		  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card card-signin my-5">
			  <div class="card-body" >
						<!--<img src="assets/images/g.png" name="luna" style="width: 90px; position: absolute; bottom: 35px; right: 0; background-repeat: no-repeat; z-index: 10;">-->
						<div class="form-label-group" style="text-align: center">
							<h3><FONT FACE="impact" SIZE=6 COLOR="BLACK">RECUPERAR PASSWORD</FONT></h3>
<hr>
					
					
				  </div>
					<div class="text-center">
						<img src="assets/images/logo1.jpg">
					</div> 
					 <div class="form-label-group">
						<a href="index.html">Iniciar Sesión</a>
					
				  </div>


				<form id="loginform" class="form-horizontal" role="form" action="guarda_pass.php" method="POST" autocomplete="off">
							
			<input type="hidden" id="user_id" name="user_id" value ="<?php echo $user_id; ?>" />
							
	        <input type="hidden" id="token" name="token" value ="<?php echo $token; ?>" />
							
							<div class="form-group">
								<label style="color: #2D75EA;" for="password" class="col-md-6 control-label">Nuevo Password</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="password" placeholder="Password" required>
								</div>
							</div>
							
							<div class="form-group">
								<label style="color: #2D75EA;" for="con_password" class="col-md-6 control-label">Confirmar Password</label>
								<div class="col-md-9">
									<input type="password" class="form-control" name="con_password" placeholder="Confirmar Password" required>
								</div>
							</div>
							
							<div style="margin-top:10px" class="form-group">
								<div class="col-sm-12 controls">
									<button id="btn-login" type="submit" class="btn btn-success">Modificar</a>
								</div>
							</div>   
						</form>

			<?php echo resultBlockCnfFinal($cnfin); ?>
				

			  </div>
			</div>
		  </div>
		</div>
	  </div>
	
<!--===============================================================================================-->
<script src="assets/js/lib/jquery/jquery.min.js"></script>


<!--===============================================================================================-->
	<script src="assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/js/lib/bootstrap/js/popper.min.js"></script>
	<script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="assets/js/lib/toastr/toastr.min.js"></script>
<!-- scripit init-->
<script src="assets/js/lib/toastr/toastr.init.js"></script>
<!--Custom JavaScript -->
<script src="assets/js/custom.min.js"></script>
<script src="assets/js/login/main.js"></script>
<script src="assets/js/toasts.js"></script>


    <script src="controller/lg.js"></script>
</body>
</html>