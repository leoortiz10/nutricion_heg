
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">

                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Producto Nombre</th>
                                                <th>Lote Codigo</th>
                                                <th>Registro Sanitario</th>
                                                <th>Fecha Vencimiento</th>
                                                <th>Estado</th>
                                                
                                               <!-- <th width="3%">Editar</th> -->
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                      
                       
                    </div>
                </div>
                <!-- End PAge Content -->
                  
                <form method="GET" id="form1">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">                                 
                                        <h2><span class="badge badge-info" id="producto" name="producto"></span></h2>
                                    </div>
                                </div>                                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Lote Codigo:</label>
                                        <input type="text" id="lote_codigo" name="lote_codigo" class="form-control">
                                    </div>
                                </div>                               
                            </div>                      
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha Vencimiento:</label>
                                        <input type="text" id="lote_fech_venc" name="lote_fech_venc" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Registro Sanitario:</label>
                                        <input type="text" id="lote_regi_sani" name="lote_regi_sani" class="form-control">
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
      <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_lote.js"></script>
    
