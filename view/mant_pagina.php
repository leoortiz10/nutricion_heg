
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                        <button type="button" id="btn_nuevo" class="btn btn-info btn-rounded m-b-10 m-l-5 btn_new">+Nuevo</button>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Pagina</th>                                           
                                                <th>Url</th> 
                                                <th>Codigo Padre Raiz</th> 
                                                <th>Pagina Hijo Raiz</th>                                                
                                                <th>Orden</th> 
                                                <th>Icono</th>
                                                <th width="6%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                      
                       
                    </div>
                </div>
                <!-- End PAge Content -->
                  
                <form method="GET" id="form1">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pagina</label>
                                        <input type="text" id="pagi_nombre" name="pagi_nombre" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Url</label>
                                        <input type="text" id="pagi_url" name="pagi_url" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                     
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Codigo Padre Razi</label>
                                        <input type="text" id="pagi_codigo" name="pagi_codigo" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pagina Raiz</label>
                                        <select class="form-control " id="pagi_raiz" name="pagi_raiz">
                                                                                               
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                           
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Prioridad Orden</label>
                                            <input type="text" id="pagi_prioridad" name="pagi_prioridad" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Icono</label>
                                            <input type="text" id="pagi_icono" name="pagi_icono" class="form-control">
                                        </div>
                                    </div>
                                    
                                </div>
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
      <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_pagina.js"></script>
    
