
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                        <button type="button" id="btn_nuevo" class="btn btn-info btn-rounded m-b-10 m-l-5 btn_new">+Nuevo</button>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Unidad Nombre</th>
                                                <th>Hospital</th>
                                                <th>Permite Receta</th>                                                                                         
                                                <th>Unidad de Pedido</th>   
                                                <th width="6%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                      
                       
                    </div>
                </div>
                <!-- End PAge Content -->
                  
                <form method="GET" id="form1">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unidad Nombre</label>
                                        <input type="text" id="unid_nombre" name="unid_nombre" class="form-control">
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nick de la Unidad</label>
                                        <input type="text" id="nick" name="nick" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                    </div>
                                </div>
                                
                                                          
                            </div>
                            <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Siglas</label>
                                        <input type="text" id="unid_siglas" name="unid_siglas" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();">
                                    </div>
                                </div>

                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Receta</label>
                                        <select class="form-control " id="unid_agrega_receta" name="unid_agrega_receta">
                                            <option value="">Elige una opción</option>
                                            <option value="0">NO</option>       
                                            <option value="1">SI</option>                                               
                                        </select>
                                    </div>
                                </div>  
                           
                            </div>    

                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unidad Pedido</label>
                                        <select class="form-control " id="unid_pedido" name="unid_pedido" >
                                            <option value="">Elige una opción</option>
                                            <option value="0">NO</option>       
                                            <option value="1">SI</option>                                               
                                        </select>
                                    </div>
                                </div>   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Hospital</label>
                                        <select class="form-control " id="hosp_id" name="hosp_id">
                                                                                    
                                        </select>
                                    </div>
                                </div>
                            </div>                  
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
      <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_unidad.js"></script>
    
