
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                    <form>
                                                <div class="form-group row">
                                                    
                                                    <label for="unid_id_s" class=" col-md-1 col-form-label">Pedido a:</label>
                                                    <div class="col-md-3">
                                                            <select class="form-control " id="unid_id_s" name="unid_id_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <label for="unid_id_destino_s" class=" col-md-1 col-form-label">Para:</label>
                                                    <div class="col-md-3">
                                                        <select class="form-control " id="unid_id_destino_s" name="unid_id_destino_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                   
                                                </div>                             
                                    </form>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_3_ped" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                                                                               
                                                <th>Numero</th>                                               
                                                <th>Pedido a</th>
                                                <th>Para</th>
                                                <th>Fecha</th>                                                
                                                <th>Autorizado Por</th>
                                                <th>Pedido Por</th>                                              
                                                <th>Estado Pedido</th>                                                
                                                <th></th>                                                 
                                                <th width="10%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
                <!-- End PAge Content -->
                  
        
<div class="modal fade" id="ModalDetallePedido" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
             <div class="modal-header">
                <h2 class="modal-title" id="ModalTitlePedido"><span class="badge badge-danger"></span></h2>
                <span class="badge badge-info" id="mtitulo1p"></span><br>
                <span class="badge badge-info" id="mtitulo2p"></span><br>
                <span class="badge badge-info" id="mtitulo3p"></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body body-full-color" >



                <div class="row">
                
                    <div class="col-8">
                        <div class="card">                                    
                            <div class="card-body" >          
                            <h4 class="card-title info_unidad">LISTA DE PEDIDOS</h4>                                                 
                                <div class="table-responsive">                                    
                                    <table id="tb_4_ped_ver" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                
                                                <th >Producto</th>                                                                                     
                                                <th >Cantidad Pedido</th>                   
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
            </div>
            
            
          
            <div class="modal-footer">           
     
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cerrar</button>            
  
            </div>
    </div>
  </div>
</div>


 <!-- MODAL NUEVO Y EDITAR-->               
  


    <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_docu_pedido_ver.js"></script>
    
