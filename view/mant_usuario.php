
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                        <button type="button" id="btn_nuevo" class="btn btn-info btn-rounded m-b-10 m-l-5 btn_new">+Nuevo</button>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Usuario</th>
                                                <th>Correo</th>                                              
                                                <th>Estado</th>                                                 
                                                <th>Nick</th>   
                                                <th>Unidad</th>                                                                                             
                                               
                                                <th>Area</th> 
                                                <th width="6%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                      
                       
                    </div>
                </div>
				
				<form method="GET" id="form3">
				
				<div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					      <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                      <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Usuario</label>
                                        <input type="text" id="user_nombre_up" name="user_nombre_up" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="text" id="user_correo_up" name="user_correo_up" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
							
							                            <div class="row">
                         
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <select class="form-control " id="stat_id_up" name="stat_id_up">
                                                                                               
                                        </select>
                                    </div>
                                </div>
								
								    <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nick</label>
                                                <input type="text" id="user_nick_up" name="user_nick_up" class="form-control">
                                            </div>
                                    </div>
                               
                                <!--/span-->
                            </div>
                            <div class="row">
                                
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label >Unidad</label>                                     
                                                    <select class="form-control " id="unid_id_up" name="unid_id_up">
                                                                                                      
                                                    </select>                                                
                                            </div>
                                    </div>
									
									   <div class="col-md-6">
                                        <div class="form-group">
                                               
                                        <label >Hospital</label>                                     
                                            <select class="form-control " id="hosp_id_up" name="hosp_id_up">
                                                                                               
                                            </select>
                                        </div>
                                    </div>
                            </div>
							
							      <div class="row">
                                 
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                   
                                            <label >Area</label>                                     
                                                <select class="form-control " id="area_id_up" name="area_id_up">
                                                                                                     
                                                </select>
                                            </div>
                                   </div>
                            </div>
                      
                            
                         
                        
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>
				
				</div>
				
				
				
				
				</form>
				
				
                <!-- End PAge Content -->
                  
                <form method="GET" id="form1">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Usuario</label>
                                        <input type="text" id="user_nombre" name="user_nombre" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="text" id="user_correo" name="user_correo" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Clave</label>
                                        <input type="password" id="user_clave" name="user_clave" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <select class="form-control " id="stat_id" name="stat_id">
                                                                                               
                                        </select>
                                    </div>
                                </div>
                               
                                <!--/span-->
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nick</label>
                                                <input type="text" id="user_nick" name="user_nick" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label >Unidad</label>                                     
                                                    <select class="form-control " id="unid_id_us" name="unid_id">
                                                                                                      
                                                    </select>                                                
                                            </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                               
                                        <label >Hospital</label>                                     
                                            <select class="form-control " id="hosp_id" name="hosp_id">
                                                                                               
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                   
                                            <label >Area</label>                                     
                                                <select class="form-control " id="area_id" name="area_id">
                                                                                                     
                                                </select>
                                            </div>
                                   </div>
                            </div>
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 




                <form method="GET" id="form2">

    <div class="modal fade" id="ModalNewUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Clave</label>
                                        <input type="password" id="user_clave_2" name="user_clave_2" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                           
                               
                                <!--/span-->
                            </div>
                      
                 
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 


      <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_usuario.js"></script>
    
