                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-20">                                                  
                                             <div class="form-group row">
                                                <label for="unid_id" class="col-sm-2 col-form-label">Unidad:</label>
                                                <div class="col-sm-6">
                                                            <select class="form-control " id="unid_id" name="unid_id">
                                                                                                
                                                            </select>
                                                </div>  
                                            </div>  
                                            <div class="form-group row">     
                                                <label for="prod_id" class="col-sm-2 col-form-label">Producto:</label>
                                                <div class="col-sm-6">
                                                            <select class="form-control " id="prod_id" name="prod_id">
                                                                                                
                                                            </select>
                                                </div>                                                                                              
                                            </div>                                                               
                        </div>
                    </div>
                </div>

                 <div class="row">
                    <div class="col-md-12">
                        <div class="card p-20"> 

                              
                                        <!--INICIO 1 -->
                                            <div class="form-group row">     
                                                <label for="lotes_info" class="col-sm-2 col-form-label">Lote Origen:</label>
                                                <div class="col-sm-10">
                                                    <div id="lotes_info"></div>
                                                </div>                                                                                              
                                            </div>   
                                            <hr>  
                                            <div class="form-group row">     
                                                <label for="prod_id" class="col-sm-2 col-form-label">Movimientos Lotes:</label>
                                                <div class="col-sm-10">
                                                    <div id="lote_movimientos"></div>
                                                </div>                                                                                              
                                            </div>    
                                            <div class="form-group row">     
                                                <label for="prod_id" class="col-sm-2 col-form-label">Suma Seleccion:</label>
                                                <div class="col-sm-4">
                                                <input type="text" class="form-control" id="suma_cant" readonly>
                                                </div>                                                                                              
                                            </div>    
                                            <div class="form-group row">     
                                                <label for="lotes_info_cruzar" class="col-sm-2 col-form-label">Cruzar Por Lote:</label>
                                                <div class="col-sm-10">
                                                    <div id="lotes_info_cruzar"></div>
                                                </div>                                                                                              
                                            </div>  
                                            <hr>                                                                  
                                            <div class="form-group row">                                                 
                                                <label class="col-sm-2 col-form-label"></label>
                                                <div class="col-sm-4">
                                                    <button type="button" id="btn_cruze" class="btn btn-inverse m-l-5"><span class="fa fa-cogs fa-lg" aria-hidden="true"></span> Realizar Cruze Lotes</button>                                                   
                                                </div>
                                            </div>
                                            <div class="form-group row">     
                                                <label for="prod_id" class="col-sm-2 col-form-label">RESULTADOS:</label>
                                                <div class="col-sm-4">
                                                 <div id="resultados"></div>
                                                </div>                                                                                              
                                            </div>                             

                        </div>
                    </div>                      
                 </div>

                
                
                
    <script src="controller/ajuste_lotes.js"></script>           
              
               
      