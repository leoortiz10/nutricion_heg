
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                    <form>
                                                <div class="form-group row">
                                                    
                                                    <label for="unid_id_s" class=" col-md-1 col-form-label">Pedido a:</label>
                                                    <div class="col-md-3">
                                                            <select class="form-control " id="unid_id_s" name="unid_id_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <label for="unid_id_destino_s" class=" col-md-1 col-form-label">Para:</label>
                                                    <div class="col-md-3">
                                                        <select class="form-control " id="unid_id_destino_s" name="unid_id_destino_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                            <button type="button" id="btn_nuevo_pedido" class="btn btn-info btn-rounded m-l-5 btn_new"><i class="ti-file"></i> Nuevo Pedido</button>                                                         
                                                    </div> 
                                                </div>                             
                                    </form>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_3_ped" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                                                                               
                                                <th>Numero</th>                                               
                                                <th>Pedido a</th>
                                                <th>Para</th>
                                                <th>Fecha</th>                                                
                                                <th>Autorizado Por</th>
                                                <th>Pedido Por</th>                                              
                                                <th>Estado Pedido</th>                                                
                                                <th></th>                                                 
                                                <th width="10%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
                <!-- End PAge Content -->
                  
        
<div class="modal fade" id="ModalDetallePedido" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
             <div class="modal-header">
                <h2 class="modal-title" id="ModalTitlePedido"><span class="badge badge-danger"></span></h2>
                <span class="badge badge-info" id="mtitulo1p"></span><br>
                <span class="badge badge-info" id="mtitulo2p"></span><br>
                <span class="badge badge-info" id="mtitulo3p"></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body body-full-color" >



                <div class="row">
    <div class="col-6">
                        <div class="card">
                                   
                            <div class="card-body" >        
                            <h4 class="card-title info_unidad">SELECCIONAR</h4>                                                    
                                <div class="table-responsive">
                                    <table id="tb_5_stock" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                
                                                <th >Producto</th>                                                                                     
                                                <th >Stock en Bodega</th>
                                                <!--<th >Mi Stock Actual</th>-->
                                                <th ></th>
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                    <div class="col-6">
                        <div class="card">                                    
                            <div class="card-body" >          
                            <h4 class="card-title info_unidad">LISTA DE PEDIDOS</h4>                                                 
                                <div class="table-responsive">                                    
                                    <table id="tb_4_ped" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                
                                                <th >Producto a Pedir</th>                                                                                     
                                                <th >Cantidad Pedido</th>                                                
                                                <th ></th>                                               
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
            </div>
            
            
          
            <div class="modal-footer">           
     
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cerrar</button>            
  
            </div>
    </div>
  </div>
</div>


 <!-- MODAL NUEVO Y EDITAR-->               
 <form method="GET" id="form22p">

    <div class="modal fade" id="ModalNewPedido"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalNewTitlePedido"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">    
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha Pedido:</label>
                                        <input type="text" id="docu_fecha" name="docu_fecha" class="form-control" required>
                                    </div>
                                </div>                             
                                <div class="col-md-6">
                                    <div class="form-group">                                    
                                        <label>Tipo Documento:</label>
                                        <select class="form-control " id="tipo_docu_id" name="tipo_docu_id" >
                                           
                                        </select>
                                    </div>
                                </div>                              
                            </div>                      
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pedir A:</label>
                                        <select class="form-control " id="unid_id" name="unid_id" >
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Para:</label>
                                        <select class="form-control " id="unid_id_destino" name="unid_id_destino" >
                                           
                                        </select>
                                    </div>
                                </div>                              
                            </div>   
                            <div class="row">                                                             
                                <div class="col-md-6">
                                    <div class="form-group">                                    
                                        <label>Documento Numero:</label>
                                        <input readonly type="text" id="docu_numero" name="docu_numero" class="form-control" required>                                  
                                    </div>
                                </div>  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Autorizado Por:</label>
                                        <input type="text" id="docu_autoriza" name="docu_autoriza" class="form-control" required>
                                    </div>
                                </div>                             
                            </div>   
                            <div class="row">                                  
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Pedido Por:</label>
                                        <input type="text" id="docu_entrega" name="docu_entrega" class="form-control" required>
                                    </div>
                                </div>    
                                                             
                            </div>                            
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Observación:</label>
                                        <input type="text" id="docu_observacion" name="docu_observacion" class="form-control" required>
                                    </div>
                                </div>                                                         
                            </div>  
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_save_pedido">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
           


    <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_docu_pedido.js"></script>
    
