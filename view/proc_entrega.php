
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-5">
                        <div class="card mb-0" >
                        
                            <div class="card-body mb-0" >                                                           
                                    <form> 
                                                <div class="form-group row">                                                    
                                                    <label for="unid_id_s" class=" col-md-3 col-form-label">Unidad:</label>
                                                    <div class="col-md-9 ">
                                                            <select class="form-control  input-focus" id="unid_id" name="unid_id">
                                                                                                
                                                            </select>
                                                    </div>                                                    
                                                </div>                             
                                    </form>
                            </div>
                        </div>               
                    </div>
                    <div class="col-lg-7">
                        <div class="card mb-0">                               
                            <div class="card-body mb-0">                                                           
                            <form> 
                                                <div class="form-group row ">                                                    
                                                    <label for="unid_id_s" class=" col-md-2 col-form-label">Turno:</label>
                                                    <div class="col-md-6   ">
                                                            <select class="form-control  input-focus" id="turn_id" name="turn_id">
                                                                                                
                                                            </select>
                                                    </div>              
                                                    <button type="button" id="btn_cerrar_turno" class="btn btn-success btn-rounded m-l-5 btn_cerrar_turno"><i class="ti-lock"></i> Cerrar Turno</button>
                                                </div>                             
                                    </form>
                            </div>
                        </div>               
                    </div>
                </div>

                <div class="row">
                
                    <div class="col-lg-5">
                        <form method="GET" id="formu1"> 
                            <div class="card">                              
                            <div class="card-body">                                                           
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label" for="noen_fecha">Fecha:</label>
                                            <div class="col-lg-6">
                                            
                                            <input type="text" class="form-control input-rounded input-focus" id="noen_fecha" name="noen_fecha"  required  >
                                          
                                               
                                            </div>
                                            
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label" for="noen_ci">Cedula:</label>
                                            <div class="col-lg-6">
                                                <!--<input type="text" class="form-control input-rounded input-focus" id="noen_ci" name="noen_ci"  required autofocus >-->                                                                                             
                                                    <input type="text" id="noen_ci" name="noen_ci" class="form-control typeahead tt-query input-rounded input-focus" autocomplete="off">                                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label" for="noen_historia_clinica">HC:</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control input-rounded input-focus" id="noen_historia_clinica" name="noen_historia_clinica" required autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label" for="noen_nombre">Apellidos y Nombres:</label>
                                            <div class="col-lg-9">
                                                <input type="text" class="form-control input-rounded input-focus" id="noen_nombre" name="noen_nombre" required autocomplete="off" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label" for="area_id">Servicio:</label>
                                            <div class="col-lg-9">
                                                    <select class="form-control input-rounded " id="area_id" name="area_id">
                                                                                                
                                                    </select>          
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-3 col-form-label" for="medi_id">Medico:</label>
                                            <div class="col-lg-9">
                                                            <select class="form-control input-rounded " id="medi_id" name="medi_id">
                                                                                                
                                                             </select>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="float-right">
                                <button type="submit" id="btn_agregar_orden" class="btn btn-warning btn-rounded m-l-5 btn_agregar_orden"><i class="ti-pencil-alt"></i> Agregar Receta</button>
                                <button type="button" id="btn_limpiar" class="btn btn-info btn-rounded m-l-5 btn_limpiar"><i class="ti-eraser"></i> Limpiar</button>
                                             
                                     </div> 
                            </div>
                        </div> 
                        </form>              
                    </div>
               
                    <div class="col-lg-7">
                     <form method="GET" id="form2"> 
                        <div class="card">                               
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tbreceta_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Receta</th>
                                                <th>Fecha</th>
                                                <th>Cedula</th>
                                                <th>Paciente</th>
                                                <th>Total</th>
                                                <th>Estado</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>               
                     </form>
                    </div>
                </div>

                <div class="row">
                     <div class="col-lg-5">
                        <div class="card">                              
                            STOCK PRODUCTOS AL INSTANTE:
                            <div class="card-body">                                 
                                <div class="table-responsive">
                                    <table id="tbstocks_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                              
                                                <th>Producto</th>                                                
                                                <th width="5%">Stock</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="card">                              
                        PRODUCTOS ENTREGADOS EN ÉSTE TURNO:
                            <div class="card-body">                                 
                                <div class="table-responsive">
                                    <table id="tbentregados_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                               
                                                <th>Producto</th>
                                                <th width="5%">Entreg</th>                                                
                                                <th width="5%">Dispon</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

               
                <!-- End PAge Content -->
                  
  <form method="GET" id="formu2">

    <div class="modal fade" id="ModalNewReceta"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog modal-full-receta" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title-receta" id="exampleModalReceta"><span id="titulo_receta" class="badge badge-inverse" style="color:#FFF"><span></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body body-full-color" >

               

                <div class="form-body">                 
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card">                              
                            <div class="card-body">                                 
                                <div class="row">
                                    <div class="col-md-12">
                                            <div class="form-group row">
                                                    <label class="col-lg-2 col-form-label" for="noen_fecha">N.-</label>
                                                    <div class="col-lg-10">
                                                        <h2><span class="badge badge-warning" id="num_receta"><span></h2>                                                       
                                                    </div>
                                            </div>                                   
                                    </div>
                                    <div class="col-md-12">
                                     <div class="form-group row">
                                            <label class="col-lg-2 col-form-label" for="noen_fecha">Item</label>
                                            <div class="col-lg-8">
                                                <!--<input type="text" class="form-control input-focus" id="prod_nombre" name="prod_nombre">-->
                                                <input type="text" class="form-control input-focus" id="prod_nombre" name="prod_nombre" autocomplete="off">
                                                
                                                <input type="hidden" id="prod_id" name="prod_id">
                                                <input type="hidden" id="deno_precio" name="deno_precio">
                                            </div>
                                            <div class="col-lg-2">
                                            <button type="button" id="btn_limp_prod" title="Limpiar Texto Producto" class="btn btn-info"><i class="ti-eraser"></i></button>
                                                </div>
                                     </div>                                   
                                    </div>  
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-lg-2 col-form-label" for="stock">Stock</label>                                            
                                            <div class="col-lg-4">
                                                <input type="text" id="stock" name="stock" class="form-control input-focus" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                         <div class="form-group row">
                                            <label class="col-lg-2 col-form-label" for="noen_fecha">Cant</label>
                                            <div class="col-lg-4">
                                                <input type="text" id="deno_cantidad" name="deno_cantidad" class="form-control input-focus" autocomplete="off" >
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <button type="submit" id="btn_agregar_item" class="btn btn-inverse" >Agregar Item</button>
                                            </div>
                                        </div>
                                     </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="card">                              
                            <div class="card-body"> 
                                <div class="table-responsive">
                                    <table id="tbrecetadetalle_2" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Item</th>
                                                <th>Cant</th>
                                                <th>Precio</th>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-inverse" id="btn_guardar_receta">Guardar e Imprimir [CTRL+ENTER]</button>  
            </div>
          </div>
        </div>

    </div>   
</form> 
  
      <script src="assets/js/toasts.js"></script>
    <script src="controller/proc_entrega.js"></script>
 


    
