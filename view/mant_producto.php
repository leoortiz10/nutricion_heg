
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                        <button type="button" id="btn_nuevo" class="btn btn-info btn-rounded m-b-10 m-l-5 btn_new">+Nuevo</button>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                               <!-- <th>Tipo</th> -->
                                               <th>ID</th>
                                               <th>Nombre</th>                              
                                               <th>Presentación</th>
                                               <th>Registro_Caducidad</th>
                                               <!-- <th>Proveedor</th>-->
                                                
                                               <!-- <th>Codigo Marco</th>-->
                                                
                                                <th>Habilitado</th>
                                                <th>Grupo</th>
                                                <th width="6%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>
                <!-- End PAge Content -->
                  
                <form method="GET" id="form1">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tipo Producto:</label>
                                        <select class="form-control " id="tipo_prod_id" name="tipo_prod_id" title="Seleccione el tipo de producto" >                                                                                        
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Código:</label>
                                        <input title="Aquí debe ingresar el código del producto que va a ingresar" type="text" id="prod_codigo" name="prod_codigo" class="form-control" placeholder="Ingrese el código del producto">
                                    </div>
                                </div>                               
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nombre Producto</label>
                                        <input  title="Aquí debe ingresar el nombre del producto a ingresar"  type="text" id="prod_descripcion" name="prod_descripcion" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Ingrese el nombre del producto">
                                    </div>
                                </div>                                                         
                            </div>

                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Grupo del Producto</label>                                       
                                        <select class="form-control " id="grupo_prod_id" name="grupo_prod_id" title="Debe seleccionar el grupo al que pertenece el producto">
                                                                                           
                                         </select>
                                    </div>
                                </div>   
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Presentación:</label>
                                        <select class="form-control " id="pres_id" name="pres_id" title="Debe seleccionar la presentación del producto">
                                                                                           
                                        </select>
                                    </div>
                                </div>   
                                                                               
                            </div>
                          
                       
                        
                            <div class="row">
                                <!--<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Código Convenio Marco:</label>
                                        <input type="text" id="prod_codi_conv_marc" name="prod_codi_conv_marc" class="form-control">
                                    </div>
                                </div>-->

                              
                                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Caducidad y Registro Sanitario:</label>
                                   
                            <select class="form-control " id="caducidad_registroSanitario" name="caducidad_registroSanitario">
                                            <option value="">Seleccione una opción</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                                                                               
                                        </select>
                                    </div>
                                </div>  

                                <div class="col-md-6">
                                    
                                    <!--    <select class="form-control " id="prod_conv_marc" name="prod_conv_marc">
                                        <label>Convenio Marco:</label>
                                        

                                            <option value="0">NO</option>       
                                            <option value="1">SI</option>                                         
                                        </select>-->

                                             <div class="form-group">
                                        <label>Habilitado:</label>
                                        <input type="hidden" id="prod_conv_marc" name="prod_conv_marc" class="form-control" value="0">
                                        <select class="form-control " id="prod_status" name="prod_status" title="Seleccione si el producto está habilitado">
                                            <option value="0">Si</option>
                                            <option value="1">No</option>                                                   
                                        </select>
                                    </div>
                                    
                                </div>                               
                            </div>
                                                    
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
      <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_producto.js"></script>
    
