
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                    <form>
                                                <div class="form-group row">
                                                    <label for="tipo_docu_id" class=" col-md-1 col-form-label">Tipo:</label>
                                                    <div class="col-md-2">
                                                        <select class="form-control " id="tipo_docu_id_s" name="tipo_docu_id_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <label for="unid_id" class=" col-md-1 col-form-label">Unidad:</label>
                                                    <div class="col-md-4">
                                                            <select class="form-control " id="unid_id_s" name="unid_id_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                            <button type="button" id="btn_new_egresos_nutricion" class="btn btn-info btn-rounded m-l-5 btn_new_egresos_nutricion"><i class="ti-file"></i> Nuevo Egreso</button>                                                         
                                                    </div> 
                                                </div>                             
                                    </form>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_3" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Documento</th>
                                                <th>Numero</th>                                               
                                                <th>Unidad</th>
                                                <th>Fecha Egreso</th>  
                                                <th>Autoriza</th>
                                                <th>Entrega</th>                                              
                                                <th>Recibe</th>
                                                <th>Estado</th>
                                                <th width="10%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
                <!-- End PAge Content -->
                  
        
<div class="modal fade bd-example-modal-lg" id="ModalDetalleIngreso" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
             <div class="modal-header">
                <h2 class="modal-title" id="ModalTitleIngreso"><span class="badge badge-danger"></span></h2>
                <span class="badge badge-info" id="mtitulo1"></span><br>
                <span class="badge badge-info" id="mtitulo2"></span><br>
                <span class="badge badge-info" id="mtitulo3"></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body body-full-color">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                    <div style="text-align:right" >
                                        <button type="button" id="btn_agregar" class="btn btn-info btn-rounded m-l-5 btn_agregar"><i class="ti-plus"></i> Agregar Producto</button>                                                         
                                    </div> 
                            <div class="card-body" >                                                           
                                <div class="table-responsive">
                                    <table id="tb_4" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                
                                                <th >Producto</th>
                                                <th >Presentación</th>
                                                <!--<th >*LOTE</th>
                                                <th >Fecha Caduca</th>
                                                <th >Reg. Sani</th>-->
                                                <th >Cant</th>
                                                <th >V. Unit</th>
                                                <th >IVA%</th>
                                                <th >Valor IVA</th>
                                                <th >Subtotal</th>                                                
                                                <th ></th> 
                                              
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
            </div>
            
            
          
            <div class="modal-footer">           
     
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cerrar</button>            
  
            </div>
    </div>
  </div>
</div>


 <!-- MODAL NUEVO Y EDITAR-->               
 <form method="GET" id="form22">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalNewTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                     

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <!--<label>Fecha:</label>-->
                                       <input type="hidden" id="docu_fecha" name="docu_fecha" class="form-control" required>
                                         <div class="form-group">
                                        <label>Tipo Documento:</label>
                                        <select class="form-control " id="tipo_docu_id" name="tipo_docu_id" >
                                           
                                        </select>
                                    </div>
                                    </div>
                                </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unidad Origen:</label>
                                        <select class="form-control " id="unid_id" name="unid_id" >
                                           
                                        </select>
                                    </div>
                                </div>                              
                            </div>                          
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Documento Numero:</label>
                                        <input readonly type="text" id="docu_numero" name="docu_numero" class="form-control">
                                    </div>
                                </div>

                                      <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Area Egreso:</label>
                                        <select class="form-control " id="area_id" name="area_id" >
                                           
                                        </select>
                                    </div>
                                </div>   
                                                       
                            </div>   
                            <div class="row">                                 
                          
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Proveedor Egreso:</label>
                                        <select class="form-control " id="prov_id" name="prov_id" >
                                           
                                        </select>
                                    </div>
                                </div>    

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Autorizado Por:</label>
                                        <input type="text" id="docu_autoriza" name="docu_autoriza" class="form-control" placeholder="¿Quién autoriza...?">
                                    </div>
                                </div>                           
                            </div>   
                            <div class="row">                              
                                                             
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Entregado Por:</label>
                                        <input type="text" id="docu_entrega" name="docu_entrega" class="form-control" placeholder="¿Quién realiza la entrega...?">
                                    </div>
                                </div>
                                       <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Recibido Por:</label>
                                        <input type="text" id="docu_recibe" name="docu_recibe" class="form-control" placeholder="¿Quién recibe...?">
                                    </div>
                                </div>  
                            </div>  
                                                  
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Observación:</label>
                                        <input type="text" id="docu_observacion" name="docu_observacion" class="form-control" placeholder="Ingrese la observación...">
                                    </div>
                                </div>
                                                         
                            </div>  
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_save_ingreso">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 

 <!-- MODAL AGREGAR PRODUCTO DETALLE-->               

<form method="GET" id="form33">
    <div class="modal fade" id="ModalNewAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalNewTitleAgregar"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
            <div class="form-body">   
               
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Producto:</label>
                                        <input type="text" id="prod_nombre" name="prod_nombre" class="form-control" autocomplete="off" required>
                                        <input type="hidden" id="prod_id" name="prod_id">
                                        <input type="hidden" id="caducidad_registroSanitario" name="caducidad_registroSanitario">
                                    </div>
                                </div>                                                          
                            </div>  

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="lotes_info"></div>
                                    </div>
                                </div>                                                          
                            </div>  
                
                                           
                         <!--   <div class="row">                                 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Presentacion:</label>
                                        <input type="hidden" id="lote_id" name="lote_id">
                                        <select class="form-control " id="pres_id" name="pres_id" >
                                           
                                        </select>
                                    </div>
                                </div>                              
                            </div>  -->  
                               <!--<div class="row">
                               
                                  
                               

                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Presentación:</label>
                                        <input  type="text" id="pres_nombre" name="pres_nombre" class="form-control" required>
                                    </div>
                                </div>

                            
                            </div> -->
                              <div class="row">
                               
                                  
                               

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>*Cantidad:</label>
                                        <input type="text" id="deta_docu_cantidad" name="deta_docu_cantidad" class="form-control" required>
                                    </div>
                                </div>

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Valor Unitario:</label>
                                        <input readonly type="text" id="deta_docu_valo_unit" name="deta_docu_valo_unit" class="form-control" required>
                                    </div>
                                </div>    
                                
                            </div>
                            <div class="row">
                               
                               <div class="col-md-6">
                                   <div class="form-group">
                                      <!-- <label>Registro Sanitario:</label>-->
                                       <input readonly type="hidden" id="deta_docu_regi_sani" name="deta_docu_regi_sani" class="form-control" >
                                   </div>
                               </div>    
                               <div class="col-md-6">
                                   <div class="form-group">
                                      <!-- <label>Fecha Vencimiento:</label>-->
                                       <input readonly type="hidden" id="deta_docu_fech_venc" name="deta_docu_fech_venc" class="form-control" >
                                   </div>
                               </div>                               
                           </div>                              

                            
                               
                </div>
                
            </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-info" id="btn_save_agregar">Agregar</button>
    
                </div>
            
          </div>
         
        </div>
       
      </div>   
</form> 

<!-- MODAL CAMBIO DOCUMENTO TEMPORAL DESCARGOS PENDIENTES -->
<!-- Modal -->
<div class="modal fade" id="ModalCamDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cambio Documento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
                                        <label>Documento:</label>
                                        <input type="hidden" id="lote_id" name="lote_id">
                                        <select class="form-control " id="cmb_docu_cambio" name="cmb_docu_cambio" >
                                           
                                        </select>
                                    </div>
         </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_cam_doc" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>

    <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_docu_egre.js"></script>
    
