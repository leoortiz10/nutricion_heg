
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                        <button type="button" id="btn_nuevo" class="btn btn-info btn-rounded m-b-10 m-l-5 btn_new">+Nuevo</button>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Hospital Nombre</th>
                                                <th>Direccion</th>                                              
                                                <th>Telefono</th>                                                 
                                                <th>Ruc</th>   
                                                <th>Tipo</th>                                                                                             
                                                <th>Canton</th>   
                                                <th>Parroquia</th> 
                                                <th>Zona</th> 
                                                <th>Distrito</th> 
                                                <th>Circuito</th> 
                                                <th>Tipo Unidad</th> 
                                                <th width="6%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>
                      
                       
                    </div>
                </div>
                <!-- End PAge Content -->
                  
                <form method="GET" id="form1">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" id="hosp_nombre" name="hosp_nombre" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Direccion</label>
                                        <input type="text" id="hosp_direccion" name="hosp_direccion" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telefono</label>
                                        <input type="text" id="hosp_telefono" name="hosp_telefono" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Ruc</label>
                                        <input type="text" id="hosp_ruc" name="hosp_ruc" class="form-control">
                                    </div>
                                </div>
                               
                                <!--/span-->
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tipo</label>
                                                <input type="text" id="hosp_tipo" name="hosp_tipo" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                    <label >Canton</label>                                     
                                                    <input type="text" id="hosp_canton" name="hosp_canton" class="form-control">                                              
                                            </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                               
                                        <label >Parroquia</label>                                     
                                        <input type="text" id="hosp_parroquia" name="hosp_parroquia" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                   
                                            <label >Zona</label>                                     
                                            <input type="text" id="hosp_zona" name="hosp_zona" class="form-control">
                                            </div>
                                   </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                               
                                        <label >Distrito</label>                                     
                                        <input type="text" id="hosp_distrito" name="hosp_distrito" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                   
                                            <label >Circuito</label>                                     
                                            <input type="text" id="hosp_circuito" name="hosp_circuito" class="form-control">
                                            </div>
                                   </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                               
                                        <label >Tipo Unidad</label>                                     
                                        <input type="text" id="hosp_tipo_unidad" name="hosp_tipo_unidad" class="form-control">
                                        </div>
                                    </div>                                    
                            </div>
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_aceptar2">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
      <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_hospital.js"></script>
    
