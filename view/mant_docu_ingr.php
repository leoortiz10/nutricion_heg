
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                    <form>
                                                <div class="form-group row">
                                                    <label for="tipo_docu_id" class=" col-md-1 col-form-label">Tipo:</label>
                                                    <div class="col-md-2">
                                                        <select class="form-control " id="tipo_docu_id_s" name="tipo_docu_id_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <label for="unid_id" class=" col-md-1 col-form-label">Unidad:</label>
                                                    <div class="col-md-4">
                                                            <select class="form-control " id="unid_id_s" name="unid_id_s">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                            <button type="button" id="btn_new_ingresos_nutricion" class="btn btn-info btn-rounded m-l-5 btn_new_ingresos_nutricion"><i class="ti-file"></i> Nuevo Ingreso</button>
                                                    </div> 
                                                </div>                             
                                    </form>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_3" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Documento</th>
                                                <th>Numero</th>                                               
                                                <th>Unidad</th>
                                                <th>Fecha Ingreso</th>
                                                <th>Proveedor</th>
                                                <th>Fecha Factura</th>
                                                <th>N. Factura</th>                                               
                                                <th>Responsable</th>
                                                <th>Estado</th>
                                                <th width="10%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
                <!-- End PAge Content -->
                  
        
<div class="modal fade bd-example-modal-lg" id="ModalDetalleIngreso"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
             <div class="modal-header">
                <h2 class="modal-title" id="ModalTitleIngreso"><span class="badge badge-danger"></span></h2>
                <span class="badge badge-info" id="mtitulo1"></span><br>
                <span class="badge badge-info" id="mtitulo2"></span><br>
                <span class="badge badge-info" id="mtitulo3"></span>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body body-full-color" >

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                    <div style="text-align:right" >
                                        <button type="button" id="btn_agregar" class="btn btn-info btn-rounded m-l-5 btn_agregar"><i class="ti-plus"></i> Agregar Producto</button>                                                         
                                    </div> 
                            <div class="card-body" >                                                           
                                <div class="table-responsive">
                                    <table id="tb_4" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                
                                                <th >Producto</th>
                                                <th >Presentacion</th>
                                                <!--<th >*LOTE</th>-->
                                                <!--<th >Fecha Caduca</th>-->
                                                <!--<th >Reg. Sani</th>-->
                                                <th >Cant</th>
                                                <th >V. Unit</th>
                                                <th >IVA%</th>
                                                <th >Valor IVA</th>
                                                <th >Subtotal</th>                                                
                                                <th ></th> 
                                              
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
            </div>
            
            
          
            <div class="modal-footer">           
     
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cerrar</button>            
  
            </div>
    </div>
  </div>
</div>


 <!-- MODAL NUEVO Y EDITAR-->               
 <form method="GET" id="form2">

    <div class="modal fade" id="ModalNew" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalNewTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                     
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tipo Documento:</label>
                                        <select class="form-control " id="tipo_docu_id" name="tipo_docu_id" >
                                           
                                        </select>
                                    </div>
                                </div>   
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <!--<label>Fecha:</label>-->
                                        <label>Unidad:</label>
                                        <select class="form-control " id="unid_id" name="unid_id" >
                                        
                                           
                                        </select>
                                        
                                        <input type="hidden" id="docu_fecha" name="docu_fecha" class="form-control" required>
                                    </div>
                                </div>                           
                            </div>                          
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Documento Numero:</label>
                                        <input type="text" id="docu_numero" name="docu_numero" class="form-control">
                                        
                                    </div>
                                </div>    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha Factura:</label>
                                        <input type="text" id="docu_prov_fech" name="docu_prov_fech" class="form-control" required>
                                          
                                        
                                    </div>
                                </div>                                                         
                            </div>   
                            <div class="row">
                              
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Proveedor:</label>
                                        <select class="form-control " id="prov_id" name="prov_id" >
                                           
                                        </select>
                                    </div>
                                </div>  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Servicios:</label>
                                        <select class="form-control " id="area_id" name="area_id" >
                                           
                                           </select>
                                    </div>
                                </div>                            
                            </div>   
                           
                            <div class="row">
                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>N. Factura:</label>
                                        <input type="text" id="docu_factura" name="docu_factura" class="form-control" maxlength="40" placeholder="Ingrese el # de factura...">
                                    </div>
                                </div>    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Responsable:</label>
                                        <input type="text" id="docu_recibe" name="docu_recibe" class="form-control" placeholder="¿Quién es el responsable?">
                                    </div>
                                </div>                               
                            </div>                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Observación:</label>
                                        <input type="text" id="docu_observacion" name="docu_observacion" class="form-control" placeholder="Ingrese la observación...">
                                    </div>
                                </div>
                                                         
                            </div>  
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_save_ingreso">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 

 <!-- MODAL AGREGAR PRODUCTO DETALLE-->               

<form method="GET" id="form3">
    <div class="modal fade" id="ModalNewAgregarIng" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalNewTitleAgregar"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
            
               
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Producto:</label>
                                        <input type="text" id="prod_nombre" name="prod_nombre" class="form-control" autocomplete="off" required>
                                        <input type="hidden" id="prod_id" name="prod_id">
                                         <input type="hidden" id="caducidad_registroSanitario" name="caducidad_registroSanitario">
                                    </div>
                                </div>                                                          
                            </div>   
                
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div id="lotes_info"></div>
                                    </div>
                                </div>                                                          
                            </div> 
                            
                     <div class="form-body">                         
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <!-- <label>Lote:</label>
                                        <input type="text" id="deta_docu_lote" name="deta_docu_lote
                                        " class="form-control" required>-->


                                        <div id="lotes_info_new"></div>
                                    </div>
                                </div>
                           
                              <!--<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Presentacion:</label>
                                        <select class="form-control " id="pres_id" name="pres_id" >
                                           
                                        </select>
                                    </div>
                                </div>   -->                         
                            </div>   
                            <div class="row">

                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <!--<label>Fecha Vencimiento:</label>
                                        <input type="text" id="deta_docu_fech_venc" name="deta_docu_fech_venc" class="form-control" required>-->
                                        <div id="fecha_venc"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <!--<label>Registro Sanitario:</label>
                                        <input type="text" id="deta_docu_regi_sani" name="deta_docu_regi_sani" class="form-control" required>-->
                                        <div id="registro_sani"></div>
                                    </div>
                                </div>                            
                            </div>   
                            <div class="row">
                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>*Cantidad:</label>
                                        <input type="text" id="deta_docu_cantidad" name="deta_docu_cantidad" class="form-control" required>
                                    </div>
                                </div>    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Valor Unitario:</label>
                                        <input type="text" id="deta_docu_valo_unit" name="deta_docu_valo_unit" class="form-control"  required>
                                    </div>
                                </div>                               
                            </div>                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>IVA %:</label>
                                        <select class="form-control " id="deta_docu_iva" name="deta_docu_iva" >
                                           <option value="0">0%</option>
                                           <option value="0.12">12%</option>
                                           <option value="0.14">14%</option>
                                        </select>
                                    </div>
                                </div>
                                                         
                            </div>  
                               
                </div>
                
            </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-info" id="btn_save_agregar">Aceptar</button>
    
                </div>
            
          </div>
         
        </div>
       
    </div>   
    </form> 


<!--MODAL EDITAR-->

<form method="GET" id="form4i">

<div class="modal fade" id="ModalEditIngreso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
           <h5 class="modal-title" >Editar</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>            
       </div>
       <div class="modal-body">
           <div class="form-body">
                
                       <div class="row">
                           <!--<div class="col-md-12">
                               <div class="form-group">
                                   <label>LOTE</label>
                                   <input type="text" id="lote_edit" name="lote_edit" class="form-control" required>
                               </div>
                           </div>-->
                         
                       </div>
                       <div class="row">
                          <!-- <div class="col-md-6">
                               <div class="form-group">
                                   <label>Fecha Caduca:</label>
                                   <input type="text" id="fecha_edit" name="fecha_edit" class="form-control" required>
                               </div>
                           </div>-->
                          
                           <!--<div class="col-md-6">
                               <div class="form-group">
                                   <label>Reg. Sani.</label>
                                   <input type="text" id="regsani_edit" name="regsani_edit" class="form-control" required>
                               </div>
                           </div>    -->                         
                       </div>
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label>Cantidad</label>
                                   <input type="text" id="cant_edit" name="cant_edit" class="form-control" required>
                               </div>
                           </div>
                          
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label>V.Unit</label>
                                   <input type="text" id="vunit_edit" name="vunit_edit" class="form-control" required>
                               </div>
                           </div>                             
                       </div>                           
           </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
         <button type="submit" class="btn btn-info" id="btn_aceptar_edit">Aceptar</button>  
       </div>
     </div>
   </div>

</div> 
</form> 
    <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_docu_ingr.js"></script>
    
