                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card p-20">
                          
                                    <form>
                                             <div class="form-group row">
                                                <label for="unid_id" class="col-sm-2 col-form-label">Unidad:</label>
                                                <div class="col-sm-4">
                                                            <select class="form-control " id="unid_id_r3" name="unid_id_r3">
                                                                                                
                                                            </select>
                                                </div>  
                                                <label for="unid_id" class="col-sm-1 col-form-label">Turno:</label>
                                                <div class="col-sm-5">
                                                            <select class="form-control " id="turn_id_r3" name="turn_id_r3">
                                                                                       
                                                            </select>
                                                </div>                                                  
                                            </div>
                                            
                                            <div class="form-group row">   
                                            <label for="unid_id" class="col-sm-2 col-form-label">Servicio:</label>
                                                <div class="col-sm-4">
                                                            <select class="form-control " id="area_id_r3" name="area_id_r3">
                                                                                       
                                                            </select>
                                                </div>  
                                                <label for="unid_id" class="col-sm-1 col-form-label">Desde:</label>
                                                <div class="col-sm-2">   
                                                    <input type="text" class="form-control" id="finir3">
                                                </div>
                                                <label for="unid_id" class="col-sm-1 col-form-label">Hasta:</label>
                                                <div class="col-sm-2">   
                                                    <input type="text" class="form-control" id="ffinr3">
                                                </div>
                                            </div> 
                                            <div class="form-group row"> 
                                                <label for="unid_id" class="col-sm-2 col-form-label">Usuario:</label>
                                                <div class="col-sm-4">
                                                            <select class="form-control " id="user_id_r3" name="user_id_r3">
                                                                                                
                                                            </select>
                                                </div>  
                                                <label for="unid_id" class="col-sm-1 col-form-label">Producto:</label>
                                                <div class="col-sm-5">
                                                            <select class="form-control " id="prod_id" name="prod_id">
                                                                                       
                                                            </select>
                                                </div> 
                                                </div>   
                                            <div class="form-group row">
                                                <label class="col-sm-7 col-form-label"></label>
                                                <div class="col-sm-4">
                                                    <button type="button" id="btn_gen3" class="btn btn-inverse m-l-5"><span class="fa fa-cogs fa-lg" aria-hidden="true"></span> Generar</button>
                                                </div>
                                            </div>
                                          
                                    </form>
                           
                        </div>
                    </div>
                     <!--
                    <div class="col-md-4">
                        <div class="card  p-20" >
                            <div class="media">
                            <button type="button" id="btn_rp2_pdf" class="btn btn-danger m-l-5"><span class="fa fa-file fa-lg" aria-hidden="true"></span> Exportar PDF</button>
                            <button type="button" id="btn_rp2_excel" class="btn btn-success  m-l-5"><span class="fa fa-file fa-lg" aria-hidden="true"></span> Exportar Excel</button>
                            
                            </div>
                        </div>
                    </div>                                      
            -->
                </div>

                <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">                                    
                                <div class="table-responsive" id="ctab">
                                    <table id="tb_tt1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Unidad</th>     
                                                <th>Usuario</th> 
                                                <th>Servicio</th> 
                                                <th>Turno</th> 
                                                <th>N.</th> 
                                                <th>Fecha</th> 
                                                <th>Cedula</th> 
                                                <th>HC</th> 
                                                <th>Paciente</th> 
                                                <th>Valor</th> 
                                                <th>Estado</th> 
                                                <th>Graba</th> 
                                                <th>Acciones</th>                                      
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                </div>

                  <div class="modal fade" id="ModalNewDetDes"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
           <h5 class="modal-title" id="ModalNewDespacho">Detalle</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>            
       </div>
       <div class="modal-body">
       <div class="form-body">   
          
                       <div class="row">
                           <div class="col-md-12" id="det_des_receta">
                              
                           </div>                                                          
                       </div>  

                                 
                       
                          
           </div>
           
       </div>
           <div class="modal-footer">
           <button type="button" class="btn btn-inverse" data-dismiss="modal">Cerrar</button>     

           </div>
       
     </div>
    
   </div>
  
</div>   

    <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_despachos_usuario.js"></script>           
              
               
      