               

                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                                <div class="float-right">
                                  <form>
                                            <div class="form-group row">
                                           
                                                <label for="unid_id" class=" col-md-1 col-form-label">Unidad:</label>
                                                <div class="col-md-4">
                                                        <select class="form-control " id="unid_id" name="unid_id">
                                                                                               
                                                         </select>
                                                </div>    

                                                <!--<label for="unid_id" class=" col-md-1 col-form-label">Psicotrópico:</label>-->
                                                <!--<div class="col-md-1">
                                                <br>
                                                        <input type="checkbox" name="psico" id="psico" > 
                                                </div>-->      
                                                     
                                                <div class="float-right"> 
                                                <button type="button" class="btn btn-inverse" id="btn_imp_inv">Imprimir Inventario</button>                                
                                                </div> 
                                            </div>                                          
                                    </form>
                                </div>
                            <div class="card-body">                                                           
                                <div class="table-responsive">
                                    <table id="tb_1" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>                                                                                               
                                                <th>Unidad</th>
                                                <th>Producto</th>                                               
                                                <th>Mínimo</th>
                                                <th>Máximo</th>
                                                <th>Stock</th>                                                
                                                <th>Entregado Reservado</th>
                                                <th>*STOCK REAL</th>
                                                <th>Costo Un.</th>
                                                <th>Total</th>
                                                <th width="7%">Acciones</th> 
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
               
                <!-- End PAge Content -->
                  
        
<div class="modal fade bd-example-modal-lg" id="ModalDetalle" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
             <div class="modal-header">
                <h2 class="modal-title" id="ModalTitle"><span class="badge badge-info" id="mtitulo"></span></h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body body-full-color" >
               

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                               
                            <div class="card-body" >                                                           
                                <div class="table-responsive">
                                    <table id="tb_2" class="display compact nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>   
                                            <th>Id</th>                                                                                            
                                                <th>Fecha Mov</th>
                                                <th>Fecha Doc</th>
                                                <th>Tipo</th>
                                                <th>Obs</th>
                                                <th>Estado</th>
                                                <th>Numero</th>
                                                <!--<th>LOTE</th>-->
                                                <th>Fecha Venc</th>
                                                <th>Cant/I</th>
                                                <th>Cost/I</th>
                                                <th>Cant/E</th>
                                                <th>Cost/E</th>
                                                <th>Saldo</th>
                                                <th>Cost Prom Uni</th>
                                                <th>Costo Total</th>
                                                
                                            </tr>
                                        </thead>                                                                             
                                    </table>
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>

                 <div class="row">
                        <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Movimientos</h4>
                                        <div id="morris-area-chart_kardex"></div>
                                    </div>
                                </div>
                        </div>
                </div> 
               
                <div class="float-right">          
                    <table style="border-collapse: collapse;">
                        <tr>
                            <td align="right">  INGRESOS:</td>
                            <td>  <h4><span class="badge badge-info" id="lblingre"></span></h4></td>                  
                        </tr>
                        <tr>
                            <td align="right">  EGRESOS:</td>
                            <td>  <h4><span class="badge badge-info" id="lblegre"></span></h4></td>
                        </tr>
                        <tr>
                            <td align="right">  TOTAL:</td>
                            <td>  <h4><span class="badge badge-danger" id="lbltot"></span></h4></td>
                        </tr>
                    </table>
                </div>
            </div>
            
            
          
            <div class="modal-footer">           
     
              <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>            
  
            </div>
    </div>
  </div>
</div>


 <!-- MODAL 2 KARDEX MINIMOS Y MAXIMOS EDITAR -->               
 <form method="GET" id="form2">

    <div class="modal fade" id="ModalKardex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>            
            </div>
            <div class="modal-body">
                <div class="form-body">
                
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kardex Mínimo:</label>
                                        <input type="text" id="kard_minimo" name="kard_minimo" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kardex Máximo:</label>
                                        <input type="text" id="kard_maximo" name="kard_maximo" class="form-control">
                                    </div>
                                </div>
                              
                            </div>  
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Costo Unitario:</label>
                                        <input type="text" id="kard_costo" name="kard_costo" class="form-control">
                                    </div>
                                </div>
                              
                              
                            </div>                            
                           
                                
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
              <button type="submit" class="btn btn-info" id="btn_save_kardex">Aceptar</button>
  
            </div>
          </div>
        </div>

    </div>   
</form> 
    <script src="assets/js/toasts.js"></script>
    <script src="controller/mant_kardex.js"></script>
    
