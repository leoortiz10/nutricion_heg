          <!-- Start Page Content -->
                <div class="row">
                    <div class="col-md-8">
                        <div class="card p-20">
                            
                            <form>    
                                    <div class="form-group row">
                                                    <label for="unid_id_r1" class=" col-sm-2 col-form-label">Unidad:</label>
                                                    <div class="col-sm-4">
                                                            <select class="form-control " id="unid_id_r1" name="unid_id_r1">
                                                                                                
                                                            </select>
                                                    </div>
                                                    <label for="unid_id" class="col-sm-2 col-form-label">Grupo:</label>
                                                    <div class="col-sm-4">
                                                            <select class="form-control " id="grupo_prod_id" name="grupo_prod_id">
                                                                                                
                                                            </select>
                                                    </div> 
                                    </div>
                                    <div class="form-group row">
                                                    <label for="unid_id" class=" col-sm-2 col-form-label">Opcion:</label>
                                                    <div class="col-sm-4">
                                                            <select class="form-control " id="cmb_opcion" name="cmb_opcion">
                                                                <!--<option value="0">GENERAL</option>-->
                                                                <option value="1">A LA FECHA</option>

                                                            </select>
                                                    </div>
                                                    <label for="unid_id" class=" col-sm-2 col-form-label">A la Fecha:</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="ffinr2">
                                                    </div>
                                    </div>

                                  <!--  <div class="form-group row">
                                                    <label for="lb_tipo_repor" class=" col-sm-2 col-form-label">Tipo de Reporte:</label>
                                                    <div class="col-sm-4">
                                                            <select class="form-control " id="tipo_repor" name="tipo_repor">
                                                            <option value="0">--Seleccione--</option>
                                                            <option value="1">POR FECHA DE PROCESO</option>
                                                           <option value="2">POR FECHA DE DOCUMENTO</option>

                                                            </select>
                                                    </div>
                                                    
                                    </div>-->

                                    <div class="form-group row"> 
                                               
                                                
                                                <label class="col-sm-8 col-form-label"></label>
                                                <div class="col-sm-4">
                                                    <button type="button" id="btn_genreceta" class="btn btn-inverse m-l-5"><span class="fa fa-cogs fa-lg" aria-hidden="true"></span> Generar Reporte</button>

                                                </div>
                                            </div>
                             </form>
                            
                        </div>
                    </div>
                      <!--
                    <div class="col-md-4">
                        <div class="card  p-20" >
                            <div class="media">
                          <button type="button" id="btn_rp1_pdf" class="btn btn-danger m-l-5"><span class="fa fa-file fa-lg" aria-hidden="true"></span> Exportar PDF</button>
                            <button type="button" id="btn_rp1_excel" class="btn btn-success  m-l-5"><span class="fa fa-file fa-lg" aria-hidden="true"></span> Exportar Excel</button>
                          
                            </div>
                        </div>                        
                    </div>    
                      -->                                  
                   
                </div>

                <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">  
                                <button type="button" id="btnexportar" class="btn btn-success m-l-5"><span class="fa fa-cogs fa-lg" aria-hidden="true"></span> Exportar Excel</button>
                                <button type="button" id="btn_rp1_pdf" class="btn btn-danger m-l-5"><span class="fa fa-file fa-lg" aria-hidden="true"></span> Exportar PDF</button><br>
                                    <div id="rep_1"></div>
                                </div>

                            </div>
                        </div>
                </div>

    
    <script src="controller/repor_kardex.js"></script>           
              
               
      