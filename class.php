<?php
class Receta{
    
    public $con; 
    public $conpg; 
    public $id;
    public $unid_id;
    public $turn_id;
    public $noen_id;
    public $deno_id;
    public $prod_id;
    public $area_id;
    public $fecha;
    public $paciente;
    public $recibi_conforme;
    public $deno_cantidad;    
    public $deno_precio;
    public $term;
    public $opc;
    public $id_usuario;
    public $campos=array();


    public function __construct(){       
        date_default_timezone_set('America/Bogota');
        //MYSQL
        include ('../config/db.php');
        $this->con=$con; 

        //POSTGRES
        include ('../config/db_pg.php');
        $this->conpg=$conpg;         
        
        //SESION 
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }            
        //OBTIENE ID USUARIO         
        $this->id_usuario=isset($_SESSION['s_user_id']) ? $_SESSION['s_user_id'] : '';
        $this->area_id=isset($_SESSION['s_user_area_id']) ? $_SESSION['s_user_area_id'] : '';
    }

  function get_area_receta(){
    if(is_null($this->area_id))
        return 0;
    else
        return $this->area_id;
  }   
  
  function get_json_productos(){
    // YA NO SE UTILIZA
    $q="SELECT p.prod_id,p.prod_nombre FROM vw_producto_nombre p 
    INNER JOIN kardex k ON k.prod_id=p.prod_id
    WHERE p.prod_status=0 AND k.unid_id=$this->unid_id; "; 
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);    
  }
 
  function generar_json_file(){
    //YA NO SE UTILIZA
    $file_name='angel.json';
    
    if(file_put_contents($file_name,$this->get_json_productos())){
      echo $file_name.' archivo creado.';
    }else{
      echo 'no creado';
    }
  }

  function color_fondo(){
    $q="SELECT color FROM turno WHERE turn_id=$this->turn_id;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetch(PDO::FETCH_ASSOC);
    return $rs['color'];

  }
  
  function get_unidad_receta(){
    $q="SELECT unidad.unid_id, unidad.unid_nombre,unidad.nick FROM unidad 
    WHERE unid_agrega_receta=1 and unid_id 
    in (select unid_id from usuario_unidad  where user_id = $this->id_usuario) and unid_id 
    in (select unid_id from turno group by unid_id); "; 
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs); 
  }
  function get_turno_unidad(){
    $q="SELECT turn_id,turn_nombre from turno where unid_id=$this->unid_id; "; 
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs); 
  }
  function get_area(){
    $q="SELECT area_id,area_nombre from area  ORDER BY 2; "; 
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs); 
  }
  function get_medico(){
    $q="SELECT medi_id,medi_nombre from medico ORDER BY 2; "; 
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs); 
  }
  function verificar_turno_unid_user(){   
       
    //VERIFICAMOS QUE TURNO CORRESPONDA A USUARIO ASIGNADO UNIDAD
    $q="SELECT count(*) as tot FROM turno WHERE turn_id=$this->turn_id AND unid_id IN(SELECT unid_id FROM usuario_unidad WHERE user_id=$this->id_usuario );";
    $st =$this->con->prepare($q);
    $st->execute();  
    $rs = $st->fetch(PDO::FETCH_ASSOC);
    return $rs['tot'];
    
  } 
  function crear_receta_paciente(){
    if($this->verificar_turno_unid_user()>0 || $this->id_usuario==NULL){
        $graba=date("Y-m-d h:i:s");    
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO notaentrega ($columns,unid_id,turn_id,user_id,fecha_graba) VALUES ($values,$this->unid_id,$this->turn_id,$this->id_usuario,'$graba')";
        $st =$this->con->prepare($q);
        $st->execute();  
        return $this->con->lastInsertId();                      
    }else
        return '0';
    
  }  
  function sum_act_kard_entregado($p,$u,$c){
    $q="UPDATE kardex set kard_entregado = kard_entregado + $c where prod_id=$p and unid_id =$u;";
    $st =$this->con->prepare($q);
    $st->execute();
  }
  function rest_act_kard_entregado($p,$u,$c){
    $q="UPDATE kardex set kard_entregado = kard_entregado - $c where prod_id=$p and unid_id =$u;";
    $st =$this->con->prepare($q);
    $st->execute();
  }
  function stock_real_disponible(){
    $q="SELECT if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) - if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre)) - if((k.kard_entregado)is null,0,(k.kard_entregado))  as stock
    FROM detalle_kardex dk
    INNER JOIN kardex k ON k.kard_id=dk.kard_id
    WHERE  k.prod_id=$this->prod_id and k.unid_id=$this->unid_id ;";
    $st =$this->con->prepare($q);
    $st->execute();   
    $rs = $st->fetch(PDO::FETCH_ASSOC);
    $cant=0;
    $cant=$rs['stock'];

    return $cant;
  }

  function verifi_prod_receta(){
    $q="SELECT prod_id FROM detallenota WHERE noen_id=$this->noen_id and prod_id=$this->prod_id; ";
    $st =$this->con->prepare($q);
    $st->execute();   
    return  $st->rowCount();
  }

  function agregar_item_receta(){
    $m="";
    if($this->verifi_prod_receta()>0)
      $m="5";
    else{
          $deno_cantidad=$this->deno_cantidad;
          if($deno_cantidad>$this->stock_real_disponible()){
              $m=0;
          }else{
              $q="INSERT INTO detallenota (noen_id,prod_id,deno_cantidad,deno_precio) VALUES ($this->noen_id,$this->prod_id,$this->deno_cantidad,$this->deno_precio)";
              $st =$this->con->prepare($q);
              $st->execute();    
              $t=0;                
              $t=$st->rowCount();
              if($t>0){
                $this->sum_act_kard_entregado($this->prod_id,$this->unid_id,$this->deno_cantidad);
              }
              $m=$t; 
          }
    }
    return $m; 
  }
  function listado_recetas(){
      $q="SELECT n.noen_id,noen_fecha,noen_ci,noen_nombre,noen_status,IF(ROUND(sum(d.deno_cantidad*deno_precio),2) IS NULL,0,ROUND(sum(d.deno_cantidad*deno_precio),2)) as total FROM notaentrega n 
      LEFT JOIN detallenota d ON n.noen_id=d.noen_id
      WHERE (n.turn_id = $this->turn_id and n.unid_id =$this->unid_id and n.user_id=$this->id_usuario and n.noen_status<=1) 
      GROUP BY n.noen_id";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function listado_entregados(){
    $q="SELECT p.prod_nombre,SUM(dn.deno_cantidad) as entregado,
    (SELECT 
    FORMAT(if(SUM(dk.deta_kard_cant_ingr) is null,0,SUM(dk.deta_kard_cant_ingr))-
    if(SUM(dk.deta_kard_cant_egre) is null,0,SUM(dk.deta_kard_cant_egre))-
    k1.kard_entregado,0)
     FROM detalle_kardex dk
    INNER JOIN kardex k1 ON k1.kard_id=dk.kard_id 
    WHERE k1.prod_id=p.prod_id and k1.unid_id=$this->unid_id) as disponible
    FROM notaentrega n
    INNER JOIN detallenota dn ON n.noen_id=dn.noen_id
    INNER JOIN vw_producto_nombre p ON dn.prod_id=p.prod_id
    WHERE n.unid_id=$this->unid_id and n.turn_id=$this->turn_id and n.user_id=$this->id_usuario and n.noen_status<=1
    GROUP BY p.prod_id ORDER BY 1";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function listado_stocks(){
    $q="SELECT p.prod_nombre, 
    FORMAT(if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) -
    if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre)) - 
    if((k.kard_entregado)is null,0,(k.kard_entregado)),0)  as stock
    FROM detalle_kardex dk
    INNER JOIN kardex k ON k.kard_id=dk.kard_id
    INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
    WHERE  k.unid_id=$this->unid_id
    GROUP BY k.prod_id;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function listado_items(){
    $q="SELECT dn.deno_id,p.prod_nombre,dn.deno_cantidad,dn.deno_precio,(dn.deno_cantidad*dn.deno_precio) as deno_total  
    FROM detallenota dn 
    INNER JOIN vw_producto_nombre p ON dn.prod_id=p.prod_id
    WHERE dn.noen_id=$this->noen_id
    order by 1 desc ;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function get_paciente_receta(){
    $q="SELECT noen_nombre FROM notaentrega WHERE noen_id=$this->noen_id ;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function eliminar_item_receta(){
    $qa="SELECT prod_id,deno_cantidad FROM detallenota WHERE deno_id=$this->deno_id";
    $sta =$this->con->prepare($qa);
    $sta->execute();
    $rs = $sta->fetch(PDO::FETCH_ASSOC);
    $ret=" No se Ejecuto";
    if($sta->rowCount()>0){
      $cant=$rs['deno_cantidad'];
      $prod=$rs['prod_id'];

      $q="DELETE dn.* FROM detallenota dn 
      INNER JOIN notaentrega n ON dn.noen_id=n.noen_id WHERE dn.deno_id=$this->deno_id AND n.noen_status<>2 ;";
      $st =$this->con->prepare($q);
      $st->execute();    
      $t=0;            
      $t=$st->rowCount();

      if($t>0){
        $this->rest_act_kard_entregado($prod,$this->unid_id,$cant);       

        //SI NO HAY NINGUN ITEM PONE RECETA EN NO GUARDADO
        $q1="SELECT deno_id FROM detallenota where noen_id=$this->noen_id; ";
        $st1 =$this->con->prepare($q1);
        $st1->execute();
        if($st1->rowCount()<1){
          $q2="UPDATE notaentrega set noen_status=0 where noen_id=$this->noen_id and noen_status=1 ; ";
          $st2 =$this->con->prepare($q2);
          $st2->execute();
        }

        $ret=$t.' Item Eliminado, cantidad: '.$cant;

      }
    }   
    return $ret;
  }
  
  function eliminar_receta(){
    $q="SELECT dn.prod_id,dn.deno_cantidad from detallenota dn 
    INNER JOIN notaentrega n ON n.noen_id=dn.noen_id
    WHERE (dn.noen_id = $this->noen_id AND n.noen_status<>2 ) and (dn.noen_id = $this->noen_id AND n.noen_status<>3 ) ;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    
    foreach($rs as $row){
      $prod=$row['prod_id'];
      $cant=$row['deno_cantidad'];
      //AL ELIMINAR RECETA, DEVUELVE CANTIDADES AL STOCK
      $this->rest_act_kard_entregado($prod,$this->unid_id,$cant);
    }

    /*
    $q1="DELETE dn.* FROM detallenota dn 
    INNER JOIN notaentrega n ON dn.noen_id=n.noen_id WHERE dn.noen_id=$this->noen_id AND n.noen_status<>2 ;";
    $st1 =$this->con->prepare($q1);
    $st1->execute();                
    $items=0;
    $items=$st1->rowCount();
    */
    $q1="UPDATE detallenota dn 
    INNER JOIN notaentrega n ON dn.noen_id=n.noen_id SET dn.deno_status=3,dn.deno_observacion='ELIMINADA ANULADA'  WHERE dn.noen_id=$this->noen_id AND n.noen_status<>2 ;";
    $st1 =$this->con->prepare($q1);
    $st1->execute();                
    $items=0;
    $items=$st1->rowCount();

    /*
    $q2="DELETE FROM notaentrega WHERE noen_id=$this->noen_id AND noen_status<>2 ;";
    $st2 =$this->con->prepare($q2);
    $st2->execute();                
    $receta=0;
    $receta=$st2->rowCount();
    */
    $q2="UPDATE notaentrega SET noen_status=3 WHERE noen_id=$this->noen_id AND noen_status<>2 ;";
    $st2 =$this->con->prepare($q2);
    $st2->execute();                
    $receta=0;
    $receta=$st2->rowCount();

    return $receta.' Receta Eliminada con: '.$items.' Items Eliminados !!!';
  }
  function consultar_stock(){    
    $q="SELECT if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) - if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre)) - if((k.kard_entregado)is null,0,(k.kard_entregado))  as stock_disponibe,k.kard_costo
    FROM detalle_kardex dk
    INNER JOIN kardex k ON k.kard_id=dk.kard_id
    WHERE  k.prod_id=$this->prod_id and k.unid_id=$this->unid_id;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function verifica_paciente_receta(){ 

    //and n.turn_id=$this->turn_id

    $q="SELECT  p.prod_nombre,sum(dn.deno_cantidad) as cant FROM notaentrega n
    INNER JOIN detallenota dn ON n.noen_id=dn.noen_id
    INNER JOIN vw_producto_nombre p ON dn.prod_id=p.prod_id
    WHERE (n.unid_id=$this->unid_id and n.noen_fecha='$this->fecha' and n.noen_nombre='$this->paciente' and n.noen_status<>3)
    GROUP BY p.prod_nombre;";  
    $st =$this->con->prepare($q);
    $st->execute();
    $num=$st->rowCount();
    if($num<1){
       return $num;
       exit;
    }
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);        
    $c=' <div class="table-responsive">';
    $c.='<span>Paciente con Recetas Anteriores, se le Despachó...</span><br><br>';
    $c.=' <table class="table table-striped">';
    $c.'<tbody>';
    foreach($rs as $row){
      $c.='<tr>';
        
        $c.='<td align="left"><h5><span class="badge badge-inverse">'.$row['prod_nombre'].'</span></h5></td>';
        $c.='<td align="left"><h5><span class="badge badge-success">'.$row['cant'].'</span></h5></td>';
        $c.='<td></td>';
      $c.='</tr>';
    }    
    $c.'</tbody>';
    $c.'</table></div>';
    return $c; 
  }
  function guardar_receta(){

    $q1="SELECT deno_id FROM detallenota where noen_id=$this->noen_id; ";
    $st1 =$this->con->prepare($q1);
    $st1->execute();
    $t="";
    $recibi_c=strtoupper($this->recibi_conforme);
    if($st1->rowCount()>0){
      $q="UPDATE notaentrega set noen_status=1,recibi_conforme='$recibi_c' where (noen_id=$this->noen_id and noen_status=0) or (noen_id=$this->noen_id and recibi_conforme<>'$this->recibi_conforme'); ";
      $st =$this->con->prepare($q);
      $st->execute();
      $t=$st->rowCount().' Receta Guardada !!!';
    }
    else{
      $q="UPDATE notaentrega set noen_status=0 where noen_id=$this->noen_id and noen_status=1 ; ";
      $st =$this->con->prepare($q);
      $st->execute();
      $t="La Receta No tiene Items, No se puede Guardar !!!";
    }
    return $t;

    
  }  
  function receta_cabecera(){
    $q="SELECT n.noen_id,n.noen_fecha,n.noen_ci,n.noen_nombre,n.noen_historia_clinica,un.unid_nombre,us.user_nick ,
    t.turn_nombre, m.medi_nombre,a.area_nombre,h.hosp_nombre,n.recibi_conforme,n.fecha_graba
    FROM notaentrega n
    INNER JOIN unidad un ON n.unid_id=un.unid_id
    INNER JOIN hospital h on un.hosp_id=h.hosp_id
    INNER JOIN usuario us ON n.user_id=us.user_id
    INNER JOIN turno t ON n.turn_id=t.turn_id
    INNER JOIN medico m ON n.medi_id=m.medi_id
    INNER JOIN area a ON n.area_id=a.area_id
    WHERE  noen_id=$this->noen_id;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function receta_detalle(){
    $q="SELECT p.prod_nombre, dn.deno_cantidad,dn.deno_precio, dn.deno_cantidad*dn.deno_precio  as total
    FROM detallenota dn
    INNER JOIN vw_producto_nombre p ON dn.prod_id=p.prod_id
    WHERE dn.noen_id=$this->noen_id	ORDER BY p.prod_nombre;";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($rs);
  }
  function Campo($sql,$campo){	
        
    $rs =$this->con->prepare($sql);
    $rs->execute();
    $row = $rs->fetch(PDO::FETCH_ASSOC);
    $CantRec=$rs->rowCount();

    if ($rs==false){
      //echo $sql;
      return "NULL";
    }               	
    if ($CantRec != 1 ){
      $campo="NULL";
    }			
    else{
      $campo = $row[$campo];			
    }		
    return $campo;	
  }
  function cerrar_turno(){
    $q="SELECT count(noen_id) as pendientes FROM notaentrega 
        where turn_id =$this->turn_id and unid_id = $this->unid_id and user_id=$this->id_usuario and (noen_status=0) ;";
    $st =$this->con->prepare($q);
    $st->execute();  
    $rs=$st->fetch(PDO::FETCH_ASSOC);
    $t=$rs['pendientes'];
    if($t>0){
      return 'No se Puede Cerra Turno, Existen '.$t.' Recetas Pendientes por Guardar...';
    }else{      
    
        if( $this->id_usuario == false || $this->unid_id==false || $this->turn_id==false ){	
          return 'Sesion ha expirado...';
          exit;
        }
    
        $error = 0;
        $str_error = "";
        
        //$sql = "SET AUTOCOMMIT=0;";
        //$resultado  =$this->con->prepare($sql);
        //$resultado ->execute();

        //$resultado  = "BEGIN;";
        //$resultado  =$this->con->prepare($sql);
        //  $resultado->execute();
        
        $sql = "SELECT count(noen_id) as noen_cantidad from notaentrega 
        where unid_id = $this->unid_id and turn_id = $this->turn_id and user_id =$this->id_usuario and noen_status =1";    
        $noen_cantidad = $this->Campo($sql,"noen_cantidad");
        if ($noen_cantidad ==0){
          $error=1;
          $str_error.="No existen datos para el cierre";						
        }
        else{        
        
                $tipo_docu_id = 4; // es Recibo Farmacia
                $sql= "SELECT max(CAST(docu_numero AS UNSIGNED)) as docu_numero from documento where unid_id = $this->unid_id and tipo_docu_id = $tipo_docu_id  ";
                $docu_numero=$this->Campo($sql,"docu_numero");
                
                if ($docu_numero=="")	
                    $docu_numero=1;
                else
                    $docu_numero++;

                $fech=date('Y-m-d');
                $docu_observacion = $this->Campo("SELECT turn_nombre from turno where turn_id = $this->turn_id","turn_nombre")."-".$this->Campo("SELECT user_nombre from usuario where user_id = $this->id_usuario","user_nombre");

                $sql="INSERT INTO documento (tipo_docu_id,unid_id,docu_numero,docu_fecha,docu_observacion) VALUES ($tipo_docu_id,$this->unid_id,'$docu_numero','$fech','$docu_observacion')";               
                $result =$this->con->prepare($sql);
                $result->execute();

                if ($result==false){
                    $error=2;
                    $str_error.="No se puede crear el documento antes del cierre del turno";		
                }
                else{
                  $docu_id=$this->con->lastInsertId();
                }
    
                $pres_id = 119;	 // por defecto unidad
                // crear el detalle de productos del turno y actualizar kard_entregado
                //SOLO CONTABILIZA LOS DIFERENTES DE GUARDADO Y DIFERENTES DE ELIMANADO ANULADO
                $sql = "SELECT prod_id, SUM(deno_cantidad) AS deno_cantidad 
                FROM notaentrega inner join detallenota on notaentrega.noen_id = detallenota.noen_id 
                where (unid_id = $this->unid_id and turn_id = $this->turn_id and user_id = $this->id_usuario  and noen_status <> 2) and
                (unid_id = $this->unid_id and turn_id = $this->turn_id and user_id = $this->id_usuario  and noen_status <> 3) 
                group by prod_id; ";
               
                $result =$this->con->prepare($sql);
                $result->execute();
                if ($result ==false){
                    $error=3;
                    $str_error.="No se listar los elementos del turno";		
                }
                
                while ($row =$result->fetch(PDO::FETCH_ASSOC)){										
                        $prod_id = $row["prod_id"];			
                        $deta_docu_cantidad = $row["deno_cantidad"];
                        $deno_cantidad=$deta_docu_cantidad;
                        $sql1 = "INSERT INTO detalle_documento (docu_id,prod_id,pres_id,deta_docu_cantidad) VALUES ($docu_id,$prod_id,$pres_id,$deta_docu_cantidad);";                                                
                        $rs_deta_docu =$this->con->prepare($sql1);
                        $rs_deta_docu->execute();
                        if ($rs_deta_docu ==false){
                            $error=4;
                            $str_error.="No se puede insertar el Detalle documento";		
                        }                         

                        $sql_update = "UPDATE kardex set kard_entregado = kard_entregado - $deno_cantidad where prod_id=$prod_id and unid_id =$this->unid_id;";
                        $rs_update =$this->con->prepare($sql);
                        $rs_update->execute();
                        if ($rs_update ==false){
                            $error=5;
                            $str_error.="No se puede actualizar Kardex entregado";		
                        }	
                }//FIN WHILE
    
                  $sql = "INSERT into turno_entrega (user_id,turn_id,unid_id,turn_entr_fecha,docu_id) values ($this->id_usuario,$this->turn_id,$this->unid_id,now(),$docu_id)";
                  $result =$this->con->prepare($sql);
                  $result->execute();
                  $turn_entre_id =$this->con->lastInsertId();
                  if ($result ==false){
                      $error=6;
                      $str_error.="No se puede crear el turno de cierre";		
                  }
            
                  $sql="UPDATE notaentrega  set noen_status = 2, turn_entr_id = $turn_entre_id where turn_id = $this->turn_id and unid_id = $this->unid_id and user_id=$this->id_usuario and noen_status=1; ";
                  $result =$this->con->prepare($sql);
                  $result->execute();
                  if ( $result == false ){
                      $error=7;
                      $str_error.="No se puede actualizar las notas de entrega";
                  }
                  if ($error == 0){
      
                                $sql = "UPDATE kardex set kard_entregado =0 where unid_id = $this->unid_id and kard_entregado > 0";
                                $result =$this->con->prepare($sql);
                                $result->execute();
                                if ( $result == false ){
                                  $error=10;
                                  $str_error.="No se puede actualizar la cantidad entregada temporalmente en farmacia";
                                }
                                else{
                                    //barrer el las cantidades de entregado hay que barrer reservado
                                    $sql = "SELECT prod_id, SUM(deno_cantidad) AS deno_cantidad 
                                    FROM notaentrega inner join detallenota on notaentrega.noen_id = detallenota.noen_id 
                                    where (unid_id = $this->unid_id and noen_status <>2 ) and (unid_id = $this->unid_id and noen_status <>3 )
                                    group by prod_id";
                                    $result =$this->con->prepare($sql);
                                    $result->execute();
                                                if ($result==false){
                                                    $error=9;
                                                    $str_error.="No se listar los elementos que faltan de ajustar";		
                                                }
                                                while ($row =$result->fetch(PDO::FETCH_ASSOC)){	
                                                      $deno_cantidad = $row["deno_cantidad"];
                                                      $prod_id = $row["prod_id"];
                                                      $sql = "UPDATE kardex set kard_entregado = $deno_cantidad where prod_id = $prod_id and unid_id = $this->unid_id";
                                                      $result_update  =$this->con->prepare($sql);
                                                      $result_update ->execute();
                                                      if ($result_update ==false){
                                                          $error = 11;
                                                          $str_error.="No se puede actualizar la cantidad entregada en farmacia";						
                                                        }
                                                }//FIN WHILE
                                    }   //fin else    
                  }   //fin if 
                  
                        if ($error == 0){		   
                          
                          //KARDEX
                          require_once ('../kardex_pro/class.php');
                          $kardex=new Ingreso();
                          $kardex->id=$docu_id; 		
                          $kardex->recibe=0; 
                          $mensajes=$kardex->procesar_documento_kardex();		                          
                              if ($mensajes!='PROCESO EXITOSO'){
                                $error=8;
                                $str_error.=$mensajes;
                             }
                        }                        
      }	//fin else



            if ($error==0){
             // $sql = "COMMIT";
             // $resultado =$this->con->prepare($sql);
              //$resultado->execute();		
              //return 'LA OPERACION SE REALIZO CORRECTAMENTE';
            }
            else{		
             // $sql = "ROLLBACK;";
             // $resultado =$this->con->prepare($sql);
             // $resultado->execute();		
            }		
            if ($error!=0)
              return $str_error;//return 'NO SE PUDO CERRAR...';
            else
                return 'TURNO CERRADO CORRECTAMENTE';
      
      
    }//FIN PRIMER ELSE
  }//FIN FUCNTION

  

    
}