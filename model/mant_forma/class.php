<?php
class Forma{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT * FROM farm_forma; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs,JSON_UNESCAPED_UNICODE);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM farm_forma WHERE form_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO farm_forma ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE farm_forma
        SET form_nombre=?
        WHERE form_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['form_nombre']
        ]);
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        //primera consulta para validar que campo no exista en tabla productos
        $validar=" SELECT prod_id FROM farm_producto  WHERE form_id=$this->id ;";
        $stVali =$this->con->prepare($validar);
        $stVali->execute();       


       
        if ($stVali->rowCount()>0)
         {
            return 0;
            //return 'No se pudo ELIMINAR el registro!!!';
           //print(Exite al menos un registro);
         } else {
           //print(No Existen registros);
            $q=" DELETE FROM farm_forma WHERE form_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';

        }

        
    }

     
}