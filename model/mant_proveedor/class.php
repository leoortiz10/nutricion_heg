<?php
class Proveedor{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT prov_id,prov_nombre,prov_ruc,
        CASE
            WHEN prov_conv_marc=1 THEN 'Si'
            ELSE 'No'
        END as convenio
         FROM proveedor ORDER BY prov_nombre ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM proveedor WHERE prov_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO proveedor ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE proveedor
        SET prov_nombre=?,prov_ruc=?,prov_conv_marc=?
        WHERE prov_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['prov_nombre'],
            $this->campos['prov_ruc'],
            $this->campos['prov_conv_marc']
        ]);
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        /*$q=" DELETE FROM proveedor WHERE prov_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';*/


                //primera consulta para validar que campo no exista en tabla productos
        $validar=" SELECT docu_id FROM documento  WHERE prov_id=$this->id ;";
        $stVali =$this->con->prepare($validar);
        $stVali->execute();       


       
        if ($stVali->rowCount()>0)
         {
            return 0;
            //return 'No se pudo ELIMINAR el registro!!!';
           //print(Exite al menos un registro);
         } else {
           //print(No Existen registros);
            $q=" DELETE FROM proveedor WHERE prov_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';

        }
        
    }

     
}