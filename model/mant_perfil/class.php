<?php
class Perfil{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT p.*,s.stat_nombre FROM perfil p 
        INNER JOIN status s ON p.stat_id=s.stat_id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM perfil WHERE perf_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO perfil ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE perfil 
        SET perf_nombre=?,stat_id=?
        WHERE perf_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['perf_nombre'],
            $this->campos['stat_id']           
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
//para validar que no existen registros asociados a otras tablas
        $validar=" SELECT perf_user_id FROM perfil_usuario  WHERE perf_id=$this->id ;";
        $stVali =$this->con->prepare($validar);
        $stVali->execute();  

        $validar2=" SELECT perf_pagi_id FROM perfil_pagina  WHERE perf_id=$this->id ;";
        $stVali2 =$this->con->prepare($validar2);
        $stVali2->execute(); 

        if ($stVali->rowCount()>0 || $stVali2->rowCount()>0)
         {
            return 0;
         }else{

            $q=" DELETE FROM perfil WHERE perf_id=$this->id ;";
            $st =$this->con->prepare($q);
            $st->execute();        
            return $st->rowCount().' registro ELIMINADO correctamente !!!';

         }


        
    }

 
    function get_stat(){            
        $q=" SELECT * FROM status ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}