<?php
class Unidad{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT u.*,h.hosp_nombre,
        CASE
            WHEN u.unid_agrega_receta = 1 THEN 'Si'
            ELSE 'No'
        END as receta,
        CASE
            WHEN u.unid_pedido = 1 THEN 'Si'
            ELSE 'No'
        END as unid_pedido
         FROM unidad u
        INNER JOIN hospital h ON u.hosp_id=h.hosp_id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM unidad WHERE unid_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
       
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO unidad ($columns) VALUES ($values)";
        $st =$this->con->prepare($q);
        $st->execute();
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE unidad
        SET unid_nombre=?,hosp_id=?,unid_agrega_receta=?,unid_pedido=?,nick=?,unid_siglas=?
        WHERE unid_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['unid_nombre'],
            $this->campos['hosp_id'],
            $this->campos['unid_agrega_receta'],
            $this->campos['unid_pedido'],
            $this->campos['nick'],
            $this->campos['unid_siglas']
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){

        /*$validar=" SELECT prod_id FROM farm_producto  WHERE conc_id=$this->id ;";
        $stVali =$this->con->prepare($validar);
        $stVali->execute();  */

        $q=" DELETE FROM unidad WHERE unid_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }

 
    function get_hosp(){            
        $q=" SELECT hosp_id,hosp_nombre FROM hospital order by 1 desc ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}