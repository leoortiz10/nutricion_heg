<?php
class Despachos{

    public $con; 
    public $id;       
    public $unid_id;
    public $area_id;
    public $turn_id;
    public $user_id;
    public $noen_id;
    public $prod_id;
    public $fecha_ini;
    public $fecha_fin;

    public function __construct(){     
        date_default_timezone_set('America/Bogota');  
        include ('../config/db.php');
        $this->con=$con;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }            
        //OBTIENE ID USUARIO 
        $this->id_usuario=$_SESSION['s_user_id'];
    }
    function get_unidad(){
        $q="SELECT * FROM unidad WHERE unid_id in (select unid_id from usuario_reporte_unidad where user_id = $this->id_usuario) ORDER by 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_area_despacho(){
        $q="SELECT distinct(a.area_id),a.area_nombre FROM notaentrega n
        INNER JOIN area a ON n.area_id=a.area_id
        WHERE unid_id=$this->unid_id; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_prod_despacho(){
        $q="SELECT p.prod_id,p.prod_nombre FROM kardex k  INNER JOIN vw_producto_nombre p  ON k.prod_id=p.prod_id
        WHERE k.unid_id=$this->unid_id AND p.prod_status=0 ORDER BY prod_nombre; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_turno(){
        $q="SELECT * FROM turno WHERE unid_id=$this->unid_id; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_user(){
        $q="SELECT user_id,user_nick FROM usuario WHERE stat_id=1 ORDER BY 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
       }

    function reporte_despachos(){
              
        $q="SELECT n.noen_id,n.noen_fecha,n.noen_ci,n.noen_historia_clinica,n.noen_nombre,a.area_nombre,n.noen_status,n.fecha_graba,u.user_nick,un.nick,t.turn_nombre,
        IF(ROUND(SUM(dn.deno_cantidad*dn.deno_precio),2) IS NULL,0,ROUND(SUM(dn.deno_cantidad*dn.deno_precio),2)) as total
        FROM notaentrega n
        LEFT JOIN detallenota dn ON n.noen_id=dn.noen_id
        INNER JOIN usuario u ON n.user_id=u.user_id
        INNER JOIN unidad un ON n.unid_id=un.unid_id
        INNER JOIN turno t ON n.turn_id=t.turn_id
        LEFT JOIN area a ON n.area_id=a.area_id
        WHERE n.unid_id=$this->unid_id and n.noen_fecha BETWEEN '$this->fecha_ini' AND '$this->fecha_fin' ";

        if($this->turn_id != '0')
            $q.=" AND n.turn_id=$this->turn_id ";
        
        if($this->user_id != '0')
            $q.=" AND n.user_id=$this->user_id ";

        if($this->area_id != '0')
            $q.=" AND n.area_id=$this->area_id ";

        if($this->prod_id != '0')
            $q.=" AND dn.prod_id=$this->prod_id ";    
        
            $q.=" GROUP BY n.noen_id ";
       
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function detalle_despacho_receta(){
        $q="SELECT  p.prod_nombre,dn.deno_cantidad as cant FROM detallenota dn
        INNER JOIN vw_producto_nombre p ON p.prod_id=dn.prod_id
        WHERE dn.noen_id=$this->noen_id; ORDER BY 1;";
         $st =$this->con->prepare($q);
         $st->execute();
         $rs = $st->fetchAll(PDO::FETCH_ASSOC);
         
         $i=1;
         $c='<table class="table table-bordered">';
         $c.='<thead>';
         $c.='<tr>';
            $c.='<th>#</th>';
            $c.='<th>Producto</th>';
            $c.='<th>Cant</th>';         
         $c.=' </tr>';
         $c.='</thead> <tbody>';
          foreach($rs as $row){              
              $c.='<tr>';              
                $c.='<td scope="row">'.$i++.'</td>';
                $c.='<td>'.$row['prod_nombre'].'</td>';
                $c.='<td><span class="badge badge-inverse">'.$row['cant'].'</span></td>';               
              $c.='</tr>';
        }
        $c.='</tbody></table>';

        return $c;
    }

    
}