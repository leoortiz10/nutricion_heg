<?php
class Area{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT a.area_id,a.area_nombre,at.aten_nombre,
        CASE
            WHEN a.area_hospitalizacion =1 THEN 'Si'
            ELSE 'No'
        END as ah,
        CASE
            WHEN a.area_dosi_unit =1 THEN 'Si'
            ELSE 'No'
        END as ad,
        CASE
            WHEN a.area_ingreso =1 THEN 'Si'
            ELSE 'No'
        END as ai
        FROM area a
        INNER JOIN atencion at ON a.aten_id=at.aten_id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM area WHERE area_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO area ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE area 
        SET area_nombre=?,aten_id=?,area_hospitalizacion=?,area_dosi_unit=?,area_ingreso=?
        WHERE area_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['area_nombre'],
            $this->campos['aten_id'],
            $this->campos['area_hospitalizacion'],
            $this->campos['area_dosi_unit'],
            $this->campos['area_ingreso'],
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        $q=" DELETE FROM area WHERE area_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }

 
    function get_aten(){            
        $q=" SELECT * FROM atencion";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function proceso1(){
        $error=0;        
        try {               
        $this->con->beginTransaction();       
         $error=$this->proceso2();   
        if($error==0)
            $this->con->commit();
       else
            $this->con->rollBack();  
        
        return $error;

        } catch (Exception $e) {
            $this->con->rollBack();
            return $str_error;
        }
    }
    function proceso2(){
        $error=0;                            
        $sql= "UPDATE area SET area_nombre='2' WHERE area_id=1;";
        $rs=$this->con->prepare($sql);        
        $rs->execute();

            if ($error==0){		                                         
                if($this->proceso3()==0)
                    $error=0;
                else
                    $error=1;
              }          
        
        return $error;
    }
    function proceso3(){
        $error=0;            
        $sql= "SELECT * FROM area WHERE area_nombre='1';";
        $rs=$this->con->prepare($sql);        
        $rs->execute();
        if($rs->rowCount()<1)
            $error=1;
        
        return $error;      
    }
}