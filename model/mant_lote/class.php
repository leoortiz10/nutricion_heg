<?php
class Kardex{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){  
        $q="SELECT l.*,
        CONCAT(IF((p.prod_descripcion IS NOT NULL),
                           p.prod_descripcion,
                           '')) AS producto 
        FROM lote l
       INNER JOIN farm_producto p ON l.prod_id=p.prod_id
       ORDER BY producto; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);               
          
    }

    function get_by_id(){            
        $q=" SELECT l.*,p.prod_nombre as producto FROM lote l
        INNER JOIN vw_producto_nombre p ON p.prod_id=l.prod_id
        WHERE lote_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
    function editar(){
        $q=" UPDATE lote
        SET lote_codigo=?,lote_fech_venc=?,lote_regi_sani=?
        WHERE lote_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            trim($this->campos['lote_codigo']),
            $this->campos['lote_fech_venc'],
            trim($this->campos['lote_regi_sani'])
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    
}