<?php
 date_default_timezone_set('America/Bogota');
 include_once '../config/db.php';
  //SESION 
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
}   
 $id_usuario=$_SESSION['s_user_id'];
 $unid_id=$_GET['unid'];
 $turn_id=$_GET['turn'];
 $q="SELECT u.unid_nombre, p.prod_nombre,SUM(dn.deno_cantidad) as entregado,
 (SELECT 
 FORMAT(if(SUM(dk.deta_kard_cant_ingr) is null,0,SUM(dk.deta_kard_cant_ingr))-
 if(SUM(dk.deta_kard_cant_egre) is null,0,SUM(dk.deta_kard_cant_egre))-
 k1.kard_entregado,0)
  FROM detalle_kardex dk
 INNER JOIN kardex k1 ON k1.kard_id=dk.kard_id 
 WHERE k1.prod_id=p.prod_id and k1.unid_id=$unid_id) as disponible
 FROM notaentrega n
 INNER JOIN detallenota dn ON n.noen_id=dn.noen_id
 INNER JOIN unidad u ON n.unid_id=u.unid_id
 INNER JOIN vw_producto_nombre p ON dn.prod_id=p.prod_id
 WHERE n.unid_id=$unid_id and n.turn_id=$turn_id and n.user_id=$id_usuario and n.noen_status<=1
 GROUP BY p.prod_id ORDER BY 2";
 $st =$con->prepare($q);
 $st->execute();
 $st2 =$con->prepare($q);
 $st2->execute();
 
 $rs = $st->fetchAll(PDO::FETCH_ASSOC);
 $rs2 = $st2->fetch(PDO::FETCH_ASSOC);
 echo '<p>&nbsp</p>';
 echo '<p>&nbsp</p>';
 echo '<p>********************</p>';
 echo date('Y-m-d H:i:s').'-'.$rs2['unid_nombre'].'<br>';  
$i=1;
 foreach($rs as $row){  
  $nom=$row['prod_nombre'];  
    //$cnom=$row['conc_nombre'];
    //if(strlen($cnom)>=15)
    // $cnom=substr($cnom,0,15);
    echo '<p>'.substr($nom,0,15).' '.substr($nom,-15).'['.number_format($row['entregado'],0).']['.number_format($row['disponible'],0).']</p>';
  
  if($i==50){
      $i=1;
      echo '<p>&nbsp</p>';
      echo '<p>&nbsp</p>';
      echo '<p>&nbsp</p>';
      echo '<p>&nbsp</p>';
      echo '<p>&nbsp</p>';
      echo '<p>&nbsp</p>';
  }
  $i++;
}
echo '<p>********************</p>';
echo '<p>&nbsp</p>';
echo '<p>&nbsp</p>';
 
