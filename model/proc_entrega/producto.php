<?php

if (session_status() == PHP_SESSION_NONE) 
session_start();

$id_usuario=isset($_SESSION['s_user_id']) ? $_SESSION['s_user_id'] : '';

if($id_usuario==NULL){
    $rs=array(["prod_nombre"=>0]);
    echo json_encode($rs); 
    exit;
  }

 include_once '../config/db.php';
 $term  =$_GET['query'];
 $unid_id=$_GET['unid_id'];

 $q="SELECT p.prod_nombre,p.prod_id,k.kard_costo FROM vw_producto_nombre p
 INNER JOIN kardex k ON p.prod_id=k.prod_id
 WHERE k.unid_id=$unid_id and p.prod_descripcion like '%$term%' collate utf8_general_ci ORDER BY p.prod_descripcion LIMIT 9; ";
 $st =$con->prepare($q);
 $st->execute();
 $rs = $st->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($rs);