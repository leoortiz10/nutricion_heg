<?php
include ('class.php');

$funcion=$_GET['funcion'];
$id=isset($_GET['id']) ? $_GET['id'] : '';
$unid_id=isset($_GET['unid_id']) ? $_GET['unid_id'] : '';
$turn_id=isset($_GET['turn_id']) ? $_GET['turn_id'] : '';
$noen_id=isset($_GET['noen_id']) ? $_GET['noen_id'] : '';
$deno_id=isset($_GET['deno_id']) ? $_GET['deno_id'] : '';
$prod_id=isset($_GET['prod_id']) ? $_GET['prod_id'] : '';
$fecha=isset($_GET['fecha']) ? $_GET['fecha'] : '';
$paciente=isset($_GET['paciente']) ? $_GET['paciente'] : '';
$recibi_conforme=isset($_GET['recibi_conforme']) ? $_GET['recibi_conforme'] : '';
$deno_cantidad=isset($_GET['deno_cantidad']) ? $_GET['deno_cantidad'] : '';
$deno_precio=isset($_GET['deno_precio']) ? $_GET['deno_precio'] : '';
$term=isset($_GET['term']) ? $_GET['term'] : '';
$opc=isset($_GET['opc']) ? $_GET['opc'] : '';
$data=isset($_GET['data']) ? $_GET['data'] : '';

//CONVIERTO DATOS A ARRAY
parse_str($data, $campos);

$datos=new Receta();
$datos->id=$id;
$datos->opc=$opc;
$datos->term=$term;
$datos->unid_id=$unid_id;
$datos->recibi_conforme=$recibi_conforme;
$datos->turn_id=$turn_id;
$datos->noen_id=$noen_id;
$datos->deno_id=$deno_id;
$datos->prod_id=$prod_id;
$datos->fecha=$fecha;
$datos->paciente=$paciente;
$datos->deno_cantidad=$deno_cantidad;
$datos->deno_precio=$deno_precio;
$datos->campos=$campos;

echo $datos->$funcion();
