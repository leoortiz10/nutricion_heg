<?php
class Pagina{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT p.*, (SELECT p2.pagi_nombre FROM pagina p2 WHERE p.pagi_raiz=p2.pagi_codigo) as pagi_raiz_nombre FROM pagina p ; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);        
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM pagina WHERE pagi_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO pagina ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE pagina 
        SET pagi_nombre=?,pagi_url=?,pagi_codigo=?,pagi_raiz=?,pagi_prioridad=?,pagi_icono=?
         WHERE  pagi_id=$this->id  ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['pagi_nombre'],
            $this->campos['pagi_url'],
            $this->campos['pagi_codigo'],
            $this->campos['pagi_raiz'],
            $this->campos['pagi_prioridad'],
            $this->campos['pagi_icono']
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        $q=" DELETE FROM pagina WHERE pagi_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }
 
    function get_pagina_raiz(){            
        $q=" SELECT * FROM pagina WHERE pagi_raiz=0 order by pagi_nombre ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}