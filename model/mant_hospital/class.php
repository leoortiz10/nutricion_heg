<?php
class Hospital{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT * FROM hospital; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM hospital WHERE hosp_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO hospital ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE hospital
        SET hosp_nombre=?,hosp_direccion=?,hosp_telefono=?,hosp_ruc=?,hosp_tipo=?,hosp_canton=?,
        hosp_parroquia=?,hosp_zona=?,hosp_distrito=?,hosp_circuito=?,hosp_tipo_unidad=?
         WHERE hosp_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['hosp_nombre'],
            $this->campos['hosp_direccion'],
            $this->campos['hosp_telefono'],
            $this->campos['hosp_ruc'],
            $this->campos['hosp_tipo'],
            $this->campos['hosp_canton'],
            $this->campos['hosp_parroquia'],
            $this->campos['hosp_zona'],
            $this->campos['hosp_distrito'],
            $this->campos['hosp_circuito'],
            $this->campos['hosp_tipo_unidad'],
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        $q=" DELETE FROM hospital WHERE hosp_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }

   
    
}