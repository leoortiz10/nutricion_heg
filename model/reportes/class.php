<?php
class Reporte{

    public $con; 
    public $id;   
    public $autoriza;
    public $entrega;
    public $recibe; 
    public $tipo_docu;
    public $tipo_docu_id;
    public $user_nombre;
    public $unid_id;
    public $grupo_prod_id;
    public $area_id;
    public $prov_id;
    public $user_id;
    public $fecha_ini;
    public $fecha_fin;
    public $opcion;

    public function __construct(){     
        date_default_timezone_set('America/Guayaquil');  
        include ('../config/db.php');

        
        $this->con=$con;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }            
        //OBTIENE ID USUARIO 
        $this->id_usuario=$_SESSION['s_user_id'];
    }

    function repor_citologia(){
        $c='';
       // $c.=$this->logo();
        $c.=$this->cabecera_docu_cito();
       // $c.=$this->cuerpo_docu();
       // $c.=$this->pie_docu();
        return $c;
    }


    function cabecera_docu_cito(){
         $c='';
          
          $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';  
         $c.='<table class="customers">';
         $c.='<tr>';
             $c.='<td align="right"><strong>NOMBRE:</strong></td>';
             $c.='<td>ALEXANDER RODRIGUEZ</td>';
              $c.='<td></td>';
               
             $c.='<td></td>';
               
             $c.='<td align="right"><strong>CÓDIGO:</strong></td>';
             $c.='<td>CITO-1252-52</td>';
        $c.='</tr>';        
            $c.='<tr>';                               
         
                    $c.='<td align="right"><strong>PROFUNDAS:</strong></td>';
                    $c.='<td>2021-02-01</td>';
               
                     $c.='<td align="right"><strong>INTERMEDIAS:</strong></td>';
                    $c.='<td>2021-06-02</td>';
                
                     $c.='<td align="right"><strong>SUPERFICIALES:</strong></td>';
                    $c.='<td>26</td>';
                
           $c.='</tr>';
          
           $c.='<tr>';
             $c.='<td align="right"><strong>MÉDICO SOLICITANTE:</strong></td>';
             $c.='<td>JAVIER FREIRE</td>';
              $c.='<td></td>';
               
             $c.='<td></td>';
               
             $c.='<td align="right"><strong>C:</strong></td>';
             $c.='<td>3892</td>';
        $c.='</tr>';    
         $c.='</table><br>';



       /*  $c.='<table class="customers">';
         $c.='<tr>';
             $c.='<td align="right"><strong>CALIDAD DE LA MUESTRA:</strong></td>';
             $c.='<td>LIMITADA POR AUSENCIA DE COMPONENTE ENDOCERVICAL</td>';
              $c.='<td></td>';
               
            
        $c.='</tr>';        
            $c.='<tr>';                               
         
                     $c.='<td align="right"><strong>PROFUNDAS:</strong></td>';
                    $c.='<td>0 %</td>';
               
                     $c.='<td align="right"><strong>INTERMEDIAS:</strong></td>';
                    $c.='<td>70 %</td>';
                
                     $c.='<td align="right"><strong>SUPERFICIALES:</strong></td>';
                    $c.='<td>30 %</td>';
                
           $c.='</tr>';
          
           $c.='<tr>';
             $c.='<td align="right">BAJO EFECTO ESTROGÉNICO</td>';
             $c.='<td></td>';
              $c.='<td></td>';
               
             $c.='<td></td>';
               
             $c.='<td align="right"><strong</strong></td>';
             $c.='<td></td>';
        $c.='</tr>';   

        $c.='<tr>';
             $c.='<td align="right">POLIFORMONUCLEARES</td>';
             $c.='<td>+</td>';
              $c.='<td></td>';
               
             $c.='<td></td>';
               
             $c.='<td align="right"></td>';
             $c.='<td></td>';
        $c.='</tr>';  
         $c.='</table><br>';*/

         $c.='<div style="width: 100%;height:70%;
  padding-left:  15px;
  padding-right: 50px;
  margin-left:   1px;
  margin-right:  1px;
  border: 1px solid black;">cxc</div>';

         return utf8_encode($c);

    }

    function logo(){
        
        $c='';
        $c.='<div style="text-align: center;"><img src="../../assets/images/msplogo.jpg" width="150px"/></div>';
         
       

        return $c;
    }
     function logo_with_qr(){
        $fecha=date("Y-m-d / H:i:s");
             QRcode::png('Hospital Enrique Garcés - www.heg.gob.ec / Documento Generado el: '.$fecha.'',"qr_detalle_egresos.png","L",10,5); 
        $c='';
        $c.='<div style="text-align: center;"><img src="../../assets/images/cabecera_reportes.png" style="margin-left:70px;" width="500px"/>
        <img src="qr_detalle_egresos.png" style="float: right; margin: -30px 0px 10px 50px;" width="80px"/></div>';
         
       

        return $c;
    }
        function qr(){
         
         $c='';
         $fecha=date("Y-m-d / H:i:s");
         QRcode::png('Hospital Enrique Garcés - www.heg.gob.ec / Reporte Detalle Stock a la Fecha Elaborado el: '.$fecha.'',"qr_detalle_egresos.png","L",10,5);
       
          $c.='<div style="text-align: center;"><img src="qr_detalle_egresos.png" width="150px"/></div>';
        

        return $c;
    }

    function cabecera_docu(){
        $c='';
        $q="SELECT  tipo_docu_nombre, tipo_docu_movimiento,h.hosp_nombre, unidad.nick as unid_nombre, 	docu_numero, 	docu_fecha, prov_nombre, 
        docu_prov_fech, 	docu_factura, 	docu_observacion, docu_autoriza,docu_entrega, 	docu_recibe, 
        stat_docu_nombre, area_nombre, user_nombre , u2.nick as unid_destino_nombre
        FROM documento   		
        inner join unidad on documento.unid_id = unidad.unid_id 
        inner join hospital h on unidad.hosp_id=h.hosp_id
        left join unidad as u2 on documento.unid_id_destino = u2.unid_id 		
        left join proveedor on documento.prov_id = proveedor.prov_id 		
        inner join status_documento on documento.stat_docu_id = status_documento.stat_docu_id 		
        inner join tipo_documento on documento.tipo_docu_id = tipo_documento.tipo_docu_id 
        left join area on documento.area_id = area.area_id
        left join usuario on documento.user_id = usuario.user_id 
        WHERE documento.docu_id=$this->id;";
         
         $st =$this->con->prepare($q);
         $st->execute();         
         $rs = $st->fetch(PDO::FETCH_ASSOC);
         $this->tipo_docu=$rs['tipo_docu_movimiento'];
         $t=$this->tipo_docu;

         $this->autoriza=$rs['docu_autoriza'];
         $this->entrega=$rs['docu_entrega'];
         $this->recibe=$rs['docu_recibe'];
         $this->user_nombre=$rs['user_nombre'];

         $c.='<div style="text-align: center;"><h5>'.$rs['hosp_nombre'].'</h5></div>';
         $c.='<table class="customers">';
         $c.='<tr>';
             $c.='<td align="right"><strong>TIPO:</strong></td>';
             $c.='<td>'.$rs['tipo_docu_nombre'].'</td>';
             $c.='<td align="right"><strong>N.:</strong></td>';
             $c.='<td>'.$rs['docu_numero'].'</td>';
        $c.='</tr>';        
            $c.='<tr>';                               
                if($t=='I' || $t=='E'){
                    $c.='<td align="right"><strong>UNIDAD:</strong></td>';
                    $c.='<td>'.$rs['unid_nombre'].'</td>';
                }elseif($t=='P'){
                    $c.='<td align="right"><strong>PEDIDO A:</strong></td>';
                    $c.='<td>'.$rs['unid_nombre'].'</td>';
                }
                else{
                    $c.='<td align="right"><strong>UNIDAD ORIGEN:</strong></td>';
                    $c.='<td>'.$rs['unid_nombre'].'</td>';
                }

                if($t=='I' || $t=='E'){
                    $c.='<td></td>';
                    $c.='<td></td>';
                }elseif($t=='P'){
                    $c.='<td align="right"><strong>PARA FARMACIA UNIDAD:</strong></td>';
                    $c.='<td>'.$rs['unid_destino_nombre'].'</td>';
                }
                else{
                    $c.='<td align="right"><strong>UNIDAD DESTINO:</strong></td>';
                    $c.='<td>'.$rs['unid_destino_nombre'].'</td>';
                }
           $c.='</tr>';
           if($t=='E'){
                $c.='<tr>';
                    if($rs['area_nombre']!=NULL){
                        $c.='<td align="right"><strong>AREA EGRESO:</strong></td>';
                        $c.='<td>'.$rs['area_nombre'].'</td>';
                    }
                    else{
                        $c.='<td align="right"></strong></td>';
                        $c.='<td></td>';
                    }
                    if($rs['prov_nombre']!=NULL){
                        $c.='<td align="right"><strong>PROVEEDOR EGRESO:</strong></td>';
                        $c.='<td>'.$rs['prov_nombre'].'</td>';
                    }
                    else{
                        $c.='<td align="right"></strong></td>';
                        $c.='<td></td>';
                    }
                $c.='</tr>';
           }
           if($t=='I'){
            $c.='<tr>';
                $c.='<td align="right"><strong>FACTURA:</strong></td>';
                $c.='<td>'.$rs['docu_factura'].'</td>';
                $c.='<td align="right"><strong>PROVEEDOR:</strong></td>';
                $c.='<td>'.$rs['prov_nombre'].'</td>';
            $c.='</tr>';
          
            $c.='<tr>';
                 $c.='<td align="right"><strong>FECHA FACTURA:</strong></td>';
                 $c.='<td>'.$rs['docu_prov_fech'].'</td>';
                 $c.='<td align="right"><strong></td>';
            $c.='<td></td>';
            $c.='</tr>';
           }  
            $c.='<tr>';
                $c.='<td align="right"><strong>FECHA DOCU.:</strong></td>';
                $c.='<td>'.$rs['docu_fecha'].'</td>';
                $c.='<td align="right">Impresión:</td>';
                $c.='<td>'.date("Y-m-d / H:i:s").'</td>';
            $c.='</tr>';
            $c.='<tr>';
                $c.='<td align="right"><strong>OBSERVACION:</strong></td>';
                $c.='<td colspan="3">'.$rs['docu_observacion'].'</td>';            
            $c.='</tr>';
         $c.='</table><br>';

         return utf8_encode($c);
    }  
    function cuerpo_docu(){            
        $c='';
        
      /*  $q="SELECT deta_docu_id, documento.docu_numero, detalle_documento.prod_id, vw_producto_nombre.prod_descripcion, vw_producto_nombre.prod_codigo, form_nombre, conc_nombre, 		deta_docu_regi_sani, 		deta_docu_lote, 		deta_docu_fech_venc, 		deta_docu_cantidad, 
		deta_docu_valo_unit, 		deta_docu_iva, 		(deta_docu_cantidad*deta_docu_valo_unit*deta_docu_iva) as deta_docu_valo_iva, 
		(deta_docu_cantidad*deta_docu_valo_unit*(1+deta_docu_iva)) as deta_docu_subtotal, farm_presentacion.pres_nombre as  presentacion 
        FROM detalle_documento 	 
        inner join documento on detalle_documento.docu_id = documento.docu_id 	 
        inner join vw_producto_nombre  on  detalle_documento.prod_id=vw_producto_nombre.prod_id 
	inner join farm_producto on farm_producto.prod_id=vw_producto_nombre.prod_id
	inner join farm_presentacion on farm_presentacion.pres_id=farm_producto.pres_id
        WHERE detalle_documento.docu_id =$this->id ORDER BY vw_producto_nombre.prod_descripcion; ";*/

  $q="SELECT deta_docu_id, documento.docu_numero, detalle_documento.prod_id, vw_producto_nombre.prod_descripcion, vw_producto_nombre.prod_codigo,deta_docu_lote,        deta_docu_cantidad, 
        deta_docu_valo_unit,        deta_docu_iva,      (deta_docu_cantidad*deta_docu_valo_unit*deta_docu_iva) as deta_docu_valo_iva, 
        (deta_docu_cantidad*deta_docu_valo_unit*(1+deta_docu_iva)) as deta_docu_subtotal, farm_presentacion.pres_nombre as  presentacion,vw_producto_nombre.grupo_prod_id 
        FROM detalle_documento   
        inner join documento on detalle_documento.docu_id = documento.docu_id    
        inner join vw_producto_nombre  on  detalle_documento.prod_id=vw_producto_nombre.prod_id 
        inner join farm_producto on farm_producto.prod_id=vw_producto_nombre.prod_id
    inner join farm_presentacion on farm_presentacion.pres_id=farm_producto.pres_id
        WHERE detalle_documento.docu_id =$this->id ORDER BY vw_producto_nombre.prod_descripcion; ";


        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" >N.</th>';        
            $c.='<th align="center"  >NOMBRE</th>';
           // $c.='<th align="center"  >FORMA FARM.</th>';
            //$c.='<th align="center" >CONCENT.</th>';
            $c.='<th align="center" >PRESENT.';  
            $c.='<th align="center"  >LOTE</th>';
          /*  if($this->tipo_docu!='P'){      
                
                //$c.='<th align="center"  >REG.SAN.</th>';
                //$c.='<th align="center"  >LOTE</th>';
                //$c.='<th align="center" >FECH VENCEMIE.</th>';
            }*/
            $c.='<th align="center" >CANT (UNID)</th>';
            if($this->tipo_docu!='P'){
                $c.='<th align="center"  ><strong>VAL UNIT</strong></th>';
                $c.='<th align="center"  ><strong>VAL IVA</strong></th>';
                $c.='<th><strong>TOTAL</strong></th>';
            }
        $c.='</tr>';
        $i=1;
        
        $tot=0;
        $cant=0;
        $subt1=0;
        $subtot=0;
        $valiva=0;
        $totiva=0;
        $valunit=0;
        foreach($rs as $row){
           
            $c.='<tr>';
            $c.='<td>'.$i++.'</td>';
            
            $c.='<td>'.ucwords(strtolower($row['prod_descripcion'])).'</td>';
         //   $c.='<td>'.utf8_encode($row['form_nombre']).'</td>';
          //  $c.='<td>'.$row['conc_nombre'].'</td>';
            $c.='<td>'.$row['presentacion'].'</td>';
            if($row['grupo_prod_id']==11){
             $c.='<td>'.$row['deta_docu_lote'].'</td>';   
            }else{
             $c.='<td>NO APLICA</td>';
            }
           /* if($this->tipo_docu!='P'){
                
               // $c.='<td>'.utf8_encode($row['deta_docu_regi_sani']).'</td>';
               // $c.='<td>'.$row['deta_docu_lote'].'</td>';
               // $c.='<td>'.$row['deta_docu_fech_venc'].'</td>';
            }*/
            $cant=$row['deta_docu_cantidad'];
            $c.='<td align="right">'.$cant.'</td>';
            if($this->tipo_docu!='P'){
                $valiva=$row['deta_docu_iva'];
                $valunit=$row['deta_docu_valo_unit'];
                $subt1=$cant*$valunit;
                $totiva=$subt1*$valiva;
                $subtot= $row['deta_docu_subtotal'];
                $c.='<td align="right" >$'.number_format($valunit,6).'</td>';
                $c.='<td align="right" >$'.number_format($totiva,4).'</td>';
                
                $tot+=$subtot;
                $c.='<td align="right">$'.number_format($subtot,6).'</td>';
            }
            $c.='</tr>';
        }
        if($this->tipo_docu!='P'){
        $c.='<tr>';
            $c.='<td colspan="7" align="right">TOTAL:</td>';            
            $c.='<td align="right">$'.number_format($tot,2).'</td>';            
        $c.='</tr>';
        }

        $c.='</table><br><br>';
        return utf8_encode($c);
    }   
    function pie_docu_egre(){
        $c='';
        if($this->tipo_docu=='I'){
            $c.='<div style="text-align: center; ">';
            $c.='<label class="titulo1">____________________</label><br>';
            $c.='<label class="titulo1">'.$this->recibe.'</label><br>';
            $c.='<label class="titulo1">RECIBÍ CONFORME:</label><br>'; 
            $c.='<label class="titulo1">Procesado: '.$this->user_nombre.'</label><br>';
            $c.='</div>';
        }
        else{
          

            $c.='<div style="text-align: center; float:left; width:180px">';
            $c.='<label class="titulo1">____________________</label><br>';
            $c.='<label class="titulo1">AUTORIZADO POR:</label><br>'; 
            $c.='<label class="titulo1">'.$this->autoriza.'</label><br>';            
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:200px">';
            /*$c.='<label class="titulo1">________________________</label><br>';*/

            if($this->tipo_docu=='P')
                $c.='<label class="titulo1">PEDIDO POR:</label><br>'; 
            else
             /*   $c.='<label class="titulo1">ENTREGADO POR:</label><br>'; 

            $c.='<label class="titulo1">'.$this->entrega.'</label><br>'; */           
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:200px">';
            $c.='<label class="titulo1">________________________</label><br>';
            $c.='<label class="titulo1">ENTREGADO POR: '.$this->recibe.'</label><br>'; 
            $c.='<label class="titulo1">Procesado: '.$this->user_nombre.'</label><br>';
            $c.='</div>';

          
        }
        
        return utf8_encode($c);
    }    
    function pie_docu(){
        $c='';
        if($this->tipo_docu=='I'){
            $c.='<div style="text-align: center; ">';
            $c.='<label class="titulo1">____________________</label><br>';
            $c.='<label class="titulo1">'.$this->recibe.'</label><br>';
            $c.='<label class="titulo1">RECIBÍ CONFORME:</label><br>'; 
            $c.='<label class="titulo1">Procesado: '.$this->user_nombre.'</label><br>';
            $c.='</div>';
        }
        else{
          

            $c.='<div style="text-align: center; float:left; width:180px">';
            $c.='<label class="titulo1">____________________</label><br>';
            $c.='<label class="titulo1">AUTORIZADO POR:</label><br>'; 
            $c.='<label class="titulo1">'.$this->autoriza.'</label><br>';            
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:200px">';
            $c.='<label class="titulo1">________________________</label><br>';

            if($this->tipo_docu=='P')
                $c.='<label class="titulo1">PEDIDO POR:</label><br>'; 
            else
            $c.='<label class="titulo1">ENTREGADO POR:</label><br>'; 

            $c.='<label class="titulo1">'.$this->entrega.'</label><br>';           
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:200px">';
            $c.='<label class="titulo1">________________________</label><br>';
            $c.='<label class="titulo1">RECIBÍ CONFORME: '.$this->recibe.'</label><br>'; 
            $c.='<label class="titulo1">Procesado: '.$this->user_nombre.'</label><br>';
            $c.='</div>';

          
        }
        
        return utf8_encode($c);
    }  
    function repor_ingresos(){
        $c='';
        $c.=$this->logo();
        $c.=$this->cabecera_docu();
        $c.=$this->cuerpo_docu();
        $c.=$this->pie_docu();
        return $c;
    }
    function repor_egresos_pdf(){
        $c='';
        $c.=$this->logo();
        $c.=$this->cabecera_docu();
        $c.=$this->cuerpo_docu();
        $c.=$this->pie_docu_egre();
        return $c;
    }
    function get_unidad(){
        $q="SELECT * FROM unidad WHERE unid_id in (select unid_id from usuario_reporte_unidad where user_id = $this->id_usuario) ORDER by 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_grupo(){
        $q="SELECT * FROM grupo_producto; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_proveedor(){
        $q="SELECT distinct(p.prov_id),p.prov_nombre FROM documento d INNER JOIN proveedor p ON d.prov_id=p.prov_id
        WHERE d.unid_id=$this->unid_id and d.tipo_docu_id=$this->tipo_docu_id ORDER BY p.prov_nombre; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_tipo_documento_ingresos(){
        $q="SELECT * FROM tipo_documento WHERE tipo_docu_movimiento='I'; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_tipo_documento_egresos(){
        $q="SELECT * FROM tipo_documento WHERE tipo_docu_movimiento='E'; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_area(){
        $q="SELECT * FROM area ORDER by area_nombre; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_user(){
        $q="SELECT user_id,user_nick FROM usuario WHERE stat_id=1 ORDER BY 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function detalle_inicial_nutricion(){
        $c='';
        $c.=$this->logo();
        $c.=$this->cuerpo_detalle_inicial();
       $c.=$this->pie_docu_detalle_consumo();
        return $c;
    }

        function detalle_egresos(){
        $c='';
        $c.=$this->logo_with_qr();
        //$c.=$this->qr();
        $c.=$this->cuerpo_detalle_egresos();
       $c.=$this->pie_docu_detalle_consumo_egreso();
        return $c;
    }

    function cuerpo_detalle_egresos(){
         $fil=0;
        $c=''; 

        $detectorGrupo=$this->grupo_prod_id;

        if($detectorGrupo==0){
            $q1="SELECT p.prod_id,p.prod_nombre,p.pres_nombre as pre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id!=0 AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);

        }else{
            $q1="SELECT p.prod_id,p.prod_nombre,p.pres_nombre as pre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id=$this->grupo_prod_id AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);

        }

        
         $c.='<br><div style="text-align: center;font-size: 80%;"><label>HOSPITAL GENERAL ENRIQUE GARCÉS</label></div>';
        $c.='<div style="text-align: center;font-size: 70%;"><label>DETALLE STOCK A LA FECHA</label></div>';
        $c.='<div style="text-align: center;font-size: 70%;"><label>CORTE AL: '.$this->fecha_fin.'</label></div>';
        $c.='<div style="text-align: center;font-size: 70%;"><label>Fecha De Elaboración: '.date("Y-m-d / H:i:s").'</label></div><br>';



        
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers" style="font-size:10px;">';
      
        $c.='<tr>';
        $c.="<th align='center'>N.</th>";
        $c.="<th align='center'>PRODUCTO</th>";
        $c.="<th align='center'>PRESENTACIÓN</th>";


        //ENCABEZADOS DIARIOS
        //$dias=date("d");
        $coutDias=$this->fecha_fin;
        $month= date("m", strtotime($coutDias));;
        $year= date("Y", strtotime($coutDias));;

        $primerDia=date('Y-m-d', mktime(0,0,0, $month, 1, $year));

        $dia = date("d", strtotime($coutDias));
        //$c.="<th align='center'>".date('d')."</th>";
       /* for ($cdias = 1; $cdias <= $dia; $cdias++) {
         $c.="<th align='center'>".$cdias."</th>";
         $c.="<th align='center'>p/u</th>";
         $c.="<th align='center'>total</th>";
        }*/

        $c.="<th align='center'>TOTAL UNIDADES</th>";
        $c.="<th align='center'>$ TOTAL EGRESO</th>";
        /*$c.="<th align='center'>$ PU</th>";
        $c.="<th align='center'>$ P/TOTAL</th>";*/
            //$c.="<th align='center'>".$primerDia."</th>";
        

    $c.='</tr>';
    $i=0;
    $tipo_docu_id=2;
    $final_total_egresos=0;
    $final_total=0;
    $suma_final_colum=0;
    

    foreach($t1 as $row1){ 

         $c.='<tr>';
         $i++;
                $prod_id=$row1['prod_id'];
                $c.='<td>'.$i.'</td>';
                $c.='<td>'.$row1['prod_nombre'].'</td>';
                $c.='<td>'.$row1['pre'].'</td>';
                $totegres=0;
                $valor_ege_diario=0;
                $final_costos_fila=0;

          //$valorfilas[35];
        for ($cdiasEgresos = 1; $cdiasEgresos <= $dia; $cdiasEgresos++) {

          $egresosDiarios=0;
          $punitarioegresosdias=0;
          $res_to=0;

         //$c.="<th align='center'>".$cdiasEgresos."</th>";
          $contarFechas=date('Y-m-d', mktime(0,0,0, $month, $cdiasEgresos, $year));
          
          $egresosDiarios=$this->sensor_egresos($prod_id,$tipo_docu_id,$contarFechas);
      //    $c.='<td align="right">'.number_format($egresosDiarios,4).'</td>';
          $punitarioegresosdias=$this->punitariodia($prod_id,$tipo_docu_id,$contarFechas);
        //  $c.='<td align="right" style="color: blue;">'.number_format($punitarioegresosdias,4).'</td>';
          $res_to=($egresosDiarios*$punitarioegresosdias);
       //   $c.='<td align="right" style="color: green;">'.number_format($res_to,4).'</td>';

          $final_costos_fila+=$res_to;

          $totegres+=$egresosDiarios;
          $valor_ege_diario+=$punitarioegresosdias;
          $valorfilas[$prod_id][$cdiasEgresos]=$egresosDiarios;
          $valorfilasdias[$prod_id][$cdiasEgresos]=$punitarioegresosdias;
          $total_dias_pre[$prod_id][$cdiasEgresos]=$res_to;
         
        }


        $ptotalegre=0;
        $punitarioegresos=0;

        
        

        $c.='<td align="right">'.number_format($totegres,4).'</td>';
        
        $punitarioegresos=$this->punitario($prod_id,$tipo_docu_id,$coutDias);
        //$c.='<td align="right" style=" background-color: #FFD133">'.number_format($punitarioegresos,6).'</td>';

        $ptotalegre=($totegres*$punitarioegresos);
        $c.='<td align="right" >'.number_format($final_costos_fila,6).'</td>';

        $final_total+=$ptotalegre;
        $final_total_egresos+=$final_costos_fila;

        $c.='</tr>' ;  
    }
    $c.='<tr>';
    $c.='<td colspan="'.(3).'"></td>';
    for ($cdiasEgresos2 = 1; $cdiasEgresos2 <= $dia; $cdiasEgresos2++) {
         $sumingfin=0; 
         $sumingfindias=0;
         $sum_dias_p=0;
        
        foreach($t1 as $row111){ 
         $prod_id=$row111['prod_id'];
                                  
         $sumingfin+=$valorfilas[$prod_id][$cdiasEgresos2];

         $sumingfindias+=$valorfilasdias[$prod_id][$cdiasEgresos2];

         $sum_dias_p+=$total_dias_pre[$prod_id][$cdiasEgresos2];

        }
  /*      $c.='<td align="right" ><strong>'.number_format($sumingfin,4).'</strong></td>';
        $c.='<td align="right" style="color: blue;"><strong>'.number_format($sumingfindias,4).'
        </strong></td>';
        $c.='<td align="right" style="color: green;"><strong>'.number_format($sum_dias_p,4).'</strong></td>';
*/
        $suma_final_colum+=$sum_dias_p;
                                          
                                                  
       }
       $c.='<td align="right"><strong>Total/Egresos:</strong></td>';
       $c.='<td colspan="2" align="right"><strong>'.number_format($final_total_egresos,6).'</strong></td>';
       //
       //$c.='<td align="right"><strong>'.number_format($suma_final_colum,4).'</strong></td>';
    $c.='</tr>' ; 
     $c.='</table><br><br>';


     return utf8_encode($c);
    }

     function cuerpo_detalle_inicial(){
               $c='';        
        
       
            $q="SELECT k.kard_id,k.prod_id,p.prod_nombre,g.grupo_prod_nombre FROM kardex k
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        INNER JOIN grupo_producto g ON g.grupo_prod_id=p.grupo_prod_id
        WHERE k.unid_id=$this->unid_id ";
        
        if($this->grupo_prod_id<>0)
        $q.=" AND p.grupo_prod_id=$this->grupo_prod_id ";

        $q.=" ORDER BY p.prod_descripcion; ";
        
     
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<br><div style="text-align: center;"><label>HOSPITAL GENERAL ENRIQUE GARCÉS</label></div>';
        $c.='<div style="text-align: center;"><label>DETALLE STOCK A LA FECHA</label></div>';
        $c.='<div style="text-align: center;"><label>CORTE AL: '.$this->fecha_fin.'</label></div>';
        $c.='<div style="text-align: center;"><label>Fecha De Elaboración: '.date("Y-m-d / H:i:s").'</label></div><br>';

        
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers" style="font-size:10px;">';
        $c.='<tr>';
            $c.='<th align="center">N.</th>';        
            $c.='<th align="center">GRUPO PRODUCTO</th>'; 
            $c.='<th align="center">PRODUCTO</th>';
            $c.='<th align="center">STOCK</th>';
            $c.='<th align="center">COST UNI/PRO. PONDERADO</th>';
            $c.='<th align="center">VALOR TOTAL ($)</th>';
            $c.='<th align="center">A LA FECHA</th>';        
                   
        $c.='</tr>';
        $i=1;
        $subtot=0;
        $tot=0;
        $cant=0;
        $costo=0;
        $costo_final=0;
        foreach($rs as $row){

            $kard_id=$row['kard_id'];
            $gr_product=$row['grupo_prod_nombre'];
            $prod_nombre=$row['prod_nombre'];

            $q2="SELECT deta_kard_id,deta_kard_cant_sald as cant,deta_kard_fech_movi as fecha,deta_kard_cost_sald FROM  detalle_kardex 
            WHERE kard_id=$kard_id AND deta_kard_fech_movi<='$this->fecha_fin 23:59:59' ORDER BY deta_kard_id DESC LIMIT 1 ;";         
            $st2 =$this->con->prepare($q2);
            $st2->execute();
            $rs2 = $st2->fetch(PDO::FETCH_ASSOC);
            
            $c.='<tr>';
            $c.='<td>'.$i++.'</td>';
            $c.='<td style="font-size:8px;">'.$gr_product.'</td>'; 
            $c.='<td style="font-size:8px;">'.$prod_nombre.'</td>';   
              
                      
            $c.='<td align="right">'.number_format($rs2['cant'],4).'</td>';
            if($rs2['deta_kard_cost_sald']<0){
                $c.='<td align="center">'.number_format(0,4).'</td>';
            }else{
                $c.='<td align="center">'.number_format($rs2['deta_kard_cost_sald'],4).'</td>';
            }
            $total_ponde=(number_format($rs2['cant'],4)*number_format($rs2['deta_kard_cost_sald'],6));
            $c.='<td align="center">'.number_format($total_ponde,4).'</td>';
             $costo_final+=$total_ponde;
            
            $c.='<td>'.$rs2['fecha'].'</td>';           
            $c.='</tr>';
        }      
        
        $c.='<tr>';
            $c.='<td colspan="4"></td>';
            $c.='<td align="right"><b>Total:</b></td>';
            $c.='<td>'.number_format($costo_final,4).'</td>';
            $c.='<td></td>';
        $c.='</tr>';

        $c.='</table><br><br>';
        

        
        return utf8_encode($c);
     }

         function pie_docu_detalle_consumo_egreso(){
         $c='';
             $c.='<div style="text-align: center; float:left; width:180px">';
             
        

                        
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:200px">';
            $c.='<label class="titulo1">________________________</label><br>';

            $c.='<label class="titulo1">GUARDALMACÉN</label><br>'; 

         //   $c.='<label class="titulo1">'.$this->entrega.'</label><br>';            
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:300px">';
            $c.='<label class="titulo1">________________________</label><br>';
           // $c.='<label class="titulo1">RESPONSABLE DE BODEGA: '.$this->recibe.'</label><br>';            
             $c.='<label class="titulo1">RESPONSABLE DE BODEGA</label><br>';            
            $c.='</div>';

            return utf8_encode($c);

    }

        function pie_docu_detalle_consumo(){
         $c='';
             $c.='<div style="text-align: center; float:left; width:180px">';
             
                        
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:200px">';
            $c.='<label class="titulo1">________________________</label><br>';

            $c.='<label class="titulo1">GUARDALMACÉN</label><br>'; 

         //   $c.='<label class="titulo1">'.$this->entrega.'</label><br>';            
            $c.='</div>';
            
            $c.='<div style="text-align: center; float:left; width:300px">';
            $c.='<label class="titulo1">________________________</label><br>';
           // $c.='<label class="titulo1">RESPONSABLE DE BODEGA: '.$this->recibe.'</label><br>';            
             $c.='<label class="titulo1">RESPONSABLE DE BODEGA</label><br>';            
            $c.='</div>';

            return utf8_encode($c);

    }

    function repor_stock_fecha(){
        $c='';        
        
       
            $q="SELECT k.kard_id,k.prod_id,p.prod_nombre,g.grupo_prod_nombre FROM kardex k
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        INNER JOIN grupo_producto g ON g.grupo_prod_id=p.grupo_prod_id
        WHERE k.unid_id=$this->unid_id ";
        
        if($this->grupo_prod_id<>0)
        $q.=" AND p.grupo_prod_id=$this->grupo_prod_id ";

        $q.=" ORDER BY p.prod_descripcion; ";
        
     
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center">N.</th>';        
            $c.='<th align="center">GRUPO PRODUCTO</th>'; 
            $c.='<th align="center">PRODUCTO</th>';
            $c.='<th align="center">STOCK</th>';
            $c.='<th align="center">COST UNI/PRO. PONDERADO</th>';
            $c.='<th align="center">VALOR TOTAL ($)</th>';
            $c.='<th align="center">A LA FECHA</th>';        
                   
        $c.='</tr>';
        $i=1;
        $subtot=0;
        $tot=0;
        $cant=0;
        $costo=0;
        $costo_final=0;
        foreach($rs as $row){

            $kard_id=$row['kard_id'];
            $gr_product=$row['grupo_prod_nombre'];
            $prod_nombre=$row['prod_nombre'];

            $q2="SELECT deta_kard_id,deta_kard_cant_sald as cant,deta_kard_fech_movi as fecha,deta_kard_cost_sald FROM  detalle_kardex 
            WHERE kard_id=$kard_id AND deta_kard_fech_movi<='$this->fecha_fin 23:59:59' ORDER BY deta_kard_id DESC LIMIT 1 ;";         
            $st2 =$this->con->prepare($q2);
            $st2->execute();
            $rs2 = $st2->fetch(PDO::FETCH_ASSOC);
            
            $c.='<tr>';
            $c.='<td>'.$i++.'</td>';
            $c.='<td>'.$gr_product.'</td>'; 
            $c.='<td>'.$prod_nombre.'</td>';   
              
                      
            $c.='<td align="right">'.number_format($rs2['cant'],4).'</td>';
            if($rs2['deta_kard_cost_sald']<0 || $rs2['deta_kard_cost_sald']===NULL){
                $c.='<td align="center">'.number_format(0,4).'</td>';
            }else{
                $c.='<td align="center">'.$rs2['deta_kard_cost_sald'].'</td>';
            }

            //$total_ponde=(number_format($rs2['cant'],4)*number_format($rs2['deta_kard_cost_sald'],6));
            $total_ponde=(($rs2['cant'])*($rs2['deta_kard_cost_sald']));
            $c.='<td align="center">'.number_format($total_ponde,4).'</td>';
             $costo_final+=$total_ponde;
            
            if($rs2['fecha']<0 || $rs2['fecha']===NULL){
                $c.='<td align="center">NO EXISTE REGISTRO</td>';
            }else{
                $c.='<td align="center">'.$rs2['fecha'].'</td>';
            }


           // $c.='<td>'.$rs2['fecha'].'</td>';           
            $c.='</tr>';
        }      
        
        $c.='<tr>';
            $c.='<td colspan="4"></td>';
            $c.='<td align="right"><b>Total:</b></td>';
            $c.='<td>'.number_format($costo_final,4).'</td>';
            $c.='<td></td>';
        $c.='</tr>';

        $c.='</table><br><br>';
        

        
        return $c;
    }
    function repor_kardex(){

        //SI ES 1 MUESTRA REPORTE STOCK A LA FECHA SEGUN ESCOJIDO
        if($this->opcion==1){
            return $this->repor_stock_fecha();
            exit;
        }

        $c='';        
        $q="SELECT kard_id, 
        kardex.unid_id, unid_nombre,
        kardex.prod_id,  prod_nombre,
        kard_minimo,
        kard_maximo,
        kard_cantidad,
        kard_entregado,	
        kard_costo,
        kard_total,        
            (SELECT 
            if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock        
            FROM detalle_kardex
            WHERE kard_id=kardex.kard_id) as stock,
            (SELECT 
            if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock_real        
            FROM detalle_kardex
            WHERE kard_id=kardex.kard_id) - if((kard_entregado)is null,0,kard_entregado) as stock_real      
     FROM kardex 
            inner join unidad on kardex.unid_id = unidad.unid_id
            inner join vw_producto_nombre on kardex.prod_id = vw_producto_nombre.prod_id and prod_status=0		
            where grupo_prod_id=$this->grupo_prod_id and kardex.unid_id in (select unid_id from usuario_reporte_unidad where user_id = $this->id_usuario) ";

     if($this->unid_id<>0)
        $q.=" and kardex.unid_id=$this->unid_id ";
        
     $q.="ORDER BY unid_nombre,prod_nombre; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" >N.</th>';        
            $c.='<th align="center"  >PRODUCTO</th>';
            $c.='<th align="center"  >STOCK</th>';
            $c.='<th align="center" >EN RECETA</th>';        
            $c.='<th align="center" >STOCK REAL</th>';  
            $c.='<th align="center"  >COST. UNIT</th>';
            $c.='<th align="center"  >TOTAL</th>';            
        $c.='</tr>';
        $i=1;
        $subtot=0;
        $tot=0;
        $cant=0;
        $costo=0;
        foreach($rs as $row){
            $c.='<tr>';
            $c.='<td>'.$i++.'</td>';
            $c.='<td>'.utf8_encode($row['prod_nombre']).'</td>';            
            $c.='<td>'.number_format($row['stock'],4).'</td>';
            $c.='<td align="right">'.number_format($row['kard_entregado'],0).'</td>';
            $cant=$row['stock_real'];
            $c.='<td align="right">'.number_format($cant,4).'</td>';
            $costo=$row['kard_costo'];
            $c.='<td align="right" >'.$costo.'</td>';
            $subtot=number_format( $cant*$costo, 4, ".", ",");
            $tot+=$subtot;
            $c.='<td align="right">'.$subtot.'</td>';
            $c.='</tr>';
        }
        $c.='<tr>';
            $c.='<td colspan="6" align="right">TOTAL:</td>';            
            $c.='<td align="right">'.number_format( $tot, 4, ".", ",").'</td>';            
        $c.='</tr>';

        $c.='</table><br><br>';
        return $c;
    }
    function repor_lote_detalle(){
        $c='';        
        $q="SELECT p.prod_nombre,l.lote_codigo,l.lote_fech_venc,l.lote_regi_sani,k.kard_costo,
        if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock        
        FROM detalle_kardex dk 
        INNER JOIN lote l ON dk.lote_id=l.lote_id
        INNER JOIN kardex k ON dk.kard_id=k.kard_id
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        WHERE k.unid_id=$this->unid_id
        GROUP BY l.lote_id
        ORDER BY p.prod_nombre; ";      

        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" >N.</th>';        
            $c.='<th align="center"  >PRODUCTO</th>';
            $c.='<th align="center"  >LOTE</th>';
            $c.='<th align="center" >FECHA VENCE.</th>';        
            $c.='<th align="center" >REG. SANITARIO</th>';  
            $c.='<th align="center"  >COST. UNIT</th>';
            $c.='<th align="center"  >STOCK</th>';            
        $c.='</tr>';
        $i=1;
        $nom='';
        foreach($rs as $row){
            $c.='<tr>';
            
            if($nom==utf8_encode($row['prod_nombre'])){
                $c.='<td></td>';
                $c.='<td></td>';
            }
            else{
               $nom=utf8_encode($row['prod_nombre']);
               $c.='<td>'.$i++.'</td>';
               $c.='<td>'.$nom.'</td>';
            }
            
            $c.='<td>'.$row['lote_codigo'].'</td>';
            $c.='<td align="right">'.$row['lote_fech_venc'].'</td>';
            $c.='<td align="right">'.$row['lote_regi_sani'].'</td>';
            $c.='<td align="right" >$ '.$row['kard_costo'].'</td>';
            $c.='<td align="right">'.number_format($row['stock'],0).'</td>';
            $c.='</tr>';

            

        }      

        $c.='</table><br><br>';
        return $c;
    }
    function repor_turn_abierto(){
        $c='';        
        $q="SELECT user_nick, min( noen_fecha ) AS desde, max( noen_fecha ) AS hasta, count(noen_id) as nume_recetas, turn_nombre
        FROM notaentrega
        INNER JOIN usuario ON notaentrega.user_id = usuario.user_id
        INNER JOIN turno ON notaentrega.turn_id = turno.turn_id
        WHERE (notaentrega.unid_id = $this->unid_id and  noen_status <>2) and  
        (notaentrega.unid_id = $this->unid_id and  noen_status <>3)
        GROUP BY user_nombre; ";      

        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" >N.</th>';        
            $c.='<th align="center"  >USUARIO</th>';
            $c.='<th align="center"  >DESDE</th>';
            $c.='<th align="center" >HASTA</th>';        
            $c.='<th align="center" >N. RECETAS</th>';  
            $c.='<th align="center"  >TURNO</th>';            
        $c.='</tr>';
        $i=1;
        $nom='';
        foreach($rs as $row){
            $c.='<tr>';    
                $c.='<td>'.$i++.'</td>';        
                $c.='<td>'.$row['user_nick'].'</td>';
                $c.='<td >'.$row['desde'].'</td>';
                $c.='<td >'.$row['hasta'].'</td>';
                $c.='<td align="right" >'.$row['nume_recetas'].'</td>';
                $c.='<td >'.$row['turn_nombre'].'</td>';
            $c.='</tr>';          

        }      

        $c.='</table><br><br>';
        return $c;
    }
    function reporte_receta(){
        $c='';        
        $q="SELECT p.prod_nombre,sum(d.deno_cantidad) as cantidad,d.deno_precio,sum(d.deno_cantidad) * d.deno_precio  as total FROM notaentrega n
        INNER JOIN detallenota d ON n.noen_id=d.noen_id
        INNER JOIN vw_producto_nombre p ON d.prod_id=p.prod_id
        WHERE n.noen_status=2 and n.noen_fecha BETWEEN '$this->fecha_ini' AND '$this->fecha_fin' ";

        if($this->unid_id != '0')
            $q.=" AND n.unid_id=$this->unid_id ";

        if($this->area_id != '0')
            $q.=" AND n.area_id=$this->area_id ";
        
        if($this->user_id != '0')
            $q.=" AND n.user_id=$this->user_id ";

        $q.=" GROUP BY p.prod_nombre ";
        $q.=" ORDER BY p.prod_nombre ";      

        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" >N.</th>';        
            $c.='<th align="center"  >PRODUCTO</th>';
            $c.='<th align="center"  >CANTIDAD</th>';
            $c.='<th align="center" >COSTO UNIT.</th>';        
            $c.='<th align="center" >TOTAL</th>';              
        $c.='</tr>';
        $i=1;
        $nom='';
        $tot=0;
        $gran_tot=0;
        $cant=0;
        foreach($rs as $row){
            $c.='<tr>';    
                $c.='<td>'.$i++.'</td>';        
                $c.='<td>'.$row['prod_nombre'].'</td>';
                $cant+=$row['cantidad'];
                $c.='<td >'.$row['cantidad'].'</td>';
                $c.='<td >'.$row['deno_precio'].'</td>';
                $tot=$row['total'];
                $gran_tot+=$tot;
                $c.='<td align="right" >'.$tot.'</td>';                
            $c.='</tr>';          

        }      
        $c.='<tr>';    
            $c.='<td></td>';        
            $c.='<td></td>';
            $c.='<td>'.$cant.'</td>';
            $c.='<td>TOTAL</td>';
            $c.='<td align="right" >'.$gran_tot.'</td>';                
         $c.='</tr>';   
        $c.='</table><br><br>';
        return $c;
    }
    function reporte_rotacion_mensual(){
        //OPCION 1 POR LOTE SUMATORIA
        //OPCION 2 UNIFICADO TODO SUMATORIA
        $c='';        
        $q="SELECT p.prod_nombre,l.lote_codigo,l.lote_fech_venc,k.kard_costo,
        if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock
         ,(SELECT 
            if(sum(dk2.deta_kard_cant_egre)is null,0,sum(dk2.deta_kard_cant_egre)) as stock2        
            FROM detalle_kardex dk2  
            WHERE dk2.kard_id=k.kard_id ";
        if($this->opcion==1) {
           $q.="  and dk2.lote_id=dk.lote_id ";
        }
        
        $q.=" and dk2.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin'  ) as rotacion 
        FROM detalle_kardex dk 
        INNER JOIN lote l ON dk.lote_id=l.lote_id
        INNER JOIN kardex k ON dk.kard_id=k.kard_id
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        WHERE k.unid_id=$this->unid_id ";

        if($this->opcion==1)
            $q.=" GROUP BY l.lote_id ";
        else        
            $q.=" GROUP BY p.prod_nombre ";
        

        
        //$q.=" HAVING stock <>0 ";
        $q.=" ORDER BY p.prod_nombre; ";      

        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';             
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" >N.</th>';        
            $c.='<th align="center"  >PRODUCTO</th>';
            $c.='<th align="center"  >LOTE</th>';
            $c.='<th align="center" >FECHA VENCE.</th>';                   
            $c.='<th align="center"  >COST. UNIT</th>';
            $c.='<th align="center"  >STOCK</th>';            
            $c.='<th align="center"  >ROTACIÓN</th>';            
        $c.='</tr>';
        $i=1;
        $nom='';
        foreach($rs as $row){
            $c.='<tr>';
            
            if($nom==utf8_encode($row['prod_nombre'])){
                $c.='<td></td>';
                $c.='<td></td>';
            }
            else{
               $nom=utf8_encode($row['prod_nombre']);
               $c.='<td>'.$i++.'</td>';
               $c.='<td>'.$nom.'</td>';
            }
            
            if($this->opcion==1){
                $c.='<td>'.$row['lote_codigo'].'</td>';            
                $c.='<td align="right">'.$row['lote_fech_venc'].'</td>';           
            }else{
                $c.='<td></td>';            
                $c.='<td align="right"></td>';           
            }
            $c.='<td align="right" >'.$row['kard_costo'].'</td>';
            $c.='<td align="right">'.number_format($row['stock'],0).'</td>';
            $c.='<td align="right">'.number_format($row['rotacion'],0).'</td>';
            $c.='</tr>';           

        }      

        $c.='</table><br><br>';
        return $c;
    }
    function repor_deta_kardex(){
        $c='';        
        $q="SELECT u.unid_nombre,dk.deta_kard_fecha,d.docu_fecha,p.prod_nombre,d.docu_numero,td.tipo_docu_nombre,prov.prov_nombre,l.lote_codigo,l.lote_fech_venc,
        dk.deta_kard_cant_ingr,dk.deta_kard_cant_egre,dk.deta_kard_cant_sald,dk.deta_kard_cost_sald,if (dk.deta_kard_status=0,'OK','ANUL') as deta_kard_status,
        d.docu_autoriza
         FROM detalle_kardex dk
        INNER JOIN documento d ON dk.docu_id=d.docu_id
        INNER JOIN tipo_documento td ON d.tipo_docu_id=td.tipo_docu_id
        INNER JOIN kardex k ON k.kard_id=dk.kard_id
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id
        INNER JOIN unidad u ON k.unid_id=u.unid_id
        LEFT JOIN proveedor prov ON prov.prov_id=d.prov_id
        LEFT JOIN lote l ON dk.lote_id=l.lote_id
        WHERE dk.kard_id=$this->id";      //KARD ID      
        

        $st =$this->con->prepare($q);
        $st->execute();
        $rs2 = $st->fetch(PDO::FETCH_ASSOC);
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);

        $c.='<link href="../../assets/css/tabla.css" rel="stylesheet">';       
        $c.=$this->logo();     
        $c.='<div style="text-align: center;"><h5>MOVIMIENTO DE INVENTARIO</h5></div>';
        $c.='<div style="text-align: center;"><label>'.$rs2['unid_nombre'].'</label></div>';
        $c.='<div style="text-align: center;"><label>'.$rs2['prod_nombre'].'</label></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.='<th align="center" ></th>';
            $c.='<th align="center" >FECH MOV.</th>';        
            $c.='<th align="center"  >FECH DOC</th>';
            $c.='<th align="center"  >DOC N.</th>';
            $c.='<th align="center" >TIPO</th>'; 
            $c.='<th align="center" >PROVEEDOR</th>';                   
            $c.='<th align="center"  >LOTE</th>';            
            $c.='<th align="center"  >FECH CAD</th>';            
            $c.='<th align="center"  >INGRESO</th>'; 
            $c.='<th align="center"  >EGRESO</th>'; 
            $c.='<th align="center"  >SALDO</th>'; 
            $c.='<th align="center"  >V.UNIT</th>'; 
            $c.='<th align="center"  >OBS</th>'; 
            $c.='<th align="center"  >RESPONSABLE</th>'; 
        $c.='</tr>';
        $i=1;
        $nom='';
        foreach($rs as $row){
            $c.='<tr>';        
                $c.='<td>'.$i++.'</td>';
                $c.='<td>'.$row['deta_kard_fecha'].'</td>';
                $c.='<td>'.$row['docu_fecha'].'</td>';
                $c.='<td>'.$row['docu_numero'].'</td>';
                $c.='<td>'.$row['tipo_docu_nombre'].'</td>';
                $c.='<td>'.$row['prov_nombre'].'</td>';
                $c.='<td>'.$row['lote_codigo'].'</td>';
                $c.='<td>'.$row['lote_fech_venc'].'</td>';
                $c.='<td>'.$row['deta_kard_cant_ingr'].'</td>';
                $c.='<td>'.$row['deta_kard_cant_egre'].'</td>';
                $c.='<td>'.$row['deta_kard_cant_sald'].'</td>';
                $c.='<td>'.$row['deta_kard_cost_sald'].'</td>';
                $c.='<td>'.$row['deta_kard_status'].'</td>';            
                $c.='<td>'.$row['docu_autoriza'].'</td>';
            $c.='</tr>';           
        }      

        $c.='</table><br><br>';
        return $c;
    }

    //para proceder al reporte de egresos
function repor_egresos(){
        $c='';        
        $q="SELECT documento.area_id, a.area_nombre
        FROM kardex k
        INNER JOIN detalle_kardex ON k.kard_id = detalle_kardex.kard_id
        INNER JOIN documento ON detalle_kardex.docu_id = documento.docu_id
        INNER JOIN area a ON documento.area_id=a.area_id
        INNER JOIN vw_producto_nombre p ON  k.prod_id=p.prod_id
        WHERE deta_kard_cant_egre >0
        and tipo_docu_id=2 
        AND k.unid_id =$this->unid_id
        AND p.grupo_prod_id=$this->grupo_prod_id
        AND docu_fecha between '$this->fecha_ini' and '$this->fecha_fin' and stat_docu_id=1             
        GROUP BY documento.area_id, a.area_nombre order by a.area_nombre;";            
        
        $st =$this->con->prepare($q);
        $st->execute();        
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);

           
        $c.='<div style="text-align: center;"><h5>MOVIMIENTO POR EGRESOS</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers" id="deftab">';  
          
        $i=1;
        $sql_area_texto = "";
        $sql_area_cabecera = "";
        $contar=0;
        $elementos="";
        $c.='<tr>';
        $c.="<th align='center'>PRODUCTO</th>";
       
        foreach($rs as $row){
                if ($sql_area_texto != ""){
                    $sql_area_texto.=",";
                    $elementos.=",";
                }
                $unid_id = $row['area_id'];
                $unid_nombre = $row['area_nombre'];          
                $sql_area_texto.="SUM(CASE WHEN area.area_id=$unid_id THEN deno_cantidad ELSE 0 END) AS '$unid_nombre'  "; 
            
                $elementos.="'$unid_nombre'";
                //cabecera
                $c.="<th align='center'> $unid_nombre</th>";
                $c.="<th align='center'> SUBTOTAL</th>";
                $contar++;                      
        }     
        $c.="<th align='center'>P.UNIT</th>";
        $c.="<th align='center'>TOTAL CANT</th>"; 
        $c.="<th align='center'>TOTAL COST</th>"; 
        $c.='</tr>';   
        
                //OBTIENE TOTALES
                $query_rs_kardex="SELECT prom_unit , prod_nombre ,
                $sql_area_texto
                FROM (

                SELECT kardex.prod_id, SUM(deta_kard_cant_egre)  AS deno_cantidad,  unid_id_destino,deta_kard_cost_egre as prom_unit,SUM(deta_kard_cant_ingr) as deno_cantidad_ing,documento.area_id as ar_id
                FROM kardex
                INNER JOIN detalle_kardex ON kardex.kard_id = detalle_kardex.kard_id
                INNER JOIN documento ON detalle_kardex.docu_id = documento.docu_id
                WHERE 
                deta_kard_cant_egre >0 
                AND tipo_docu_id=2
                AND kardex.unid_id =$this->unid_id
                
                AND documento.docu_fecha between '$this->fecha_ini' and '$this->fecha_fin'                
                GROUP BY prod_id, documento.area_id

                ) AS T1 
                INNER JOIN area on T1.ar_id = area.area_id
                inner join vw_producto_nombre on T1.prod_id = vw_producto_nombre.prod_id                
                WHERE vw_producto_nombre.grupo_prod_id=$this->grupo_prod_id
                group by prod_codigo, prod_nombre order by prod_nombre;";

                $rs_kardex =$this->con->prepare($query_rs_kardex);
                $rs_kardex->execute();        
                $row_rs_kardex = $rs_kardex->fetchAll(PDO::FETCH_BOTH);
                $totalRows_rs_kardex = $rs_kardex->rowCount();
                //$total_columna[][]=array();
                $total_columna[]='';
                $a=0;
                $sum_gran_cant=0;
                $sum_tot=0;
                foreach($row_rs_kardex as $rowk){
                     
                    $precio_unit=$rowk['prom_unit'];
                   
                    $sum_cant=0;
                    $sum_subtot=0;       
                    $cantidad=0;        
                    $c.='<tr>';
                    $c.='<td>'.$rowk['prod_nombre'].'</td>';                                      
                   
                    for ($i=0;$i<$contar;$i++) {
                        $subtot=0;
                        $cantidad=$rowk[$i+2];                        
                        $sum_cant+=$cantidad;
                      
                        $subtot=$cantidad*$precio_unit;                        
                        if($a<1)
                            $total_columna[$i]=$subtot;
                        else
                            $total_columna[$i]+=$subtot;
                        $sum_subtot+=$subtot;

                        $imp_c='';
                        if($cantidad>0)
                            $imp_c=number_format($cantidad,0);

                        $imp_s='';
                        if($subtot>0)
                            $imp_s=number_format($subtot,4);
                            
                        $c.='<td align="right">'.$imp_c.'</td>';                
                        $c.='<td align="right">'.$imp_s.'</td>';    
                    
                    }
                    $a++;

                    $sum_tot+=$sum_subtot;
                    $sum_gran_cant+=$sum_cant;
                    $c.='<td align="right">'.$precio_unit.'</td>';
                    $c.='<td align="right"> '.$sum_cant.'</td>';
                    $c.='<td align="right">'.number_format($sum_subtot,4).'</td>';
                    $c.='</tr>';
                } 
        
                $c.='<td align="right"></td>';
               for ($i=0;$i<$contar;$i++) {                     
                   $c.='<td align="right"></td>';               
                   $c.='<td align="right">'.number_format($total_columna[$i],4).'</td>';
                }
                $c.='<td align="right"></td>';  
                $c.='<td align="right">'.$sum_gran_cant.'</td>';
                $c.='<td align="right">'.$sum_tot.'</td>';  
                    

        $c.='</table><br><br>';

        $query_rs_kardex =" SELECT documento.unid_id_destino,unidad.unid_nombre as area_nombre, sum(deta_kard_cant_egre) as cant_total
        FROM kardex
        INNER JOIN detalle_kardex ON kardex.kard_id = detalle_kardex.kard_id
        INNER JOIN documento ON detalle_kardex.docu_id = documento.docu_id
        INNER JOIN unidad ON documento.unid_id_destino=unidad.unid_id
        INNER JOIN vw_producto_nombre p ON  kardex.prod_id=p.prod_id
        WHERE deta_kard_cant_egre >0
        AND tipo_docu_id=5
        AND kardex.unid_id =$this->unid_id
        AND docu_fecha between '$this->fecha_ini' and '$this->fecha_fin'        
        AND p.grupo_prod_id=$this->grupo_prod_id
        GROUP BY documento.unid_id_destino;";

        
        //TABLA 2 DESCPACHOS
/*
        $c.='<table class="customers">';  
        $c.='<tr>';
            $c.="<th align='center'>UNIDAD</th>";
            $c.="<th align='center'>DESPACHOS</th>";
            $c.="<th align='center'>TOTAL CANTIDAD</th>";
        $c.='</tr>';
        
        $rs_kardex =$this->con->prepare($query_rs_kardex);
        $rs_kardex->execute();        
        $row_rs_kardex = $rs_kardex->fetchAll(PDO::FETCH_ASSOC);
        $gran_total = 0;  
        $tot_despacho=0;
        foreach($row_rs_kardex as $row){
            $c.='<tr>';             

                $unid_destino=$row['unid_id_destino'];
                $sql_d="SELECT count(unid_id_destino) as despacho
                FROM documento  where unid_id_destino=$unid_destino and tipo_docu_id=5 and unid_id=$this->unid_id
                and docu_fecha between '$this->fecha_ini' and '$this->fecha_fin' 
                group by unid_id_destino";
                 $rs=$this->con->prepare($sql_d);
                 $rs->execute();        
                 $rw = $rs->fetch(PDO::FETCH_ASSOC);
                
                $cant_despacho=$rw['despacho'];
                $tot_despacho += $cant_despacho;

                $cant_total=$row['cant_total'];
                $gran_total +=  $cant_total;                 

                $c.='<td align="center">'.$row['area_nombre'].'</td>';
                $c.='<td align="center">'.$cant_despacho.'</td>';                
                $c.='<td align="center">'.number_format($cant_total,0).'</td>';
            $c.='</tr>';
        }
        $c.='<tr>';            
            $c.='<td></td>';
            $c.='<td align="center">'.$tot_despacho.'</td>';
            $c.='<td>'.$gran_total.'</td>';
        $c.='<tr>';

        $c.='</table><br><br>';
        
    */
      
      
        return $c;
    }



    function repor_transferencias(){
        $c='';        
        $q="SELECT u.unid_id, u.unid_nombre
        FROM kardex k
        INNER JOIN detalle_kardex ON k.kard_id = detalle_kardex.kard_id
        INNER JOIN documento ON detalle_kardex.docu_id = documento.docu_id
        INNER JOIN unidad u ON documento.unid_id_destino=u.unid_id
        INNER JOIN vw_producto_nombre p ON  k.prod_id=p.prod_id
        WHERE deta_kard_cant_egre >0
        and tipo_docu_id=5 
        AND k.unid_id =$this->unid_id
        AND p.grupo_prod_id=$this->grupo_prod_id
        AND docu_fecha between '$this->fecha_ini' and '$this->fecha_fin' and stat_docu_id=1             
        GROUP BY unid_id, unid_nombre order by unid_nombre;";            
        
        $st =$this->con->prepare($q);
        $st->execute();        
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);

           
        $c.='<div style="text-align: center;"><h5>MOVIMIENTO POR TRANSFERENCIAS</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers" id="deftab">';  
          
        $i=1;
        $sql_area_texto = "";
        $sql_area_cabecera = "";
        $contar=0;
        $elementos="";
        $c.='<tr>';
        $c.="<th align='center'>PRODUCTO</th>";
       
        foreach($rs as $row){
                if ($sql_area_texto != ""){
                    $sql_area_texto.=",";
                    $elementos.=",";
                }
                $unid_id = $row['unid_id'];
                $unid_nombre = $row['unid_nombre'];          
                $sql_area_texto.="SUM(CASE WHEN unidad.unid_id=$unid_id THEN deno_cantidad ELSE 0 END) AS '$unid_nombre'  "; 
            
                $elementos.="'$unid_nombre'";
                //cabecera
                $c.="<th align='center'> $unid_nombre</th>";
                $c.="<th align='center'> SUBTOTAL</th>";
                $contar++;                      
        }     
        $c.="<th align='center'>P.UNIT</th>";
        $c.="<th align='center'>TOTAL CANT</th>"; 
        $c.="<th align='center'>TOTAL COST</th>"; 
        $c.='</tr>';   
        
                //OBTIENE TOTALES
                $query_rs_kardex="SELECT prom_unit , prod_nombre ,
                $sql_area_texto
                FROM (

                SELECT kardex.prod_id, SUM(deta_kard_cant_egre)  AS deno_cantidad,  unid_id_destino,deta_kard_cost_egre as prom_unit,SUM(deta_kard_cant_ingr) as deno_cantidad_ing
                FROM kardex
                INNER JOIN detalle_kardex ON kardex.kard_id = detalle_kardex.kard_id
                INNER JOIN documento ON detalle_kardex.docu_id = documento.docu_id
                WHERE 
                deta_kard_cant_egre >0 
                AND tipo_docu_id=5
                AND kardex.unid_id =$this->unid_id
                
                AND documento.docu_fecha between '$this->fecha_ini' and '$this->fecha_fin'                
                GROUP BY prod_id, documento.unid_id_destino

                ) AS T1 
                inner join unidad on T1.unid_id_destino = unidad.unid_id 
                inner join vw_producto_nombre on T1.prod_id = vw_producto_nombre.prod_id                
                WHERE vw_producto_nombre.grupo_prod_id=$this->grupo_prod_id
                group by prod_codigo, prod_nombre order by prod_nombre;";

                $rs_kardex =$this->con->prepare($query_rs_kardex);
                $rs_kardex->execute();        
                $row_rs_kardex = $rs_kardex->fetchAll(PDO::FETCH_BOTH);
                $totalRows_rs_kardex = $rs_kardex->rowCount();
                //$total_columna[][]=array();
                $total_columna[]='';
                $a=0;
                $sum_gran_cant=0;
                foreach($row_rs_kardex as $rowk){
                     
                    $precio_unit=$rowk['prom_unit'];
                   
                    $sum_cant=0;
                    $sum_subtot=0;       
                    $cantidad=0;        
                    $c.='<tr>';
                    $c.='<td>'.$rowk['prod_nombre'].'</td>';                                      
                   
                    for ($i=0;$i<$contar;$i++) {
                        $subtot=0;
                        $cantidad=$rowk[$i+2];                        
                        $sum_cant+=$cantidad;
                      
                        $subtot=$cantidad*$precio_unit;                        
                        if($a<1)
                            $total_columna[$i]=$subtot;
                        else
                            $total_columna[$i]+=$subtot;
                        $sum_subtot+=$subtot;

                        $imp_c='';
                        if($cantidad>0)
                            $imp_c=number_format($cantidad,0);

                        $imp_s='';
                        if($subtot>0)
                            $imp_s=number_format($subtot,4);
                            
                        $c.='<td align="right">'.$imp_c.'</td>';				
                        $c.='<td align="right">'.$imp_s.'</td>';	
                    
                    }
                    $a++;
                    $sum_gran_cant+=$sum_cant;
                    $c.='<td align="right">'.$precio_unit.'</td>';
                    $c.='<td align="right"> '.$sum_cant.'</td>';
                    $c.='<td align="right">'.number_format($sum_subtot,4).'</td>';
                    $c.='</tr>';
                } 
        
                $c.='<td align="right"></td>';
               for ($i=0;$i<$contar;$i++) {                  	
                   $c.='<td align="right"></td>';				
                   $c.='<td align="right">'.number_format($total_columna[$i],4).'</td>';
                }
                $c.='<td align="right"></td>';	
                $c.='<td align="right">'.$sum_gran_cant.'</td>';
                $c.='<td align="right"></td>';	
                	

        $c.='</table><br><br>';

        $query_rs_kardex =" SELECT documento.unid_id_destino,unidad.unid_nombre as area_nombre, sum(deta_kard_cant_egre) as cant_total
        FROM kardex
        INNER JOIN detalle_kardex ON kardex.kard_id = detalle_kardex.kard_id
        INNER JOIN documento ON detalle_kardex.docu_id = documento.docu_id
        INNER JOIN unidad ON documento.unid_id_destino=unidad.unid_id
        INNER JOIN vw_producto_nombre p ON  kardex.prod_id=p.prod_id
        WHERE deta_kard_cant_egre >0
        AND tipo_docu_id=5
        AND kardex.unid_id =$this->unid_id
        AND docu_fecha between '$this->fecha_ini' and '$this->fecha_fin'        
        AND p.grupo_prod_id=$this->grupo_prod_id
        GROUP BY documento.unid_id_destino;";

        
        //TABLA 2 DESCPACHOS
        $c.='<table class="customers">';  
        $c.='<tr>';
            $c.="<th align='center'>UNIDAD</th>";
            $c.="<th align='center'>DESPACHOS</th>";
            $c.="<th align='center'>TOTAL CANTIDAD</th>";
        $c.='</tr>';
        
        $rs_kardex =$this->con->prepare($query_rs_kardex);
        $rs_kardex->execute();        
        $row_rs_kardex = $rs_kardex->fetchAll(PDO::FETCH_ASSOC);
        $gran_total = 0;  
        $tot_despacho=0;
        foreach($row_rs_kardex as $row){
            $c.='<tr>';             

                $unid_destino=$row['unid_id_destino'];
                $sql_d="SELECT count(unid_id_destino) as despacho
                FROM documento  where unid_id_destino=$unid_destino and tipo_docu_id=5 and unid_id=$this->unid_id
                and docu_fecha between '$this->fecha_ini' and '$this->fecha_fin' 
                group by unid_id_destino";
                 $rs=$this->con->prepare($sql_d);
                 $rs->execute();        
                 $rw = $rs->fetch(PDO::FETCH_ASSOC);
                
                $cant_despacho=$rw['despacho'];
                $tot_despacho += $cant_despacho;

                $cant_total=$row['cant_total'];
                $gran_total +=  $cant_total;                 

                $c.='<td align="center">'.$row['area_nombre'].'</td>';
                $c.='<td align="center">'.$cant_despacho.'</td>';                
                $c.='<td align="center">'.number_format($cant_total,0).'</td>';
            $c.='</tr>';
        }
        $c.='<tr>';            
            $c.='<td></td>';
            $c.='<td align="center">'.$tot_despacho.'</td>';
            $c.='<td>'.$gran_total.'</td>';
        $c.='<tr>';

        $c.='</table><br><br>';
        
    
      
      
        return $c;
    }
    function repor_compras(){
        $c='';
        $c.='<div style="text-align: center;"><h5>REPORTE</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers" >'; 
        $c.='<tr>';
            $c.="<th align='center'>FECHA</th>";
            $c.="<th align='center'>PROVEEDOR</th>";
            $c.="<th align='center'>FACTURA</th>";
            $c.="<th align='center'>NUM.COMP</th>";
            $c.="<th align='center'>PRODUCTO</th>";
            $c.="<th align='center'>LOTE</th>";
            $c.="<th align='center'>FECH.CAD</th>";          
            $c.="<th align='center'>CANT</th>";
            $c.="<th align='center'>P.UNIT</th>";
            $c.="<th align='center'>TOTAL</th>";
            $c.="<th align='center'>TOTAL/FACT</th>";
        $c.='</tr>';

        $q="SELECT d.docu_prov_fech,p.prov_nombre,d.docu_factura,d.docu_numero,
        prod.prod_nombre,dd.deta_docu_lote,dd.deta_docu_fech_venc,d.docu_fecha,dd.deta_docu_cantidad,dd.deta_docu_valo_unit,(dd.deta_docu_cantidad*dd.deta_docu_valo_unit) as total
        FROM documento d
        INNER JOIN detalle_documento dd ON d.docu_id=dd.docu_id
        INNER JOIN vw_producto_nombre prod ON dd.prod_id=prod.prod_id
        INNER JOIN proveedor p ON d.prov_id=p.prov_id
        WHERE (d.stat_docu_id=1 AND d.unid_id=$this->unid_id AND d.tipo_docu_id=$this->tipo_docu_id AND d.docu_fech_regi BETWEEN '$this->fecha_ini' AND '$this->fecha_fin') 
        ";
        //OR (d.unid_id=$this->unid_id AND d.tipo_docu_id=$this->tipo_docu_id AND d.docu_fecha BETWEEN '$this->fecha_ini' AND '$this->fecha_fin')

        if($this->prov_id <> '0')
            $q.=" AND d.prov_id=$this->prov_id";
        
        $q.=" ORDER BY d.docu_prov_fech,d.docu_factura,prod.prod_nombre ";

        $rs =$this->con->prepare($q);
        $rs->execute();        
        $t = $rs->fetchAll(PDO::FETCH_ASSOC);        
        $gran_tot=0;
        $f='';
        $tot_fact=0;
        $j=0;
        $k='';
        function tot_fact($f,$j,$k,$tot_fact){
            $a='';
            if(strcmp($f,$k) && $j>0){   
                $a.='<tr>';  
                    $a.='<td colspan="9"></td>';
                    $a.='<td >Total/Factura:</td>';
                    $a.='<td >'.number_format($tot_fact,2).'</td>';
                $a.='</tr>'; 
            }  
            return  $a;

        }
        foreach($t as $row){   
            $k=$row["docu_factura"];
            $c.=tot_fact($f,$j,$k,$tot_fact);
            $j++;
            $c.='<tr>';               
                         
                    if(strcmp($f,$k)){   
                        $tot_fact=0;              
                        $f=$row['docu_factura'];
                        $c.='<td align="center">'.$row['docu_fecha'].'</td>';
                        $c.='<td>'.$row['prov_nombre'].'</td>';
                        $c.='<td >'.$f.'</td>';
                        $c.='<td >'.$row['docu_numero'].'</td>';             
                    }else{
                        $c.='<td colspan="4"></td>';   
                    }
                    $c.='<td >'.$row['prod_nombre'].'</td>';
                    $c.='<td align="right">'.$row['deta_docu_lote'].'</td>';
                    $c.='<td align="center">'.$row['deta_docu_fech_venc'].'</td>';
                    $c.='<td align="right">'.$row['deta_docu_cantidad'].'</td>';
                    $c.='<td align="right">'.$row['deta_docu_valo_unit'].'</td>';
                    $tot=round($row['total'],2);
                    $tot_fact+=$tot;
                    $gran_tot+=$tot;            

                    $c.='<td align="right">'.number_format($tot,2).'</td>';                      
                    $c.='<td align="right"></td>';                                    
        $c.='</tr>';
       
        }
        $k='';
         $c.=tot_fact($f,$j,$k,$tot_fact);
        $c.='<tr>';
            $c.='<td colspan="9"></td>';
            $c.='<td>'.number_format($gran_tot,2).'</td>';
            $c.='<td></td>';
        $c.='</tr>';
        $c.='</table><br><br>'; 

        return $c;
    }
    function hijo_calculo_item_ingre($prod_id,$tipo_docu_id){
        $q4="SELECT if(sum(dk.deta_kard_cost_ingr) is null,0,sum(dk.deta_kard_cost_ingr) ) as toti FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id=$tipo_docu_id and dk.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin'; ";
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['toti'];
    }
        function hijo_calculo_item_ingre_new($prod_id,$tipo_docu_id){
        $q4="SELECT if(sum(dk.deta_kard_cost_ingr*dk.deta_kard_cant_ingr) is null,0,sum(dk.deta_kard_cost_ingr*dk.deta_kard_cant_ingr) ) as toti FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id=$tipo_docu_id and dk.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin'; ";
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['toti'];
    }

         function hijo_calculo_transf_envios($prod_id,$tipo_docu_id){
        $q4="SELECT if(sum(dk.deta_kard_cost_egre*dk.deta_kard_cant_egre) is null,0,sum(dk.deta_kard_cost_egre*dk.deta_kard_cant_egre) ) as toti FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id=5 and dk.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin'; ";
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['toti'];
    }

             function hijo_calculo_transf_recibo($prod_id,$tipo_docu_id){
        $q4="SELECT if(sum(dk.deta_kard_cost_ingr*dk.deta_kard_cant_ingr) is null,0,sum(dk.deta_kard_cost_ingr*dk.deta_kard_cant_ingr) ) as toti FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND d.unid_id_destino=$this->unid_id AND d.tipo_docu_id=5 and dk.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin'; ";
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['toti'];
    }

    function hijo_calculo_item_egre($prod_id,$tipo_docu_id){
        $q4="SELECT if(sum(dk.deta_kard_cost_egre) is null,0,sum(dk.deta_kard_cost_egre) ) as toti FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id=$tipo_docu_id and dk.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin';";
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['toti'];
    }
        function hijo_calculo_item_egre_new($prod_id,$tipo_docu_id){
        $q4="SELECT if(sum(dk.deta_kard_cost_egre*dk.deta_kard_cant_egre) is null,0,sum(dk.deta_kard_cost_egre*dk.deta_kard_cant_egre) ) as toti FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id=$tipo_docu_id and dk.deta_kard_fecha between '$this->fecha_ini' and '$this->fecha_fin';";
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['toti'];
    }
    function hijo_encabezados($tipo){
        
        $q2="SELECT td.tipo_docu_id,td.tipo_docu_nombre FROM tipo_documento td
        INNER JOIN documento d ON td.tipo_docu_id=d.tipo_docu_id
        WHERE td.tipo_docu_movimiento='$tipo' AND d.unid_id=$this->unid_id AND d.docu_fecha BETWEEN '$this->fecha_ini' AND '$this->fecha_fin'
         GROUP BY td.tipo_docu_id ORDER BY td.tipo_docu_id ;";
        return $q2;       
    }
        function hijo_encabezados_resum_valorado($tipo){
        
        $q2="SELECT td.tipo_docu_id,td.tipo_docu_nombre FROM tipo_documento td
        INNER JOIN documento d ON td.tipo_docu_id=d.tipo_docu_id
        WHERE td.tipo_docu_movimiento='$tipo' AND d.unid_id=$this->unid_id AND d.docu_fecha BETWEEN '$this->fecha_ini' AND '$this->fecha_fin'
        and td.tipo_docu_id=1
         GROUP BY td.tipo_docu_id ORDER BY td.tipo_docu_id ;";
        return $q2;       
    }

            function hijo_encabezados_resum_valorado_egreso($tipo){
        
        $q2="SELECT td.tipo_docu_id,td.tipo_docu_nombre FROM tipo_documento td
        INNER JOIN documento d ON td.tipo_docu_id=d.tipo_docu_id
        WHERE td.tipo_docu_movimiento='$tipo' AND d.unid_id=$this->unid_id AND d.docu_fecha BETWEEN '$this->fecha_ini' AND '$this->fecha_fin'
        and td.tipo_docu_id!=3
        and td.tipo_docu_id!=12
         GROUP BY td.tipo_docu_id ORDER BY td.tipo_docu_id ;";
        return $q2;       
    }

    function resumen_valorado_hijo_inicial($fecha_ini,$prod_id){
        //FECHA RESUMEN VALORADO POR MES
        //DEL 01 AL 31 DE CADA MES
        //SE RESTA UN DIA AL 01 Y SE DA AL 31 DEL MES ANTERIOR SALDO CANTIDAD PARA INICIAR INV INICIAL
        $q1="SELECT (dk.deta_kard_cant_sald*dk.deta_kard_cost_sald)  as tot_ini FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id
        WHERE dk.deta_kard_fech_movi<=DATE_ADD('$fecha_ini 23:59:59', INTERVAL -1 DAY) and k.unid_id=$this->unid_id and k.prod_id=$prod_id
        ORDER BY dk.deta_kard_fech_movi DESC LIMIT 1;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetch(PDO::FETCH_ASSOC);
        return $t1['tot_ini'];
    }
    function repor_resumen_valorado(){
        $fil=0;
        $c='';        
        $q1="SELECT p.prod_id,p.prod_nombre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id=$this->grupo_prod_id AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);
        $c.='<div style="text-align: center;"><h5>RESUMEN VALORADO</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.="<th align='center'>N.</th>";
            $c.="<th align='center'>PRODUCTO</th>";
            $c.="<th align='center'>INV.INICIAL</th>";
            //ENCABEZADOS INGRESOS           
            $rs2 =$this->con->prepare($this->hijo_encabezados('I'));
            $rs2->execute();        
            $t2=$rs2->fetchAll(PDO::FETCH_ASSOC);
            foreach($t2 as $row2){
                    $c.='<th>'.$row2['tipo_docu_nombre'].'</th>';    
                    $fil++;
            }
            //TOTAL INGRESOSENCABEZADO
                $c.='<th>TOTAL INGRESOS</th>';   
            //ENCABEZADOS EGRESOS
            $rs2_1 =$this->con->prepare($this->hijo_encabezados('E'));
            $rs2_1->execute();        
            $t2_1=$rs2_1->fetchAll(PDO::FETCH_ASSOC);
            foreach($t2_1 as $row2_1){
                    $c.='<th>'.$row2_1['tipo_docu_nombre'].'</th>';
                    $fil++;
            }
            //TOTAL EGRESOS ENCABEZADO
            $c.='<th>TOTAL EGRESOS</th>';
            $c.='<th>TOTAL INV. FINAL</th>';   


        $c.='</tr>';
        $i=0;
        $gran_tot=0;
        foreach($t1 as $row1){   
            $c.='<tr>'; 
                $i++;
                $prod_id=$row1['prod_id'];
                $c.='<td>'.$i.'</td>';
                $c.='<td>'.$row1['prod_nombre'].'</td>';  

                    //INVENTARIO INICIAL COSTO UN DIA ANTERIOR AL INICIAL ESCOJIDO
                    $tot_inv_ini=0;
                    $tot_inv_ini=round($this->resumen_valorado_hijo_inicial($this->fecha_ini,$prod_id),2);
                    
                    $c.='<td align="right"><strong>'.number_format($tot_inv_ini,2).'</strong></td>';  
                    
                    //INGRESOS
                    $rs3 =$this->con->prepare($this->hijo_encabezados('I'));
                    $rs3->execute();        
                    $t3=$rs3->fetchAll(PDO::FETCH_ASSOC);

                    $tot_ingre=$tot_inv_ini;                    
                    $tot_egre=0;
                   
                    foreach($t3 as $row3){
                            
                            $ti=0;
                            $tipo_docu_id=$row3['tipo_docu_id'];
                            $ti=$this->hijo_calculo_item_ingre($prod_id,$tipo_docu_id);
                            $tot_ingre+=$ti;                            
                            $c.='<td align="right">'.$ti.'</td>';
                           
                    }      
                    //TOTAL INGRESOS SUMA   X FILA                 
                    $c.='<td align="right"><strong>'.number_format($tot_ingre,2).'</strong></td>';
                    //EGRESOS
                    $rs5 =$this->con->prepare($this->hijo_encabezados('E'));
                    $rs5->execute();        
                    $t5=$rs5->fetchAll(PDO::FETCH_ASSOC);
                   
                    foreach($t5 as $row5){
                            $te=0;
                            $tipo_docu_id=$row5['tipo_docu_id'];
                            $te=$this->hijo_calculo_item_egre($prod_id,$tipo_docu_id);
                            $tot_egre+=$te;
                            $c.='<td align="right">'.number_format($te,2).'</td>';
                    }
                    //TOTAL EGRESOS SUMA X FILA
                    $c.='<td align="right"><strong>'.number_format($tot_egre,2).'</strong></td>';

                     //TOTAL INVENTARIO FINAL X FILA
                     $tot_inv_fila=round($tot_ingre-$tot_egre,2);
                     $gran_tot+=$tot_inv_fila;
                     $c.='<td align="right"><strong>'.number_format($tot_inv_fila,2).'</strong></td>';


            $c.='</tr>' ;               
        }
        $c.='<tr>';
            $c.='<td colspan="'.($fil+5).'"></td>';
            $c.='<td align="right" ><strong>'.number_format($gran_tot,2).'</strong></td>';
        $c.='</tr>';
        $c.='</table>';
        return $c;
    }

        function get_unidad_2(){
        $q="SELECT * FROM unidad WHERE unid_id in (select unid_id from usuario_reporte_unidad where user_id = $this->id_usuario and unid_id not in (28,29)) ORDER by 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

//reporte resumen bodega

    function repor_resumen_valorado_bodega(){

        $fil=0;
        $c=''; 

        $detectorGrupo=$this->grupo_prod_id;

        if($detectorGrupo==0){
            $q1="SELECT p.prod_id,p.prod_nombre,p.pres_nombre as pre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id!=0 AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);

        }else{
            $q1="SELECT p.prod_id,p.prod_nombre,p.pres_nombre as pre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id=$this->grupo_prod_id AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);

        }

        
        $c.='<div style="text-align: center;"><h5>BODEGA DE ALIMENTOS</h5></div>';
        $c.='<div style="text-align: center;"><h5>RESUMEN GENERAL DE EGRESOS</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers">';
        $c.='<tr>';
        $c.="<th align='center'>N.</th>";
        $c.="<th align='center'>PRODUCTO</th>";
        $c.="<th align='center'>PRESENTACIÓN</th>";


        //ENCABEZADOS DIARIOS
        //$dias=date("d");
        $coutDias=$this->fecha_ini;
        $month= date("m", strtotime($coutDias));;
        $year= date("Y", strtotime($coutDias));;

        $primerDia=date('Y-m-d', mktime(0,0,0, $month, 1, $year));

        $dia = date("d", strtotime($coutDias));
        //$c.="<th align='center'>".date('d')."</th>";
        for ($cdias = 1; $cdias <= $dia; $cdias++) {
         $c.="<th align='center'>".$cdias."</th>";
         $c.="<th align='center'>p/u</th>";
         $c.="<th align='center'>total</th>";
        }

        $c.="<th align='center'>TOTAL UNIDADES</th>";
        $c.="<th align='center'>$ TOTAL EGRESO</th>";
        /*$c.="<th align='center'>$ PU</th>";
        $c.="<th align='center'>$ P/TOTAL</th>";*/
            //$c.="<th align='center'>".$primerDia."</th>";
        

    $c.='</tr>';
    $i=0;
    $tipo_docu_id=2;
    $final_total_egresos=0;
    $final_total=0;
    $suma_final_colum=0;
    

    foreach($t1 as $row1){ 

         $c.='<tr>';
         $i++;
                $prod_id=$row1['prod_id'];
                $c.='<td>'.$i.'</td>';
                $c.='<td>'.$row1['prod_nombre'].'</td>';
                $c.='<td>'.$row1['pre'].'</td>';
                $totegres=0;
                $valor_ege_diario=0;
                $final_costos_fila=0;

          //$valorfilas[35];
        for ($cdiasEgresos = 1; $cdiasEgresos <= $dia; $cdiasEgresos++) {

          $egresosDiarios=0;
          $punitarioegresosdias=0;
          $res_to=0;

         //$c.="<th align='center'>".$cdiasEgresos."</th>";
          $contarFechas=date('Y-m-d', mktime(0,0,0, $month, $cdiasEgresos, $year));
          
          $egresosDiarios=$this->sensor_egresos($prod_id,$tipo_docu_id,$contarFechas);
          $c.='<td align="right">'.number_format($egresosDiarios,4).'</td>';
          $punitarioegresosdias=$this->punitariodia($prod_id,$tipo_docu_id,$contarFechas);
          $c.='<td align="right" style="color: blue;">'.number_format($punitarioegresosdias,4).'</td>';
          $res_to=($egresosDiarios*$punitarioegresosdias);
          $c.='<td align="right" style="color: green;">'.number_format($res_to,4).'</td>';

          $final_costos_fila+=$res_to;

          $totegres+=$egresosDiarios;
          $valor_ege_diario+=$punitarioegresosdias;
          $valorfilas[$prod_id][$cdiasEgresos]=$egresosDiarios;
          $valorfilasdias[$prod_id][$cdiasEgresos]=$punitarioegresosdias;
          $total_dias_pre[$prod_id][$cdiasEgresos]=$res_to;
         
        }


        $ptotalegre=0;
        $punitarioegresos=0;

        
        

        $c.='<td align="right" style=" background-color: #3380FF">'.number_format($totegres,4).'</td>';
        
        $punitarioegresos=$this->punitario($prod_id,$tipo_docu_id,$coutDias);
        //$c.='<td align="right" style=" background-color: #FFD133">'.number_format($punitarioegresos,6).'</td>';

        $ptotalegre=($totegres*$punitarioegresos);
        $c.='<td align="right"  style=" background-color: #A0E7B1">'.number_format($final_costos_fila,6).'</td>';

        $final_total+=$ptotalegre;
        $final_total_egresos+=$final_costos_fila;

        $c.='</tr>' ;  
    }
    $c.='<tr>';
    $c.='<td colspan="'.(3).'"></td>';
    for ($cdiasEgresos2 = 1; $cdiasEgresos2 <= $dia; $cdiasEgresos2++) {
         $sumingfin=0; 
         $sumingfindias=0;
         $sum_dias_p=0;
        
        foreach($t1 as $row111){ 
         $prod_id=$row111['prod_id'];
                                  
         $sumingfin+=$valorfilas[$prod_id][$cdiasEgresos2];

         $sumingfindias+=$valorfilasdias[$prod_id][$cdiasEgresos2];

         $sum_dias_p+=$total_dias_pre[$prod_id][$cdiasEgresos2];

        }
        $c.='<td align="right" ><strong>'.number_format($sumingfin,4).'</strong></td>';
        $c.='<td align="right" style="color: blue;"><strong>'.number_format($sumingfindias,4).'
        </strong></td>';
        $c.='<td align="right" style="color: green;"><strong>'.number_format($sum_dias_p,4).'</strong></td>';

        $suma_final_colum+=$sum_dias_p;
                                          
                                                  
       }
       $c.='<td colspan="2" align="right"><strong>'.number_format($final_total_egresos,6).'</strong></td>';
       //$c.='<td align="right"><strong>Total/Egresos:</strong></td>';
       //$c.='<td align="right"><strong>'.number_format($suma_final_colum,4).'</strong></td>';
    $c.='</tr>' ; 

   

    $c.='<tr>';
       $c.='<td align="left"><strong>Total/Egresos $:</strong></td>';
       $c.='<td align="right"><strong>'.number_format($suma_final_colum,6).'</strong></td>';
    $c.='</tr>' ; 

     return $c;

    }

//fin resumen bodega

        function sensor_egresos($prod_id,$tipo_docu_id, $contarFechas){
        $q4="SELECT if(sum(dk.deta_kard_cant_egre) is null,0,sum(dk.deta_kard_cant_egre) ) as senso_egre FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id in (2,12) and CAST(dk.deta_kard_fech_movi AS DATE)='$contarFechas';";
        
         $rs4 =$this->con->prepare($q4);
         $rs4->execute();        
         $rw4=$rs4->fetch(PDO::FETCH_ASSOC);
         return $rw4['senso_egre'];
    }

    function punitario($prod_id,$tipo_docu_id,$coutDias){

        $q55="SELECT if(dk.deta_kard_cost_egre is null,0,dk.deta_kard_cost_egre) as puegre, dk.deta_kard_fecha FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id in (2,12) and dk.deta_kard_fecha<='$coutDias' ORDER BY dk.deta_kard_fecha DESC LIMIT 1;";

         $rs400 =$this->con->prepare($q55);
         $rs400->execute();        
         $rw400=$rs400->fetch(PDO::FETCH_ASSOC);
         return $rw400['puegre'];

    }

        function punitariodia($prod_id,$tipo_docu_id,$coutDias){

        $q55="SELECT if(dk.deta_kard_cost_egre is null,0,dk.deta_kard_cost_egre) as puegre, dk.deta_kard_fecha FROM detalle_kardex dk
        INNER JOIN kardex k ON dk.kard_id=k.kard_id INNER JOIN documento d ON dk.docu_id=d.docu_id  
        WHERE k.prod_id=$prod_id AND k.unid_id=$this->unid_id AND d.tipo_docu_id in (2,12) and CAST(dk.deta_kard_fech_movi AS DATE) BETWEEN '$coutDias' AND '$coutDias' ORDER BY dk.deta_kard_fech_movi DESC LIMIT 1;";

         $rs400 =$this->con->prepare($q55);
         $rs400->execute();        
         $rw400=$rs400->fetch(PDO::FETCH_ASSOC);
         return $rw400['puegre'];

    }




        function repor_resumen_valorado_new(){
            $unidad_reporte=$this->unid_id;
            if($unidad_reporte==4 || $unidad_reporte==9){

        $fil=0;
        $c='';        
        $q1="SELECT p.prod_id,p.prod_nombre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id=$this->grupo_prod_id AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);
        $c.='<div style="text-align: center;"><h5>RESUMEN VALORADO</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.="<th align='center'>N.</th>";
            $c.="<th align='center'>PRODUCTO</th>";
            $c.="<th align='center'>INV.INICIAL</th>";
            //ENCABEZADOS INGRESOS           
            $rs2 =$this->con->prepare($this->hijo_encabezados_resum_valorado('I'));
            $rs2->execute();        
            $t2=$rs2->fetchAll(PDO::FETCH_ASSOC);
            foreach($t2 as $row2){
                    $c.='<th>'.$row2['tipo_docu_nombre'].'</th>';    
                    $fil++;
            }
            // transferencias a satelites si es de bodega principal

             

            //TOTAL INGRESOSENCABEZADO
                $c.='<th>TOTAL INGRESOS</th>';  

                $c.="<th align='center'>TRANS.ENVIADAS</th>";

            //ENCABEZADOS EGRESOS
            $rs2_1 =$this->con->prepare($this->hijo_encabezados_resum_valorado_egreso('E'));
            $rs2_1->execute();        
            $t2_1=$rs2_1->fetchAll(PDO::FETCH_ASSOC);
            foreach($t2_1 as $row2_1){
                    $c.='<th>'.$row2_1['tipo_docu_nombre'].'</th>';
                    $fil++;
            }
            //TOTAL EGRESOS ENCABEZADO
            $c.='<th>TOTAL EGRESOS</th>';
            $c.='<th>TOTAL INV. FINAL</th>';   


        $c.='</tr>';
        $i=0;
        $gran_tot=0;
        foreach($t1 as $row1){   
            $c.='<tr>'; 
                $i++;
                $prod_id=$row1['prod_id'];
                $c.='<td>'.$i.'</td>';
                $c.='<td>'.$row1['prod_nombre'].'</td>';  

                    //INVENTARIO INICIAL COSTO UN DIA ANTERIOR AL INICIAL ESCOJIDO
                    $tot_inv_ini=0;
                    $tot_inv_ini=round($this->resumen_valorado_hijo_inicial($this->fecha_ini,$prod_id),2);
                    
                    $c.='<td align="right"><strong>'.number_format($tot_inv_ini,4).'</strong></td>';  
                    
                    //INGRESOS
                    $rs3 =$this->con->prepare($this->hijo_encabezados_resum_valorado('I'));
                    $rs3->execute();        
                    $t3=$rs3->fetchAll(PDO::FETCH_ASSOC);

                    $tot_ingre=$tot_inv_ini;                    
                    $tot_egre=0;
                   
                    foreach($t3 as $row3){
                            
                            $ti=0;
                            $tipo_docu_id=$row3['tipo_docu_id'];
                            $ti=$this->hijo_calculo_item_ingre_new($prod_id,$tipo_docu_id);
                            $tot_ingre+=$ti;                            
                            $c.='<td align="right">'.number_format($ti,4).'</td>';
                           
                    }   
                      
                    //TOTAL INGRESOS SUMA   X FILA                 
                  //  $c.='<td align="right"><strong>'.number_format($tot_transf,2).'</strong></td>';
                    $c.='<td align="right"><strong>'.number_format($tot_ingre,4).'</strong></td>';
                           foreach($t3 as $row3){
                            
                            $ti_trans=0;
                            $tipo_docu_id=$row3['tipo_docu_id'];
                            $ti_trans=$this->hijo_calculo_transf_envios($prod_id,$tipo_docu_id);
                           // $tot_transf+=$ti_trans;                            
                            $c.='<td align="right">'.number_format($ti_trans,4).'</td>';
                           
                    } 
                    //EGRESOS
                    $rs5 =$this->con->prepare($this->hijo_encabezados_resum_valorado_egreso('E'));
                    $rs5->execute();        
                    $t5=$rs5->fetchAll(PDO::FETCH_ASSOC);

  
                    foreach($t5 as $row5){
                            $te=0;
                            $tipo_docu_id=$row5['tipo_docu_id'];
                            $te=$this->hijo_calculo_item_egre_new($prod_id,$tipo_docu_id);
                            $tot_egre+=$te;
                            $c.='<td align="right">'.number_format($te,4).'</td>';
                    }

                    //TOTAL EGRESOS SUMA X FILA
                    $c.='<td align="right"><strong>'.number_format($tot_egre,4).'</strong></td>';

                            
                     //TOTAL INVENTARIO FINAL X FILA
                     $tot_inv_fila=round($tot_ingre-$tot_egre,4);
                     $gran_tot+=$tot_inv_fila;
                     $c.='<td align="right"><strong>'.number_format($tot_inv_fila,4).'</strong></td>';


            $c.='</tr>' ;               
        }
        $c.='<tr>';
            $c.='<td colspan="'.($fil+5).'"></td>';
            $c.='<td align="right" ><strong>'.number_format($gran_tot,4).'</strong></td>';
        $c.='</tr>';
        $c.='</table>';
        return $c;
            }
            else{

        $fil=0;
        $c='';        
        $q1="SELECT p.prod_id,p.prod_nombre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON p.prod_id=k.prod_id 
        WHERE p.grupo_prod_id=$this->grupo_prod_id AND k.unid_id=$this->unid_id ORDER BY p.prod_descripcion;";
        $rs1 =$this->con->prepare($q1);
        $rs1->execute();        
        $t1=$rs1->fetchAll(PDO::FETCH_ASSOC);
        $c.='<div style="text-align: center;"><h5>RESUMEN VALORADO</h5></div>';
        $c.='<div style="text-align: center;"><label>'.date("Y-m-d / H:i:s").'</label></div>';
        $c.='<table class="customers">';
        $c.='<tr>';
            $c.="<th align='center'>N.</th>";
            $c.="<th align='center'>PRODUCTO</th>";
            $c.="<th align='center'>INV.INICIAL</th>";
            //ENCABEZADOS INGRESOS           
            $rs2 =$this->con->prepare($this->hijo_encabezados_resum_valorado('I'));
            $rs2->execute();        
            $t2=$rs2->fetchAll(PDO::FETCH_ASSOC);
            foreach($t2 as $row2){
                    $c.='<th>'.$row2['tipo_docu_nombre'].'</th>';    
                    $fil++;
            }
            // transferencias a satelites si es de bodega principal

             

            //TOTAL INGRESOSENCABEZADO
            $c.="<th align='center'>TRANS.RECIBIDAS</th>";
                $c.='<th>TOTAL INGRESOS</th>';  

                

            //ENCABEZADOS EGRESOS
            $rs2_1 =$this->con->prepare($this->hijo_encabezados_resum_valorado_egreso('E'));
            $rs2_1->execute();        
            $t2_1=$rs2_1->fetchAll(PDO::FETCH_ASSOC);
            foreach($t2_1 as $row2_1){
                    $c.='<th>'.$row2_1['tipo_docu_nombre'].'</th>';
                    $fil++;
            }
            //TOTAL EGRESOS ENCABEZADO
            $c.='<th>TOTAL EGRESOS</th>';
            $c.='<th>TOTAL INV. FINAL</th>';   


        $c.='</tr>';
        $i=0;
        $gran_tot=0;
        foreach($t1 as $row1){   
            $c.='<tr>'; 
                $i++;
                $prod_id=$row1['prod_id'];
                $c.='<td>'.$i.'</td>';
                $c.='<td>'.$row1['prod_nombre'].'</td>';  

                    //INVENTARIO INICIAL COSTO UN DIA ANTERIOR AL INICIAL ESCOJIDO
                    $tot_inv_ini=0;
                    $tot_inv_ini=round($this->resumen_valorado_hijo_inicial($this->fecha_ini,$prod_id),2);
                    
                    $c.='<td align="right"><strong>'.number_format($tot_inv_ini,4).'</strong></td>';  
                    
                    //INGRESOS
                    $rs3 =$this->con->prepare($this->hijo_encabezados_resum_valorado('I'));
                    $rs3->execute();        
                    $t3=$rs3->fetchAll(PDO::FETCH_ASSOC);

                    $tot_ingre=$tot_inv_ini;                    
                    $tot_egre=0;
                   
                    foreach($t3 as $row3){
                            
                            $ti=0;
                            $tipo_docu_id=$row3['tipo_docu_id'];
                            $ti=$this->hijo_calculo_item_ingre_new($prod_id,$tipo_docu_id);
                            $tot_ingre+=$ti;                            
                            $c.='<td align="right">'.number_format($ti,4).'</td>';
                           
                    }   
                               foreach($t3 as $row3){
                            
                            $ti_trans=0;
                            $tipo_docu_id=$row3['tipo_docu_id'];
                            $ti_trans=$this->hijo_calculo_transf_recibo($prod_id,$tipo_docu_id);
                            $tot_ingre+=$ti_trans; 
                            $c.='<td align="right">'.number_format($ti_trans,4).'</td>';
                           
                    } 
                    //TOTAL INGRESOS SUMA   X FILA                 
                  //  $c.='<td align="right"><strong>'.number_format($tot_transf,2).'</strong></td>';
                    $c.='<td align="right"><strong>'.number_format($tot_ingre,4).'</strong></td>';
                  
                    //EGRESOS
                    $rs5 =$this->con->prepare($this->hijo_encabezados_resum_valorado_egreso('E'));
                    $rs5->execute();        
                    $t5=$rs5->fetchAll(PDO::FETCH_ASSOC);

  
                    foreach($t5 as $row5){
                            $te=0;
                            $tipo_docu_id=$row5['tipo_docu_id'];
                            $te=$this->hijo_calculo_item_egre_new($prod_id,$tipo_docu_id);
                            $tot_egre+=$te;
                            $c.='<td align="right">'.number_format($te,4).'</td>';
                    }

                    //TOTAL EGRESOS SUMA X FILA
                    $c.='<td align="right"><strong>'.number_format($tot_egre,4).'</strong></td>';

                            
                     //TOTAL INVENTARIO FINAL X FILA
                     $tot_inv_fila=round($tot_ingre-$tot_egre,2);
                     $gran_tot+=$tot_inv_fila;
                     $c.='<td align="right"><strong>'.number_format($tot_inv_fila,4).'</strong></td>';


            $c.='</tr>' ;               
        }
        $c.='<tr>';
            $c.='<td colspan="'.($fil+5).'"></td>';
            $c.='<td align="right" ><strong>'.number_format($gran_tot,4).'</strong></td>';
        $c.='</tr>';
        $c.='</table>'; 
        return $c;

            }
       
    }

    
    
}
