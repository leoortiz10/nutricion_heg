<?php
$funcion=$_GET['f'];
$id=isset($_GET['id']) ? $_GET['id'] : '';
$unid_id=isset($_GET['unid_id']) ? $_GET['unid_id'] : '';
$fecha_ini=isset($_GET['f_ini']) ? $_GET['f_ini'] : '';
$fecha_fin=isset($_GET['f_fin']) ? $_GET['f_fin'] : '';
$grupo_prod_id=isset($_GET['grupo_prod_id']) ? $_GET['grupo_prod_id'] : '';



include('class.php');
require_once '../dompdf/autoload.inc.php';
require_once '../phpqrcode/qrlib.php';

$cv='22';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$options = new Options();
$options->set('isRemoteEnabled', TRUE);

$datos=new Reporte();
$datos->id=$id;
$datos->unid_id=$unid_id;
$datos->fecha_ini=$fecha_ini;
$datos->fecha_fin=$fecha_fin;
$datos->grupo_prod_id=$grupo_prod_id;
$datos->ft=$cv;
$contenido=$datos->$funcion();

$dompdf->loadHtml(utf8_decode($contenido));
//landscape  :horizontal
//portrait :vertical 
$dompdf->setPaper('A4', 'portrait');
// Render the HTML as PDF
$dompdf->render();
// Output the generated PDF to Browser
//DESCARGA AUTOMATICA ARCHIVO PDF
  //  $dompdf->stream();

//VISUALIZA EN NAVEGADOR
$dompdf->stream('Nutricion',array('Attachment'=>0));

