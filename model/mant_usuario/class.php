<?php
class Usuario{

    public $con; 
    public $id;
    public $campos=array();
	public $id_usuario;

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
		
		if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }  
		$this->id_usuario=isset($_SESSION['s_user_id']) ? $_SESSION['s_user_id'] : '';
    }
	
	


    function get_all(){            
        $q="SELECT u.*,un.unid_nombre,a.area_nombre,h.hosp_nombre,stat_nombre FROM usuario u 
        INNER JOIN unidad un ON u.unid_id=un.unid_id
        INNER JOIN area a ON u.area_id=a.area_id
        INNER JOIN hospital h ON u.hosp_id=h.hosp_id
        INNER JOIN status s ON u.stat_id=s.stat_id
        ; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM usuario WHERE user_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
	
	   function get_by_id_password(){            
        $q=" SELECT * FROM usuario WHERE user_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
	
	
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
      
	  
	   $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
	    $value1=$escaped_values[0];
		$value2=$escaped_values[1];
		$value3=$escaped_values[2];
		$value4=$escaped_values[3];
		$value5=$escaped_values[4];
		$value6=$escaped_values[5];
		$value7=$escaped_values[6];
		$value8=$escaped_values[7];
		//echo $value3;
       // $values  = implode(", ", $escaped_values);
		//print_r($values);
		
		
        $q="INSERT INTO usuario ($columns,user_system) VALUES ($value1,$value2,MD5($value3),$value4,$value5,$value6,$value7,$value8,$this->id_usuario)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE usuario 
        SET user_nombre=?,user_correo=?,stat_id=?,user_nick=?,unid_id=?,hosp_id=?,area_id=?,user_system=$this->id_usuario
         WHERE user_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
		
            $this->campos['user_nombre_up'],
            $this->campos['user_correo_up'],
           // $this->campos['user_clave_up'],
			$this->campos['stat_id_up'],
            $this->campos['user_nick_up'],
            $this->campos['unid_id_up'],
            $this->campos['hosp_id_up'],
            $this->campos['area_id_up']
            //$this->campos['user_system']			
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }
	
	    function editarPassword(){
        
		$valuePasss=$this->campos['user_clave_2'];
		$mdPass=md5($valuePasss);
		//echo $mdPass;
		
	$q=" UPDATE usuario 
        SET user_clave='$mdPass'
         WHERE user_id=$this->id ;";
		  $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
		 
	
		 
       /* $st =$this->con->prepare($q);
        $st->execute([
            
            $this->campos['user_clave_2']
		
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';*/
    }

    function eliminar(){
        $q=" DELETE FROM usuario WHERE user_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }

    function get_unidad_usuario(){            
        $q=" SELECT * FROM unidad order by unid_nombre ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function get_area_usuario(){            
        $q=" SELECT area_id,area_nombre FROM area order by area_nombre ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function get_hosp(){            
        $q=" SELECT hosp_id,hosp_nombre FROM hospital order by hosp_nombre ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function get_stat(){            
        $q=" SELECT * FROM status ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}
