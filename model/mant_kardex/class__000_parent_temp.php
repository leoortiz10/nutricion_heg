<?php
include ('../config/db1.php');
class Kardex extends Conexion{
   
    public $id; //=>$kard_id; //docu_id //SEGUN ENVIO
    public $unid_id;
    public $id_usuario;
    public $campos=array();

    function __construct(){         
        parent::__construct();    
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }            
        //OBTIENE ID USUARIO 
        $this->id_usuario=$_SESSION['s_user_id'];
    
    }

    function get_all(){            
        $q="SELECT kard_id, 
        kardex.unid_id, nick as unid_nombre,
        kardex.prod_id,  prod_nombre,
        kard_minimo,
        kard_maximo,
        kard_cantidad,
        kard_entregado,	
        kard_costo,
        kard_total,        
            (SELECT 
            if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock        
            FROM detalle_kardex
            WHERE kard_id=kardex.kard_id) as stock,
            (SELECT 
            if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock_real        
            FROM detalle_kardex
            WHERE kard_id=kardex.kard_id) - if((kard_entregado)is null,0,kard_entregado) as stock_real      
        FROM kardex 
            inner join unidad on kardex.unid_id = unidad.unid_id
            inner join vw_producto_nombre on kardex.prod_id = vw_producto_nombre.prod_id and prod_status=0	";            

        if($this->unid_id<>'0')
            $q.="WHERE kardex.unid_id=$this->unid_id ";
        else
            $q.="WHERE kardex.unid_id in (select unid_id from usuario_unidad where user_id = $this->id_usuario)";         
        
        $q.="ORDER BY unid_nombre,prod_nombre; ";
        return json_encode(parent::ejecutar($q));      
    }

    function detalle_kardex(){            
        $q=" SELECT  
		deta_kard_id, 
		detalle_kardex.kard_id, 
		detalle_kardex.docu_id, documento.docu_numero, tipo_docu_nombre,
        documento.docu_observacion,
		deta_kard_fecha,
		if (deta_kard_status=0,'OK','ANUL') as deta_kard_status,
		detalle_kardex.pres_id, pres_nombre,
		detalle_kardex.lote_id, lote_codigo, lote_fech_venc,
		deta_kard_cant_ingr,
		deta_kard_cost_ingr,
		deta_kard_cant_egre,
		deta_kard_cost_egre,
		deta_kard_cant_sald,
		deta_kard_cost_sald,
		deta_kard_total,
		deta_kard_observacion,
		DATE_FORMAT(deta_kard_fech_movi,'%X-%m-%d') as deta_kard_fech_movi
     FROM detalle_kardex
	 left join farm_presentacion on detalle_kardex.pres_id = farm_presentacion.pres_id
 	 left join lote on detalle_kardex.lote_id = lote.lote_id
	 inner join documento on detalle_kardex.docu_id = documento.docu_id 
 	 inner join tipo_documento on documento.tipo_docu_id = tipo_documento.tipo_docu_id 
	 where kard_id=$this->id; ";
       return json_encode(parent::ejecutar($q));   
    }

    function editar(){
       
            $q=" UPDATE kardex
            SET kard_minimo=?,kard_maximo=?
            WHERE kard_id=$this->id ;";
            $st =$this->con->prepare($q);
            $st->execute([
                $this->campos['kard_minimo'],
                $this->campos['kard_maximo'],                
            ]);        
            return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
        
    }

    function info_kardex_documento(){
       $q="SELECT u1.unid_nombre as origen,u2.unid_nombre as destino,docu_autoriza,docu_entrega,docu_recibe,docu_observacion FROM documento d 
       INNER JOIN unidad u1 ON d.unid_id=u1.unid_id
       LEFT JOIN unidad u2 ON d.unid_id_destino=u2.unid_id
       WHERE docu_id=$this->id ;";
       $rs =parent::ejecutar($q);
       $c='<table class="table">';
       $c.='<tbody>';
       foreach($rs as $row){
            $c.='<tr>';
                $c.='<td align="right"><span class="badge badge-success">De:</span></td>';
                $c.='<td align="left">'.$row['origen'].'</td>';
                $c.='<td></td>';
            $c.='</tr>';
            $c.='<tr>';
                $c.='<td align="right"><span class="badge badge-success">A:</span></td>';
                $c.='<td align="left">'.$row['destino'].'</td>';
                $c.='<td></td>';
            $c.='</tr>';
            $c.='<tr>';
                $c.='<td align="right"><span class="badge badge-success">Autoriza:</span></td>';
                $c.='<td align="left">'.$row['docu_autoriza'].'</td>';
                $c.='<td></td>';
            $c.='</tr>';
            $c.='<tr>';
                $c.='<td align="right"><span class="badge badge-success">Entrega:</span></td>';
                $c.='<td align="left">'.$row['docu_entrega'].'</td>';
                $c.='<td></td>';
            $c.='</tr>';
            $c.='<tr>';
                $c.='<td align="right"><span class="badge badge-success">Recibe:</span></td>';
                $c.='<td align="left">'.$row['docu_recibe'].'</td>';
                $c.='<td></td>';
            $c.='</tr>';
            $c.='<tr>';
                $c.='<td align="right"><span class="badge badge-success">Observación:</span></td>';
                $c.='<td align="left">'.$row['docu_observacion'].'</td>';
                $c.='<td></td>';
            $c.='</tr>';
       }
       $c.='</tbody>';
       $c.='</table>';
       return $c;
    }

    function get_by_id(){
        $q=" SELECT * FROM kardex WHERE kard_id=$this->id; ";
        return json_encode(parent::ejecutar($q));    
        
        
    }

    function get_producto_info(){
        $q="SELECT p.prod_nombre FROM kardex k
        INNER JOIN vw_producto_nombre p ON  p.prod_id=k.prod_id
        WHERE k.kard_id=$this->id; "; 
       return json_encode(parent::ejecutar($q)); 
    }

    function get_totales(){
        $q="SELECT if(sum(deta_kard_cant_ingr) is null,0,sum(deta_kard_cant_ingr)) as ingreso,
        if(sum(deta_kard_cant_egre) is null,0 ,sum(deta_kard_cant_egre)) as egreso,
        if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as total
        FROM detalle_kardex
        WHERE kard_id=$this->id;";
        return json_encode(parent::ejecutar($q));
    }

    function get_unidad(){
        $q="SELECT * FROM unidad WHERE unid_id in (select unid_id from usuario_reporte_unidad where user_id = $this->id_usuario) ORDER by 2; "; 
      return json_encode(parent::ejecutar($q));
    }

    function get_movimientos_kardex(){
        $q="SELECT distinct(dk.deta_kard_fecha) as fecha,
        if (sum(deta_kard_cant_egre) is null,0,sum(deta_kard_cant_egre)) as salida,
        if (sum(deta_kard_cant_ingr) is null,0,sum(deta_kard_cant_ingr)) as ingreso
        FROM detalle_kardex dk
                INNER JOIN kardex k ON k.kard_id=dk.kard_id
                WHERE k.kard_id=$this->id
                GROUP BY 1
                ORDER BY 1 desc
                LIMIT 60;";
       return json_encode(parent::ejecutar($q)); 
       
    }

    function ajuste_kardex(){
		$sql="SELECT deta_kard_id, deta_kard_cant_ingr,deta_kard_cost_ingr ,deta_kard_cant_egre ,deta_kard_cost_egre, deta_kard_cant_sald, deta_kard_cost_sald, deta_kard_total FROM detalle_kardex	
	    where kard_id = $this->id";
		$rs_detalle_documento =$this->con->prepare($sql);
        $rs_detalle_documento->execute();      
		
		$deta_kard_id="";
		$deta_kard_cant_ingr=""; 	
		$deta_kard_cost_ingr=""; 	
		$deta_kard_cant_egre=""; 	
		$deta_kard_cost_egre=""; 	
		$deta_kard_cant_sald=""; 	
		$deta_kard_cost_sald=""; 	
		$deta_kard_total="";
		$acumulado_cant_sald=0;
		$acumulado_cost_sald=0;
		
        //while($fila=mysqli_fetch_object($result))
        while (($fila= $rs_detalle_documento->fetch(PDO::FETCH_OBJ))){
			$deta_kard_id = $fila->deta_kard_id;
			$deta_kard_cant_ingr=$fila->deta_kard_cant_ingr; 	
			$deta_kard_cost_ingr=$fila->deta_kard_cost_ingr; 	
			$deta_kard_cant_egre=$fila->deta_kard_cant_egre; 	
			$deta_kard_cost_egre=$fila->deta_kard_cost_egre; 	
			$deta_kard_cant_sald=$fila->deta_kard_cant_sald; 	
			$deta_kard_cost_sald=$fila->deta_kard_cost_sald; 	
			$deta_kard_total=$fila->deta_kard_total;
			
			
			if ($deta_kard_cant_ingr!=null){
				$nuevo_kard_cant_sald = $acumulado_cant_sald+$deta_kard_cant_ingr;
				if ($deta_kard_cant_ingr + $acumulado_cant_sald!=0){
					$nuevo_kard_cost_sald =( ($deta_kard_cant_ingr * $deta_kard_cost_ingr) + ($acumulado_cant_sald * $acumulado_cost_sald)) / ($nuevo_kard_cant_sald);
				}
				else{
					$nuevo_kard_cost_sald=0;
				}
				$nuevo_kard_cost_sald=round($nuevo_kard_cost_sald, 4);
				$nuevo_kard_total = ($nuevo_kard_cant_sald * $nuevo_kard_cost_sald);
				$nuevo_kard_total=round($nuevo_kard_total, 4);
				
				if (($nuevo_kard_cant_sald) == $deta_kard_cant_sald && $nuevo_kard_cost_sald == $deta_kard_cost_sald && $nuevo_kard_total == $deta_kard_total ){
					 $acumulado_cant_sald = $deta_kard_cant_sald;
					 $acumulado_cost_sald = $deta_kard_cost_sald;
				}
				else{
					
					$deta_kard_cant_sald = $deta_kard_cant_ingr + $acumulado_cant_sald;
					if ($deta_kard_cant_sald !=0){
						$deta_kard_cost_sald = (($deta_kard_cant_ingr * $deta_kard_cost_ingr) + ($acumulado_cant_sald* $acumulado_cost_sald)) / ($deta_kard_cant_ingr + $acumulado_cant_sald);
					}
					else{
						$deta_kard_cost_sald=0;
					}
					$deta_kard_total = $deta_kard_cant_sald * $deta_kard_cost_sald;
					$sql="UPDATE detalle_kardex  set deta_kard_cant_sald=$deta_kard_cant_sald , deta_kard_cost_sald=$deta_kard_cost_sald ,deta_kard_total=$deta_kard_total   where deta_kard_id=$deta_kard_id";					
                    $rs =$this->con->prepare($sql);
                    $rs->execute();

					 $acumulado_cant_sald = $deta_kard_cant_sald;
					 $acumulado_cost_sald = $deta_kard_cost_sald;
				}
				
				
			}
			
			if ($deta_kard_cant_egre!=null){
				$nuevo_kard_cant_sald = $acumulado_cant_sald-$deta_kard_cant_egre;
				if ( $nuevo_kard_cant_sald !=0){
					$nuevo_kard_cost_sald =(  ($acumulado_cant_sald * $acumulado_cost_sald)-($deta_kard_cant_egre * $deta_kard_cost_egre)) / ($acumulado_cant_sald-$deta_kard_cant_egre);
				}
				else{
					$nuevo_kard_cost_sald=0;
				}
				$nuevo_kard_cost_sald=round($nuevo_kard_cost_sald, 4);
				$nuevo_kard_total = ($nuevo_kard_cant_sald * $nuevo_kard_cost_sald);
				$nuevo_kard_total=round($nuevo_kard_total, 4);

				
				if ( $nuevo_kard_cant_sald == $deta_kard_cant_sald && $nuevo_kard_cost_sald == $deta_kard_cost_sald && $nuevo_kard_total == $deta_kard_total ){
					 $acumulado_cant_sald = $deta_kard_cant_sald;
					 $acumulado_cost_sald = $deta_kard_cost_sald;
				}
				else{
					
					$deta_kard_cant_sald =  $acumulado_cant_sald-$deta_kard_cant_egre;
					if ($deta_kard_cant_sald !=0){
						$deta_kard_cost_sald = ( ($acumulado_cant_sald* $acumulado_cost_sald)-($deta_kard_cant_egre * $deta_kard_cost_egre)) / ( $acumulado_cant_sald-$deta_kard_cant_egre);
					}
					else{
						$deta_kard_cost_sald=0;
					}
					$deta_kard_total = $deta_kard_cant_sald * $deta_kard_cost_sald;
					$sql="UPDATE detalle_kardex  set deta_kard_cant_sald=$deta_kard_cant_sald , deta_kard_cost_sald=$deta_kard_cost_sald ,deta_kard_total=$deta_kard_total   where deta_kard_id=$deta_kard_id";
                    $rs =$this->con->prepare($sql);
                    $rs->execute();	
					 $acumulado_cant_sald = $deta_kard_cant_sald;
					 $acumulado_cost_sald = $deta_kard_cost_sald;
				}
			}
			
								
		}
		// actualizar los valores del kardex
		
		$sql = "UPDATE kardex set kard_cantidad = $acumulado_cant_sald , kard_costo = $acumulado_cost_sald, kard_total = $acumulado_cant_sald * $acumulado_cost_sald where kard_id =$this->id   ";
        $rs =$this->con->prepare($sql);
        $rs->execute();
	}

}