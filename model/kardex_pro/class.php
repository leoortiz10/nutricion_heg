<?php
class Ingreso{

    public $con; 
    public $id;
	public $id_d;
	public $hosp_id;
	public $unid_id;
	public $unid_id_destino;
	public $prod_id;
	public $kard_id;
	public $cantidad_pedido;
    public $tipo_docu_id;
    public $id_usuario;
    public $term;
	public $campos=array();
	
	public $recibe;
	public $tipo;

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }            
        //OBTIENE ID USUARIO 
		//$this->id_usuario=$_SESSION['s_user_id'];
		//$this->hosp_id=$_SESSION['s_user_hosp_id'];    

		$this->id_usuario=isset($_SESSION['s_user_id']) ? $_SESSION['s_user_id'] : '';
		$this->hosp_id=isset($_SESSION['s_user_hosp_id']) ? $_SESSION['s_user_hosp_id'] : '';
		
    }

    function get_all(){            
        $q="SELECT docu_id, 
        documento.tipo_docu_id, tipo_docu_nombre,
        documento.unid_id,	u1.nick as unid_nombre,a.area_nombre,
		documento.unid_id_destino,	u2.nick as unid_destino,
        docu_numero,
        docu_fecha,
        IFNULL(documento.prov_id,0 ) as prov_id, IFNULL(prov_nombre,'S/P') as prov_nombre,
        docu_prov_fech,
        docu_factura,
        docu_observacion,        
		IFNULL(docu_autoriza,'') as docu_autoriza,
		IFNULL(docu_entrega,'') as docu_entrega,
		IFNULL(docu_recibe,'') as docu_recibe,
		
        documento.stat_docu_id,documento.stat_docu_id_destino, stat_docu_nombre
     FROM documento 
            inner join unidad u1 on documento.unid_id = u1.unid_id            			
            inner join status_documento on documento.stat_docu_id = status_documento.stat_docu_id
            inner join tipo_documento on documento.tipo_docu_id = tipo_documento.tipo_docu_id
			left join area a on documento.area_id = a.area_id  
			left join unidad u2 on documento.unid_id_destino = u2.unid_id  
			left join proveedor on documento.prov_id = proveedor.prov_id
		 where tipo_docu_movimiento = '$this->tipo' ";
		 
		 if($this->tipo=='P' and $this->recibe==1)
			 $q.=" and documento.stat_docu_id=1 ";
		 else
		 	 $q.=" and documento.stat_docu_id<=2 ";
        
		if($this->tipo=='T' and $this->recibe=='1')
			$q.=" and u2.unid_id in (select unid_id from usuario_unidad where user_id =$this->id_usuario)  ";		
		elseif($this->tipo=='P' && $this->recibe=='0')
			$q.=" and u2.unid_id in (select unid_id from usuario_unidad where user_id =$this->id_usuario)";
		else
			$q.=" and u1.unid_id in (select unid_id from usuario_unidad where user_id =$this->id_usuario)  ";

    	if($this->unid_id>0)
		$q.=" and documento.unid_id=$this->unid_id ";
		
		if($this->unid_id_destino>0)
        $q.=" and documento.unid_id_destino=$this->unid_id_destino ";
    
    	if($this->tipo_docu_id>0)
        $q.=" and documento.tipo_docu_id=$this->tipo_docu_id ";
        
    	$q.=" ORDER BY docu_id desc ";

        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
	}

    function detalle_ingreso(){            
        $q=" SELECT  
		deta_docu_id, documento.docu_numero, 
		detalle_documento.prod_id, 
		prod_nombre,
		farm_presentacion.pres_nombre as presentacion,
		deta_docu_regi_sani,
		deta_docu_lote,
		deta_docu_fech_venc,
		deta_docu_cantidad,
		FORMAT(ROUND(deta_docu_valo_unit,6),6) as deta_docu_valo_unit,
		deta_docu_iva,
		FORMAT(ROUND((deta_docu_cantidad*deta_docu_valo_unit*deta_docu_iva),4),4) as deta_docu_valo_iva,
		FORMAT(ROUND((deta_docu_cantidad*deta_docu_valo_unit*(1+deta_docu_iva)),6),6) as deta_docu_subtotal
     FROM detalle_documento
	 inner join documento on detalle_documento.docu_id = documento.docu_id
	 inner join vw_producto_nombre  on  detalle_documento.prod_id=vw_producto_nombre.prod_id
	 left join (farm_producto left join farm_presentacion on farm_presentacion.pres_id = farm_producto.pres_id)
	 on detalle_documento.prod_id=farm_producto.prod_id
	 
     WHERE detalle_documento.docu_id=$this->id ORDER by prod_nombre; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);       
	}
	

	function docu_numero(){
		$t=$this->tipo;

			$q="SELECT count(docu_id)+1 as num FROM documento WHERE unid_id=$this->unid_id and tipo_docu_id=$this->tipo_docu_id ;";
			$st =$this->con->prepare($q);
        	$st->execute();
			$rs = $st->fetch(PDO::FETCH_ASSOC);
			$num=$rs['num'];

		if($t=='T' || $t=='P'){			
			//$q="SELECT count(docu_id)+1 as num FROM documento WHERE unid_id=$this->unid_id and unid_id_destino=$this->unid_id_destino and tipo_docu_id=$this->tipo_docu_id ;";
			//$st =$this->con->prepare($q);
        	//$st->execute();
			//$rs = $st->fetch(PDO::FETCH_ASSOC);
			//$num=$rs['num'];
			
			$qa="SELECT unid_siglas FROM unidad WHERE unid_id=$this->unid_id_destino;";
			$sta =$this->con->prepare($qa);
        	$sta->execute();
			$rsa = $sta->fetch(PDO::FETCH_ASSOC);
			$unid2=$rsa['unid_siglas'];
		}

		


		$q2="SELECT h.hosp_siglas,u.unid_siglas FROM hospital h INNER JOIN unidad u ON u.hosp_id=h.hosp_id	WHERE u.unid_id=$this->unid_id LIMIT 1;";
		$st2 =$this->con->prepare($q2);
        $st2->execute();
		$rs2 = $st2->fetch(PDO::FETCH_ASSOC);
		$hosp=$rs2['hosp_siglas'];
		$unid1=$rs2['unid_siglas'];

		$q3="SELECT tipo_docu_siglas FROM tipo_documento WHERE tipo_docu_id=$this->tipo_docu_id; ";
		$st3 =$this->con->prepare($q3);
        $st3->execute();
		$rs3 = $st3->fetch(PDO::FETCH_ASSOC);
		$tdoc=$rs3['tipo_docu_siglas'];

		if($t=='T')
			$u=$unid1.'/'.$unid2;
		elseif($t=='P')
			$u=$unid2.'/'.$unid1;
		else
			$u=$unid1;
		
		

		$doc=$hosp.'-'.$u.'-'.$tdoc.'-'.$t.'-'.$num;

        return $doc;
	}

	function count_lote(){
		

			$q="SELECT count(id_codigos)+1 as num FROM codigos_lotes_incrementables";
			$st =$this->con->prepare($q);
        	$st->execute();
			$rs = $st->fetch(PDO::FETCH_ASSOC);
			$num=$rs['num'];
			$add_lote_instantaneo='HEG-LOTE-NUTRICION';
			return $add_lote_instantaneo.'-'.$num;

		}

			function registro_sanitario(){
		

			$q="SELECT count(id_codigos)+1 as num_reg FROM codigos_lotes_incrementables";
			$st =$this->con->prepare($q);
        	$st->execute();
			$rs = $st->fetch(PDO::FETCH_ASSOC);
			$num_reg=$rs['num_reg'];
			$add_lote_instantaneo='HEG-RSANITARIO-NUTRICION';
			return $add_lote_instantaneo.'-'.$num_reg;

		}


					function registro_lote(){
		

			$q="SELECT count(id_codigos)+1 as num_reg FROM codigos_lotes_incrementables";
			$st =$this->con->prepare($q);
        	$st->execute();
			$rs = $st->fetch(PDO::FETCH_ASSOC);
			$num_reg=$rs['num_reg'];
			$add_lote_instantaneo='HEG-RSANITARIO-NUTRICION';
			return $add_lote_instantaneo.'-'.$num_reg;

		}

	function stock_bodega_pedido(){            
        $q=" SELECT kard_id, 
        prod_nombre, 
            (SELECT 
            if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock1       
            FROM detalle_kardex
            WHERE kard_id=k1.kard_id) - if((kard_entregado)is null,0,kard_entregado) as stock_real,
             (SELECT 
            if(sum(dk2.deta_kard_cant_ingr)is null,0,sum(dk2.deta_kard_cant_ingr)) 
            - if(sum(dk2.deta_kard_cant_egre)is null,0,sum(dk2.deta_kard_cant_egre))
            - if((k2.kard_entregado)is null,0,k2.kard_entregado) as stock1       
            FROM detalle_kardex dk2
            INNER JOIN kardex k2 ON k2.kard_id=dk2.kard_id 
            WHERE  k2.unid_id=$this->unid_id_destino and k1.prod_id=k2.prod_id) as mi_stock
     FROM kardex k1
            inner join unidad on k1.unid_id = unidad.unid_id
            inner join vw_producto_nombre on k1.prod_id = vw_producto_nombre.prod_id and prod_status=0	 
          WHERE k1.unid_id=$this->unid_id       
     ORDER BY unid_nombre,prod_nombre; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);       
	}
	

	function procesar_documento_pedido(){
		$fecha_hora=date("Y-m-d H:i:s");
		$q="UPDATE documento d INNER JOIN detalle_documento dd ON dd.docu_id=d.docu_id SET d.stat_docu_id=1,d.docu_procesado='$fecha_hora' WHERE d.stat_docu_id=0 and stat_docu_id_destino=0 and d.docu_id=$this->id;";
		$st =$this->con->prepare($q);
		$st->execute();
		$m="";
		if($st->rowCount()>0)
			$m="Pedido Realizado Correctamente !!!";
		else	
			$m="No se pudo procesar, verifique que contenga items, ó el pedido ya está procesado.";
		
		return $m;
	}
	
	function revisado_documento_pedido(){
			$fecha_hora=date("Y-m-d H:i:s");
			$q="UPDATE documento SET stat_docu_id_destino=1,docu_revisado='$fecha_hora',user_id=$this->id_usuario WHERE docu_id=$this->id and stat_docu_id=1 and stat_docu_id_destino=0;";
			$st =$this->con->prepare($q);
        	$st->execute();
			return $st->rowCount().' Pedido Marcado a Revisado';
	}

   function consultar_documentos_pendientes(){    
    $q="SELECT count(docu_id) as tot_documentos FROM documento d
        INNER JOIN tipo_documento tm ON tm.tipo_docu_id=d.tipo_docu_id
        WHERE d.user_id=$this->id_usuario and stat_docu_id = 0 and tm.tipo_docu_movimiento='I';";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetch(PDO::FETCH_ASSOC);
    return $rs['tot_documentos'];
    }

       function consultar_documentos_pendientes_egresos(){    
    $q="SELECT count(docu_id) as tot_documentos FROM documento d
        INNER JOIN tipo_documento tm ON tm.tipo_docu_id=d.tipo_docu_id
        WHERE d.user_id=$this->id_usuario and stat_docu_id = 0 and tm.tipo_docu_movimiento='E';";
    $st =$this->con->prepare($q);
    $st->execute();
    $rs = $st->fetch(PDO::FETCH_ASSOC);
    return $rs['tot_documentos'];
    }

    function editar(){


       switch($this->tipo){
		   case 'I':
		   			$status_doc="SELECT stat_docu_id FROM documento WHERE docu_id=$this->id;";
		   			$std =$this->con->prepare($status_doc);
		   			$std->execute();
		   		    $rsd = $std->fetch(PDO::FETCH_ASSOC);
			        $status=$rsd['stat_docu_id'];   

			        if($status==0){

			        	$q=" UPDATE documento
					SET docu_fecha=?,tipo_docu_id=?,unid_id=?,docu_numero=?,docu_prov_fech=?,prov_id=?,area_id=?,docu_factura=?,docu_recibe=?,docu_observacion=?,user_id=$this->id_usuario
					WHERE docu_id=$this->id ;";
					$st =$this->con->prepare($q);
					$st->execute([
						$this->campos['docu_fecha'],
						$this->campos['tipo_docu_id'],
						$this->campos['unid_id'],
						trim($this->campos['docu_numero']),
						$this->campos['docu_prov_fech'],
						$this->campos['prov_id'],					
						isset($this->campos['area_id']) ? $this->campos['area_id'] : NULL,
						trim($this->campos['docu_factura']),
						$this->campos['docu_recibe'],
						$this->campos['docu_observacion'],
					        
					]);     
					return $st->rowCount();
			        }else{
			        	$q=" UPDATE documento
					SET docu_observacion=?,user_id=$this->id_usuario
					WHERE docu_id=$this->id ;";
					$st =$this->con->prepare($q);
					$st->execute([
						
						$this->campos['docu_observacion'],
					        
					]);     
					return $st->rowCount();
			        }

					
			break;
			case 'E':
					$status_doc="SELECT stat_docu_id FROM documento WHERE docu_id=$this->id;";
		   			$std =$this->con->prepare($status_doc);
		   			$std->execute();
		   		    $rsd = $std->fetch(PDO::FETCH_ASSOC);
			        $status=$rsd['stat_docu_id'];   

			        if($status==0){

					$q=" UPDATE documento
					SET docu_fecha=?,tipo_docu_id=?,unid_id=?,docu_numero=?,docu_autoriza=?,docu_entrega=?,docu_recibe=?,docu_observacion=?,area_id=?,prov_id=?,user_id=$this->id_usuario
					WHERE docu_id=$this->id ;";
					$st =$this->con->prepare($q);
					$st->execute([
						$this->campos['docu_fecha'],
						$this->campos['tipo_docu_id'],
						$this->campos['unid_id'],
						trim($this->campos['docu_numero']),					
						$this->campos['docu_autoriza'],					
						$this->campos['docu_entrega'],	
						$this->campos['docu_recibe'],	
						$this->campos['docu_observacion'],						
						isset($this->campos['area_id']) ? $this->campos['area_id'] : NULL,
						isset($this->campos['prov_id']) ? $this->campos['prov_id'] : NULL
					]);     
					return $st->rowCount();
				}else{
			        	$q=" UPDATE documento
					SET docu_observacion=?,user_id=$this->id_usuario
					WHERE docu_id=$this->id ;";
					$st =$this->con->prepare($q);
					$st->execute([
						
						$this->campos['docu_observacion'],
					        
					]);     
					return $st->rowCount();
			        }
			break;
			case 'T':
					$q=" UPDATE documento
					SET docu_fecha=?,docu_numero=?,unid_id=?,unid_id_destino=?,docu_autoriza=?,docu_entrega=?,docu_recibe=?,docu_observacion=?,user_id=$this->id_usuario
					WHERE docu_id=$this->id and stat_docu_id=0 ;";
					$st =$this->con->prepare($q);
					$st->execute([
						$this->campos['docu_fecha'],
						trim($this->campos['docu_numero']),					
						(!isset($this->campos['unid_id']) || $this->campos['unid_id'] == false) ? 'true ' :$this->campos['unid_id'],
						(!isset($this->campos['unid_id_destino']) || $this->campos['unid_id_destino'] == false) ? 'true ' :$this->campos['unid_id_destino'],						
						$this->campos['docu_autoriza'],
						$this->campos['docu_entrega'],
						$this->campos['docu_recibe'],
						$this->campos['docu_observacion']               
					]);  				
					return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
			break;
		}
          
        
	}
	
	function editar_deta_ingre(){
		
		//$d1=$this->campos['regsani_edit'];
		//$d2=trim($this->campos['lote_edit']);
		//$d3=$this->campos['fecha_edit'];
		$d4=$this->campos['cant_edit'];
		$d5=$this->campos['vunit_edit'];
					 /*$q=" UPDATE detalle_documento dd
					 INNER JOIN documento d ON dd.docu_id=d.docu_id
					 SET deta_docu_regi_sani='$d1',deta_docu_lote='$d2',deta_docu_fech_venc='$d3',deta_docu_cantidad=$d4,deta_docu_valo_unit=$d5
					 WHERE deta_docu_id=$this->id_d and d.stat_docu_id=0 ;";
					 $st =$this->con->prepare($q);
					 $st->execute();     					 
					 $r="";        
					 if($st->rowCount()>0)
						 $r='ACTUALIZADO CORRECTAMENTE';    
					 else
						 $r='NO SE PUEDE ACTUALIZAR EL REGISTRO!!!';    
					 
					 return $r;*/

					 	 $q=" UPDATE detalle_documento dd
					 INNER JOIN documento d ON dd.docu_id=d.docu_id
					 SET deta_docu_cantidad=$d4,deta_docu_valo_unit=$d5
					 WHERE deta_docu_id=$this->id_d and d.stat_docu_id=0 ;";
					 $st =$this->con->prepare($q);
					 $st->execute();     					 
					 $r="";        
					 if($st->rowCount()>0)
						 $r='ACTUALIZADO CORRECTAMENTE';    
					 else
						 $r='NO SE PUEDE ACTUALIZAR EL REGISTRO!!!';    
					 
					 return $r;
	 }
 

	
	

	 function eliminar(){       
        //2 = ANULADO
        //SOLO ANULA LOS DOCUMENTOS QUE ESTAN SIN PROCESAR ESTADO 0
		
        $q=" UPDATE documento SET stat_docu_id=2,user_id=$this->id_usuario WHERE docu_id=$this->id and stat_docu_id=0 ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        $r="";        
        if($st->rowCount()>0)
            $r='Documento Anulado/Eliminado Correctamente !!!';    
        else
            $r='No se Puede Anular/Eliminar éste Documento !!!';    
        
        return $r;
	}
	
	function get_det_ingre_by_id(){               
        $q=" SELECT * FROM detalle_documento WHERE deta_docu_id=$this->id_d;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO documento ($columns,user_id) VALUES ($values,$this->id_usuario)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount();
	}
	
	function lotes_productos(){
		//HAVING >0;   SOLO MUESTRA PRODUCTOS CON LOTES QUE TENGAN STOCK MAYOR A >0
		$fecha_actual=date('Y-m-d');
		$unidad_transfer=$this->unid_id;
		$unidad_transfer_dest=$this->unid_id_destino;
/*
		if($unidad_transfer==4 || $unidad_transfer==9){

				$q="SELECT sk.lote_id,fm.pres_id as pres, l.lote_codigo,sum(sk.sald_kard_cantidad ) as cantidad,l.lote_fech_venc as fech,l.lote_regi_sani as regi,sk.sald_kard_precio
		FROM lote l 
        INNER JOIN saldo_kardex sk ON l.lote_id=sk.lote_id
        INNER JOIN kardex k ON sk.kard_id=k.kard_id
        INNER JOIN farm_producto fm ON fm.prod_id=l.prod_id
        WHERE l.prod_id=$this->prod_id and k.unid_id=$this->unid_id
        and l.lote_fech_venc>='$fecha_actual' 
		GROUP BY sk.lote_id HAVING cantidad >0 ORDER BY lote_fech_venc ASC";
		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);  

		}else{
			if($unidad_transfer_dest==4){
				$q="SELECT sk.lote_id,fm.pres_id as pres, l.lote_codigo,sum(sk.sald_kard_cantidad ) as cantidad,l.lote_fech_venc as fech,l.lote_regi_sani as regi,sk.sald_kard_precio
		FROM lote l 
        INNER JOIN saldo_kardex sk ON l.lote_id=sk.lote_id
        INNER JOIN kardex k ON sk.kard_id=k.kard_id
        INNER JOIN farm_producto fm ON fm.prod_id=l.prod_id
        WHERE l.prod_id=$this->prod_id and k.unid_id=$this->unid_id        
		GROUP BY sk.lote_id HAVING cantidad >0 ORDER BY lote_fech_venc ASC";
		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);  

		}else{
						if($unidad_transfer_dest==9){
				$q="SELECT sk.lote_id,fm.pres_id as pres, l.lote_codigo,sum(sk.sald_kard_cantidad ) as cantidad,l.lote_fech_venc as fech,l.lote_regi_sani as regi,sk.sald_kard_precio
		FROM lote l 
        INNER JOIN saldo_kardex sk ON l.lote_id=sk.lote_id
        INNER JOIN kardex k ON sk.kard_id=k.kard_id
        INNER JOIN farm_producto fm ON fm.prod_id=l.prod_id
        WHERE l.prod_id=$this->prod_id and k.unid_id=$this->unid_id
		GROUP BY sk.lote_id HAVING cantidad >0 ORDER BY lote_fech_venc ASC";
		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);  

		}else{
							$q="SELECT sk.lote_id,fm.pres_id as pres, l.lote_codigo,sum(sk.sald_kard_cantidad ) as cantidad,l.lote_fech_venc as fech,l.lote_regi_sani as regi,sk.sald_kard_precio
		FROM lote l 
        INNER JOIN saldo_kardex sk ON l.lote_id=sk.lote_id
        INNER JOIN kardex k ON sk.kard_id=k.kard_id
        INNER JOIN farm_producto fm ON fm.prod_id=l.prod_id
        WHERE l.prod_id=$this->prod_id and k.unid_id=$this->unid_id
        and l.lote_fech_venc>='$fecha_actual'
		GROUP BY sk.lote_id HAVING cantidad >0 ORDER BY lote_fech_venc ASC";
		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
		}
		}
	}*/
	$q="SELECT pr.pres_nombre as pn ,sk.lote_id,fm.pres_id as pres, l.lote_codigo,sum(sk.sald_kard_cantidad ) as cantidad,l.lote_fech_venc as fech,l.lote_regi_sani as regi,sk.sald_kard_precio
		FROM lote l 
        INNER JOIN saldo_kardex sk ON l.lote_id=sk.lote_id
        INNER JOIN kardex k ON sk.kard_id=k.kard_id
        INNER JOIN farm_producto fm ON fm.prod_id=l.prod_id
        INNER JOIN farm_presentacion pr on pr.pres_id=fm.pres_id
        WHERE l.prod_id=$this->prod_id and k.unid_id=$this->unid_id
		GROUP BY sk.lote_id HAVING cantidad >0 ORDER BY lote_fech_venc ASC";
		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
	}

	function lotes_productos_reingreso(){
		//HAVING >0;   SOLO MUESTRA PRODUCTOS CON LOTES QUE TENGAN STOCK MAYOR A >0
		$q="SELECT sk.lote_id, l.lote_codigo,l.lote_fech_venc as fech,l.lote_regi_sani as regi,sk.sald_kard_precio
		FROM lote l 
        INNER JOIN saldo_kardex sk ON l.lote_id=sk.lote_id
        INNER JOIN kardex k ON sk.kard_id=k.kard_id
        WHERE l.prod_id=$this->prod_id and k.unid_id=$this->unid_id 
		GROUP BY sk.lote_id ORDER BY lote_fech_venc ASC;";
		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);  
	}


    function get_by_id(){
        $q=" SELECT * FROM documento WHERE docu_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);    
	}
	
		function get_unidad_todos_admin(){
		//$identificador = $_REQUEST['identificador'];
		//$idorigen_quitar=4;
		
		$q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id;"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 

		
       
    }

	function get_unidad_todos(){
		$identificador = $_REQUEST['identificador'];
		//$idorigen_quitar=4;
		if($identificador==0){


        $q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and unid_id!=$identificador;"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 

		}else
		{

			if($identificador==9){
				$q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and unid_id!=$identificador and unid_id in (8,11);"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
				
			}else
			if($identificador==4)
			{
		$q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and unid_id!=$identificador and unid_id not in (8,9,11);"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }else
    if($identificador==8)
    {
    	$q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and unid_id!=$identificador and unid_id in (9,11);"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }else
        if($identificador==11)
    {
    	$q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and unid_id!=$identificador and unid_id in (9,8);"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }else
    //if($identificador==5)
    {

    	$q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and unid_id!=$identificador and unid_id not in (8,9,11); "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }

		}
       
    }
	function get_unidad_pedido(){
        $q="SELECT * FROM unidad u
		INNER JOIN hospital h ON u.hosp_id=h.hosp_id
		WHERE h.hosp_id=$this->hosp_id and u.unid_id=4;"; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function get_unidad(){
        $q="SELECT * FROM unidad WHERE unid_id in (select unid_id from usuario_unidad where user_id = $this->id_usuario) ORDER by 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    
    function get_tipo_doc(){

    	$idhospitalaria="SELECT count(user_unid_id) as counter FROM `usuario_unidad` WHERE user_id=$this->id_usuario and unid_id=10;";
    	$hospita =$this->con->prepare($idhospitalaria);
		   			$hospita->execute();
		   		    $rsdhospi = $hospita->fetch(PDO::FETCH_ASSOC);
			        $statuscount=$rsdhospi['counter']; 
	    if($statuscount!=0){
	    	$q="SELECT * FROM tipo_documento WHERE tipo_docu_movimiento='$this->tipo' and tipo_docu_id!=9  and tipo_docu_id!=4 ORDER BY 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 

	    }else{
	    	$q="SELECT * FROM tipo_documento WHERE tipo_docu_movimiento='$this->tipo' and tipo_docu_id!=9  and tipo_docu_id!=4 and tipo_docu_id!=6 ORDER BY 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
	    }

        
    }
     function get_tipo_doc_admin(){
        $q="SELECT * FROM tipo_documento WHERE tipo_docu_movimiento='$this->tipo' and tipo_docu_id!=9 ORDER BY 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function get_info_docu(){
        $q="SELECT d.docu_numero,d.docu_fecha,u.unid_nombre,t.tipo_docu_nombre,
		u.unid_id,d.unid_id_destino,d.stat_docu_id,d.tipo_docu_id,t.propio_unidad FROM documento d 
        INNER JOIN unidad u ON d.unid_id=u.unid_id
		LEFT JOIN unidad u2 ON d.unid_id_destino=u2.unid_id
        INNER JOIN tipo_documento t ON d.tipo_docu_id=t.tipo_docu_id
        WHERE d.docu_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function get_obs_doc(){
        $q="SELECT docu_observacion as obs FROM documento WHERE docu_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function get_prov(){
        $q="SELECT * FROM proveedor ORDER BY 2; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
	}
	function get_area(){
        $q="SELECT * FROM area ORDER BY 2; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function get_pres(){
        $q="SELECT * FROM farm_presentacion ORDER BY 2; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    function buscar_producto(){
		
   	 if($this->id_usuario==NULL){
    	$rs=array(["prod_nombre"=>0]);
    	echo json_encode($rs); 
    	exit;
  	 }

		$q="SELECT propio_unidad FROM tipo_documento where tipo_docu_id=$this->tipo_docu_id; ";
		$st =$this->con->prepare($q);
        $st->execute();
		$rs = $st->fetch(PDO::FETCH_ASSOC);
		$propio=$rs['propio_unidad'];

		//SI ES PRPIO 1// OBTIENE PRECIO PROMEDIO DEL PRODUCTO YA QUE ESTA REINGRESANDO
		if($propio=='1'){		
			$q="SELECT p.prod_nombre as value,p.prod_id,k.kard_costo,p.caducidad_registroSanitario, p.grupo_prod_id FROM vw_producto_nombre p
			INNER JOIN kardex k ON p.prod_id=k.prod_id
			WHERE k.unid_id=$this->unid_id and p.prod_descripcion like '%$this->term%' collate utf8_general_ci ORDER BY p.prod_nombre LIMIT 10;";
		}else{			
			$q="SELECT p.prod_nombre as value,p.prod_id,p.caducidad_registroSanitario, p.grupo_prod_id FROM vw_producto_nombre p		
			WHERE p.prod_descripcion like '%$this->term%' collate utf8_general_ci ORDER BY p.prod_descripcion LIMIT 10; ";			
		}
				
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
	}

	function consultar_stock(){    
		$q="SELECT if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) - if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre)) - if((k.kard_entregado)is null,0,(k.kard_entregado))  as stock_disponible
		FROM detalle_kardex dk
		INNER JOIN kardex k ON k.kard_id=dk.kard_id
		WHERE  k.prod_id=$this->prod_id and k.unid_id=$this->unid_id;";
		$st =$this->con->prepare($q);
		$st->execute();
		$rs = $st->fetch(PDO::FETCH_ASSOC);
		return $rs['stock_disponible'];
	  }
	
	
	  function agregar_detalle_producto(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO detalle_documento (docu_id,$columns,user_id) 
		VALUES ($this->id,$values,$this->id_usuario)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount();
	}
	
	function agregar_detalle_pedido(){
		$q="SELECT if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) - if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre)) - if((k.kard_entregado)is null,0,(k.kard_entregado))  as stock
		FROM detalle_kardex dk
		INNER JOIN kardex k ON k.kard_id=dk.kard_id
		WHERE  k.kard_id=$this->kard_id;";
		$st =$this->con->prepare($q);
		$st->execute();   
		$rs = $st->fetch(PDO::FETCH_ASSOC);
		$cant_stock=0;
		$cant_stock=$rs['stock'];
		$cant_ped=$this->cantidad_pedido;
		$m=0;
		if($cant_ped>$cant_stock)
			//$m="Cantidad a Pedir Excede a lo Disponible en Stock !!!!!";
			$m=1;
		else{
			//OBTIENE ID PRODUCTO;
			$q2="SELECT k.prod_id FROM .kardex k INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id	WHERE kard_id=$this->kard_id;";
			$st2 =$this->con->prepare($q2);
			$st2->execute();   
			$rs2 = $st2->fetch(PDO::FETCH_ASSOC);			
			$prod_id=$rs2['prod_id'];

				$q22="SELECT  p.caducidad_registroSanitario FROM .kardex k INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id	WHERE kard_id=$this->kard_id;";
			$st22 =$this->con->prepare($q22);
			$st22->execute();   
			$rs22 = $st22->fetch(PDO::FETCH_ASSOC);			
			$caducidad_registroSanitario=$rs22['caducidad_registroSanitario'];

			//ID DOCUMENTO ASOCIADO
			$id_doc=$this->id;

			//VERIFICA SI YA ESTA PRODUCTO EN PEDIDO Y MANDA ALERTA
			$q4="SELECT deta_docu_cantidad FROM detalle_documento WHERE prod_id=$prod_id and docu_id=$id_doc;";
			$st4 =$this->con->prepare($q4);
			$st4->execute();   
			if($st4->rowCount()<1){

						//VERIFICA SI YA ESTA PRODUCTO EN PEDIDO Y MANDA ALERTA
						$q5="SELECT docu_id FROM documento WHERE docu_id=$id_doc and stat_docu_id<>0";
						$st5 =$this->con->prepare($q5);
						$st5->execute();   
						if($st5->rowCount()<1){
			
								//INSERTA PRODUCTO DETALLE PEDIDO SOLO SI DOCUMENTO NO ESTA PROCESADO 0
								$q3="INSERT INTO detalle_documento (docu_id,prod_id,deta_docu_cantidad,user_id,caducidad_registroSanitario) 
									VALUES($id_doc,$prod_id,$cant_ped,$this->id_usuario,$caducidad_registroSanitario);";
								$st3 =$this->con->prepare($q3);
								$st3->execute();   
								if($st3->rowCount()>0)
									//$m="Producto Agregado a Pedido";
									$m=2;
								else
									//$m=" NO SE PUEDE AGREGAR...";			
									$m=3;
						}
						else
							//$m="No se puede Agregar Documento ya está procesado !!!";
							$m=4;
			}
			else
				//$m="Éste Producto Ya está Agregado en esta Lista...";
				$m=5;

		}

		return $m;
	}

	//***********TTEMPORAL */
	function get_docu_cambio_receta(){
		//SOLO RECETAS
		$q="SELECT * FROM documento WHERE unid_id=$this->unid_id and tipo_docu_id=4 and stat_docu_id=0 ORDER BY 1 DESC; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
	}

	function cambiar_docu(){
		$q="UPDATE detalle_documento SET docu_id=$this->id_d WHERE deta_docu_id=$this->id AND deta_docu_lote is null; ";
        $st =$this->con->prepare($q);
		$st->execute();
		return $st->rowCount();
	}

	//********* */
    function eliminar_producto_detalle(){               
        //SOLO ELIMINA PRODUCTOS DE DOCUMENTOS QUE AUN NO SON PROCESADOS
        $q=" DELETE dd.* FROM detalle_documento dd 
        INNER JOIN documento d ON d.docu_id=dd.docu_id WHERE dd.deta_docu_id=$this->id_d AND d.stat_docu_id=0   ";
        $st =$this->con->prepare($q);
        $st->execute();        
        $r="";        
        if($st->rowCount()>0)
            $r='Producto Eliminado Correctamente !!!';    
        else
            $r='No se Puede Eliminar éste Producto !!!';    
        
        return $r;
    }

    //*************** CORAZON DEL KARDEX *****************

    public function Lote_Id ($prod_id, $lote_codigo, $lote_regi_sani, $lote_fech_venc, $tipo_docu_movimiento){
        //CORAZON DE LOS LOTES******************
        $lote_codigo=trim($lote_codigo);
		$sql= "SELECT lote_id from lote where prod_id =$prod_id and lote_codigo='$lote_codigo' ";		
		$lote_id="NULL";
        $st =$this->con->prepare($sql);
        $st->execute();                    
        $row = $st->fetch(PDO::FETCH_ASSOC);
        $CantRec=$st->rowCount();        
		if ($CantRec>0){    	
				$lote_id = $row['lote_id'];			
		}
		else{		
			
			if ($tipo_docu_movimiento=='I'){
				if (is_null($lote_regi_sani) || is_null($lote_fech_venc) ){
					$lote_id="-1";
				}
				else{
					$sqli="INSERT INTO lote (prod_id,lote_codigo,lote_regi_sani,lote_fech_venc) values ($prod_id, '$lote_codigo', '$lote_regi_sani', '$lote_fech_venc');";
					$sti=$this->con->prepare($sqli);
                    $sti->execute();
                    $lote_id=$this->con->lastInsertId();
					//$lote_id =  mysql_insert_id();
				}
			}
			else{
				$lote_id=-2;
			}
		}		
		return $lote_id;
    }
    
    public function Kard_Id ($unid_id, $prod_id){
		$sql= "SELECT kard_id from kardex where prod_id =$prod_id and unid_id=$unid_id;";		
        $st =$this->con->prepare($sql);
        $st->execute();      
        $row = $st->fetch(PDO::FETCH_ASSOC);              
        $CantRec=$st->rowCount(); 

		if ($CantRec>0)	{            	
				$kard_id = $row['kard_id'];		
        }
       
		else{
            $sqlk = "INSERT into kardex (prod_id,unid_id, kard_cantidad, kard_costo) values ($prod_id,$unid_id,0,0);";            
            $stk=$this->con->prepare($sqlk);
            $stk->execute();
            $kard_id=$this->con->lastInsertId();    
    		}		
		return $kard_id;	
	}

    //PROCESO OBTENER CAMPO
    public function Campo($sql,$campo){	
        
        $rs =$this->con->prepare($sql);
        $rs->execute();
        $row = $rs->fetch(PDO::FETCH_ASSOC);
        $CantRec=$rs->rowCount();

		if ($rs==false){
			//echo $sql;
			return "NULL";
		}               	
		if ($CantRec != 1 ){
			$campo="NULL";
		}			
		else{
			$campo = $row[$campo];			
		}		
		return $campo;	
	}
	
    //PROCESO KARDEX
    function procesar_documento_kardex(){
    	date_default_timezone_set('America/Guayaquil');
        $time = time();

		$user_id=$this->id_usuario;		

		//$this->ajustar_todos_kardex();

		
		if($user_id == false){	
			return 'Su Sesión ha Expirado !!!';
			exit;
		}	
		$fecha_hora_procesos=date("Y-m-d H:i:s",$time);
		
        $docu_id=$this->id;
        $recibe=$this->recibe;        

        $linea = 0;
		$error = 0;
        $str_error = "";

	   	
	   	$sql = "SET AUTOCOMMIT=0;";
        $st =$this->con->prepare($sql);
        $st->execute();
        $sql = "BEGIN;";
        $st =$this->con->prepare($sql);
		$st->execute();
		

		
        $sql = "SELECT d.unid_id, d.tipo_docu_id, d.stat_docu_id,d.docu_fecha , d.unid_id_destino, d.stat_docu_id_destino ,t.tipo_docu_movimiento
        FROM documento d INNER JOIN tipo_documento t ON d.tipo_docu_id=t.tipo_docu_id 
        WHERE d.docu_id =  $docu_id;";
        $st =$this->con->prepare($sql);
        $st->execute();
        $row = $st->fetch(PDO::FETCH_ASSOC);
            $unid_id=$row['unid_id'];
            $unid_id_destino=$row['unid_id_destino'];
            $tipo_docu_id=$row['tipo_docu_id'];
            $stat_docu_id=$row['stat_docu_id'];
            $stat_docu_id_destino=$row['stat_docu_id_destino'];
            $docu_fecha=$row['docu_fecha'];
            $tipo_docu_movimiento=$row['tipo_docu_movimiento'];

            //SOLO PARA TRANSFERENCIA ESTA SECCION
            if ($tipo_docu_movimiento=='T'){	
                if ($unid_id==0 || $unid_id_destino==0){
                    $error=102;
                    $str_error.="Debe definir las unidades de Salida e Ingreso de mercadería.";
                }	                
                if ($unid_id !=0 && $unid_id == $unid_id_destino){
                    $error=102;
                    $str_error.="La unidad de Salida y Destino de mercadería deben ser distintas.";				
                }			
			}
			
			
		$a=0;
        //SOLO PARA TRANSFERENCIA ESTA SECCION
        if ($tipo_docu_movimiento=='T' && $recibe==0){			
			$a=1;
            $unid_id = $unid_id;
			$tipo_docu_movimiento = 'E';
			//validar que en el destino 
			if ($stat_docu_id_destino==1){
				$error=101;
				$str_error.="La transferencia esta procesada en el destino. Solicite que primero sea revertido en la unidad destino.";								
			}								
        }

        //SOLO PARA TRANSFERENCIA ESTA SECCION
        if ($tipo_docu_movimiento=='T' && $recibe==1){
			$a=2;
			$unid_id = $unid_id_destino;
			$tipo_docu_movimiento = 'I';			
			if ($stat_docu_id<>1){
				$error=101;
				$str_error.="La transferencia Debe ser procesada primero en la Unidad de Origen. Solicita que procesen la Transferencia en la Unidad de Origen.";								
			}
			$stat_docu_id = $stat_docu_id_destino;								
		}
		
	//$str_error=$a;
	//$error=555;

		//$str_error=$tipo_docu_movimiento.' '.$recibe.' '.$stat_docu_id.' '.$stat_docu_id_destino.' '.$docu_id;
		//$error=555;

        if ($stat_docu_id==1 ){			
			// BUSCAR PERMISOS PARA ANULAR DOCUMENTO
			$pagi_url = "AnularDocumento";
			$sql="SELECT pagi_nombre,pagi_url, pagi_codigo,pagi_raiz  from usuario inner join perfil_usuario on perfil_usuario.user_id = usuario.user_id 
                      inner join perfil_pagina on  perfil_usuario.perf_id = perfil_pagina.perf_id
                      inner join pagina on pagina.pagi_id = perfil_pagina.pagi_id
                      where usuario.user_id = $user_id and pagi_url='$pagi_url' ; ";                     
            $st =$this->con->prepare($sql);
            $st->execute();                    
            $CantRec=$st->rowCount();
			if ($CantRec==0 ){
					$error=55;
					$str_error.="No tiene permiso para Abrir/Revertir el Documento. Comuniquese con el Administrador del Sistema.";
			}
        }        
       
		if ($stat_docu_id ==0 || $stat_docu_id==1 ){						
		}
		else{
			$error=1;
			$str_error.="No se puede completar la acción.";
		}           
		
	

        //actualizar los stocks de entregado y reservado.
        //TEMP: OJO VERIFICAR Y SEPARAR LUEGO SI ES NECESARIO
		if ($unid_id!=0 && $error==0){
            
            $sql = "UPDATE kardex set kard_entregado =0 where unid_id = $unid_id and kard_entregado <> 0";
            $st =$this->con->prepare($sql);
            $st->execute();				
            
            $sql = "SELECT prod_id, SUM(deno_cantidad) AS deno_cantidad 
			FROM notaentrega inner join detallenota on notaentrega.noen_id = detallenota.noen_id 
			where (unid_id = $unid_id and noen_status <>2) and (unid_id = $unid_id and noen_status <>3) 
            group by prod_id";

            $st =$this->con->prepare($sql);
            $st->execute();	
            $rs = $st->fetchAll(PDO::FETCH_ASSOC);
            foreach($rs as $row){
				$deno_cantidad = $row["deno_cantidad"];
				$prod_id = $row["prod_id"];
				$sql = "UPDATE kardex set kard_entregado = $deno_cantidad where prod_id = $prod_id and unid_id = $unid_id";
				$st2 =$this->con->prepare($sql);
                $st2->execute();						
            }	
            		
			$sql = "UPDATE kardex set kard_reservado =0 where unid_id = $unid_id and kard_reservado <> 0";
			$st =$this->con->prepare($sql);
            $st->execute();			

			$sql = "SELECT prod_id, SUM(deno_cantidad) AS deno_cantidad 
			FROM notaentrega inner join detallenota on notaentrega.noen_id = detallenota.noen_id
            where unid_id = $unid_id and noen_status = 0 group by prod_id";  

		    $st3 =$this->con->prepare($sql);
            $st3->execute();
            $rs3 = $st3->fetchAll(PDO::FETCH_ASSOC);
            foreach($rs3 as $row){
				$deno_cantidad = $row["deno_cantidad"];
				$prod_id = $row["prod_id"];
				$sql = "UPDATE kardex set kard_reservado = $deno_cantidad where prod_id = $prod_id and unid_id = $unid_id";
				$st2 =$this->con->prepare($sql);
                $st2->execute();
			}
        }
        
        /*********/
        //Abrir el detalle para obtener los campos para el kardex		
		$sql="SELECT * from detalle_documento 
        inner join vw_producto_nombre on detalle_documento.prod_id = vw_producto_nombre.prod_id 
        where docu_id = $docu_id;";
       
       $rs_detalle_documento=$this->con->prepare($sql);
       $rs_detalle_documento->execute();                    
       $CantRec=$rs_detalle_documento->rowCount();
       if ($CantRec==0){
           $error=11;
           $str_error.=" No se puede procesar debido a que el Documento No contiene ningun detalle".$this->id;
       }
       
       if (is_null($tipo_docu_movimiento)){
			$error = 4;
			$str_error.= "No se pudo determinar el tipo de movimiento error comuniquese con el Administrador";					
		}else{
			$deta_kard_status = $stat_docu_id;			
			if ($deta_kard_status==1){
				if ($tipo_docu_movimiento=='I'){
					$tipo_docu_movimiento='E';
				}else{
					if ($tipo_docu_movimiento=='E')
						$tipo_docu_movimiento='I';
				}
			}			
        }
        
        /*****/
        // abrir para obtener los resultados y comenzar los inserts de los kardex
        while (($row = $rs_detalle_documento->fetch(PDO::FETCH_ASSOC)) && $error==0){
        
			$prod_nombre = $row['prod_nombre']; 
			$prod_id = $row['prod_id'];
			$pres_id = $row['pres_id'];		
			$deta_docu_id=$row['deta_docu_id'];		
			$deta_docu_cantidad = $row['deta_docu_cantidad'];
			$deta_docu_valo_unit = $row['deta_docu_valo_unit'];
			// valores para calcular el lote
			$deta_docu_lote =$row['deta_docu_lote'];
			$deta_docu_regi_sani =$row['deta_docu_regi_sani'];
			$deta_docu_fech_venc =$row['deta_docu_fech_venc'];	
			$lote_id = "NULL";
			$linea++;
			if (trim($prod_id) ==""){
				$error = 211;
				$str_error.= "No se puede determinar el producto Id LINEA".$linea;				
			}			
			if ( !is_null($deta_docu_lote) && $deta_docu_lote!="" ){
				$lote_id = $this->Lote_Id($prod_id,$deta_docu_lote,$deta_docu_regi_sani,$deta_docu_fech_venc,$tipo_docu_movimiento);
				if ($lote_id==-1){
					$error = 2;
					$str_error.= "Datos erroneos para determinar el lote falta Datos como la Fecha de Caducidad o Registro Sanitario $prod_nombre " ;
				}				
				
				if ($lote_id==-2){
					$error = 21;
					$str_error.= "El lote no se encuentra en inventario $prod_nombre";					
				}				
			}						
			//calcular el kardex_id 			
			$kard_id = $this->Kard_Id($unid_id, $prod_id);
			$kard_cantidad = $this->Campo("SELECT kard_cantidad from kardex where kard_id=$kard_id; ","kard_cantidad");
			$kard_entregado = $this->Campo("SELECT kard_entregado from kardex where kard_id=$kard_id; ","kard_entregado");
			$kard_reservado= $this->Campo("SELECT kard_reservado from kardex where kard_id=$kard_id; ","kard_reservado");

			$kard_total = $this->Campo("SELECT kard_total from kardex where kard_id=$kard_id; ","kard_total");
			
			if ($kard_id==0){
				$error = 3;
				$str_error.= "No se pudo determinar el Kardex ".$prod_nombre;							
			}
			if ($error==0){
				//obtener el tipo de documento
				//Ingresa el deta_kard_id
				$tipo_prod_id = $this->Campo("SELECT tipo_prod_id from farm_producto where prod_id = $prod_id","tipo_prod_id");			
				$tipo_prod_caducidad = $this->Campo("SELECT tipo_prod_caducidad from tipo_producto where tipo_prod_id = $tipo_prod_id", "tipo_prod_caducidad");				
					if ($tipo_docu_movimiento=='I'){
						if ($deta_docu_cantidad ==0){
							$error=23;
							$str_error.= "La cantidad no puede ser 0 en $prod_nombre";								
						}
						$deta_kard_cant_ingr = $deta_docu_cantidad;
						$deta_kard_cost_ingr = $deta_docu_valo_unit;
						$deta_kard_cant_sald = $kard_cantidad + $deta_docu_cantidad;
						$deta_kard_total = $kard_total + $deta_docu_cantidad*$deta_docu_valo_unit;
						if ($deta_kard_cant_sald !=0){
							$deta_kard_cost_sald = $deta_kard_total / $deta_kard_cant_sald ;
						}
						else{
							$deta_kard_cost_sald = 0;
						}
						$deta_kard_cant_egre = "NULL";
						$deta_kard_cost_egre = "NULL";	
														
						if ($tipo_prod_caducidad==1 && $lote_id=="NULL"){
							$error=21;
							$str_error.= "Los Productos deben poseer Lote, Caducidad y Registro Sanitario $prod_nombre";					
						}
						if ($tipo_prod_caducidad==0){
							if ($lote_id==""){
								$lote_id="NULL";
							}
						}
						if ($tipo_prod_caducidad==1 && $lote_id != "NULL" && $error==0){
							
							$sald_kard_id = $this->Campo("SELECT sald_kard_id from saldo_kardex where kard_id=$kard_id and lote_id =$lote_id  ","sald_kard_id");
							$a=$sald_kard_id;						
							
							if ($sald_kard_id =="NULL"){								
								$sql="INSERT INTO saldo_kardex(kard_id,lote_id, sald_kard_cantidad, sald_kard_precio) values  ($kard_id,$lote_id, $deta_docu_cantidad, $deta_docu_valo_unit) ";
							}
							else{								
								$sql="UPDATE saldo_kardex set sald_kard_cantidad=sald_kard_cantidad+$deta_docu_cantidad  where sald_kard_id = $sald_kard_id";
							}							
							$result_lote =$this->con->prepare($sql);
							$result_lote->execute();      
							if ($result_lote->rowCount()<1 ){
								$error=15;
								$str_error.="No se puede actualizar el Saldo kardex.: $prod_nombre"."deta_docu_cant:".$deta_docu_cantidad." sald_kard_id:".$sald_kard_id.") a:".$a." kard:".$kard_id." lote_id:".$lote_id;
							}							
						}
						if ($error ==0){
							$sql = "INSERT into detalle_kardex
							(kard_id, deta_kard_fecha,deta_kard_status,docu_id,lote_id,
							deta_kard_cant_ingr,
							deta_kard_cost_ingr ,
							deta_kard_cant_egre ,
							deta_kard_cost_egre ,
							deta_kard_cant_sald ,
							deta_kard_cost_sald , deta_kard_total  ) 
							values ($kard_id,'$docu_fecha',$deta_kard_status,$docu_id,$lote_id, $deta_kard_cant_ingr,
							$deta_kard_cost_ingr ,
							$deta_kard_cant_egre ,
							$deta_kard_cost_egre ,
							$deta_kard_cant_sald ,
							$deta_kard_cost_sald ,$deta_kard_total );";
							$ins_deta_kardex  =$this->con->prepare($sql);
							$ins_deta_kardex ->execute();   
							
							if (  $ins_deta_kardex->rowCount()<1 ){
								$error=149;
								$str_error.="Error: $error .No se puede insertar el detalle del kardex $prod_nombre. ".$sql ;
							}
						}						
					}					
					//egreso de mercaderia				
					//aqui va el egreso de mercaderia
					if ($tipo_docu_movimiento=='E'){
						
						$deta_kard_cant_ingr ="NULL";
						$deta_kard_cost_ingr ="NULL"; 					
						
						if (($kard_cantidad-$kard_entregado) < $deta_docu_cantidad){
							$error=101;
							$str_error.="kard-id: ".$kard_id." Cantidad Insuficiente del producto solo se disponen de ".($kard_cantidad-$kard_entregado)." de ".$deta_docu_cantidad." solicitadas  unidades en el producto : ".$prod_nombre. " Ya que $kard_entregado se encuentran reservadas";																											
							}
						else{							
							
							if ($tipo_prod_caducidad==0){								
								
								$deta_kard_cant_egre = $deta_docu_cantidad;
								if ($kard_cantidad !=0){
									$deta_kard_cost_egre = $kard_total/$kard_cantidad;
								}else{
									$deta_kard_cost_egre = 0;
								}
								$deta_kard_cant_sald = $kard_cantidad - $deta_docu_cantidad;
								if ($kard_cantidad!=0){
									$deta_kard_cost_sald = $kard_total/$kard_cantidad ;								
								}else{
									$deta_kard_cost_sald = 0;
								}								

								$deta_kard_total  = $kard_total - $deta_kard_cant_egre * $deta_kard_cost_egre ;
								$sql = "INSERT into detalle_kardex
								(kard_id, deta_kard_fecha,deta_kard_status,docu_id,lote_id,
								deta_kard_cant_ingr,
								deta_kard_cost_ingr ,
								deta_kard_cant_egre ,
								deta_kard_cost_egre ,
								deta_kard_cant_sald ,
								deta_kard_cost_sald , deta_kard_total  ) 
								values ($kard_id,'$docu_fecha',$deta_kard_status,$docu_id,$lote_id, 					   $deta_kard_cant_ingr,
								$deta_kard_cost_ingr ,
								$deta_kard_cant_egre ,
								$deta_kard_cost_egre ,
								$deta_kard_cant_sald ,
								$deta_kard_cost_sald ,$deta_kard_total ) ";																
								$ins_deta_kardex =$this->con->prepare($sql);
								$ins_deta_kardex->execute();      						

								if (  $ins_deta_kardex->rowCount()<1 ){
									$error=14;
									$str_error.="Error: $error.  No se puede insertar el detalle del kardex";
								}
								//actualizar el precio al que salio el insumo o producto sin historial de lote
								$sql="UPDATE detalle_documento set deta_docu_valo_unit = $deta_kard_cost_egre where deta_docu_id = $deta_docu_id ";									
								
								$rs_upd_detalle_saldo =$this->con->prepare($sql);
								$rs_upd_detalle_saldo->execute();      
								if ($rs_upd_detalle_saldo->rowCount()<1){
									$error=1521;
									$str_error.="No se puede actualizar el detalle de venta del producto o Costo de Entrega producto $prod_nombre";
								}
							}
							else{	
								
								if ($lote_id!="NULL"){
									//solicitamos el saldo actual y verificamos la existencia

									
								/*
								if ($deta_docu_valo_unit<=0){
										$error=15;
										$str_error.="Por favor verifique el precio unitario no puede ser cero o negativo de: ".$prod_nombre;
									}
									*/
									

									if (is_null($deta_docu_valo_unit)){
										$sql = "SELECT sald_kard_id from saldo_kardex  where kard_id =$kard_id and lote_id =$lote_id order by sald_kard_cantidad desc limit 0,1 ";
									}
									else{
										//$sql = "SELECT sald_kard_id from saldo_kardex  where kard_id =$kard_id and lote_id =$lote_id and sald_kard_precio =  $deta_docu_valo_unit  order by sald_kard_cantidad desc limit 0,1 ";										
										$sql = "SELECT sald_kard_id from saldo_kardex  where kard_id =$kard_id and lote_id =$lote_id   order by sald_kard_cantidad desc limit 0,1 ";										
									}
									$sald_kard_id= $this->Campo($sql,"sald_kard_id");
									if  ($sald_kard_id=="NULL"){
										$error =15;
										$str_error.="Error en el precio definido por el usuario $prod_nombre";
									}
									$sql= "SELECT sald_kard_cantidad from saldo_kardex where sald_kard_id =$sald_kard_id";
									$sald_kard_cantidad= $this->Campo($sql,"sald_kard_cantidad");
									if 	($sald_kard_cantidad<$deta_docu_cantidad){
										$error =15;
										$str_error.="No existe productos suficientes del lote escogido de $prod_nombre lote_id:".$lote_id;										
									}
									else{										
										$sald_kard_precio= $this->Campo("SELECT sald_kard_precio from saldo_kardex  where sald_kard_id =$sald_kard_id ","sald_kard_precio");
										if (is_null($deta_docu_valo_unit)) 
										{
											$sql="UPDATE detalle_documento set deta_docu_valo_unit = $sald_kard_precio where deta_docu_id = $deta_docu_id ";																				
											$rs_upd_detalle_saldo =$this->con->prepare($sql);
											$rs_upd_detalle_saldo->execute();    
											if ( $rs_upd_detalle_saldo->rowCount()<1 ){
												$error=152;
												$str_error.="No se puede actualizar el detalle del producto $prod_nombre";												
											}
										}										
										$deta_kard_cant_egre = $deta_docu_cantidad;
										$deta_kard_cost_egre = $sald_kard_precio;
										$deta_kard_cant_sald = $kard_cantidad - $deta_docu_cantidad;
										$deta_kard_total  = $kard_total - $deta_kard_cant_egre * $deta_kard_cost_egre ;
										if ($deta_kard_cant_sald!=0)
											$deta_kard_cost_sald = $deta_kard_total /$deta_kard_cant_sald ;	
										else
											$deta_kard_cost_sald = 0;	
										
										//actualizar el saldo lote
										$sql = "UPDATE saldo_kardex set sald_kard_cantidad = sald_kard_cantidad - $deta_docu_cantidad where sald_kard_id =$sald_kard_id; ";																	
										$rs_upd_saldo_kardex =$this->con->prepare($sql);
										$rs_upd_saldo_kardex->execute();      
										if ( $rs_upd_saldo_kardex->rowCount()<1 ){
											$error=141;
											$str_error.="Error $error . No se puede insertar el detalle del kardex $prod_nombre";
										}																													
										$sql = "INSERT into detalle_kardex
										(kard_id, deta_kard_fecha,deta_kard_status,docu_id,lote_id,
										deta_kard_cant_ingr,
										deta_kard_cost_ingr ,
										deta_kard_cant_egre ,
										deta_kard_cost_egre ,
										deta_kard_cant_sald ,
										deta_kard_cost_sald , deta_kard_total  ) 
										values ($kard_id,'$docu_fecha',$deta_kard_status,$docu_id,$lote_id, 					   $deta_kard_cant_ingr,
										$deta_kard_cost_ingr ,
										$deta_kard_cant_egre ,
										$deta_kard_cost_egre ,
										$deta_kard_cant_sald ,
										$deta_kard_cost_sald ,$deta_kard_total ) ";										
										
										$ins_deta_kardex =$this->con->prepare($sql);
										$ins_deta_kardex->execute();     										
										if ( $ins_deta_kardex->rowCount()<1){
											$error=144;
											$str_error.="Error: $error . No se puede insertar el detalle del kardex $prod_nombre.";
										}																													
									}
								}
								else{
									
									//PROCESO CORAZON

									$this->kard_id=$kard_id;
									$this->ajustar_saldo_kardex();

									$ban=0;
									$total = $deta_docu_cantidad;
									$sql = "SELECT sald_kard_id, saldo_kardex.lote_id, sald_kard_cantidad, sald_kard_precio, lote_fech_venc, lote_regi_sani, lote_codigo 
									from saldo_kardex inner join  lote on saldo_kardex.lote_id = lote.lote_id
									 where kard_id=$kard_id and sald_kard_cantidad>0  order by lote_fech_venc, sald_kard_id ;";
									$rs_saldo_kardex =$this->con->prepare($sql);
									$rs_saldo_kardex->execute();    
									$cant=0;
									$cant=$rs_saldo_kardex->rowCount();									
									if($cant<1){
										$error=14.2;
										$str_error.=" Verificar Producto en Saldo_Kardex: Kard_id:".$kard_id;
									}else{
											while (($row_saldo_kardex =$rs_saldo_kardex->fetch(PDO::FETCH_ASSOC)) && $total>0){	
												$lote_id = $row_saldo_kardex['lote_id'];
												$saldo_kard_id =  $row_saldo_kardex['sald_kard_id'];	
												$sald_kard_cantidad = $row_saldo_kardex['sald_kard_cantidad'];	
												$sald_kard_precio = $row_saldo_kardex['sald_kard_precio'];	
												$lote_fech_venc = $row_saldo_kardex['lote_fech_venc'];	
												$lote_codigo = $row_saldo_kardex['lote_codigo'];	
												$lote_regi_sani = $row_saldo_kardex['lote_regi_sani'];	
											
												if ($total>=$sald_kard_cantidad){
													$total = $total - $sald_kard_cantidad;
													$deta_docu_cantidad = $sald_kard_cantidad;
													$sald_kard_cantidad= 0;
												}
												else{
													$deta_docu_cantidad = $total;											
													$sald_kard_cantidad= $sald_kard_cantidad - $total;
													$total =0;
												}
												if ($ban==0){
													//actualizar la primera fila
													//ANGEL REVISAR TEMP LOTE  - YA QUE INSERTA LOTE ID NULL
													$sql = "UPDATE detalle_documento set lote_id = $lote_id , deta_docu_cantidad = $deta_docu_cantidad, deta_docu_regi_sani = '$lote_regi_sani', deta_docu_lote = '$lote_codigo', deta_docu_fech_venc = '$lote_fech_venc', deta_docu_valo_unit = $sald_kard_precio where deta_docu_id = $deta_docu_id";									
													$ban++;											
													
													$rs_upd_deta_docu =$this->con->prepare($sql);
													$rs_upd_deta_docu->execute();
													
													//if ($rs_upd_deta_docu==false){
													if($rs_upd_deta_docu->rowCount()<1){
														$error = 50;
														$str_error.=$sql." No se puede actualizar el detalle del documento para insertar los datos del lote $prod_nombre";	
													}
												} 
												else{

													//en caso de crear revisar la informacion necesaria
													$sql = "INSERT into detalle_documento 
													(docu_id , prod_id , pres_id, lote_id, deta_docu_lote, deta_docu_fech_venc, deta_docu_regi_sani, deta_docu_cantidad, deta_docu_valo_unit, deta_docu_iva ) 
													values
													($docu_id, $prod_id, $pres_id, $lote_id, '$lote_codigo','$lote_fech_venc', '$lote_regi_sani', $deta_docu_cantidad, $sald_kard_precio, 0 )";
													
													$rs_upd_deta_docu=$this->con->prepare($sql);
													$rs_upd_deta_docu->execute();
													if ($rs_upd_deta_docu->rowCount()<1){
														$error = 50;
														$str_error.="No se puede insertar el nuevo detalle del documento para insertar los datos del lote $prod_nombre";	
													}																					
												}
												// insertar el detalle del kardex
												$deta_kard_cant_egre = $deta_docu_cantidad;
												$deta_kard_cost_egre = $sald_kard_precio;										
												$deta_kard_cant_sald = $kard_cantidad - $deta_docu_cantidad;										
												$deta_kard_total  = $kard_total - $deta_kard_cant_egre * $deta_kard_cost_egre ;
												$kard_cantidad = $deta_kard_cant_sald;
												$kard_total = $deta_kard_total;
												
												if ($deta_kard_cant_sald!=0)
													$deta_kard_cost_sald = $deta_kard_total /$deta_kard_cant_sald ;	
												else
													$deta_kard_cost_sald =0;
												//actualizar el saldo lote
												$sql = "UPDATE saldo_kardex set sald_kard_cantidad = $sald_kard_cantidad where sald_kard_id = $saldo_kard_id  ";
												
												$rs_upd_saldo_kardex =$this->con->prepare($sql);
												$rs_upd_saldo_kardex->execute();  	
												if ( $rs_upd_saldo_kardex->rowCount()<1 ){
													$error=14;
													$str_error.="No se puede actualizar el saldo del saldo kardex: $prod_nombre";
												}																													
												$sql = "INSERT into detalle_kardex
												(kard_id, deta_kard_fecha,deta_kard_status,docu_id,lote_id,
												deta_kard_cant_ingr,
												deta_kard_cost_ingr ,
												deta_kard_cant_egre ,
												deta_kard_cost_egre ,
												deta_kard_cant_sald ,
												deta_kard_cost_sald , deta_kard_total  ) 
												values ($kard_id,'$docu_fecha',$deta_kard_status,$docu_id,$lote_id, 					   $deta_kard_cant_ingr,
												$deta_kard_cost_ingr ,
												$deta_kard_cant_egre ,
												$deta_kard_cost_egre ,
												$deta_kard_cant_sald ,
												$deta_kard_cost_sald ,$deta_kard_total ) ";
																						
												$ins_deta_kardex =$this->con->prepare($sql);
												$ins_deta_kardex->execute();  
												if ( $ins_deta_kardex->rowCount()<1 ){
													$error=149;
													$str_error.="Error: $error. No se puede insertar el detalle del kardex $prod_nombre";
												}															
											}//FIN WHILE
									}
								}
							}
						}
					}	
					
					if ($error==0){
					//actualizar saldo de mi kardex					
						try {
							$sql = "UPDATE kardex set kard_cantidad = $deta_kard_cant_sald, 
							kard_costo= $deta_kard_cost_sald, kard_total=$deta_kard_total where kard_id = $kard_id";											
							$upd_kardex =$this->con->prepare($sql);			
							$upd_kardex->execute();
						}catch (Exception $e) {
							//die("Oh noes! There's an error in the query!");
							$error=14;
							$str_error.="No se puede actualizar el kardex la cantidad o el costo $prod_nombre - $sql";
						}
						//if (){
						//	$error=14;
						//	$str_error.="No se puede actualizar el kardex la cantidad o el costo $prod_nombre - $sql";
						//}											
					}								
			}
			if ($error!=0){				
				echo "DETALLE: ".$linea."  ".$str_error."   ";
			}
		}

		if ($error==0){
			if ($recibe==0){
				if ($stat_docu_id==0){
					
					$sql = "UPDATE documento set stat_docu_id = 1,docu_procesado='$fecha_hora_procesos',docu_fech_regi='$fecha_hora_procesos' where docu_id = $docu_id;";
				}
				else{
					$sql = "UPDATE documento set stat_docu_id = 0 where docu_id = $docu_id;";
				}
			}
			else{
				if ($stat_docu_id==0){
					$sql = "UPDATE documento set stat_docu_id_destino = 1,docu_revisado='$fecha_hora_procesos' where docu_id = $docu_id;";
				}
				else{
					$sql = "UPDATE documento set stat_docu_id_destino = 0 where docu_id = $docu_id;";
				}			
			}
			
			$resultado =$this->con->prepare($sql);
			$resultado->execute();  
			if ( $resultado->rowCount()<1){
				$error=50;
				$str_error.="No se puede procesar el documento.";
			}	 		
		}		

		if ($error==0){
			$sql = "COMMIT";
			
			$resultado =$this->con->prepare($sql);
			$resultado->execute();  
			return 'PROCESO EXITOSO';
		}
		else{
			$sql = "ROLLBACK;";
			$resultado =$this->con->prepare($sql);
			$resultado->execute();
			return $str_error;
		}		
        
	}//FIN FUNCTION KARDEX

	//FUNCTION AGRAGAR REGISTRO A TABLA SALDO KARDEX SI NO TIENE	
	function ajustar_saldo_kardex(){
		$k=$this->kard_id;
        //AJUSTA INSERTA A TABLA SALDO_KARDEX CON STOCKS REALES DE SUS LOTES POR PRODUCTO
        $sql2="SELECT kard_id,unid_id,kard_costo FROM kardex WHERE kard_id=$k; ";
        $st2 =$this->con->prepare($sql2);
        $st2->execute();
        $row2 = $st2->fetch(PDO::FETCH_ASSOC);
    
        //OBTENGO KARD_ID SEGUNPROD_ID
        $kard_costo=0; 
            
            $kard_id=$row2['kard_id'];
            $unid_id=$row2['unid_id'];
            $kard_costo=$row2['kard_costo'];
            
            $sql3="SELECT lote_id,
            if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) - if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre))   as stock
            FROM detalle_kardex dk WHERE kard_id=$kard_id group by lote_id";
            $st3 =$this->con->prepare($sql3);
            $st3->execute();
            $rs3 = $st3->fetchAll(PDO::FETCH_ASSOC);
			$stock=0;
			$tot_stock=0;
    
            //OBTENGO LOTE ID Y CNATIDAD STOCK SEGUN KARD_ID
            foreach($rs3 as $row3){
                $lote_id=$row3['lote_id'];
				$stock=$row3['stock'];
				$tot_stock+=$stock;    
                  $cant=0;
                  $sql4="SELECT * FROM saldo_kardex WHERE kard_id=$kard_id and lote_id=$lote_id";
                  $st4 =$this->con->prepare($sql4);
                  $st4->execute();
                  $cant=$st4->rowCount();                     
                  if($cant>0){
                         //SI EXISTE UPDATE CANTIDAD                       
						 $sql = "UPDATE saldo_kardex set sald_kard_cantidad=$stock where kard_id=$kard_id and lote_id=$lote_id and sald_kard_cantidad<>$stock ";
                         $st =$this->con->prepare($sql);
                         $st->execute();
                         //echo 'b';
    
                  }else{
                    //SI NO EXISTE INSERTA TODO        
                        $sql5 = "INSERT INTO saldo_kardex (kard_id,lote_id,sald_kard_cantidad,sald_kard_precio) VALUES ($kard_id,$lote_id,$stock,$kard_costo)";
                        $st5 =$this->con->prepare($sql5);
                        $st5->execute();
                        //echo 'a'; //echo 'kard_id: '.$kard_id.' Lote_id: '.$lote_id.' stock: '.$stock.' Precio: '.$kard_costo;
                  }
		   }
		   
		    //ACTUALIZA TABLA KARDEX: CAMPO KARD_CANTIDAD Y RECALCULA COSTO
			$sqlkk = "UPDATE kardex set kard_cantidad=$tot_stock,kard_total=kard_costo*$tot_stock where kard_id=$kard_id and unid_id=$unid_id and kard_cantidad<>$tot_stock; ";
			$stkk =$this->con->prepare($sqlkk);
			$stkk->execute();
        
	}

}