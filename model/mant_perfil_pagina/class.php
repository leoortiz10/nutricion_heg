<?php
class Perfil{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT pp.perf_pagi_id,pe.perf_nombre,pa.pagi_nombre FROM perfil_pagina pp
        INNER JOIN perfil pe ON pp.perf_id=pe.perf_id
        INNER JOIN pagina pa ON pp.pagi_id=pa.pagi_id
        ORDER BY 2,3; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM perfil_pagina WHERE perf_pagi_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO perfil_pagina ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE perfil_pagina
        SET perf_id=?,pagi_id=?
        WHERE perf_pagi_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['perf_id'],
            $this->campos['pagi_id']           
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        $q=" DELETE FROM perfil_pagina WHERE perf_pagi_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }
 
    function get_perf(){            
        $q=" SELECT * FROM perfil order by 2 ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_pagi(){            
        $q=" SELECT * FROM pagina order by 2 ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}