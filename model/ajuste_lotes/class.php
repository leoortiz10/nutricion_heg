<?php
class Ajuste{

    public $con; 
    public $id_usuario;
    public $unid_id;
    public $prod_id;
    public $lote_id_origen;
    public $lote_fech_destino;
    public $lote_regi_destino;
    public $lote_cant_origen;
    public $lote_id_destino;
    public $lote_codigo_destino;
    public $lote_codigo_origen;
    public $kard_id_origen;
    public $array_deta_kard_id;

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
        if (session_status() == PHP_SESSION_NONE)
            session_start();                       
		$this->id_usuario=$_SESSION['s_user_id'];	
    }

    function get_unidad(){
        $q="SELECT * FROM unidad WHERE unid_id in (select unid_id from usuario_unidad where user_id = $this->id_usuario) ORDER by 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function get_producto(){
        $q="SELECT p.prod_id,p.prod_nombre FROM kardex k 
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id 
        WHERE k.unid_id=$this->unid_id ORDER BY 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function lotes_productos(){		        
		$q="SELECT k.kard_id,dk.lote_id,l.lote_codigo,
        FORMAT(if(sum(dk.deta_kard_cant_ingr) is null,0,sum(dk.deta_kard_cant_ingr))-
        if(sum(dk.deta_kard_cant_egre) is null,0,sum(dk.deta_kard_cant_egre)),0) as cantidad,
        l.lote_fech_venc as fech,l.lote_regi_sani as regi FROM detalle_kardex dk 
        INNER JOIN kardex k ON dk.kard_id=k.kard_id
        INNER JOIN lote l ON dk.lote_id=l.lote_id
        WHERE k.prod_id=$this->prod_id AND k.unid_id=$this->unid_id ";

        if($this->lote_id_origen!=NULL)
            $q.=" AND dk.lote_id<>$this->lote_id_origen ";

        $q.=" GROUP BY dk.lote_id ";

        //if($this->lote_id_origen==NULL)
         //   $q.="  HAVING cantidad <>0; ";

		$st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);  
    }   
    function lotes_productos_cruzar(){
        $q="SELECT k.kard_id,l.lote_id,l.prod_id,l.lote_codigo,l.lote_fech_venc as fech,l.lote_regi_sani as regi 
        FROM lote l
        INNER JOIN kardex k ON k.prod_id=l.prod_id
        WHERE l.prod_id=$this->prod_id AND k.unid_id=$this->unid_id AND l.lote_id<>$this->lote_id_origen;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 

    }
    function lote_movimientos(){
        $q="SELECT t.tipo_docu_nombre,deta_kard_id,dk.docu_id,deta_kard_fech_movi as fecha_movi,
        if((deta_kard_cant_ingr) is null,'NO TIENE INGRESO',FORMAT(deta_kard_cant_ingr,0)) as cant_ingr,
        if((deta_kard_cant_egre) is null,'NO TIENE EGRESO',FORMAT(deta_kard_cant_egre,0)) as cant_egre       
        FROM detalle_kardex dk
        INNER JOIN documento d ON dk.docu_id=d.docu_id
        INNER JOIN tipo_documento t ON d.tipo_docu_id=t.tipo_docu_id
        WHERE kard_id=$this->kard_id_origen AND lote_id=$this->lote_id_origen ORDER BY deta_kard_cant_ingr DESC,deta_kard_cant_egre DESC; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }
    function cruze_lote(){
        $ac1=0;
        $ac3=0;
        foreach($this->array_deta_kard_id as $clave=>$deta_kard_id){            

            //ACTUALIZA DETALLE KARDEX
            $q1="UPDATE detalle_kardex SET lote_id=$this->lote_id_destino WHERE lote_id=$this->lote_id_origen and deta_kard_id=$deta_kard_id; ";
            $st1 =$this->con->prepare($q1);
            $st1->execute(); 
            $ac1+=$st1->rowCount();          

            //OBTIENE DOCU_ID PARA ACTUALIZAR EN DETALLE DOCUMENTO
            $q2="SELECT docu_id, if((deta_kard_cant_ingr) is null,FORMAT(deta_kard_cant_egre,0),FORMAT(deta_kard_cant_ingr,0)) as can 
            from detalle_kardex where deta_kard_id=$deta_kard_id ";
            $st2 =$this->con->prepare($q2);
            $st2->execute(); 
            $rs2 = $st2->fetchAll(PDO::FETCH_ASSOC);

            /*
            $qq6="SELECT d2.docu_id FROM detalle_documento d2
           // WHERE d2.docu_id=$docu_id AND d2.prod_id=$this->prod_id 
            AND d2.deta_docu_cantidad=$can AND d2.deta_docu_lote='$this->lote_codigo_origen'
            //LIMIT 1;";
            */

            foreach($rs2 as $row2){
                $docu_id=$row2['docu_id'];
                $can=$row2['can'];
                //SOLO ACTUALIZO UN REGISTRO UNICO EN DETALLE DOCUMENTO
                $q3="UPDATE detalle_documento d1 JOIN
                    (
                        SELECT d2.deta_docu_id FROM detalle_documento d2
                        WHERE d2.docu_id=$docu_id AND d2.prod_id=$this->prod_id AND d2.deta_docu_cantidad=$can AND d2.deta_docu_lote='$this->lote_codigo_origen'
                        LIMIT 1
                    )  AS sel2
                    ON sel2.deta_docu_id=d1.deta_docu_id
                    SET d1.lote_id=$this->lote_id_destino, d1.deta_docu_regi_sani='$this->lote_regi_destino' ,
                    d1.deta_docu_lote='$this->lote_codigo_destino', d1.deta_docu_fech_venc='$this->lote_fech_destino'
                    WHERE d1.docu_id=$docu_id and d1.deta_docu_lote='$this->lote_codigo_origen' and d1.prod_id=$this->prod_id and d1.deta_docu_cantidad=$can; ";
                $st3 =$this->con->prepare($q3);
                $st3->execute(); 
                $ac3+=$st3->rowCount();
            }

        }        
        return $ac1.'<br>'.$ac3;
    }

    
}