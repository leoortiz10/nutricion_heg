<?php

class Dashboard{

    public $con;    
    public $fecha;
    public $id_usuario;
    public $id_unid_usuario;
    public $base;
    

    public function __construct(){  
        date_default_timezone_set('America/Bogota');         
        include ('../config/db.php');    
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }       
        $this->con=$con;        
        $this->fecha=date('Y-m-d');
        $this->id_usuario=$_SESSION['s_user_id'];
        $this->id_unid_usuario=$_SESSION['s_user_unidad_id'];
    }

    function transfer_pendientes(){            
        $q=" SELECT count(docu_id) as total FROM documento WHERE unid_id_destino=$this->id_unid_usuario and tipo_docu_id=5 and stat_docu_id_destino=0 and stat_docu_id=1; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function total_turnos(){            
        $q="SELECT user_nombre FROM notaentrega
        INNER JOIN usuario ON notaentrega.user_id = usuario.user_id
        INNER JOIN turno ON notaentrega.turn_id = turno.turn_id
        WHERE ( notaentrega.unid_id = $this->id_unid_usuario  and  noen_status <>2)
         and ( notaentrega.unid_id = $this->id_unid_usuario  and  noen_status <>3)  GROUP BY user_nombre;";
        $st =$this->con->prepare($q);
        $st->execute();
        $cant=$st->rowCount();
        return $cant;
    }
    function mi_turno(){            
        $q=" SELECT n.unid_id FROM notaentrega n INNER JOIN usuario u ON n.user_id = u.user_id
        WHERE (noen_status <>2 and n.user_id=$this->id_usuario) and (noen_status <>3 and n.user_id=$this->id_usuario )  GROUP BY n.unid_id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $cant=$st->rowCount();
        return $cant;        
    }
    function recetas_hoy(){  
        $hoy=date("Y-m-d");
        $q=" SELECT count(n.noen_id) as total FROM notaentrega n INNER JOIN usuario u ON n.user_id = u.user_id
        WHERE (n.unid_id = $this->id_unid_usuario  and noen_status <>2  and n.noen_fecha='$hoy') and
         (n.unid_id = $this->id_unid_usuario  and noen_status <>3  and n.noen_fecha='$hoy') ; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function get_lotes_caduca(){
        $q="SELECT p.prod_nombre as producto,l.lote_codigo,l.lote_fech_venc,
        if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock
        FROM detalle_kardex dk
        INNER JOIN kardex k on dk.kard_id=k.kard_id
        INNER JOIN lote l on dk.lote_id=l.lote_id
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        WHERE k.unid_id=$this->id_unid_usuario AND lote_fech_venc>=now()
        GROUP BY l.lote_id
        HAVING stock>0
        ORDER BY lote_fech_venc asc LIMIT 30;";
         $st =$this->con->prepare($q);
         $st->execute();
         $rs = $st->fetchAll(PDO::FETCH_ASSOC);
         return json_encode($rs);   
    }
    function get_nombre_unidad(){
        $q="SELECT nick as unid_nombre FROM unidad WHERE unid_id=$this->id_unid_usuario;";
         $st =$this->con->prepare($q);
         $st->execute();
         $rs = $st->fetchAll(PDO::FETCH_ASSOC);
         return json_encode($rs);   
    }
    function get_turnos_abiertos(){
        $q="SELECT user_nick, min( noen_fecha ) AS desde, max( noen_fecha ) AS hasta, count(noen_id) as nume_recetas, turn_nombre
        FROM notaentrega
        INNER JOIN usuario ON notaentrega.user_id = usuario.user_id
        INNER JOIN turno ON notaentrega.turn_id = turno.turn_id
        WHERE (notaentrega.unid_id = $this->id_unid_usuario and  noen_status <>2) and (notaentrega.unid_id = $this->id_unid_usuario and  noen_status <>3)
        GROUP BY user_nombre;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);   
    }
    function get_consumo_medicamentos(){
        $q="SELECT distinct(dk.deta_kard_fecha) as fecha,if (sum(deta_kard_cant_egre) is null,0,sum(deta_kard_cant_egre)) as total FROM detalle_kardex dk
        INNER JOIN kardex k ON k.kard_id=dk.kard_id
        WHERE k.unid_id=$this->id_unid_usuario
        GROUP BY 1
        ORDER BY 1 desc
        LIMIT 60;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);   
    }  
    function detalle_notificacion(){
        $q=" SELECT d.docu_fecha,u1.nick as origen, u2.nick as destino FROM documento d
        INNER JOIN unidad u1 ON d.unid_id=u1.unid_id
        INNER JOIN unidad u2 ON d.unid_id_destino=u2.unid_id
        WHERE d.unid_id_destino=$this->id_unid_usuario and d.tipo_docu_id=5 and d.stat_docu_id_destino=0 and d.stat_docu_id=1;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function detalle_notificacion_pedidos(){
        $q=" SELECT d.docu_fecha,d.docu_procesado,u1.nick as origen, u2.nick as destino FROM documento d
        INNER JOIN unidad u1 ON d.unid_id=u1.unid_id
        INNER JOIN unidad u2 ON d.unid_id_destino=u2.unid_id
        WHERE d.unid_id=$this->id_unid_usuario and d.tipo_docu_id=19 and d.stat_docu_id=1 and d.stat_docu_id_destino=0;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }   
    function ajustar_saldo_kardex(){

        //AJUSTA INSERTA A TABLA SALDO_KARDEX CON STOCKS REALES DE SUS LOTES POR PRODUCTO
        $sql2="SELECT kard_id,unid_id,kard_costo FROM kardex; ";
        $st2 =$this->con->prepare($sql2);
        $st2->execute();
        $rs2 = $st2->fetchAll(PDO::FETCH_ASSOC);
    
        //OBTENGO KARD_ID SEGUNPROD_ID
        $kard_costo=0;
        
        foreach($rs2 as $row2){
            
            $kard_id=$row2['kard_id'];
            $unid_id=$row2['unid_id'];
            $kard_costo=$row2['kard_costo'];
            
            $sql3="SELECT lote_id,
            if(sum(dk.deta_kard_cant_ingr)is null,0,sum(dk.deta_kard_cant_ingr)) - if(sum(dk.deta_kard_cant_egre)is null,0,sum(dk.deta_kard_cant_egre))   as stock
            FROM `detalle_kardex` dk WHERE kard_id=$kard_id group by lote_id";
            $st3 =$this->con->prepare($sql3);
            $st3->execute();
            $rs3 = $st3->fetchAll(PDO::FETCH_ASSOC);
            $stock=0;
            $tot_stock=0;
    
            //OBTENGO LOTE ID Y CNATIDAD STOCK SEGUN KARD_ID
            foreach($rs3 as $row3){
                $lote_id=$row3['lote_id'];
                $stock=$row3['stock'];
                $tot_stock+=$stock;
                  $cant=0;
                  $sql4="SELECT * FROM saldo_kardex WHERE kard_id=$kard_id and lote_id=$lote_id";
                  $st4 =$this->con->prepare($sql4);
                  $st4->execute();
                  $cant=$st4->rowCount();    
                  $costo=0; 

                  if($stock>0)
                      $costo=$kard_costo;                                           

                  if($cant>0){
                         //SI EXISTE UPDATE CANTIDAD
                         $sql = "UPDATE saldo_kardex set sald_kard_cantidad=$stock, sald_kard_precio=$costo
                          where (kard_id=$kard_id and lote_id=$lote_id and sald_kard_cantidad<>$stock)
                           or ( kard_id=$kard_id and lote_id=$lote_id and  sald_kard_precio<>$costo );";
                         $st =$this->con->prepare($sql);
                         $st->execute();
                         //echo 'b';    
                  }else{
                    //SI NO EXISTE INSERTA TODO        
                        $sql5 = "INSERT INTO saldo_kardex (kard_id,lote_id,sald_kard_cantidad,sald_kard_precio) VALUES ($kard_id,$lote_id,$stock,$costo)";
                        $st5 =$this->con->prepare($sql5);
                        $st5->execute();
                        //echo 'a'; //echo 'kard_id: '.$kard_id.' Lote_id: '.$lote_id.' stock: '.$stock.' Precio: '.$kard_costo;
                  }
           }

           //ACTUALIZA TABLA KARDEX: CAMPO KARD_CANTIDAD Y RECALCULA COSTO
           $sqlkk = "UPDATE kardex set kard_cantidad=$tot_stock,kard_total=kard_costo*$tot_stock where kard_id=$kard_id and unid_id=$unid_id and kard_cantidad<>$tot_stock; ";
           $stkk =$this->con->prepare($sqlkk);
           $stkk->execute();
        }
    }
    


    
}