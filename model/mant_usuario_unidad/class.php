<?php
class Perfil{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT uu.user_unid_id,us.user_nombre,ud.unid_nombre FROM usuario_unidad uu
        INNER JOIN usuario us ON uu.user_id=us.user_id
        INNER JOIN unidad ud ON uu.unid_id=ud.unid_id
        ORDER BY 2,3;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM usuario_unidad WHERE user_unid_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO usuario_unidad($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE usuario_unidad
        SET user_id=?,unid_id=?
        WHERE user_unid_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['user_id'],
            $this->campos['unid_id']           
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        $q=" DELETE FROM usuario_unidad WHERE user_unid_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }
 
    function get_unid(){            
        $q=" SELECT * FROM unidad order by 2 ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_user(){            
        $q=" SELECT user_id,user_nombre FROM usuario order by 2 ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}