<?php
class Producto{

    public $con; 
    public $id;
    public $id_usuario;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
         if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }   

        $this->id_usuario=isset($_SESSION['s_user_id']) ? $_SESSION['s_user_id'] : '';
    }

    function get_all(){            
        $q="SELECT p.*,pr.pres_nombre as presen,tp.tipo_prod_nombre as tipo,pve.prov_nombre as proveedor,grupo_prod_nombre,
        CASE
            WHEN prod_conv_marc=1 THEN 'Si'
            ELSE 'No'
        END as convenio,
        
        CASE
            WHEN prod_status=0 THEN 'Si'
            ELSE 'No'
        END as habilitado
        FROM farm_producto p
        LEFT JOIN tipo_producto tp ON p.tipo_prod_id=tp.tipo_prod_id
        LEFT JOIN proveedor pve ON p.prod_id=pve.prov_id        
        LEFT JOIN farm_presentacion pr ON pr.pres_id=p.pres_id
        LEFT JOIN grupo_producto gp ON p.grupo_prod_id=gp.grupo_prod_id
        ORDER BY p.prod_descripcion; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM farm_producto WHERE prod_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO farm_producto ($columns,user_id) VALUES ($values,$this->id_usuario)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE farm_producto
        SET tipo_prod_id=?,prod_descripcion=?,prod_codigo=?,prod_status=?,grupo_prod_id=?,pres_id=?,caducidad_registroSanitario=?,user_id=$this->id_usuario
        WHERE prod_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['tipo_prod_id'],
            $this->campos['prod_descripcion'],                    
            $this->campos['prod_codigo'],
            $this->campos['prod_status'],            
            $this->campos['grupo_prod_id'],
            $this->campos['pres_id'],
            $this->campos['caducidad_registroSanitario']

        ]);
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){

         $validar=" SELECT deta_docu_id FROM detalle_documento  WHERE prod_id=$this->id ;";
        $stVali =$this->con->prepare($validar);
        $stVali->execute();  


         if ($stVali->rowCount()>0)
         {
            return 0;
            //return 'No se pudo ELIMINAR el registro!!!';
           //print(Exite al menos un registro);
         } else {
           //print(No Existen registros);
             $q="DELETE FROM farm_producto WHERE prod_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';

        
        }



       
    }

    function get_forma(){            
        $q=" SELECT * FROM farm_forma ORDER BY 2; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function get_grupo(){            
        $q=" SELECT * FROM grupo_producto; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function get_concentracion(){            
        $q=" SELECT * FROM concentracion ORDER BY 2;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
        function get_presentacion(){            
        $q=" SELECT * FROM farm_presentacion ORDER BY 2;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
    function get_tipo(){            
        $q=" SELECT * FROM tipo_producto ORDER BY 2; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);
    }
     
}