<?php
class Kardex{

    public $con; 
    public $id;
    public $campos=array();
    public $unid_id;

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }


    function get_lotes_caduca(){
        $unidades=$this->unid_id;
      if ($unidades==0) {
        # code...
        $q="SELECT p.prod_nombre as producto,l.lote_codigo,l.lote_fech_venc,
        if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock,p.grupo_prod_id,if(p.grupo_prod_id=11,l.lote_codigo,'NO APLICA') as lt
        FROM detalle_kardex dk
        INNER JOIN kardex k on dk.kard_id=k.kard_id
        INNER JOIN lote l on dk.lote_id=l.lote_id
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        WHERE lote_fech_venc>=now()
        GROUP BY l.lote_id
        HAVING stock>0
        ORDER BY lote_fech_venc asc LIMIT 60;";
      }else{
        $q="SELECT p.prod_nombre as producto,l.lote_codigo,l.lote_fech_venc,
        if(sum(deta_kard_cant_ingr)is null,0,sum(deta_kard_cant_ingr)) - if(sum(deta_kard_cant_egre)is null,0,sum(deta_kard_cant_egre)) as stock,p.grupo_prod_id,if(p.grupo_prod_id=11,l.lote_codigo,'NO APLICA') as lt
        FROM detalle_kardex dk
        INNER JOIN kardex k on dk.kard_id=k.kard_id
        INNER JOIN lote l on dk.lote_id=l.lote_id
        INNER JOIN vw_producto_nombre p ON k.prod_id=p.prod_id
        WHERE k.unid_id=$this->unid_id AND lote_fech_venc>=now()
        GROUP BY l.lote_id
        HAVING stock>0
        ORDER BY lote_fech_venc asc LIMIT 30;";
      }        
         $st =$this->con->prepare($q);
         $st->execute();
         $rs = $st->fetchAll(PDO::FETCH_ASSOC);
         return json_encode($rs);   
    }

        function get_unidad(){
        $q="SELECT * FROM unidad ORDER by 2; "; 
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs); 
    }

    
}