<?php
//SERVIDOR 1

class Conexion{
    public $host;
	public $user;
	public $pass;
	public $base;

		 function __construct(){
		$this->host='localhost';
		$this->user='root';
		$this->pass='';
		$this->base='hosp_farmacia';
	}

	public function conectar(){			
		try{			
			$c=new PDO( 'mysql:host='.$this->host.';dbname='.$this->base, $this->user, $this->pass );						
			//PARA CARACTERES ESPECIALES TILDES Ñ ETC
			$c->exec("set names utf8");
			if($c)
				return $c;
			else
				'No se puede Conectar';
		   }catch (PDOException $e){				
				echo $e->getMessage();
				print_r($e);
		   }
	}

	function ejecutar($sql){
		$con=$this->conectar();
		$st =$con->prepare($sql);
        $st->execute();		
		return $st->fetchAll(PDO::FETCH_ASSOC);
	}

	function ejecutar_count($sql){
		$con=$this->conectar();
		$st =$con->prepare($sql);
		$st->execute();				
		return $st->rowCount();
	}
}
?>
