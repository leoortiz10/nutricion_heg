<?php
class Perfil{

    public $con; 
    public $id;
    public $campos=array();

    public function __construct(){       
        include ('../config/db.php');
        $this->con=$con;
    }

    function get_all(){            
        $q="SELECT pu.perf_user_id,pe.perf_nombre,user_nombre FROM perfil_usuario pu 
        INNER JOIN perfil pe ON pu.perf_id=pe.perf_id
        INNER JOIN usuario u ON pu.user_id=u.user_id
        ORDER BY 2,3;";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_by_id(){            
        $q=" SELECT * FROM perfil_usuario WHERE perf_user_id=$this->id; ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    function nuevo(){
        $columns = implode(", ",array_keys($this->campos));
        $escaped_values = array_map(array($this->con, 'quote'), array_values($this->campos));
        $values  = implode(", ", $escaped_values);
        $q="INSERT INTO perfil_usuario ($columns) VALUES ($values)";                
        $st =$this->con->prepare($q);
        $st->execute();                    
        return $st->rowCount().' registro AGREGADO correctamente !!!';
    }

    function editar(){
        $q=" UPDATE perfil_usuario
        SET perf_id=?,user_id=?
        WHERE perf_user_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute([
            $this->campos['perf_id'],
            $this->campos['user_id']           
        ]);        
        return $st->rowCount().' registro ACTUALIZADO correctamente !!!';
    }

    function eliminar(){
        $q=" DELETE FROM perfil_usuario WHERE perf_user_id=$this->id ;";
        $st =$this->con->prepare($q);
        $st->execute();        
        return $st->rowCount().' registro ELIMINADO correctamente !!!';
    }
 
    function get_perf(){            
        $q=" SELECT * FROM perfil order by 2 ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }

    function get_user(){            
        $q=" SELECT user_id,user_nombre FROM usuario order by 2 ";
        $st =$this->con->prepare($q);
        $st->execute();
        $rs = $st->fetchAll(PDO::FETCH_ASSOC);
        return json_encode($rs);        
    }
    
}